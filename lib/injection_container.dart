import 'package:bdc/core/storage/cache_helper.dart';
import 'package:bdc/core/storage/db_client.dart';
import 'package:flutter/services.dart';

import 'core/modal/constraints/global_values.dart';
import 'core/modal/languages.dart';

Future init() async {
  await SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown,
  ]);

  await CacheHelper.init();
  await getLocale();
  databaseClient = DatabaseClient.instance;
  await databaseClient.database;

  companyID = CacheHelper.getData(key: COMPANY_ID) ?? 1;
  userID = CacheHelper.getData(key: USER_ID) ?? 0;
  userType = CacheHelper.getData(key: USER_TYPE) ?? '';
}
