import 'dart:async';
import 'dart:developer';

import 'package:bdc/core/repository/main_repository/otp_repository.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meta/meta.dart';

import '../../../core/modal/otp_modal/sms_body.dart';

part 'otp_state.dart';

class OtpCubit extends Cubit<OtpState> {
  OtpCubit() : super(OtpInitial());

  static OtpCubit get(context) => BlocProvider.of(context);

  String smsCode = '';
  late String _userMobile = '';
  late bool onMobileChange = false;
  late SmsBody smsBody;
  int start = 30;

  late Timer timer;

  userMobile(String mobile) {
    if (_userMobile.isNotEmpty && onChangeMobile(mobile)) {
      _userMobile = mobile;
      onMobileChange = !onMobileChange;
      getMassage(mobile, true);
    } else {
      _userMobile = mobile;
      if (!onMobileChange) {
        onMobileChange = true;
        getMassage(mobile, true);
      }
    }
  }

  getMassage(String mobile, [bool resetPassword = false]) async {
    emit(OtpFetchMassage());
    await GetOtpRepository().registrationCode(mobile, getError, onMessageDone);
  }

  sendSMS(SmsBody smsBody) async {
    emit(OtpOnSendSMS());
    log('smsBody.toString() ${smsBody.toJson().toString()}');
    await GetOtpRepository().sendSmsRequest(smsBody, getError, onSendSmsDone);
  }

  checkSmsCode(String code, Function() onError) async {
    if (smsCode.compareTo(code) == 0) {
      emit(OtpCodeMatch());
    } else {
      emit(OtpCodeNotMatchError());
      onError();
    }
  }

  bool onChangeMobile(String mobile) {
    if (_userMobile == mobile) return false;
    return true;
  }

  onMessageDone(dynamic value) async {
    smsBody = SmsBody.fromJson(value);
    emit(OtpGetMassage());
    smsCode = smsBody.code;

    await sendSMS(smsBody);
  }

  onSendSmsDone(dynamic value) async {
    emit(OtpOnDoneSendSMS());
    startTimer();
  }

  getError(String message) {
    emit(OtpOnError(message: message));
  }

  void startTimer() {
    const oneSec = Duration(seconds: 1);
    if (start == 0) {
      start = 30;
    }

    timer = Timer.periodic(
      oneSec,
      (Timer timer) {
        if (start == 0) {
          // isTimerActive = !isTimerActive;
          timer.cancel();
          emit(OtpCodeNotMatchError());
        } else {
          start--;
          emit(OtpTimer());
        }
      },
    );
  }
}
