part of 'otp_cubit.dart';

@immutable
abstract class OtpState {}

class OtpInitial extends OtpState {}

class OtpFetchMassage extends OtpState {}

class OtpGetMassage extends OtpState {}

class OtpOnSendSMS extends OtpState {}

class OtpOnDoneSendSMS extends OtpState {}

class OtpTimer extends OtpState {}

class OtpCodeMatch extends OtpState {}

class OtpCodeNotMatched extends OtpState {}

class OtpCodeNotMatchError extends OtpState {}

class OtpOnError extends OtpState {
  final String message;
  OtpOnError({required this.message});
}
