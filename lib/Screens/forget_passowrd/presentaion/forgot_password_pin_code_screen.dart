import 'dart:async';

import 'package:bdc/Screens/LoginScreen/widget//cancel_button.dart';
import 'package:bdc/components/buttons/rounded_button.dart';
import 'package:bdc/core/helper/error_handler/errorhandler.dart';
import 'package:bdc/core/modal/otp_modal/sms_body.dart';
import 'package:bdc/core/theme/app_Theme.dart';
import 'package:bdc/core/theme/sizeConfig.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:pin_code_text_field/pin_code_text_field.dart';

import '../../../core/utils/Navigation.dart';
import '../bloc/otp_cubit.dart';
import 'confirm_password_screen.dart';

// import '../../core/modal/LoginModal/certificate.dart';
// import '../../core/modal/registration_modal/registrationModal.dart';
// import '../../core/helper/login.dart';
// import '../../core/repository/main_repository/main_repository.dart';

class ForgotPasswordPinCodeScreen extends StatefulWidget {
  const ForgotPasswordPinCodeScreen({
    Key? key,
    required this.mobile,
    required this.userID,
  }) : super(key: key);
  final String mobile;
  final String userID;
  @override
  _ForgotPasswordPinCodeScreenState createState() =>
      _ForgotPasswordPinCodeScreenState();
}

class _ForgotPasswordPinCodeScreenState
    extends State<ForgotPasswordPinCodeScreen>
    with SingleTickerProviderStateMixin {
  late OtpCubit otpCubit;
  bool isLogin = true;
  Animation<double>? containerSize;
  AnimationController? animationController;
  Duration animationDuration = const Duration(milliseconds: 270);
  SmsBody? smsBody;

  bool isTimerActive = false;
  @override
  void initState() {
    otpCubit = OtpCubit.get(context);
    OtpCubit.get(context).userMobile(widget.mobile);

    super.initState();
    animationController =
        AnimationController(vsync: this, duration: animationDuration);
  }

  late Timer timer;

  TextEditingController controller = TextEditingController(text: '');

  @override
  Widget build(BuildContext context) {
    double defaultRegisterSize = MediaQuery.of(context).size.height -
        (MediaQuery.of(context).size.height * 0.1);
    containerSize = Tween<double>(
            begin: MediaQuery.of(context).size.height * 0.1,
            end: defaultRegisterSize)
        .animate(CurvedAnimation(
            parent: animationController!, curve: Curves.linear));
    return Scaffold(
      body: SafeArea(
        child: BlocListener<OtpCubit, OtpState>(
          listener: (context, state) {
            if (state is OtpCodeMatch) {
              Navigation.nextAndResetStack(
                  context, ResetPasswordScreen(userID: widget.userID));
            }
          },
          child: Stack(
            children: [
              Positioned(
                  top: 100,
                  right: -50,
                  child: Container(
                    width: 100,
                    height: 100,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(50),
                        color: kPrimaryColor.withOpacity(0.5)),
                  )),
              Positioned(
                  top: -50,
                  left: -50,
                  child: Container(
                    width: 200,
                    height: 200,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(100),
                        color: kPrimaryColorGreen.withOpacity(0.5)),
                  )),
              CancelButton(
                isLogin: isLogin,
                animationDuration: animationDuration,
                height: SizeConfig.height,
                width: SizeConfig.width,
                animationController: animationController,
                tapEvent: () {
                  animationController!.reverse();
                  setState(() {
                    isLogin = !isLogin;
                  });
                },
              ),
              Center(
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const Text(
                        'Verification code',
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 24),
                      ),
                      const SizedBox(height: 40),
                      PinCodeTextField(
                        autofocus: true,
                        controller: controller,
                        highlight: true,
                        highlightColor: Colors.transparent,
                        pinBoxColor: Colors.transparent,
                        defaultBorderColor: Colors.transparent,
                        hasTextBorderColor: Colors.transparent,
                        highlightPinBoxColor:
                            kPrimaryColorGreen.withOpacity(0.5),
                        maxLength: 4,
                        onTextChanged: (text) {
                          setState(() {
                            text = text;
                          });
                        },
                        onDone: (text) async {
                          otpCubit.checkSmsCode(text, () {
                            onAppError(
                                context,
                                AppLocalizations.of(context)!
                                    .wrongCodeTryAgain);
                          });
                        },
                        pinBoxWidth: 60,
                        pinBoxHeight: 64,
                        wrapAlignment: WrapAlignment.spaceAround,
                        pinTextStyle: const TextStyle(fontSize: 22.0),
                        highlightAnimationBeginColor: Colors.black,
                        keyboardType: TextInputType.emailAddress,
                      ),
                      const SizedBox(height: 20),
                      BlocBuilder<OtpCubit, OtpState>(
                        builder: (context, state) {
                          if (state is OtpTimer) {
                            return Center(
                              child: Text(
                                AppLocalizations.of(context)!.tryAfter +
                                    " " +
                                    otpCubit.start.toString(),
                                style: Theme.of(context).textTheme.headline6,
                              ),
                            );
                          }
                          if (state is OtpOnError ||
                              state is OtpCodeNotMatchError) {
                            return RoundedButton(
                                title: AppLocalizations.of(context)!.resend,
                                isLoading: isTimerActive,
                                onTap: () async {
                                  otpCubit.getMassage(widget.mobile, true);
                                });
                          }
                          return Container();
                        },
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                    ],
                  ),
                ),
              ),
              // const PositionedDirectional(child: BackButton()),
            ],
          ),
        ),
      ),
    );
  }

  onError(String massage) {
    setState(() {});
    ErrorHandler.errorMassage = massage;
    ErrorHandler.showErrorMassageDialog(context);
  }

  @override
  void dispose() {
    animationController!.dispose();

    super.dispose();
  }
}
