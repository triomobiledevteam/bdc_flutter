import 'package:bdc/components/loading.dart';
import 'package:bdc/core/repository/forget_password_repository.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import '../../../components/buttons/rounded_button.dart';
import '../../../components/textfield/rounded_password_input.dart';
import '../../../core/helper/error_handler/errorhandler.dart';
import '../../../core/theme/app_Theme.dart';
import '../../../core/utils/Navigation.dart';
import '../../../core/utils/string_validation_extension.dart';
import '../../LoginScreen/LoginScreen.dart';
import '../../LoginScreen/widget/language_widget.dart';

class ResetPasswordScreen extends StatefulWidget {
  const ResetPasswordScreen({Key? key, required this.userID}) : super(key: key);
  final String userID;
  @override
  State<ResetPasswordScreen> createState() => _ResetPasswordScreenState();
}

class _ResetPasswordScreenState extends State<ResetPasswordScreen> {
  final passwordController = TextEditingController();
  final confirmPasswordController = TextEditingController();

  final keyForm = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: mainBackgroundColor,
        actions: const [
          LanguageWidget(),
        ],
      ),
      body: SafeArea(
        minimum: const EdgeInsets.symmetric(horizontal: 16),
        child: Form(
          key: keyForm,
          child: Column(
            children: [
              RoundedPasswordInput(
                hint: AppLocalizations.of(context)!.password,
                textInputAction: TextInputAction.done,
                controller: passwordController,
                validator: (value) {
                  if (!isValidPassword(value ?? '')) {
                    onError(AppLocalizations.of(context)!.passwordValidation);
                    return '';
                  }
                  return null;
                },
              ),
              RoundedPasswordInput(
                hint: AppLocalizations.of(context)!.confirmPassword,
                textInputAction: TextInputAction.done,
                controller: confirmPasswordController,
                validator: (value) {
                  if (value!.isEmpty) {
                    return AppLocalizations.of(context)!.thisFieldIsEmpty;
                  }
                  if (value.compareTo(passwordController.text) != 0) {
                    return AppLocalizations.of(context)!.passwordDoesNotMatch;
                  }
                  return null;
                },
              ),
              const Spacer(),
              Padding(
                  padding: const EdgeInsets.symmetric(vertical: 10),
                  child: RoundedButton(
                      title: AppLocalizations.of(context)!.confirm,
                      onTap: () async {
                        if (keyForm.currentState!.validate()) {
                          progressDialogue(context);
                          await ForgetPasswordRepository().userPasswordReset(
                              passwordController.text,
                              widget.userID,
                              onDone,
                              onError);
                        }
                      })),
            ],
          ),
        ),
      ),
    );
  }

  onError(String massage) {
    Navigation.back(context);
    ErrorHandler.errorMassage = massage;
    ErrorHandler.showErrorMassageDialog(context);
  }

  onDone(dynamic value) {
    ErrorHandler.onSuccessSnackBar("Done", context);
    Navigation.nextAndResetStack(context, const LoginScreen());
  }
}
