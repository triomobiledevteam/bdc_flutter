import 'package:bdc/components/loading.dart';
import 'package:bdc/core/network/api_msg.dart';
import 'package:bdc/core/repository/forget_password_repository.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import '../../../components/buttons/rounded_button.dart';
import '../../../components/input_container.dart';
import '../../../core/helper/error_handler/errorhandler.dart';
import '../../../core/theme/app_Theme.dart';
import '../../../core/utils/Navigation.dart';
import '../../../core/utils/string_validation_extension.dart';
import '../../LoginScreen/widget/language_widget.dart';
import '../../LoginScreen/widget/white_app_bar.dart';
import 'forgot_password_pin_code_screen.dart';

class CheckUserScreen extends StatefulWidget {
  const CheckUserScreen({Key? key}) : super(key: key);

  @override
  State<CheckUserScreen> createState() => _CheckUserScreenState();
}

class _CheckUserScreenState extends State<CheckUserScreen> {
  final formKey = GlobalKey<FormState>();
  TextEditingController userCodeController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: whiteAppBar(
        actions: [
          const LanguageWidget(),
        ],
      ),
      body: SafeArea(
        minimum: const EdgeInsets.symmetric(horizontal: 16),
        child: Form(
          key: formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const SizedBox(
                height: 40,
              ),
              Text(
                AppLocalizations.of(context)!.forgotPassword,
                style: Theme.of(context).textTheme.headline5,
              ),
              const SizedBox(
                height: 20,
              ),
              const Text(
                  "Don't worry! it happens. Please enter the phone nubmer associated with your account"),
              const SizedBox(
                height: 20,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 5),
                child: InputContainer(
                    child: TextFormField(
                  controller: userCodeController,
                  cursorColor: kPrimaryColor,
                  validator: (value) {
                    if (!isValidMobile(value ?? '')) {
                      return AppLocalizations.of(context)!.invalidMobile;
                    }
                    return null;
                  },
                  textInputAction: TextInputAction.next,
                  keyboardType: TextInputType.phone,
                  decoration: InputDecoration(
                      icon: Icon(Icons.phone, color: kPrimaryColor),
                      hintText:
                          AppLocalizations.of(context)!.mobile + " 07********",
                      border: InputBorder.none),
                )),
              ),
              const Spacer(),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 10),
                child: RoundedButton(
                    title: AppLocalizations.of(context)!.confirm,
                    onTap: () async {
                      if (formKey.currentState!.validate()) {
                        progressDialogue(context);

                        await ForgetPasswordRepository().checkUser(
                            userCodeController.text, onDone, onError);
                      }
                    }),
              ),
            ],
          ),
        ),
      ),
    );
  }

  onError(String massage, [bool navBack = true]) {
    if (navBack) {
      Navigation.back(context);
    }
    ErrorHandler.errorMassage = massage;
    ErrorHandler.showErrorMassageDialog(context);
  }

  onDone(dynamic value) {
    Navigation.back(context);
    ApiMsg apiMsg = ApiMsg.toJsonUserID(value);
    ErrorHandler.onSuccessSnackBar("Done", context);

    Navigation.next(
        context,
        ForgotPasswordPinCodeScreen(
          mobile: userCodeController.text,
          userID: apiMsg.iD.toString(),
        ));
  }
}
