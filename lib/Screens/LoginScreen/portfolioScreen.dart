import 'package:bdc/components/loading.dart';
import 'package:bdc/core/helper/error_handler/errorhandler.dart';
import 'package:bdc/core/provider/programs_provider.dart';
import 'package:bdc/core/utils/Navigation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:provider/provider.dart';

import 'program_list_screen.dart';
import 'widget/language_widget.dart';
import 'widget/white_app_bar.dart';

class PortfolioScreen extends StatefulWidget {
  const PortfolioScreen({Key? key}) : super(key: key);

  @override
  State<PortfolioScreen> createState() => _PortfolioScreenState();
}

class _PortfolioScreenState extends State<PortfolioScreen> {
  late ProgramsProvider programsProvider;

  @override
  void initState() {
    super.initState();

    programsProvider = Provider.of<ProgramsProvider>(context, listen: false);
    programsProvider.init();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: whiteAppBar(
        titleText: AppLocalizations.of(context)!.portfolios,
        actions: [
          const LanguageWidget(),
        ],
      ),
      body: RefreshIndicator(
        onRefresh: programsProvider.fetchProject,
        child: Consumer<ProgramsProvider>(
          builder: (context, programsProvider, child) {
            if (programsProvider.isLoading) {
              return Loading.loading(context);
            }
            if (programsProvider.errorMessage.isNotEmpty) {
              onAppError(context, programsProvider.errorMessage);
              return child!;
            }
            return ListView.separated(
              itemCount: programsProvider.projectPortfolios!.portfolios!.length,
              itemBuilder: (context, index) {
                return ListTile(
                  key: UniqueKey(),
                  onTap: () {
                    Navigation.next(
                        context,
                        ProgramListScreen(
                          portfolios: programsProvider
                              .projectPortfolios!.portfolios![index],
                        ));
                  },
                  title: Text(programsProvider
                      .projectPortfolios!.portfolios![index].description!),
                  trailing: const Icon(Icons.arrow_forward_ios_rounded),
                );
              },
              separatorBuilder: (BuildContext context, int index) =>
                  const Divider(),
            );
          },
          child: Container(),
        ),
      ),
    );
  }
}
