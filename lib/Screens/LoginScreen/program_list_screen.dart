import 'package:bdc/Screens/Users/Participant/MainTabs/Programs/programCard.dart';
import 'package:bdc/core/modal/program/portfolios.dart';
import 'package:bdc/core/provider/programs_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'widget/language_widget.dart';
import 'widget/white_app_bar.dart';

class ProgramListScreen extends StatefulWidget {
  const ProgramListScreen(
      {Key? key,
      required this.portfolios,
      this.mainAppBArTheme = false,
      this.viewMode = true})
      : super(key: key);
  final Portfolios portfolios;
  final bool mainAppBArTheme;
  final bool viewMode;

  @override
  State<ProgramListScreen> createState() => _ProgramListScreenState();
}

class _ProgramListScreenState extends State<ProgramListScreen> {
  late ProgramsProvider programsProvider;
  @override
  void initState() {
    // TODO: implement initState
    programsProvider = Provider.of<ProgramsProvider>(context, listen: false);
    programsProvider.getProgramsWhere(widget.portfolios.portfolioID!);

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: widget.mainAppBArTheme
          ? AppBar(
              title: Text(widget.portfolios.description!),
            )
          : whiteAppBar(
              titleText: widget.portfolios.description!,
              actions: [
                const LanguageWidget(),
              ],
            ),
      body: ListView.builder(
          itemCount: programsProvider.tempProgramList.length,
          itemBuilder: (context, index) => ProgramCard(
                program: programsProvider.tempProgramList[index],
                viewMode: widget.viewMode,
              )),
    );
  }
}
