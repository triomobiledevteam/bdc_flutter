import 'package:bdc/Screens/LoginScreen/portfolioScreen.dart';
import 'package:bdc/Screens/LoginScreen/widget/trainer_registration_form.dart';
import 'package:bdc/components/buttons/rounded_button.dart';
import 'package:bdc/core/modal/enums/UsersType.dart';
import 'package:bdc/core/theme/app_Theme.dart';
import 'package:bdc/core/utils/Navigation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import 'widget/cancel_button.dart';
import 'widget/cbo_registration_form.dart';
import 'widget/company_registration_form.dart';
import 'widget/language_widget.dart';
import 'widget/white_app_bar.dart';

class UserTypeScreen extends StatefulWidget {
  const UserTypeScreen({Key? key, required this.defaultLoginSize})
      : super(key: key);
  final double defaultLoginSize;

  @override
  State<UserTypeScreen> createState() => _UserTypeScreenState();
}

class _UserTypeScreenState extends State<UserTypeScreen>
    with SingleTickerProviderStateMixin {
  late AnimationController animationController;
  Widget child = Container();
  bool register = false;
  Duration animationDuration = const Duration(milliseconds: 270);

  @override
  void initState() {
    // TODO: implement initState
    animationController =
        AnimationController(vsync: this, duration: animationDuration);

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: whiteAppBar(
        actions: [
          const LanguageWidget(),
        ],
      ),
      body: Stack(
        children: [
          SafeArea(
            minimum: const EdgeInsets.symmetric(horizontal: 16, vertical: 20),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                FittedBox(
                  child: Text(
                    AppLocalizations.of(context)!.areYou,
                    style: Theme.of(context).textTheme.headline6,
                  ),
                ),
                Flexible(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      const SizedBox(
                        height: 20,
                      ),
                      RoundedButton(
                        title: AppLocalizations.of(context)!.jobSeeker,
                        onTap: () {
                          Navigation.next(context, const PortfolioScreen());
                        },
                        decoration: BoxDecoration(
                          color: kPrimaryColorGreen,
                          borderRadius: BorderRadius.circular(10),
                        ),
                        style: Theme.of(context)
                            .textTheme
                            .bodyText1!
                            .copyWith(fontSize: 18),
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      RoundedButton(
                        title: AppLocalizations.of(context)!.co,
                        onTap: () {
                          buildRegisterContainer(UserType.Company);
                        },
                        decoration: BoxDecoration(
                          color: kPrimaryColorGreen,
                          borderRadius: BorderRadius.circular(10),
                        ),
                        style: Theme.of(context)
                            .textTheme
                            .bodyText1!
                            .copyWith(fontSize: 18),
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      RoundedButton(
                        title: AppLocalizations.of(context)!.cBO,
                        onTap: () {
                          buildRegisterContainer(UserType.CBO);
                        },
                        decoration: BoxDecoration(
                          color: kPrimaryColorGreen,
                          borderRadius: BorderRadius.circular(10),
                        ),
                        style: Theme.of(context)
                            .textTheme
                            .bodyText1!
                            .copyWith(color: blackColor, fontSize: 18),
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      RoundedButton(
                        title: AppLocalizations.of(context)!.trainer,
                        onTap: () {
                          buildRegisterContainer(UserType.Trainer);
                        },
                        decoration: BoxDecoration(
                          color: kPrimaryColorGreen,
                          borderRadius: BorderRadius.circular(10),
                        ),
                        style: Theme.of(context)
                            .textTheme
                            .bodyText1!
                            .copyWith(color: blackColor, fontSize: 18),
                      ),
                    ],
                  ),
                ),
                RoundedButton(
                  title: AppLocalizations.of(context)!.checkPrograms,
                  onTap: () {
                    Navigation.next(context, const PortfolioScreen());
                  },
                  decoration: BoxDecoration(
                      color: Colors.transparent,
                      borderRadius: BorderRadius.circular(10),
                      border: Border.all(color: blackColor)),
                  style: Theme.of(context)
                      .textTheme
                      .bodyText1!
                      .copyWith(color: blackColor, fontSize: 18),
                ),
              ],
            ),
          ),
          Visibility(visible: register, child: child),
          CancelButton(
            isLogin: !register,
            animationDuration: animationDuration,
            animationController: animationController,
            tapEvent: () {
              animationController.reverse();
              register = !register;
              setState(() {});
            },
          ),
        ],
      ),
    );
  }

  buildRegisterContainer(UserType userType) {
    switch (userType) {
      case UserType.Company:
        child = CompanyRegistrationForm(
            animationDuration: animationDuration,
            defaultLoginSize: widget.defaultLoginSize);
        break;
      case UserType.CBO:
        child = CPORegistrationForm(
            animationDuration: animationDuration,
            defaultLoginSize: widget.defaultLoginSize);
        break;

      case UserType.Trainer:
        child = TrainerRegistrationForm(
            animationDuration: animationDuration,
            defaultLoginSize: widget.defaultLoginSize);
        break;
      default:
        child = Container();
    }
    register = !register;
    setState(() {});
  }
}
