import 'package:bdc/components/buttons/rounded_button.dart';
import 'package:bdc/components/input_container.dart';
import 'package:bdc/components/textfield/rounded_password_input.dart';
import 'package:bdc/core/helper/error_handler/errorhandler.dart';
import 'package:bdc/core/helper/login.dart';
import 'package:bdc/core/modal/LoginModal/certificate.dart';
import 'package:bdc/core/modal/enums/accountType.dart';
import 'package:bdc/core/theme/app_Theme.dart';
import 'package:bdc/core/theme/sizeConfig.dart';
import 'package:bdc/core/utils/Navigation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import '../../forget_passowrd/presentaion/check_user_screen.dart';
import '../portfolioScreen.dart';
import '../user_type_screen.dart';

class LoginForm extends StatefulWidget {
  const LoginForm({
    Key? key,
    required this.isLogin,
    required this.animationDuration,
  }) : super(key: key);

  final bool isLogin;
  final Duration animationDuration;

  @override
  _LoginFormState createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  bool wait = false;
  final formKey = GlobalKey<FormState>();
  late TextEditingController userCodeController;

  late TextEditingController passwordController;

  @override
  void initState() {
    userCodeController = TextEditingController(text: '');
    passwordController = TextEditingController(text: '');
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return AnimatedOpacity(
      opacity: widget.isLogin ? 1.0 : 0.0,
      duration: widget.animationDuration * 4,
      child: Container(
        width: double.infinity,
        padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 10),
        // height: widget.defaultLoginSize,
        child: Align(
          alignment: Alignment.center,
          child: Form(
            key: formKey,
            child: SingleChildScrollView(
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    const SizedBox(height: 10),
                    Image.asset(
                      'assets/bdc.png',
                      fit: BoxFit.fill,
                      height: 80,
                      width: 150,
                    ),
                    const SizedBox(height: 10),
                    Text(
                      accountType == AccountType.GenUsers
                          ? AppLocalizations.of(context)!.signIn
                          : AppLocalizations.of(context)!.signInAsAdmin,
                      style: Theme.of(context).textTheme.bodyText1,
                    ),
                    const SizedBox(height: 10),
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 5),
                      child: InputContainer(
                          child: TextFormField(
                        controller: userCodeController,
                        cursorColor: kPrimaryColor,
                        validator: (value) {
                          if (value!.isEmpty) {
                            return AppLocalizations.of(context)!
                                .thisFieldIsEmpty;
                          }
                          return null;
                        },
                        textInputAction: TextInputAction.next,
                        decoration: InputDecoration(
                            icon: Icon(Icons.account_box_outlined,
                                color: kPrimaryColor),
                            hintText: accountType == AccountType.GenUsers
                                ? AppLocalizations.of(context)!.mobile +
                                    '/' +
                                    AppLocalizations.of(context)!.email
                                : AppLocalizations.of(context)!.trioAccount,
                            border: InputBorder.none),
                      )),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 5),
                      child: RoundedPasswordInput(
                        hint: AppLocalizations.of(context)!.password,
                        textInputAction: TextInputAction.done,
                        validator: (value) {
                          if (value!.isEmpty) {
                            return AppLocalizations.of(context)!
                                .thisFieldIsEmpty;
                          }
                          return null;
                        },
                        controller: passwordController,
                      ),
                    ),
                    const SizedBox(height: 10),
                    Align(
                      alignment: AlignmentDirectional.centerStart,
                      child: TextButton(
                          onPressed: () {
                            Navigation.next(context, const CheckUserScreen());
                          },
                          child: Text(
                              AppLocalizations.of(context)!.forgotPassword)),
                    ),
                    const SizedBox(height: 10),
                    RoundedButton(
                        title: AppLocalizations.of(context)!.login,
                        onTap: () async {
                          if (formKey.currentState!.validate()) {
                            setWait();
                            setState(() {});
                            await login(
                                Certificate(
                                    email: userCodeController.text,
                                    password: passwordController.text,
                                    accountType: accountType.name),
                                onError,
                                context);
                          }
                        },
                        isLoading: wait),
                    const SizedBox(height: 10),
                    Visibility(
                      visible: accountType == AccountType.GenUsers,
                      child: Row(
                        children: [
                          Expanded(
                            child: Container(
                                margin: const EdgeInsets.symmetric(
                                    horizontal: 30.0),
                                child: const Divider(
                                  color: Colors.black,
                                  height: 10,
                                )),
                          ),
                          Text(AppLocalizations.of(context)!.orUsing),
                          Expanded(
                            child: Container(
                                margin: const EdgeInsets.symmetric(
                                    horizontal: 30.0),
                                child: const Divider(
                                  color: Colors.black,
                                  height: 10,
                                )),
                          ),
                        ],
                      ),
                    ),
                    Visibility(
                      visible: accountType == AccountType.GenUsers,
                      child: Padding(
                        padding: const EdgeInsets.symmetric(vertical: 15),
                        child: Align(
                          alignment: AlignmentDirectional.centerStart,
                          child: Text(
                            AppLocalizations.of(context)!.bdcMassage,
                            style: Theme.of(context).textTheme.bodyText1,
                          ),
                        ),
                      ),
                    ),
                    Visibility(
                      visible: accountType == AccountType.GenUsers,
                      child: RoundedButton(
                        title: AppLocalizations.of(context)!.checkPrograms,
                        onTap: () {
                          Navigation.next(context, const PortfolioScreen());
                        },
                        decoration: BoxDecoration(
                            color: Colors.transparent,
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(color: blackColor)),
                        style: Theme.of(context)
                            .textTheme
                            .bodyText1!
                            .copyWith(color: blackColor, fontSize: 18),
                      ),
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    Visibility(
                      visible: accountType == AccountType.GenUsers,
                      child: RoundedButton(
                        title: AppLocalizations.of(context)!.signUpWithBDC,
                        onTap: () {
                          double defaultRegisterSize =
                              SizeConfig.height! - (SizeConfig.height! * 0.1);
                          Navigation.next(
                              context,
                              UserTypeScreen(
                                defaultLoginSize: defaultRegisterSize,
                              ));
                        },
                      ),
                    ),
                    const SizedBox(height: 10),
                    TextButton(
                      onPressed: changeAccountType,
                      child: Text(
                        accountType == AccountType.GenUsers
                            ? AppLocalizations.of(context)!.signInAsAdmin
                            : AppLocalizations.of(context)!.signInAsUser,
                        style: Theme.of(context)
                            .textTheme
                            .bodyText1!
                            .copyWith(color: Colors.blue),
                      ),
                    ),
                  ]),
            ),
          ),
        ),
      ),
    );
  }

  onError(String errorMessage) {
    ErrorHandler.errorMassage = errorMessage;
    ErrorHandler.showErrorMassageDialog(context);
    setWait();
    setState(() {});
  }

  changeAccountType() {
    switch (accountType) {
      case AccountType.GenUsers:
        accountType = AccountType.Employee;
        break;
      case AccountType.Employee:
        accountType = AccountType.GenUsers;
        break;
    }
    print(accountType.name);
    setState(() {});
  }

  setWait() {
    wait = !wait;
  }
}
