import 'package:bdc/Screens/pinCode/PinCodeScreen.dart';
import 'package:bdc/components/buttons/rounded_button.dart';
import 'package:bdc/components/textfield/rounded_input.dart';
import 'package:bdc/components/textfield/rounded_password_input.dart';
import 'package:bdc/core/helper/error_handler/errorhandler.dart';
import 'package:bdc/core/modal/Lookups.dart';
import 'package:bdc/core/modal/constraints/LookUpTypes.dart';
import 'package:bdc/core/modal/enums/UsersType.dart';
import 'package:bdc/core/modal/registration_modal/registrationModal.dart';
import 'package:bdc/core/theme/app_Theme.dart';
import 'package:bdc/core/theme/app_icons_icons.dart';
import 'package:bdc/core/theme/sizeConfig.dart';
import 'package:bdc/core/utils/Navigation.dart';
import 'package:bdc/core/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import '../../../components/loading.dart';
import '../../../core/modal/registration_modal/trainer_registration_modal.dart';
import '../../../core/repository/main_repository/main_repository.dart';
import '../../../core/utils/string_validation_extension.dart';

class TrainerRegistrationForm extends StatefulWidget {
  const TrainerRegistrationForm({
    Key? key,
    required this.animationDuration,
    required this.defaultLoginSize,
  }) : super(key: key);

  final Duration animationDuration;
  final double defaultLoginSize;

  @override
  _TrainerRegistrationFormState createState() =>
      _TrainerRegistrationFormState();
}

class _TrainerRegistrationFormState extends State<TrainerRegistrationForm> {
  final formKey = GlobalKey<FormState>();
  String eduLevel = '';
  String major = '';
  String city = '';

  TextEditingController majorController = TextEditingController(text: '');
  TextEditingController eduLevelController = TextEditingController(text: '');
  TextEditingController cityController = TextEditingController(text: '');
  TextEditingController userNameController = TextEditingController(text: '');
  TextEditingController mobileController = TextEditingController(text: '');
  TextEditingController emailController = TextEditingController(text: '');
  TextEditingController passwordController = TextEditingController(text: '');
  TextEditingController confirmPasswordController =
      TextEditingController(text: '');

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedOpacity(
      opacity: 1.0,
      duration: widget.animationDuration * 2,
      child: Align(
        alignment: Alignment.bottomCenter,
        child: Container(
          width: double.infinity,
          padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 10),
          color: mainBackgroundColor,
          height: widget.defaultLoginSize,
          child: Form(
            key: formKey,
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const SizedBox(height: 40),
                  Text(
                    AppLocalizations.of(context)!.registerToBDCAs +
                        ' ' +
                        AppLocalizations.of(context)!.trainer,
                    style: const TextStyle(
                        fontWeight: FontWeight.bold, fontSize: 24),
                  ),
                  const SizedBox(height: 10),
                  Container(
                    width: SizeConfig.width! * 0.8,
                    decoration: BoxDecoration(
                        border: Border.all(color: const Color(0xffbbbbbb)),
                        borderRadius:
                            const BorderRadius.all(Radius.circular(27))),
                  ),
                  Column(
                    children: [
                      const SizedBox(height: 10),
                      RoundedInput(
                          controller: userNameController,
                          icon: Icons.person,
                          hint: AppLocalizations.of(context)!.name),
                      RoundedInput(
                          controller: mobileController,
                          icon: Icons.phone,
                          validator: (value) {
                            if (!isValidMobile(value ?? "")) {
                              return AppLocalizations.of(context)!
                                  .invalidMobile;
                            }
                            return null;
                          },
                          hint: AppLocalizations.of(context)!.mobile +
                              '  07*******'),
                      RoundedInput(
                          controller: emailController,
                          icon: Icons.mail,
                          validator: (value) {
                            if (!isValidEmail(value ?? '')) {
                              return AppLocalizations.of(context)!.invalidEmail;
                            }
                            return null;
                          },
                          hint: AppLocalizations.of(context)!.email),
                      RoundedInput(
                          controller: majorController,
                          icon: Icons.work,
                          dialogType: LookUpTypes.major,
                          isDefaultDialog: true,
                          readOnly: true,
                          getData: getMajor,
                          hint: AppLocalizations.of(context)!.majorID),
                      RoundedInput(
                          controller: cityController,
                          icon: AppIcons.city,
                          dialogType: LookUpTypes.city,
                          isDefaultDialog: true,
                          readOnly: true,
                          getData: getCity,
                          hint: AppLocalizations.of(context)!.governorate),
                      RoundedInput(
                          controller: eduLevelController,
                          icon: AppIcons.mortarboard,
                          dialogType: LookUpTypes.eduLevel,
                          isDefaultDialog: true,
                          readOnly: true,
                          getData: getEduLevel,
                          hint: AppLocalizations.of(context)!.educationalLevel),
                      RoundedPasswordInput(
                        hint: AppLocalizations.of(context)!.password,
                        textInputAction: TextInputAction.done,
                        controller: passwordController,
                        validator: (value) {
                          if (!isValidPassword(value ?? '')) {
                            onError(AppLocalizations.of(context)!
                                .passwordValidation);
                            return '';
                          }
                          return null;
                        },
                      ),
                      RoundedPasswordInput(
                        hint: AppLocalizations.of(context)!.confirmPassword,
                        textInputAction: TextInputAction.done,
                        controller: confirmPasswordController,
                        validator: (value) {
                          if (value!.isEmpty) {
                            return AppLocalizations.of(context)!
                                .thisFieldIsEmpty;
                          }
                          if (value.compareTo(passwordController.text) != 0) {
                            return AppLocalizations.of(context)!
                                .passwordDoesNotMatch;
                          }
                          return null;
                        },
                      ),
                      const SizedBox(height: 10),
                      RoundedButton(
                          title: AppLocalizations.of(context)!.signUp,
                          onTap: signUp),
                    ],
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  getCity(LookUps lookUp) {
    city = lookUp.iD!;
  }

  getEduLevel(LookUps lookUp) {
    eduLevel = lookUp.iD!;
  }

  getMajor(LookUps lookUp) {
    major = lookUp.iD!;
  }

  onError(String massage) {
    ErrorHandler.errorMassage = massage;
    ErrorHandler.showErrorMassageDialog(context);
  }

  onDone(dynamic value) {
    Navigation.back(context);

    TrainerRegistrationModal trainer = TrainerRegistrationModal(
      email: emailController.text,
      userName: userNameController.text,
      password: passwordController.text,
      mobile: mobileController.text,
      city: parseInt(city),
      eduLevel: parseInt(eduLevel),
      major: parseInt(major),
    );
    Navigation.next(
        context,
        PinCodeScreen(
          registration: Registration(
              userType: UserType.Trainer.name, registrationModal: trainer),
        ));
  }

  signUp() async {
    if (formKey.currentState!.validate()) {
      progressDialogue(context);

      await GetMainRepository()
          .checkData(emailController.text, mobileController.text, (message) {
        Navigation.back(context);

        onError(message);
      }, onDone);
    }
  }
}
