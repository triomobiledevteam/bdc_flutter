import 'package:bdc/core/modal/constraints/global_values.dart';
import 'package:bdc/core/modal/languages.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../core/provider/programs_provider.dart';
import '../../../main.dart';

class LanguageWidget extends StatefulWidget {
  const LanguageWidget({Key? key}) : super(key: key);

  @override
  State<LanguageWidget> createState() => _LanguageWidgetState();
}

class _LanguageWidgetState extends State<LanguageWidget> {
  @override
  Widget build(BuildContext context) {
    ProgramsProvider programsProvider =
        Provider.of<ProgramsProvider>(context, listen: false);
    return IconButton(
        onPressed: () async {
          if (languageID == 1) {
            MyApp.of(context)!.setLocale(await saveLocale(2));
          } else {
            MyApp.of(context)!.setLocale(await saveLocale(1));
          }

          setState(() {});
          programsProvider.fetchProject();
        },
        icon: SizedBox(
          width: 30,
          // color: Colors.grey.withOpacity(0.8),
          child: Center(
            child: FittedBox(
              child: Text(
                languageID == 1 ? 'ع' : 'E',
                style: Theme.of(context).textTheme.headline5,
              ),
            ),
          ),
        ));
  }
}
