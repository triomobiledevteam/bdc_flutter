import 'package:bdc/core/theme/app_Theme.dart';
import 'package:flutter/material.dart';

class CancelButton extends StatelessWidget {
  const CancelButton(
      {Key? key,
      required this.isLogin,
      required this.animationDuration,
      this.height,
      this.width,
      required this.animationController,
      required this.tapEvent})
      : super(key: key);

  final bool isLogin;
  final Duration animationDuration;
  final double? width;
  final double? height;
  final AnimationController? animationController;
  final GestureTapCallback tapEvent;

  @override
  Widget build(BuildContext context) {
    return AnimatedOpacity(
      opacity: isLogin ? 0.0 : 1.0,
      duration: animationDuration,
      child: Align(
        alignment: Alignment.topCenter,
        child: Container(
          width: width ?? double.infinity,
          height: height ?? 50,
          alignment: Alignment.bottomCenter,
          child: IconButton(
            icon: const Icon(Icons.close),
            onPressed: tapEvent,
            color: kPrimaryColor,
          ),
        ),
      ),
    );
  }
}
