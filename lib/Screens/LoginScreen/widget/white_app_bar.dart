import 'package:bdc/core/theme/app_Theme.dart';
import 'package:flutter/material.dart';

whiteAppBar(
    {Key? key,
    Color? backgroundColor,
    List<Widget>? actions,
    String titleText = '',
    Widget? title}) {
  return AppBar(
    elevation: 0,
    title: title ??
        Text(
          titleText,
          style: TextStyle(color: blackColor),
        ),
    backgroundColor: backgroundColor ?? mainBackgroundColor,
    leading: BackButton(
      color: blackColor,
    ),
    actions: actions ??
        [
          // LanguageWidget(),
        ],
  );
}
