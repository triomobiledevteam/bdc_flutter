import 'package:bdc/Screens/pinCode/PinCodeScreen.dart';
import 'package:bdc/components/buttons/rounded_button.dart';
import 'package:bdc/components/textfield/rounded_input.dart';
import 'package:bdc/components/textfield/rounded_password_input.dart';
import 'package:bdc/core/helper/error_handler/errorhandler.dart';
import 'package:bdc/core/modal/Lookups.dart';
import 'package:bdc/core/modal/constraints/LookUpTypes.dart';
import 'package:bdc/core/modal/enums/UsersType.dart';
import 'package:bdc/core/modal/registration_modal/company_registration_modal.dart';
import 'package:bdc/core/theme/app_Theme.dart';
import 'package:bdc/core/theme/sizeConfig.dart';
import 'package:bdc/core/utils/Navigation.dart';
import 'package:bdc/core/utils/string_validation_extension.dart';
import 'package:bdc/core/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import '../../../components/loading.dart';
import '../../../core/modal/registration_modal/registrationModal.dart';
import '../../../core/repository/main_repository/main_repository.dart';

class CompanyRegistrationForm extends StatefulWidget {
  const CompanyRegistrationForm({
    Key? key,
    required this.animationDuration,
    required this.defaultLoginSize,
  }) : super(key: key);

  final Duration animationDuration;
  final double defaultLoginSize;

  @override
  _CompanyRegistrationFormState createState() =>
      _CompanyRegistrationFormState();
}

class _CompanyRegistrationFormState extends State<CompanyRegistrationForm> {
  final formKey = GlobalKey<FormState>();
  String sectorType = '';
  String city = '';

  TextEditingController cityController = TextEditingController(text: '');
  TextEditingController sectorController = TextEditingController(text: '');
  TextEditingController userNameController = TextEditingController(text: '');
  TextEditingController mobileController = TextEditingController(text: '');
  TextEditingController emailController = TextEditingController(text: '');
  TextEditingController passwordController = TextEditingController(text: '');
  TextEditingController confirmPasswordController =
      TextEditingController(text: '');

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedOpacity(
      opacity: 1.0,
      duration: widget.animationDuration * 2,
      child: Align(
        alignment: Alignment.bottomCenter,
        child: Container(
          width: double.infinity,
          padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 10),
          color: mainBackgroundColor,
          height: widget.defaultLoginSize,
          child: Form(
            key: formKey,
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const SizedBox(height: 40),
                  Text(
                    AppLocalizations.of(context)!.registerToBDCAs +
                        ' ' +
                        AppLocalizations.of(context)!.co,
                    style: const TextStyle(
                        fontWeight: FontWeight.bold, fontSize: 24),
                  ),
                  const SizedBox(height: 10),
                  Container(
                    width: SizeConfig.width! * 0.8,
                    decoration: BoxDecoration(
                        border: Border.all(color: const Color(0xffbbbbbb)),
                        borderRadius:
                            const BorderRadius.all(Radius.circular(27))),
                  ),
                  const SizedBox(height: 10),
                  Column(
                    children: [
                      RoundedInput(
                          controller: userNameController,
                          icon: Icons.person,
                          validator: (value) {
                            if (value!.isEmpty) {
                              return AppLocalizations.of(context)!
                                  .thisFieldIsEmpty;
                            }
                            return null;
                          },
                          hint: AppLocalizations.of(context)!.companyName),
                      RoundedInput(
                          controller: mobileController,
                          icon: Icons.phone,
                          validator: (value) {
                            if (value!.isEmpty) {
                              return AppLocalizations.of(context)!
                                  .thisFieldIsEmpty;
                            }
                            if (!isValidMobile(value)) {
                              AppLocalizations.of(context)!.invalidMobile;
                            }
                            return null;
                          },
                          hint: AppLocalizations.of(context)!.mobile +
                              '  07*******'),
                      RoundedInput(
                        controller: cityController,
                        icon: Icons.location_city,
                        dialogType: LookUpTypes.city,
                        isDefaultDialog: true,
                        readOnly: true,
                        validator: (value) {
                          if (value!.isEmpty) {
                            return AppLocalizations.of(context)!
                                .thisFieldIsEmpty;
                          }
                          return null;
                        },
                        getData: getCity,
                        hint: AppLocalizations.of(context)!.governorate,
                      ),
                      RoundedInput(
                        controller: sectorController,
                        icon: Icons.work,
                        dialogType: LookUpTypes.sector,
                        isDefaultDialog: true,
                        readOnly: true,
                        validator: (value) {
                          if (value!.isEmpty) {
                            return AppLocalizations.of(context)!
                                .thisFieldIsEmpty;
                          }
                          return null;
                        },
                        getData: getSectorType,
                        hint: AppLocalizations.of(context)!.sectorTypeID,
                      ),
                      RoundedInput(
                          controller: emailController,
                          icon: Icons.mail,
                          validator: (value) {
                            if (!isValidEmail(value ?? '')) {
                              return AppLocalizations.of(context)!.invalidEmail;
                            }
                            return null;
                          },
                          hint: AppLocalizations.of(context)!.email),
                      RoundedPasswordInput(
                        hint: AppLocalizations.of(context)!.password,
                        textInputAction: TextInputAction.done,
                        validator: (value) {
                          if (!isValidPassword(value ?? '')) {
                            onError(AppLocalizations.of(context)!
                                .passwordValidation);
                            return '';
                          }
                          return null;
                        },
                        controller: passwordController,
                      ),
                      RoundedPasswordInput(
                        hint: AppLocalizations.of(context)!.confirmPassword,
                        textInputAction: TextInputAction.done,
                        validator: (value) {
                          if (value!.isEmpty) {
                            return AppLocalizations.of(context)!
                                .thisFieldIsEmpty;
                          }
                          if (value.compareTo(passwordController.text) != 0) {
                            return AppLocalizations.of(context)!
                                .passwordDoesNotMatch;
                          }
                          return null;
                        },
                        controller: confirmPasswordController,
                      ),
                      RoundedButton(
                          title: AppLocalizations.of(context)!.signUp,
                          onTap: signUp),
                      // SizedBox(height: 30),
                    ],
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  getCity(LookUps lookUp) {
    city = lookUp.iD!;
  }

  getSectorType(LookUps lookUp) {
    sectorType = lookUp.iD!;
  }

  onError(String massage) {
    ErrorHandler.errorMassage = massage;
    ErrorHandler.showErrorMassageDialog(context);
  }

  onDone(dynamic value) {
    Navigation.back(context);

    CompanyRegistrationModal company = CompanyRegistrationModal(
        email: emailController.text,
        companyName: userNameController.text,
        password: passwordController.text,
        mobile: mobileController.text,
        sectorType: parseInt(sectorType),
        city: parseInt(city));

    Navigation.next(
        context,
        PinCodeScreen(
          registration: Registration(
              userType: UserType.Company.name, registrationModal: company),
        ));
  }

  signUp() async {
    if (formKey.currentState!.validate()) {
      progressDialogue(context);

      await GetMainRepository()
          .checkData(emailController.text, mobileController.text, (message) {
        Navigation.back(context);

        onError(message);
      }, onDone);
    }
  }
}
