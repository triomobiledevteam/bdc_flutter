import 'package:bdc/Screens/pinCode/PinCodeScreen.dart';
import 'package:bdc/components/buttons/rounded_button.dart';
import 'package:bdc/components/loading.dart';
import 'package:bdc/components/textfield/rounded_input.dart';
import 'package:bdc/components/textfield/rounded_password_input.dart';
import 'package:bdc/core/modal/Lookups.dart';
import 'package:bdc/core/modal/constraints/LookUpTypes.dart';
import 'package:bdc/core/modal/enums/UsersType.dart';
import 'package:bdc/core/modal/registration_modal/cbo_registration_modal.dart';
import 'package:bdc/core/modal/registration_modal/registrationModal.dart';
import 'package:bdc/core/theme/app_Theme.dart';
import 'package:bdc/core/theme/sizeConfig.dart';
import 'package:bdc/core/utils/Navigation.dart';
import 'package:bdc/core/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import '../../../core/helper/error_handler/errorhandler.dart';
import '../../../core/repository/main_repository/main_repository.dart';
import '../../../core/utils/string_validation_extension.dart';

class CPORegistrationForm extends StatefulWidget {
  const CPORegistrationForm({
    Key? key,
    required this.animationDuration,
    required this.defaultLoginSize,
  }) : super(key: key);

  final Duration animationDuration;
  final double defaultLoginSize;

  @override
  _CPORegistrationFormState createState() => _CPORegistrationFormState();
}

class _CPORegistrationFormState extends State<CPORegistrationForm> {
  final formKey = GlobalKey<FormState>();
  String city = '';

  TextEditingController cityController = TextEditingController(text: '');
  TextEditingController companyNameController = TextEditingController(text: '');
  TextEditingController mobileController = TextEditingController(text: '');
  TextEditingController emailController = TextEditingController(text: '');
  TextEditingController passwordController = TextEditingController(text: '');
  TextEditingController confirmPasswordController =
      TextEditingController(text: '');

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedOpacity(
      opacity: 1,
      duration: widget.animationDuration * 2,
      child: Align(
        alignment: Alignment.bottomCenter,
        child: Container(
          width: double.infinity,
          padding: const EdgeInsets.symmetric(horizontal: 16),
          height: widget.defaultLoginSize,
          color: mainBackgroundColor,
          child: Form(
            key: formKey,
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const SizedBox(height: 40),
                  Text(
                    AppLocalizations.of(context)!.registerToBDCAs +
                        ' ' +
                        AppLocalizations.of(context)!.cBO,
                    style: const TextStyle(
                        fontWeight: FontWeight.bold, fontSize: 24),
                  ),
                  const SizedBox(height: 10),
                  Container(
                    width: SizeConfig.width! * 0.8,
                    decoration: BoxDecoration(
                        border: Border.all(color: const Color(0xffbbbbbb)),
                        borderRadius:
                            const BorderRadius.all(Radius.circular(27))),
                  ),
                  const SizedBox(height: 10),

                  RoundedInput(
                      controller: companyNameController,
                      icon: Icons.person,
                      validator: (value) {
                        if (value!.isEmpty) {
                          return AppLocalizations.of(context)!.thisFieldIsEmpty;
                        }
                        return null;
                      },
                      hint: AppLocalizations.of(context)!.associationName),

                  RoundedInput(
                      controller: cityController,
                      icon: Icons.featured_video_outlined,
                      dialogType: LookUpTypes.city,
                      isDefaultDialog: true,
                      readOnly: true,
                      // parentRecordID: ,
                      getData: getCity,
                      validator: (value) {
                        if (value!.isEmpty) {
                          return AppLocalizations.of(context)!.thisFieldIsEmpty;
                        }
                        return null;
                      },
                      hint: AppLocalizations.of(context)!.governorate),
                  RoundedInput(
                      controller: mobileController,
                      icon: Icons.phone,
                      validator: (value) {
                        if (value!.isEmpty) {
                          return AppLocalizations.of(context)!.thisFieldIsEmpty;
                        }
                        if (!isValidMobile(value)) {
                          return AppLocalizations.of(context)!.invalidMobile;
                        }
                        return null;
                      },
                      hint:
                          AppLocalizations.of(context)!.mobile + '  07*******'),
                  RoundedInput(
                      controller: emailController,
                      icon: Icons.mail,
                      validator: (value) {
                        if (value!.isEmpty) {
                          return AppLocalizations.of(context)!.thisFieldIsEmpty;
                        }
                        if (!isValidEmail(value)) {
                          return AppLocalizations.of(context)!.invalidEmail;
                        }
                        return null;
                      },
                      hint: AppLocalizations.of(context)!.email),
                  RoundedPasswordInput(
                    hint: AppLocalizations.of(context)!.password,
                    controller: passwordController,
                    validator: (value) {
                      if (!isValidPassword(value ?? '')) {
                        onError(
                            AppLocalizations.of(context)!.passwordValidation);
                        return '';
                      }
                      if (value!.isEmpty) {
                        return AppLocalizations.of(context)!.passwordValidation;
                      }
                      return null;
                    },
                  ),
                  RoundedPasswordInput(
                    hint: AppLocalizations.of(context)!.confirmPassword,
                    textInputAction: TextInputAction.done,
                    validator: (value) {
                      if (value!.isEmpty) {
                        return AppLocalizations.of(context)!.thisFieldIsEmpty;
                      }
                      if (value.compareTo(passwordController.text) != 0) {
                        return AppLocalizations.of(context)!
                            .passwordDoesNotMatch;
                      }
                      return null;
                    },
                    controller: confirmPasswordController,
                  ),
                  const SizedBox(height: 10),
                  RoundedButton(
                      title: AppLocalizations.of(context)!.signUp,
                      onTap: signUp),
                  // SizedBox(height: 30),
                  // Row(
                  //   children: [
                  //     Expanded(
                  //       child: Container(
                  //           margin: EdgeInsets.only(left: 30.0, right: 30.0),
                  //           child: Divider(
                  //             color: Colors.black,
                  //             height: 10,
                  //           )),
                  //     ),
                  //     Text('Or using'),
                  //     Expanded(
                  //       child: new Container(
                  //           margin: const EdgeInsets.only(
                  //               left: 30.0, right: 30.0),
                  //           child: Divider(
                  //             color: Colors.black,
                  //             height: 10,
                  //           )),
                  //     ),
                  //   ],
                  // ),
                  // SizedBox(height: 30),
                  // Row(
                  //   mainAxisAlignment: MainAxisAlignment.center,
                  //   children: [
                  //     Image.asset(
                  //       'assets/images/google.png',
                  //       width: 50,
                  //       color: Colors.red,
                  //     ),
                  //     SizedBox(width: 30),
                  //     Image.asset(
                  //       'assets/images/facebook.png',
                  //       width: 40,
                  //     ),
                  //   ],
                  // ),
                  // SizedBox(height: 10)
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  getCity(LookUps lookUp) {
    city = lookUp.iD!;
  }

  onError(String massage) {
    ErrorHandler.errorMassage = massage;
    ErrorHandler.showErrorMassageDialog(context);
  }

  onDone(dynamic value) {
    Navigation.back(context);

    CBORegistrationModal cBO = CBORegistrationModal(
        email: emailController.text,
        companyName: companyNameController.text,
        password: passwordController.text,
        mobile: mobileController.text,
        city: parseInt(city));
    Navigation.next(
        context,
        PinCodeScreen(
            registration: Registration(
                userType: UserType.CBO.name, registrationModal: cBO)));
  }

  signUp() async {
    if (formKey.currentState!.validate()) {
      progressDialogue(context);
      await GetMainRepository()
          .checkData(emailController.text, mobileController.text, (message) {
        Navigation.back(context);
        onError(message);
      }, onDone);
    }
  }
}
