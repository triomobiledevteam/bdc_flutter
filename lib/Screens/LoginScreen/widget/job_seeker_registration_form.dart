import 'package:bdc/Screens/LoginScreen/widget/white_app_bar.dart';
import 'package:bdc/Screens/pinCode/PinCodeScreen.dart';
import 'package:bdc/components/buttons/rounded_button.dart';
import 'package:bdc/components/textfield/rounded_input.dart';
import 'package:bdc/components/textfield/rounded_password_input.dart';
import 'package:bdc/core/helper/error_handler/errorhandler.dart';
import 'package:bdc/core/modal/Lookups.dart';
import 'package:bdc/core/modal/constraints/LookUpTypes.dart';
import 'package:bdc/core/modal/enums/UsersType.dart';
import 'package:bdc/core/modal/registration_modal/participant_registration_modal.dart';
import 'package:bdc/core/modal/registration_modal/registrationModal.dart';
import 'package:bdc/core/theme/app_Theme.dart';
import 'package:bdc/core/utils/Navigation.dart';
import 'package:bdc/core/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import '../../../components/loading.dart';

import '../../../core/repository/main_repository/main_repository.dart';
import '../../../core/utils/string_validation_extension.dart';

class JobSeekerRegistrationForm extends StatefulWidget {
  const JobSeekerRegistrationForm({
    Key? key,
    required this.projectID,
  }) : super(key: key);
  final int projectID;

  @override
  _JobSeekerRegistrationFormState createState() =>
      _JobSeekerRegistrationFormState();
}

class _JobSeekerRegistrationFormState extends State<JobSeekerRegistrationForm> {
  String nationality = '';
  String city = '';
  String dateOfBirth = '';
  Duration animationDuration = const Duration(milliseconds: 270);

  TextEditingController nationalityController = TextEditingController(text: '');
  TextEditingController cityController = TextEditingController(text: '');
  TextEditingController userNameController = TextEditingController(text: '');
  TextEditingController birthOfDateController = TextEditingController(text: '');
  TextEditingController mobileController = TextEditingController(text: '');
  TextEditingController dateOfBirthController = TextEditingController(text: '');
  TextEditingController emailController = TextEditingController(text: '');
  TextEditingController passwordController = TextEditingController(text: '');
  TextEditingController confirmPasswordController =
      TextEditingController(text: '');

  final formKey = GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: whiteAppBar(),
      body: AnimatedOpacity(
        opacity: 1,
        duration: animationDuration * 2,
        child: Container(
          width: double.infinity,
          padding: const EdgeInsets.symmetric(horizontal: 16),
          color: mainBackgroundColor,
          child: Form(
            key: formKey,
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 10),
                    child: Text(
                      AppLocalizations.of(context)!.registerToBDC,
                      style: const TextStyle(
                          fontWeight: FontWeight.bold, fontSize: 24),
                    ),
                  ),
                  RoundedInput(
                      controller: nationalityController,
                      icon: Icons.featured_video_outlined,
                      dialogType: LookUpTypes.nationality,
                      isDefaultDialog: true,
                      validator: (value) {
                        if (value!.isEmpty) {
                          return AppLocalizations.of(context)!.thisFieldIsEmpty;
                        }
                        return null;
                      },
                      readOnly: true,
                      getData: getNationality,
                      hint: AppLocalizations.of(context)!.nationalityID),
                  RoundedInput(
                      controller: cityController,
                      icon: Icons.location_city,
                      dialogType: LookUpTypes.city,
                      isDefaultDialog: true,
                      readOnly: true,
                      getData: getCity,
                      validator: (value) {
                        if (value!.isEmpty) {
                          return AppLocalizations.of(context)!.thisFieldIsEmpty;
                        }
                        return null;
                      },
                      hint: AppLocalizations.of(context)!.governorate),
                  RoundedInput(
                      controller: userNameController,
                      icon: Icons.person,
                      validator: (value) {
                        if (value!.isEmpty) {
                          return AppLocalizations.of(context)!.thisFieldIsEmpty;
                        }
                        return null;
                      },
                      hint: AppLocalizations.of(context)!.name),
                  RoundedInput(
                      controller: birthOfDateController,
                      icon: Icons.calendar_today_outlined,
                      getDate: getDOB,
                      isDateDialog: true,
                      readOnly: true,
                      validator: (value) {
                        if (value!.isEmpty) {
                          return AppLocalizations.of(context)!.thisFieldIsEmpty;
                        }
                        return null;
                      },
                      hint: AppLocalizations.of(context)!.birthDate),
                  RoundedInput(
                      controller: mobileController,
                      icon: Icons.phone,
                      validator: (value) {
                        if (!isValidMobile(value ?? '')) {
                          return AppLocalizations.of(context)!.invalidMobile;
                        }
                        return null;
                      },
                      hint:
                          AppLocalizations.of(context)!.mobile + '  07*******'),
                  RoundedInput(
                      controller: emailController,
                      icon: Icons.mail,
                      validator: (value) {
                        if (value!.isEmpty) {
                          return AppLocalizations.of(context)!.thisFieldIsEmpty;
                        }
                        if (!isValidEmail(value)) {
                          return AppLocalizations.of(context)!.invalidEmail;
                        }
                        return null;
                      },
                      hint: AppLocalizations.of(context)!.email),
                  RoundedPasswordInput(
                    hint: AppLocalizations.of(context)!.password,
                    textInputAction: TextInputAction.done,
                    controller: passwordController,
                    validator: (value) {
                      if (!isValidPassword(value ?? '')) {
                        onError(
                            AppLocalizations.of(context)!.passwordValidation);
                        return '';
                      }
                      return null;
                    },
                  ),
                  RoundedPasswordInput(
                    hint: AppLocalizations.of(context)!.confirmPassword,
                    textInputAction: TextInputAction.done,
                    controller: confirmPasswordController,
                    validator: (value) {
                      if (value!.isEmpty) {
                        return AppLocalizations.of(context)!.thisFieldIsEmpty;
                      }
                      if (value.compareTo(passwordController.text) != 0) {
                        return AppLocalizations.of(context)!
                            .passwordDoesNotMatch;
                      }
                      return null;
                    },
                  ),
                  const SizedBox(height: 10),
                  RoundedButton(
                      title: AppLocalizations.of(context)!.signUp,
                      onTap: signUp),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  getNationality(LookUps lookUp) {
    nationality = lookUp.iD!;
  }

  getCity(LookUps lookUp) {
    city = lookUp.iD!;
  }

  getDOB(String date) {
    dateOfBirth = date;
  }

  onError(String massage) {
    ErrorHandler.errorMassage = massage;
    ErrorHandler.showErrorMassageDialog(context);
  }

  onDone(dynamic value) async {
    Navigation.back(context);

    ParticipantRegistrationModal participant = ParticipantRegistrationModal(
        email: emailController.text,
        project: widget.projectID,
        userName: userNameController.text,
        password: passwordController.text,
        birthDate: dateOfBirth,
        mobile: mobileController.text,
        nationality: nationality,
        city: parseInt(city));
    Navigation.next(
        context,
        PinCodeScreen(
            registration: Registration(
                userType: UserType.Participant.name,
                registrationModal: participant)));
  }

  signUp() async {
    if (formKey.currentState!.validate()) {
      progressDialogue(context);

      await GetMainRepository()
          .checkData(emailController.text, mobileController.text, (message) {
        Navigation.back(context);

        onError(message);
      }, onDone);
    }
  }
}
