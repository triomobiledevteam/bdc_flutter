import 'package:bdc/Screens/LoginScreen/widget/login_form.dart';
import 'package:bdc/core/theme/app_Theme.dart';
import 'package:bdc/core/theme/sizeConfig.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import 'widget/language_widget.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen>
    with SingleTickerProviderStateMixin {
  bool isLogin = true;
  late Animation<double> containerSize;
  AnimationController? animationController;
  Duration animationDuration = const Duration(milliseconds: 270);

  @override
  void initState() {
    super.initState();
    animationController =
        AnimationController(vsync: this, duration: animationDuration);
  }

  @override
  void dispose() {
    animationController!.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);

    double defaultRegisterSize =
        SizeConfig.height! - (SizeConfig.height! * 0.1);

    containerSize =
        Tween<double>(begin: SizeConfig.height! * 0.1, end: defaultRegisterSize)
            .animate(CurvedAnimation(
                parent: animationController!, curve: Curves.linear));
    return Scaffold(
      body: SafeArea(
        child: Stack(
          children: [
            const PositionedDirectional(
                top: 10, end: 16, child: LanguageWidget()),
            // Positioned(
            //     top: -50,
            //     left: -50,
            //     child: Container(
            //       width: 200,
            //       height: 200,
            //       decoration: BoxDecoration(
            //           borderRadius: BorderRadius.circular(100),
            //           color: kPrimaryColor.withOpacity(0.5)),
            //     )),
            // CancelButton(
            //   isLogin: isLogin,
            //   animationDuration: animationDuration,
            //   height: SizeConfig.height,
            //   width: SizeConfig.width,
            //   animationController: animationController,
            //   tapEvent: isLogin
            //       ? null
            //       : () {
            //           animationController!.reverse();
            //           setState(() {
            //             isLogin = !isLogin;
            //           });
            //         },
            // ),
            LoginForm(
              isLogin: isLogin,
              animationDuration: animationDuration,
              // defaultLoginSize: defaultLoginSize
            ),
            // AnimatedBuilder(
            //   animation: animationController!,
            //   builder: (context, child) {
            //     if (viewInset == 0 && isLogin) {
            //       return buildRegisterContainer();
            //     } else if (!isLogin) {
            //       return buildRegisterContainer();
            //     }
            //
            //     return Container();
            //   },
            // ),
            // RegisterForm(
            //     isLogin: isLogin,
            //     animationDuration: animationDuration,
            //     defaultLoginSize: defaultRegisterSize),)
          ],
        ),
      ),
    );
  }

  Widget buildRegisterContainer() {
    return Align(
      alignment: Alignment.bottomCenter,
      child: Container(
        width: double.infinity,
        height: containerSize.value,
        decoration: BoxDecoration(
            borderRadius: const BorderRadius.only(
              topLeft: Radius.circular(50),
              topRight: Radius.circular(50),
            ),
            color: kBackgroundColor),
        alignment: Alignment.center,
        child: Center(
          child: Center(
            child: GestureDetector(
              onTap: !isLogin
                  ? null
                  : () {
                      animationController!.forward();
                      setState(() {
                        isLogin = !isLogin;
                      });
                    },
              child: isLogin
                  ? Text(
                      '${AppLocalizations.of(context)!.doNotHaveAnAccount}?${AppLocalizations.of(context)!.signUp}',
                      style: TextStyle(color: kPrimaryColor, fontSize: 18),
                    )
                  : null,
            ),
          ),
        ),
      ),
    );
  }
}
