import 'dart:io';

import 'package:bdc/core/theme/app_Theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:image_picker/image_picker.dart';

import '../../core/helper/error_handler/errorhandler.dart';
import '../../core/utils/Navigation.dart';
import 'attachment/attachment_bloc.dart';

class AttachmentScreen extends StatefulWidget {
  const AttachmentScreen(
      {Key? key, required this.participantLocalID, required this.participantID})
      : super(key: key);
  final String participantLocalID;
  final String participantID;
  @override
  _AttachmentScreenState createState() => _AttachmentScreenState();
}

class _AttachmentScreenState extends State<AttachmentScreen> {
  final ImagePicker _picker = ImagePicker();

  @override
  Widget build(BuildContext context) {
    final attachmentBloc = BlocProvider.of<AttachmentBloc>(context);

    attachmentBloc.add(AttachmentGetSavedAttachmentEvent(
        localID: widget.participantLocalID, iD: widget.participantID));

    return Scaffold(
      appBar: AppBar(),
      body: Padding(
        padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 16),
        child: BlocConsumer<AttachmentBloc, AttachmentState>(
            listener: (context, state) {
          if (state is AttachmentSaved) {
            Navigation.back(context);
          }
          if (state is AttachmentError) {
            print(state.message);
            ErrorHandler.errorMassage =
                AppLocalizations.of(context)!.attachmentValidation;
            ErrorHandler.showErrorMassage(context);
            // onAppError(
            //     context, AppLocalizations.of(context)!.attachmentValidation);
          }
        }, builder: (context, state) {
          return Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                  child: attachmentBloc.attachments.isEmpty
                      ? Center(
                          child: SingleChildScrollView(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              InkWell(
                                onTap: () async {
                                  _showPicker(context);
                                },
                                child: Container(
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(6),
                                      border: Border.all(
                                        color: kPrimaryColor,
                                      )),
                                  height: 140,
                                  width: double.infinity,
                                  child: Icon(
                                    Icons.cloud_upload_outlined,
                                    color: mainColorTheme,
                                    size: 70,
                                  ),
                                ),
                              )
                            ],
                          ),
                        ))
                      : SingleChildScrollView(
                          child: Wrap(
                            crossAxisAlignment: WrapCrossAlignment.start,
                            spacing: 10,
                            runSpacing: 10,
                            direction: Axis.horizontal,
                            children: [
                              ...List.generate(
                                  attachmentBloc.attachments.length,
                                  (index) => Stack(
                                        children: [
                                          Container(
                                            decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(6),
                                              border: Border.all(
                                                color: kPrimaryColor,
                                              ),
                                            ),
                                            height: 150,
                                            width: 150,
                                            child: Image.memory(
                                              attachmentBloc.attachments[index]
                                                  .readAsBytesSync(),
                                              fit: BoxFit.cover,
                                            ),
                                          ),
                                          IconButton(
                                              onPressed: () {
                                                attachmentBloc.add(
                                                    RemoveAttachmentEvent(
                                                        attachment:
                                                            attachmentBloc
                                                                    .attachments[
                                                                index]));
                                              },
                                              icon: const Icon(
                                                Icons.remove_circle_outline,
                                                color: Colors.red,
                                              ))
                                        ],
                                      )),
                              if ((attachmentBloc.attachments.length < 2))
                                InkWell(
                                  onTap: () async {
                                    _showPicker(context);
                                  },
                                  child: Container(
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(6),
                                        border: Border.all(
                                          color: kPrimaryColor,
                                        )),
                                    height: 150,
                                    width: 150,
                                    child: Icon(
                                      Icons.add_circle_outline_rounded,
                                      color: mainColorTheme,
                                      size: 50,
                                    ),
                                  ),
                                ),
                            ],
                          ),
                        )),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 10),
                child: Text(
                  AppLocalizations.of(context)!.attachmentValidation,
                  style: Theme.of(context).textTheme.bodyText1!.copyWith(
                      fontWeight: FontWeight.bold,
                      fontSize: 16,
                      color: Colors.red),
                ),
              ),
              const Spacer(),
              SafeArea(
                child: AbsorbPointer(
                  absorbing: attachmentBloc.attachments.length != 2,
                  child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                          backgroundColor:
                              attachmentBloc.attachments.length != 2
                                  ? mainColorTheme.withOpacity(0.1)
                                  : mainColorTheme,
                          minimumSize: const Size(double.infinity, 50)),
                      onPressed: () {
                        attachmentBloc.add(SaveAttachmentEvent(
                            localID: widget.participantLocalID,
                            iD: widget.participantID));
                      },
                      child: Text(AppLocalizations.of(context)!.save)),
                ),
              )
            ],
          );
        }),
      ),
    );
  }

  Future _imgFromCamera() async {
    final attachmentBloc = BlocProvider.of<AttachmentBloc>(context);
    var pickedFile =
        await _picker.pickImage(source: ImageSource.camera, imageQuality: 50);

    attachmentBloc.add(AddAttachmentEvent(attachment: File(pickedFile!.path)));
  }

  Future _imgFromGallery() async {
    final attachmentBloc = BlocProvider.of<AttachmentBloc>(context);
    var pickedFile =
        await _picker.pickImage(source: ImageSource.gallery, imageQuality: 50);

    attachmentBloc.add(AddAttachmentEvent(attachment: File(pickedFile!.path)));
  }

  void _showPicker(context) {
    // final local = AppLocalizations.of(context)!;

    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return SafeArea(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                ListTile(
                    leading: const Icon(Icons.photo_library),
                    title: const Text('Photo Library'),
                    onTap: () {
                      _imgFromGallery();
                      Navigator.of(context).pop();
                    }),
                ListTile(
                    leading: const Icon(Icons.photo_camera),
                    title: const Text('Camera'),
                    onTap: () async {
                      _imgFromCamera();
                    }),
              ],
            ),
          );
        });
  }
}
