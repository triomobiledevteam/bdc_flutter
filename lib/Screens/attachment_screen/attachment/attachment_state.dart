part of 'attachment_bloc.dart';

@immutable
abstract class AttachmentState {}

class AttachmentInitial extends AttachmentState {}

class AttachmentUploading extends AttachmentState {}

class AttachmentSaved extends AttachmentState {}

class AttachmentLoad extends AttachmentState {}

class AttachmentError extends AttachmentState {
  final String message;
  AttachmentError({required this.message});
}
