import 'dart:io';
import 'dart:typed_data';

import 'package:bdc/core/modal/attachment.dart';
import 'package:bdc/core/modal/constraints/global_values.dart';
import 'package:bdc/core/modal/enums/UsersType.dart';
import 'package:bdc/core/storage/sql_table/attachment/attachment_db.dart';
import 'package:bdc/core/utils/utils.dart';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:path_provider/path_provider.dart';

part 'attachment_event.dart';
part 'attachment_state.dart';

class AttachmentBloc extends Bloc<AttachmentEvent, AttachmentState> {
  // final UploadAttachments uploadAttachments;
  //{required this.uploadAttachments}
  AttachmentDatabase? attachmentDatabase;

  AttachmentBloc() : super(AttachmentInitial()) {
    on<AttachmentInitialDatabaseEvent>((event, emit) async {
      attachmentDatabase = AttachmentDatabase.instance;
      await attachmentDatabase!.database;

      emit(AttachmentInitial());
    });
    on<AddAttachmentEvent>((event, emit) => _addAttachment(event, emit));
    on<AttachmentGetSavedAttachmentEvent>(
        (event, emit) => _getAttachment(event, emit));
    on<RemoveAttachmentEvent>((event, emit) => _removeAttachment(event, emit));
    on<SaveAttachmentEvent>((event, emit) => _uploadAttachments(event, emit));
    on<EmptyAttachmentEvent>((event, emit) => _emptyAttachment(event, emit));
  }
  List<File> attachments = [];

  _addAttachment(AddAttachmentEvent event, emit) {
    attachments.add(event.attachment);
    emit(AttachmentInitial());
  }

  _getAttachment(AttachmentGetSavedAttachmentEvent event, emit) async {
    // await attachmentDatabase!.getAttachments();
    attachments.clear();

    final list = userType == UserType.Employee.name
        ? await attachmentDatabase!.getAdminParticipantAttachment(event.localID)
        : await attachmentDatabase!.getParticipantAttachment(event.iD);

    if (list.isNotEmpty) {
      for (var attachment in list) {
        Uint8List imageInUnit8List = attachment.image;
        // store unit8List image here ;
        final tempDir = await getTemporaryDirectory();
        File file = await File('${tempDir.path}/${attachment.id}.png').create();
        file.writeAsBytesSync(imageInUnit8List);
        attachments.add(file);
      }
    }

    emit(AttachmentInitial());
  }

  _removeAttachment(RemoveAttachmentEvent event, emit) async {
    attachments.remove(event.attachment);

    // String image = String.fromCharCodes(event.attachment.readAsBytesSync());
    // var outputAsUint8List = Uint8List.fromList(s.codeUnits);

    var att = await attachmentDatabase!
        .deleteAttachmentImage(event.attachment.readAsBytesSync());

    emit(AttachmentInitial());
  }

  _uploadAttachments(SaveAttachmentEvent event, emit) async {
    print(attachments.length == 2);
    if (attachments.length == 2) {
      for (var attachment in attachments) {
        var att = await attachmentDatabase!.attachmentDB(AttachmentModal(
            localID: parseInt(event.localID),
            trioID: parseInt(event.iD),
            image: attachment.readAsBytesSync()));
      }
      emit(AttachmentSaved());
    } else {
      emit(AttachmentError(message: ''));
    }
  }

  _emptyAttachment(EmptyAttachmentEvent event, emit) {
    attachments = [];
    emit(AttachmentInitial());
  }
}
