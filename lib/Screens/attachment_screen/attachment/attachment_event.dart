part of 'attachment_bloc.dart';

abstract class AttachmentEvent {}

class AttachmentInitialDatabaseEvent extends AttachmentEvent {}

class AddAttachmentEvent extends AttachmentEvent {
  final File attachment;
  AddAttachmentEvent({required this.attachment});
}

class AttachmentGetSavedAttachmentEvent extends AttachmentEvent {
  final String localID;
  final String iD;

  AttachmentGetSavedAttachmentEvent({required this.localID, required this.iD});
}

class RemoveAttachmentEvent extends AttachmentEvent {
  final File attachment;
  RemoveAttachmentEvent({required this.attachment});
}

class SaveAttachmentEvent extends AttachmentEvent {
  final String localID;
  final String iD;
  SaveAttachmentEvent({required this.localID, required this.iD});
}

class EmptyAttachmentEvent extends AttachmentEvent {}
