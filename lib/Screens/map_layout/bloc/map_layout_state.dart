abstract class MapLayoutStates {}

class MapLayoutInitState extends MapLayoutStates {}

class MapLayoutDisplayLocationStates extends MapLayoutStates {}

class MapLayoutGetAddressState extends MapLayoutStates {}

class MapLayoutGetMapLogState extends MapLayoutStates {}

class MapLayoutGetCurrentLocationState extends MapLayoutStates {}

class MapLayoutPermissionGrantedState extends MapLayoutStates {}

class MapLayoutPermissionDeniedState extends MapLayoutStates {}

class MapLayoutSetMarkerState extends MapLayoutStates {}

class MapLayoutLoadingState extends MapLayoutStates {}

class MapLayoutSetAddressState extends MapLayoutStates {}

class MapLayoutSetMapLogState extends MapLayoutStates {}

class MapLayoutLoadingPredictionsState extends MapLayoutStates {}

class MapLayoutDoneLoadingPredictionsState extends MapLayoutStates {}

class MapLayoutDoneLoadingPlaceState extends MapLayoutStates {}

class MapLayoutLoadingPlaceState extends MapLayoutStates {}

class MapLayoutOnSelectPlaceState extends MapLayoutStates {}

class MapLayoutDisabledLocationState extends MapLayoutStates {}
