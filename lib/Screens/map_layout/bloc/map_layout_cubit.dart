import 'package:bdc/core/services/places_service.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';
import 'package:permission_handler/permission_handler.dart';

import '../../../core/modal/map_service/MapLog.dart';
import '../../../core/modal/map_service/Place.dart';
import '../../../core/modal/map_service/address.dart';
import 'map_layout_state.dart';

class MapLayoutCubit extends Cubit<MapLayoutStates> {
  MapLayoutCubit() : super(MapLayoutInitState());

  static MapLayoutCubit get(context) =>
      BlocProvider.of<MapLayoutCubit>(context);

  late TextEditingController addressController =
      TextEditingController(text: '');
  late TextEditingController searchController = TextEditingController(text: '');
  MapLog? _mapLog;

  MapLog get mapLog => _mapLog!;

  // late CameraPosition cameraPosition;

  late CameraPosition cameraPosition;

  late double lat, lng, zoom;

  late bool disable = false;
  var location = Location();

  bool isCurrentLocation = true;
  Set<Marker> markers = {};

  late GoogleMapController mapController;

  List<Place> placeList = [];
  Place place = Place();
  List<Predictions> placeSearch = [];

  init() async {
    _mapLog =
        MapLog(latitude: 37.43296265331129, longitude: -122.08832357078792);
    cameraPosition = const CameraPosition(
        bearing: 192.8334901395799,
        target: LatLng(37.43296265331129, -122.08832357078792),
        tilt: 59.440717697143555,
        zoom: 19.151926040649414);

    markers = {};

    await getLocationPermission();
  }

  onMapCreate(GoogleMapController controller) async {
    mapController = (controller);
    mapController.animateCamera(CameraUpdate.newCameraPosition(cameraPosition));
  }

  _getLocation() async {
    emit(MapLayoutLoadingState());

    if (isCurrentLocation) {
      Location currentLocation = Location();

      var location = await currentLocation.getLocation().catchError((onError) {
        emit(MapLayoutDisplayLocationStates());
      }).timeout(const Duration(seconds: 30));

      _mapLog = MapLog(
          latitude: location.latitude ?? 0,
          longitude: location.longitude ?? 0,
          zoom: 17);
    }

    await getCurrentAddress();

    cameraPosition = CameraPosition(
        target: LatLng(_mapLog!.latitude, _mapLog!.longitude), zoom: 17.0);
    await getCurrentAddress();

    markers.add(Marker(
        markerId: const MarkerId('location'),
        draggable: true,
        position: LatLng(_mapLog!.latitude, _mapLog!.longitude),
        infoWindow: const InfoWindow(title: 'location'),
        icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueRed),
        onDragEnd: (_currentLatLng) {
          _mapLog!.latitude = _currentLatLng.latitude;
          _mapLog!.longitude = _currentLatLng.longitude;
          // latLng = _currentLatLng;
        }));
    mapController.animateCamera(CameraUpdate.newCameraPosition(cameraPosition));

    emit(MapLayoutDisplayLocationStates());
  }

  setAddress(String address) {
    addressController.text = address;
    emit(MapLayoutSetAddressState());
  }

  clearAddress() {
    addressController.clear();
    markers = {};
    isCurrentLocation = false;
    _mapLog = MapLog();
    emit(MapLayoutSetAddressState());
  }

  setMapLog(
    MapLog map,
  ) async {
    _mapLog = map;
    emit(MapLayoutSetMapLogState());
    if (map.longitude == 0 || map.latitude == 0) {
      isCurrentLocation = true;
    } else {
      isCurrentLocation = false;
    }
    _getLocation();

    await getCurrentAddress();
  }

  // getAddressFromMapUrl(MapLog map) async {
  //   Place place =
  //       await PlacesService().getPlaceFormPosition(map.latitude, map.longitude);
  // }

  getAddress(LatLng latLng) async {
    List<Address> address = await PlacesService()
        .getAddressesFromPosition(latLng.latitude, latLng.longitude);

    mapLog.address = address.first.addressLine!;

    setAddress(address.first.addressLine!);
  }

  handleTap(LatLng? point) async {
    markers = {};
    lat = point!.latitude;
    lng = point.longitude;
    zoom = 17;

    markers.add(Marker(
        markerId: MarkerId(point.toString()), position: point, visible: true));
    await getAddress(LatLng(lat, lng));

    emit(MapLayoutSetMarkerState());
  }

  Future getCurrentLocation() async {
    LocationPermission permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      LocationPermission permission = await Geolocator.requestPermission();
      if (permission != LocationPermission.always &&
          permission != LocationPermission.whileInUse) {
        emit(MapLayoutPermissionDeniedState());
      } else {
        emit(MapLayoutPermissionGrantedState());
        _getLocation();
      }
    } else if (permission == LocationPermission.deniedForever) {
      emit(MapLayoutPermissionDeniedState());
    } else {
      emit(MapLayoutPermissionGrantedState());
      _getLocation();
    }
  }

  getCurrentAddress() async {
    List<Address> address = await PlacesService()
        .getAddressesFromPosition(mapLog.latitude, mapLog.longitude);
    mapLog.address = address.first.addressLine!;

    if (isCurrentLocation) {
      setAddress(mapLog.address);
    }
    emit(MapLayoutGetAddressState());
  }

  searchPlaces(String query) async {
    emit(MapLayoutLoadingPredictionsState());
    placeSearch = await PlacesService().getAutocomplete(query);

    emit(MapLayoutDoneLoadingPredictionsState());
  }

  Future<void> getPlace(String placeID) async {
    emit(MapLayoutLoadingPredictionsState());
    place = await PlacesService().getPlace(placeID);
    MapLog map = MapLog(
        latitude: place.geometry!.location.lat,
        longitude: place.geometry!.location.lng);
    searchController.text = place.name!;

    emit(MapLayoutDoneLoadingPredictionsState());
    await setMapLog(map);
    emit(MapLayoutOnSelectPlaceState());
  }

  Future<bool> isLocationEnable() async {
    if (await Permission.locationWhenInUse.serviceStatus.isEnabled) {
      emit(MapLayoutInitState());
      return true;
    }
    emit(MapLayoutDisabledLocationState());
    return false;
  }

  Future<void> getLocationPermission() async {
    if (await isLocationEnable()) {
      await getCurrentLocation();
    } else {
      if (!await location.serviceEnabled()) {
        await location.requestService();
        if (await location.serviceEnabled()) {
          await getLocationPermission();
        }
      }
    }
  }
}
