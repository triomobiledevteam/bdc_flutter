import 'dart:async';
import 'dart:io';

import 'package:bdc/core/helper/error_handler/errorhandler.dart';
import 'package:bdc/core/modal/LoginModal/Trainer.dart';
import 'package:bdc/core/repository/main_repository/main_repository.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class UploadCVScreen extends StatefulWidget {
  const UploadCVScreen({Key? key}) : super(key: key);

  @override
  _UploadCVScreenState createState() => _UploadCVScreenState();
}

class _UploadCVScreenState extends State<UploadCVScreen> {
  FilePickerResult? selectedFile;
  String progress = '';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(AppLocalizations.of(context)!.uploadCV),
        ),
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Center(
              child: InkWell(
                onTap: selectFile,
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(6),
                    border: Border.all(),
                    image: const DecorationImage(
                      image: AssetImage('assets/images/uploadIcon.png'),
                    ),
                  ),
                ),
              ),
            ),
            if (selectedFile != null) Text('${selectedFile!.names.first}'),
            const SizedBox(
              height: 60.0,
            ),
            Text(
              progress,
              style: const TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 40.5,
              ),
            ),
            const SizedBox(
              height: 15.0,
            ),
            Padding(
              padding: const EdgeInsets.only(left: 30.0, right: 30.0),
              child: ElevatedButton(
                onPressed: () {
                  File file = File(selectedFile!.files.first.path!);
                  uploadCV(file);
                },
                child: Text(
                  AppLocalizations.of(context)!.upload,
                  textAlign: TextAlign.center,
                  style: const TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 20.5,
                  ),
                ),
              ),
            ),
            const SizedBox(
              height: 15.0,
            ),
          ],
        ));
  }

  selectFile() async {
    selectedFile = await FilePicker.platform.pickFiles(
      type: FileType.custom,
      allowedExtensions: ['pdf'],
    );
    setState(() {});
  }

  Future uploadCV(File file) async {
    await GetMainRepository().trainerUploadFile(mainTrainer!, file, onError,
        (value) {
      ErrorHandler.onSuccessSnackBar("Done", context);
    });
  }

  onError(String massage) {
    onAppError(context, massage);
  }
}
