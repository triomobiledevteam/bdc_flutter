import 'package:bdc/components/textfield/rounded_password_input.dart';
import 'package:bdc/core/utils/Navigation.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SettingScreen extends StatefulWidget {
  const SettingScreen({Key? key}) : super(key: key);

  @override
  _SettingScreenState createState() => _SettingScreenState();
}

class _SettingScreenState extends State<SettingScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Setting'),
      ),
      body: Container(
        padding: const EdgeInsets.only(left: 16, top: 25, right: 16),
        child: ListView(
          children: [
            Row(
              children: const [
                Icon(
                  Icons.person,
                  color: Colors.green,
                ),
                SizedBox(
                  width: 8,
                ),
                Text(
                  "Account",
                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                ),
              ],
            ),
            const Divider(
              height: 15,
              thickness: 2,
            ),
            const SizedBox(
              height: 10,
            ),
            buildAccountOptionRow(context, "Change password", changePassword),
            buildAccountOptionRow(context, "Change phone", changePassword),
            buildAccountOptionRow(context, "Change email", changePassword),
            buildAccountOptionRow(context, "Social", changePassword),
            buildAccountOptionRow(context, "Language", changePassword),
            const SizedBox(
              height: 40,
            ),
            const SizedBox(
              height: 50,
            ),
          ],
        ),
      ),
    );
  }

  Row buildNotificationOptionRow(String title, bool isActive) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          title,
          style: TextStyle(
              fontSize: 18,
              fontWeight: FontWeight.w500,
              color: Colors.grey[600]),
        ),
        Transform.scale(
            scale: 0.7,
            child: CupertinoSwitch(
              value: isActive,
              onChanged: (bool val) {},
            ))
      ],
    );
  }

  Widget buildAccountOptionRow(
      BuildContext context, String title, void Function() onTap) {
    return InkWell(
      onTap: onTap,
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 8.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              title,
              style: TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.w500,
                color: Colors.grey[600],
              ),
            ),
            const Icon(
              Icons.arrow_forward_ios,
              color: Colors.grey,
            ),
          ],
        ),
      ),
    );
  }

  changePassword() {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: const Text("Change password"),
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                RoundedPasswordInput(
                  hint: 'Old Password',
                  textInputAction: TextInputAction.done,
                  controller: TextEditingController(),
                ),
                RoundedPasswordInput(
                  hint: 'New Password',
                  textInputAction: TextInputAction.done,
                  controller: TextEditingController(),
                ),
                RoundedPasswordInput(
                  hint: 'Confirm New Password',
                  textInputAction: TextInputAction.done,
                  controller: TextEditingController(),
                ),
              ],
            ),
            actions: [
              ElevatedButton(
                  onPressed: () {
                    Navigation.back(context);
                  },
                  child: const Text("Close")),
              ElevatedButton(
                  onPressed: () {
                    Navigation.back(context);
                  },
                  child: const Text("Done")),
            ],
          );
        });
  }
}
