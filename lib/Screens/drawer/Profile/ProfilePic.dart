import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';

import 'package:bdc/core/helper/error_handler/errorhandler.dart';
import 'package:bdc/core/modal/LoginModal/Employee.dart';
import 'package:bdc/core/modal/LoginModal/GenUser.dart';
import 'package:bdc/core/modal/constraints/global_values.dart';
import 'package:bdc/core/repository/main_repository/main_repository.dart';
import 'package:bdc/core/theme/app_Theme.dart';
import 'package:bdc/core/theme/sizeConfig.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

import '../../../core/modal/enums/UsersType.dart';

class ProfilePic extends StatefulWidget {
  const ProfilePic({
    Key? key,
  }) : super(key: key);

  @override
  _ProfilePicState createState() => _ProfilePicState();
}

class _ProfilePicState extends State<ProfilePic> {
  @override
  void initState() {
    // TODO: implement initState
    checkUserType();
    super.initState();
  }

  Uint8List? bytes;
  String userName = '';
  String image = '';

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 20),
      child: SizedBox(
        height: SizeConfig.height! * 0.12,
        width: SizeConfig.height! * 0.12,
        child: Stack(
          clipBehavior: Clip.none,
          fit: StackFit.expand,
          children: [
            CircleAvatar(
              backgroundColor: kPrimaryColorGreen,
              backgroundImage: MemoryImage(bytes!, scale: 01),
            ),
            Positioned(
              right: -16,
              bottom: 0,
              child: SizedBox(
                height: SizeConfig.height! * 0.05,
                width: SizeConfig.height! * 0.05,
                child: InkWell(
                  onTap: () {
                    _showPicker(context);
                  },
                  child: Container(
                    decoration: const BoxDecoration(
                      color: Color(0xFFF5F6F9),
                      shape: BoxShape.circle,
                    ),
                    // onPressed: () {},
                    child: const Center(child: Icon(Icons.camera_alt)),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  checkUserType() {
    if (UserType.Employee.name == userType) {
      userName = mainEmployee.fullName!;
      image = mainEmployee.image!;
    } else {
      userName = mainUser.userName!;
      image = mainUser.image!;
    }
    convertUnit6List(image);
  }

  convertUnit6List(String? image) {
    bytes = const Base64Decoder().convert(image!);
    setState(() {});
  }

  final ImagePicker _picker = ImagePicker();

  void _showPicker(context) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return SafeArea(
            child: Wrap(
              children: <Widget>[
                ListTile(
                    leading: const Icon(Icons.photo_library),
                    title: const Text('Photo Library'),
                    onTap: () {
                      _imgFromGallery();
                      Navigator.of(context).pop();
                    }),
                ListTile(
                    leading: const Icon(Icons.photo_camera),
                    title: const Text('Camera'),
                    onTap: () async {
                      _imgFromCamera();
                    }),
              ],
            ),
          );
        });
  }

  Future _imgFromCamera() async {
    var pickedFile =
        await _picker.pickImage(source: ImageSource.camera, imageQuality: 25);

    if (pickedFile != null) {
      XFile selectedImage = XFile(pickedFile.path);
      bytes = await selectedImage.readAsBytes();
      setState(() {});
      uploadImage(selectedImage);
    } else {
      ErrorHandler.errorMassage = ('No image selected.');
      ErrorHandler.showErrorMassageDialog(context);
    }
  }

  Future _imgFromGallery() async {
    var pickedFile =
        await _picker.pickImage(source: ImageSource.gallery, imageQuality: 50);
    if (pickedFile != null) {
      XFile selectedImage = XFile(pickedFile.path);
      bytes = await selectedImage.readAsBytes();
      setState(() {});
      uploadImage(selectedImage);
    } else {
      ErrorHandler.errorMassage = ('No image selected.');
      ErrorHandler.showErrorMassageDialog(context);
    }
  }

  uploadImage(XFile selectedImage) async {
    print(selectedImage.name);
    if (UserType.Employee.name == userType) {
      await GetMainRepository().employeeUploadImage(
          File(selectedImage.path), onError, (value) async {
        ErrorHandler.onSuccessSnackBar("Done", context);
        final bytes = File(selectedImage.path).readAsBytesSync();

        mainEmployee.image = base64Encode(bytes);
      });
    } else {
      await GetMainRepository()
          .userUploadImage(File(selectedImage.path), onError, (value) async {
        ErrorHandler.onSuccessSnackBar("Done", context);
        final bytes = File(selectedImage.path).readAsBytesSync();
        mainUser.image = base64Encode(bytes);
      });
    }
  }

  onError(String massage) {
    onAppError(context, massage);
  }
}
