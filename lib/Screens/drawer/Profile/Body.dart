import 'package:bdc/Screens/drawer/Profile/profile_menu.dart';
import 'package:bdc/core/modal/LoginModal/Employee.dart';
import 'package:bdc/core/modal/LoginModal/GenUser.dart';
import 'package:bdc/core/modal/Lookups.dart';
import 'package:bdc/core/modal/constraints/global_values.dart';
import 'package:bdc/core/modal/enums/UsersType.dart';
import 'package:bdc/core/storage/db_client.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class ProfileBody extends StatefulWidget {
  const ProfileBody({Key? key}) : super(key: key);

  @override
  _ProfileBodyState createState() => _ProfileBodyState();
}

class _ProfileBodyState extends State<ProfileBody> {
  final db = DatabaseClient.instance;

  String userName = '';
  String mobile = '';
  String email = '';
  String nationality = '';
  LookUps? selectedLookup;

  @override
  void initState() {
    print(userType);
    // TODO: implement initState
    getDefaultData();
    if (UserType.Employee.name == userType) {
      userName = mainEmployee.fullName!;
      mobile = mainEmployee.mobile!;
      email = mainEmployee.email!;
    } else {
      userName = mainUser.userName!;
      mobile = mainUser.mobile!;
      email = mainUser.email!;
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 16),
      child: Column(
        children: [
          ProfileMenu(
            text: AppLocalizations.of(context)!.userName,
            text2: userName,
          ),
          ProfileMenu(
            text: AppLocalizations.of(context)!.mobile,
            text2: mobile,
          ),
          ProfileMenu(
            text: AppLocalizations.of(context)!.email,
            text2: email,
          ),
          Visibility(
            visible: UserType.Employee.name != userType &&
                UserType.Company.name != userType,
            child: ProfileMenu(
              text: AppLocalizations.of(context)!.nationalityID,
              text2: nationality,
            ),
          ),
        ],
      ),
    );
  }

  getDefaultData() async {
    selectedLookup = await db.getWhereNationality(mainUser.nationality);

    nationality = selectedLookup!.descriptionEn!;
    setState(() {});
  }
}
