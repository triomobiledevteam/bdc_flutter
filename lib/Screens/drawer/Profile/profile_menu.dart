import 'package:bdc/components/input_container.dart';
import 'package:bdc/core/theme/app_Theme.dart';
import 'package:flutter/material.dart';

class ProfileMenu extends StatelessWidget {
  const ProfileMenu({
    Key? key,
    required this.text,
    this.text2,
  }) : super(key: key);

  final String? text, text2;

  @override
  Widget build(BuildContext context) {
    return InputContainer(
      child: Padding(
        padding: const EdgeInsets.symmetric(
          vertical: 10.0,
        ),
        child: Row(
          children: [
            Expanded(
                child: Text(
              text!,
              style: basicTextStyle,
            )),
            const SizedBox(width: 10),
            Expanded(
                child: Text(
              text2!,
              style: basicTextStyle,
            )),
          ],
        ),
      ),
    );
  }
}
