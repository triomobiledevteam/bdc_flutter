import 'package:bdc/components/textfield/rounded_input.dart';
import 'package:bdc/core/helper/error_handler/errorhandler.dart';
import 'package:bdc/core/modal/LoginModal/GenUser.dart';
import 'package:bdc/core/modal/Lookups.dart';
import 'package:bdc/core/modal/constraints/LookUpTypes.dart';
import 'package:bdc/core/modal/constraints/global_values.dart';
import 'package:bdc/core/repository/main_repository/main_repository.dart';
import 'package:bdc/core/theme/app_Theme.dart';
import 'package:bdc/core/theme/sizeConfig.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import '../../../core/utils/string_validation_extension.dart';

class ProfileBodyUpdate extends StatefulWidget {
  const ProfileBodyUpdate({Key? key}) : super(key: key);

  @override
  _ProfileBodyUpdateState createState() => _ProfileBodyUpdateState();
}

class _ProfileBodyUpdateState extends State<ProfileBodyUpdate> {
  TextEditingController? userNameController;
  TextEditingController? mobileController;
  TextEditingController? emailController;
  TextEditingController? nationalityController;

  String nationality = '';
  String userName = '';
  String mobile = '';
  String email = '';
  String password = '';

  @override
  void initState() {
    print(userType);
    // TODO: implement initState

    nationality = mainUser.nationality!;
    userName = mainUser.userName!;
    mobile = mainUser.mobile!;
    email = mainUser.email!;

    userNameController = TextEditingController(text: mainUser.userName!);
    mobileController = TextEditingController(text: mainUser.mobile!);
    emailController = TextEditingController(text: mainUser.email!);
    nationalityController = TextEditingController(text: '');

    mobileController!.addListener(() {
      mobile = mobileController!.text;
    });
    emailController!.addListener(() {
      email = emailController!.text;
    });
    userNameController!.addListener(() {
      userName = userNameController!.text;
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
        padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 16),
        child: SingleChildScrollView(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              RoundedInput(
                  controller: userNameController!,
                  icon: Icons.person,
                  validator: (value) {
                    if (!isValidEmail(value ?? '')) {
                      return AppLocalizations.of(context)!.invalidEmail;
                    }
                    return null;
                  },
                  autofocus: true,
                  hint: AppLocalizations.of(context)!.name),
              RoundedInput(
                  controller: mobileController!,
                  validator: (value) {
                    if (!isValidMobile(value ?? '')) {
                      return AppLocalizations.of(context)!.invalidMobile;
                    }
                    return null;
                  },
                  icon: Icons.phone,
                  hint: AppLocalizations.of(context)!.mobile),
              RoundedInput(
                  controller: emailController!,
                  icon: Icons.mail,
                  validator: (value) {
                    if (!isValidEmail(value ?? '')) {
                      return AppLocalizations.of(context)!.invalidEmail;
                    }
                    return null;
                  },
                  hint: AppLocalizations.of(context)!.email),
              RoundedInput(
                  controller: nationalityController!,
                  icon: Icons.featured_video_outlined,
                  dialogType: LookUpTypes.nationality,
                  isDefaultDialog: true,
                  recordID: mainUser.nationality!,
                  readOnly: true,
                  getData: getNationality,
                  hint: AppLocalizations.of(context)!.nationalityID),
              // SizedBox(
              //   height: 20,
              // ),
              Container(
                width: SizeConfig.width! * 0.9,
                padding: const EdgeInsets.symmetric(
                  vertical: 10,
                ),
                child: ElevatedButton(
                  style:
                      ElevatedButton.styleFrom(backgroundColor: mainColorTheme),
                  onPressed: () async {
                    await GetMainRepository().genUserUpdateInfo(
                        userName, nationality, mobile, email, onError, onDone);
                    // print('Update');
                  },
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                      vertical: 10,
                    ),
                    child: Text(AppLocalizations.of(context)!.update),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  getNationality(LookUps lookUp) {
    nationality = lookUp.iD!;
  }

  onError(String massage) {
    ErrorHandler.errorMassage = massage;
    ErrorHandler.showErrorMassageDialog(context);
  }

  onDone() {
    // Navigation.back(context);
    ErrorHandler.onSuccessSnackBar("Done", context);
    print('Done');
  }
}
