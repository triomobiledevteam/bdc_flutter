import 'package:bdc/Screens/drawer/Profile/ProfileBodyUpdateData.dart';
import 'package:bdc/Screens/drawer/Profile/ProfilePic.dart';
import 'package:bdc/components/bottom_sheet/ChangePassword_BottomSheet.dart';
import 'package:bdc/core/modal/constraints/global_values.dart';
import 'package:bdc/core/modal/enums/UsersType.dart';
import 'package:bdc/core/theme/app_Theme.dart';
import 'package:bdc/core/theme/sizeConfig.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import 'Body.dart';

class ProfileScreen extends StatefulWidget {
  const ProfileScreen({Key? key}) : super(key: key);

  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  bool edit = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar:
          AppBar(title: Text(AppLocalizations.of(context)!.profile), actions: [
        Visibility(
          visible: UserType.Employee.name != userType &&
              UserType.Company.name != userType,
          child: IconButton(
            icon: const Icon(Icons.edit),
            onPressed: () {
              edit = !edit;
              setState(() {});
            },
          ),
        ),
      ]),
      body: Column(
        children: [
          const ProfilePic(),
          Visibility(
            visible: !edit,
            child: const ProfileBody(),
            replacement: const ProfileBodyUpdate(),
          ),
          Visibility(
            visible: !edit,
            child: SizedBox(
              width: SizeConfig.width! * 0.9,
              child: ElevatedButton(
                style:
                    ElevatedButton.styleFrom(backgroundColor: mainColorTheme),
                onPressed: () {
                  showModalBottomSheet(
                      context: context,
                      isScrollControlled: true,
                      builder: (context) => ChangePasswordBottomSheet(
                            onPress: () {},
                          ));
                },
                child: Padding(
                  padding: const EdgeInsets.symmetric(
                    vertical: 15,
                  ),
                  child: Text(AppLocalizations.of(context)!.changePassword),
                ),
              ),
            ),
          ),
        ],
      ),
      // bottomNavigationBar: CustomBottomNavBar(selectedMenu: MenuState.profile),
    );
  }
}
