import 'package:bdc/core/theme/app_Theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:url_launcher/url_launcher.dart';

class AboutUs extends StatelessWidget {
  const AboutUs({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(AppLocalizations.of(context)!.aboutUs),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 30),
              child: InkWell(
                onTap: _launchURL,
                child: Image.asset(
                  'assets/bdc.png',
                  fit: BoxFit.fill,
                  height: 120,
                  width: double.infinity,
                ),
              ),
            ),
            const SizedBox(height: 20),
            TextButton(
              onPressed: _launchURL,
              child: Text(
                'Visit our website',
                style: TextStyle(
                    decoration: TextDecoration.underline, color: kPrimaryColor),
              ),
            )
          ],
        ),
      ),
    );
  }
}

void _launchURL() async {
  if (!await launchUrl(Uri.parse('https://bdc.org.jo/#About'))) {
    throw 'Could not launch ';
  }
}
