import 'dart:convert';
import 'dart:typed_data';

import 'package:bdc/Screens/Forms/CompanyForm/CompanyInfoScreen.dart';
import 'package:bdc/Screens/Forms/ParticipantForm/UserInfoScreen.dart';
import 'package:bdc/Screens/Forms/trainer_form/trainer_info_screen.dart';
import 'package:bdc/Screens/LoginScreen/LoginScreen.dart';
import 'package:bdc/Screens/Users/EMPHomeScreen/CompaniesScreen.dart';
import 'package:bdc/Screens/Users/EMPHomeScreen/ParticipantsInfoScreen.dart';
import 'package:bdc/Screens/drawer/ContactUs.dart';
import 'package:bdc/Screens/drawer/Profile/Profile.dart';
import 'package:bdc/Screens/drawer/UploadCVScreen.dart';
import 'package:bdc/components/dialog/language_popup.dart';
import 'package:bdc/core/modal/LoginModal/Employee.dart';
import 'package:bdc/core/modal/LoginModal/GenUser.dart';
import 'package:bdc/core/modal/constraints/global_values.dart';
import 'package:bdc/core/modal/enums/UsersType.dart';
import 'package:bdc/core/storage/cache_helper.dart';
import 'package:bdc/core/theme/app_Theme.dart';
import 'package:bdc/core/theme/sizeConfig.dart';
import 'package:bdc/core/utils/Navigation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import 'AboutUs.dart';

class MainDrawer extends StatefulWidget {
  const MainDrawer({Key? key}) : super(key: key);

  @override
  _MainDrawerState createState() => _MainDrawerState();
}

class _MainDrawerState extends State<MainDrawer> {
  @override
  void initState() {
    // TODO: implement initState
    checkUserType();
    super.initState();
  }

  Uint8List bytes = Uint8List(1);
  String userName = '';
  String image = '';

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          SizedBox(
            width: double.infinity,
            height: MediaQuery.of(context).size.height * 0.25,
            child: DrawerHeader(
              padding: const EdgeInsets.symmetric(
                horizontal: 15.0,
                vertical: 10,
              ),
              decoration: BoxDecoration(
                color: mainColorTheme,
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SizedBox(
                    height: MediaQuery.of(context).size.height * 0.11,
                    width: MediaQuery.of(context).size.height * 0.11,
                    child: Stack(
                      clipBehavior: Clip.none,
                      fit: StackFit.expand,
                      children: [
                        CircleAvatar(
                          backgroundColor: kPrimaryColorGreen,
                          backgroundImage: bytes.isNotEmpty
                              ? MemoryImage(bytes, scale: 01)
                              : null,
                        ),
                        Positioned(
                          right: -16,
                          bottom: 0,
                          child: InkWell(
                              onTap: () {
                                Navigation.next(context, const ProfileScreen());
                              },
                              child: Container(
                                height:
                                    MediaQuery.of(context).size.height * 0.05,
                                width:
                                    MediaQuery.of(context).size.height * 0.05,
                                decoration: const BoxDecoration(
                                  color: Color(0xFFF5F6F9),
                                  shape: BoxShape.circle,
                                ),
                                child: const Center(
                                  child: Icon(
                                    Icons.edit,
                                  ),
                                ),
                              )),
                        ),
                      ],
                    ),
                  ),
                  const SizedBox(height: 10),
                  FittedBox(
                    child: Text(userName,
                        style: Theme.of(context)
                            .textTheme
                            .headline6!
                            .copyWith(color: whiteColor)),
                  ),
                ],
              ),
            ),
          ),
          SingleChildScrollView(
            child: Column(
              children: [
                Visibility(
                  visible: userType == UserType.Employee.name,
                  child: ListTile(
                    title: Row(
                      children: [
                        const Icon(Icons.supervised_user_circle),
                        const SizedBox(width: 10),
                        Text(AppLocalizations.of(context)!.participants),
                      ],
                    ),
                    onTap: () {
                      Navigation.next(context, const ParticipantsInfoScreen());
                    },
                  ),
                ),
                Visibility(
                  visible: userType == UserType.Employee.name,
                  child: ListTile(
                    title: Row(
                      children: [
                        const Icon(Icons.store),
                        const SizedBox(width: 10),
                        Text(AppLocalizations.of(context)!.companies),
                      ],
                    ),
                    onTap: () {
                      Navigation.next(context, const AddedCompanies());
                    },
                  ),
                ),
                Visibility(
                  visible: userType == UserType.Participant.name,
                  child: ListTile(
                    title: Row(
                      children: [
                        const Icon(Icons.subject_rounded),
                        const SizedBox(width: 10),
                        Text(AppLocalizations.of(context)!.participantProfile),
                      ],
                    ),
                    onTap: () {
                      Navigation.next(context, const UserInfoScreen());
                    },
                  ),
                ),
                Visibility(
                  visible: userType == UserType.Company.name,
                  child: ListTile(
                    title: Row(
                      children: [
                        const Icon(Icons.subject_rounded),
                        const SizedBox(width: 10),
                        Text(AppLocalizations.of(context)!.companyProfile),
                      ],
                    ),
                    onTap: () {
                      Navigation.next(context, const CompanyInfoScreen());
                    },
                  ),
                ),
                Visibility(
                  visible: userType == UserType.CBO.name,
                  child: ListTile(
                    title: Row(
                      children: [
                        const Icon(Icons.subject_rounded),
                        const SizedBox(width: 10),
                        Text(AppLocalizations.of(context)!.cBOProfile),
                      ],
                    ),
                    onTap: () {
                      Navigation.next(context, const CompanyInfoScreen());
                    },
                  ),
                ),
                Visibility(
                  visible: userType == UserType.Trainer.name,
                  child: ListTile(
                    title: Row(
                      children: [
                        const Icon(Icons.subject_rounded),
                        const SizedBox(width: 10),
                        Text(AppLocalizations.of(context)!.trainerProfile),
                      ],
                    ),
                    onTap: () {
                      Navigation.next(context, const TrainerInfoScreen());
                    },
                  ),
                ),
                Visibility(
                  visible: userType == UserType.Trainer.name,
                  child: ListTile(
                    title: Row(
                      children: [
                        const Icon(Icons.contact_mail_outlined),
                        const SizedBox(width: 10),
                        Text(AppLocalizations.of(context)!.uploadCV),
                      ],
                    ),
                    onTap: () {
                      Navigation.next(context, const UploadCVScreen());
                    },
                  ),
                ),
                const Divider(thickness: 0.5),
                Visibility(
                  visible: user != UserType.Employee,
                  child: ListTile(
                    title: Row(
                      children: [
                        const Icon(Icons.store),
                        SizedBox(width: SizeConfig.width! * 0.05),
                        Text(AppLocalizations.of(context)!.aboutUs),
                      ],
                    ),
                    onTap: () {
                      Navigation.next(context, const AboutUs());
                    },
                  ),
                ),
                Visibility(
                  visible: user != UserType.Employee,
                  child: ListTile(
                    title: Row(
                      children: [
                        const Icon(Icons.contact_mail_outlined),
                        const SizedBox(width: 10),
                        Text(AppLocalizations.of(context)!.contactUs),
                      ],
                    ),
                    onTap: () {
                      Navigation.next(context, const ContactUs());
                    },
                  ),
                ),
                const Divider(thickness: 0.5),
                ListTile(
                  title: Row(
                    children: [
                      const Icon(Icons.language),
                      const SizedBox(width: 10),
                      Text(AppLocalizations.of(context)!.languages),
                    ],
                  ),
                  onTap: () {
                    showDialog(
                      context: context,
                      builder: (context) {
                        return const LanguagePopup();
                      },
                    );
                  },
                ),
                ListTile(
                  title: Row(
                    children: [
                      const Icon(Icons.logout),
                      const SizedBox(width: 10),
                      Text(AppLocalizations.of(context)!.logout),
                    ],
                  ),
                  onTap: () {
                    CacheHelper.remove();
                    Navigation.nextAndResetStack(context, const LoginScreen());
                  },
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  checkUserType() {
    if (UserType.Employee.name == userType) {
      userName = mainEmployee.fullName!;
      image = mainEmployee.image!;
    } else {
      userName = mainUser.userName!;
      image = mainUser.image!;
    }
    convertUnit6List(image);
  }

  convertUnit6List(String? image) {
    bytes = const Base64Decoder().convert(image!);
    setState(() {});
  }
}
