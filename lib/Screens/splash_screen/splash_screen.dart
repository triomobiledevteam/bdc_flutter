import 'dart:convert';

import 'package:bdc/Screens/LoginScreen/LoginScreen.dart';
import 'package:bdc/components/loading.dart';
import 'package:bdc/core/helper/checkConnection.dart';
import 'package:bdc/core/helper/error_handler/errorhandler.dart';
import 'package:bdc/core/helper/login.dart';
import 'package:bdc/core/modal/LoginModal/certificate.dart';
import 'package:bdc/core/modal/constraints/global_values.dart';
import 'package:bdc/core/repository/main_repository/main_repository.dart';
import 'package:bdc/core/storage/cache_helper.dart';
import 'package:bdc/core/storage/db_client.dart';
import 'package:bdc/core/theme/app_Theme.dart';
import 'package:bdc/core/utils/Navigation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import '../../core/theme/sizeConfig.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  final db = DatabaseClient.instance;
  int timeCounter = 1;

  @override
  void initState() {
    // TODO: implement initState
    CheckConnection().checkConnectivity2(getUerData, timer);
    super.initState();
  }

  timer() {
    // Future.delayed(Duration(seconds: timeCounter), () {
    if (userType.isEmpty) {
      Navigation.nextAndResetStack(context, const LoginScreen());
    } else {
      String data = CacheHelper.getData(key: CERTIFICATE) ?? '';
      if (data.isNotEmpty) {
        checkLocalUser(context);
      } else {
        Navigation.next(context, const LoginScreen());
      }
    }
    // });
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      body: Container(
        height: double.infinity,
        width: double.infinity,
        color: mainColorTheme,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Center(
              child: textLogo,
            ),
            Visibility(
                visible: timeCounter < 2,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Loading.loading(context, size: 30),
                    const SizedBox(
                      height: 10,
                    ),
                    Text(
                      AppLocalizations.of(context)!.loading + ' ... ',
                      style: Theme.of(context)
                          .textTheme
                          .bodyText1!
                          .copyWith(color: whiteColor),
                    )
                  ],
                ))
          ],
        ),
      ),
    );
  }

  getUerData() async {
    // bool isLookupsEmpty = await db.isLookupsEmpty();
    await GetMainRepository().getApplicationForm(context: context);
    // print(isLookupsEmpty);
    // if (isLookupsEmpty) {
    timeCounter = 1;
    setState(() {});
    await databaseClient.clearLookups();
    await GetMainRepository()
        .getLookUps(companyID: companyID, context: context);
    // }
    checkUser();
    timer();
  }

  checkUser() async {
    try {
      if (userType.isEmpty) {
        Navigation.nextAndResetStack(context, const LoginScreen());
      } else {
        String data = CacheHelper.getData(key: CERTIFICATE) ?? '';

        // EncryptData.encryptAES(CacheHelper.getData(key: CERTIFICATE) ?? '');

        if (data.isNotEmpty) {
          Certificate certificate = Certificate.fromMap(json.decode(data));
          await login(certificate, onError, context);
        } else {
          Navigation.nextAndResetStack(context, const LoginScreen());
        }
      }
    } catch (e) {
      Navigation.nextAndResetStack(context, const LoginScreen());
    }
  }

  onError(String massage) {
    onAppError(context, massage);
    Navigation.nextAndResetStack(context, const LoginScreen());
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }
}
