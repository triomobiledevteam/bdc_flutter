import 'dart:convert';
import 'dart:typed_data';

import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:bdc/Screens/Forms/ParticipantForm/UserInfoScreen.dart';
import 'package:bdc/Screens/LoginScreen/user_type_screen.dart';
import 'package:bdc/Screens/LoginScreen/widget/job_seeker_registration_form.dart';
import 'package:bdc/components/buttons/rounded_button.dart';
import 'package:bdc/core/modal/LoginModal/Participant.dart';
import 'package:bdc/core/modal/constraints/global_values.dart';
import 'package:bdc/core/modal/program/Program.dart';
import 'package:bdc/core/repository/particiapnt_repository/particiapnt_repository.dart';
import 'package:bdc/core/theme/app_Theme.dart';
import 'package:bdc/core/theme/sizeConfig.dart';
import 'package:bdc/core/utils/Navigation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:html/dom.dart' as dom;

import '../../../../webViewScreen.dart';

// import '../../../../webViewScreen.dart';

class ProgramDetailsScreen extends StatefulWidget {
  final Programs program;
  final bool viewMode;

  const ProgramDetailsScreen(
      {Key? key, required this.program, this.viewMode = false})
      : super(key: key);

  @override
  _ProgramDetailsScreenState createState() => _ProgramDetailsScreenState();
}

class _ProgramDetailsScreenState extends State<ProgramDetailsScreen> {
  Uint8List bytes = Uint8List(0);

  @override
  void initState() {
    convertUnit6List();
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: NestedScrollView(
        headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) =>
            [
          SliverAppBar(
            backgroundColor: kPrimaryColor,
            expandedHeight: MediaQuery.of(context).size.height * 0.3,
            floating: false,
            pinned: true,
            title: Visibility(
              visible: innerBoxIsScrolled,
              child: Text(widget.program.description!,
                  style: Theme.of(context)
                      .textTheme
                      .headline6!
                      .copyWith(color: whiteColor)),
            ),
            flexibleSpace: FlexibleSpaceBar(
              title: Text(
                widget.program.description!,
                style: Theme.of(context)
                    .textTheme
                    .headline6!
                    .copyWith(color: whiteColor),
              ),
              background: Stack(
                children: [
                  SizedBox(
                    width: SizeConfig.width,
                    height: SizeConfig.height,
                    child: bytes.isNotEmpty
                        ? Image.memory(
                            bytes,
                            fit: BoxFit.cover,
                          )
                        : Image.asset(
                            'assets/images/image.png',
                            fit: BoxFit.cover,
                          ),
                  ),
                  Container(
                    decoration: BoxDecoration(
                      gradient: LinearGradient(
                        colors: [
                          blackColor.withOpacity(0.8),
                          Colors.transparent,
                          Colors.transparent,
                          blackColor.withOpacity(0.8)
                        ],
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                        stops: const [0, 0.2, 0.8, 1],
                        // end: Alignment.topCenter,
                      ),
                    ),
                    width: double.infinity,
                    padding: const EdgeInsets.symmetric(
                      vertical: 10,
                    ),
                  ),
                ],
              ),
            ),
          )
        ],
        body: Stack(
          children: [
            Container(
              height: MediaQuery.of(context).size.height * 0.8,
              width: double.infinity,
              padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 10),
              // child: SingleChildScrollView(
              //   child: WebviewScaffold(
              //     url: Uri.dataFromString(
              //             '<html><body>hello world</body></html>',
              //             mimeType: 'text/html')
              //         .toString(),
              //   ),
              child: Html(
                  data: widget.program.projectBriefDescription,
                  onLinkTap: (String? url, RenderContext rContext,
                      Map<String, String> attributes, dom.Element? element) {
                    print(url);
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => WebViewClass(
                                  url: url,
                                )));
                  }),
            ),

            Visibility(
                visible: widget.viewMode,
                child: PositionedDirectional(
                  bottom: 0,
                  child: Container(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 20, vertical: 40),
                    width: MediaQuery.of(context).size.width,
                    color: mainBackgroundColor,
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Text(
                          AppLocalizations.of(context)!.signUpAs + ' :',
                          style: Theme.of(context).textTheme.bodyText1,
                        ),
                        const SizedBox(
                          height: 20,
                        ),
                        RoundedButton(
                          title: AppLocalizations.of(context)!.jobSeeker +
                              ' / ' +
                              AppLocalizations.of(context)!.entrepreneur,
                          onTap: () {
                            Navigation.next(
                                context,
                                JobSeekerRegistrationForm(
                                  projectID: widget.program.projectID!,
                                ));
                          },
                          decoration: BoxDecoration(
                              color: Colors.transparent,
                              borderRadius: BorderRadius.circular(10),
                              border: Border.all(color: blackColor)),
                          style: Theme.of(context)
                              .textTheme
                              .bodyText1!
                              .copyWith(color: blackColor, fontSize: 18),
                        ),
                        const SizedBox(
                          height: 20,
                        ),
                        RoundedButton(
                          title: AppLocalizations.of(context)!.others,
                          onTap: () {
                            double defaultRegisterSize =
                                SizeConfig.height! - (SizeConfig.height! * 0.1);
                            Navigation.next(
                                context,
                                UserTypeScreen(
                                  defaultLoginSize: defaultRegisterSize,
                                ));
                          },
                          decoration: BoxDecoration(
                            color: kPrimaryColorGreen,
                            borderRadius: BorderRadius.circular(10),
                          ),
                          style: Theme.of(context)
                              .textTheme
                              .bodyText1!
                              .copyWith(color: blackColor, fontSize: 18),
                        ),
                      ],
                    ),
                  ),
                )),
            if (!widget.viewMode)
              Visibility(
                visible: !mainParticipant!.projectList.any(
                    (element) => element.projectID == widget.program.projectID),
                child: PositionedDirectional(
                  bottom: 0,
                  end: 0,
                  child: InkWell(
                    onTap: () async {
                      await checkProgress();
                    },
                    child: Container(
                      height: SizeConfig.height! * 0.09,
                      width: SizeConfig.width! * 0.45,
                      decoration: BoxDecoration(
                        color: kPrimaryColorGreen.withOpacity(.9),
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(languageID == 1 ? 60 : 0),
                          topRight: Radius.circular(languageID == 1 ? 0 : 60),
                        ),
                      ),
                      child: Center(
                        child: Text(
                          AppLocalizations.of(context)!.enroll,
                          style: const TextStyle(
                              color: Colors.white,
                              fontSize: 20,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                    ),
                  ),
                ),
              )
            // : Container(),
          ],
        ),
      ),
    );
  }

  onError(String massage) {
    print(massage);
    AwesomeDialog(
      context: context,
      dialogType: DialogType.ERROR,
      animType: AnimType.BOTTOMSLIDE,
      title: '',
      desc: massage,
      btnOkText: AppLocalizations.of(context)!.confirm,
      btnCancelText: AppLocalizations.of(context)!.cancel,
      btnCancelOnPress: () {
        Navigator.pop(context);
      },
      btnOkOnPress: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (ctx) => const UserInfoScreen(),
          ),
        );
      },
    ).show();
  }

  onDone(String massage) {
    // Navigation.back(context);
    // ErrorHandler.onSuccessSnackBar(massage, context);
    // print('Done');
    print(massage);

    AwesomeDialog(
      context: context,
      dialogType: DialogType.SUCCES,
      animType: AnimType.BOTTOMSLIDE,
      title: '',
      desc: massage,
      btnCancelText: AppLocalizations.of(context)!.cancel,
      btnCancelOnPress: () {
        Navigator.pop(context);
      },
    ).show();
  }

  checkProgress() {
    if (mainParticipant!.percentageOfCompletion < 100) {
      AwesomeDialog(
        context: context,
        dialogType: DialogType.INFO,
        animType: AnimType.BOTTOMSLIDE,
        title: '',
        desc: AppLocalizations.of(context)!.enrollProcessMassage,
        btnCancelText: AppLocalizations.of(context)!.later,
        btnOkText: AppLocalizations.of(context)!.confirm,
        btnCancelOnPress: () {
          Navigation.back(context);
        },
        btnOkOnPress: () {
          Navigation.next(context, const UserInfoScreen());
        },
      ).show();
    } else {
      GetParticipantRepository()
          .participantProjectEnroll(widget.program.projectID!, onError, onDone);
    }
  }

  convertUnit6List() {
    bytes = const Base64Decoder().convert(widget.program.projectLogo!);
    setState(() {});
  }
}
