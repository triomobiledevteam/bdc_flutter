import 'dart:convert';
import 'dart:typed_data';

import 'package:bdc/Screens/Users/Participant/MainTabs/Programs/Program_Details.dart';
import 'package:bdc/core/modal/program/Program.dart';
import 'package:bdc/core/theme/sizeConfig.dart';
import 'package:flutter/material.dart';

class ProgramMiniCard extends StatefulWidget {
  const ProgramMiniCard({Key? key, required this.program}) : super(key: key);
  final Programs program;

  @override
  _ProgramMiniCardState createState() => _ProgramMiniCardState();
}

class _ProgramMiniCardState extends State<ProgramMiniCard> {
  Uint8List bytes = Uint8List(1);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (ctx) => ProgramDetailsScreen(
              program: widget.program,
              // isEnroll: widget.program.isEnrolled,
            ),
          ),
        );
      },
      child: Container(
        margin: const EdgeInsets.all(5),
        width: SizeConfig.width! * 0.36,
        decoration: BoxDecoration(
          image: DecorationImage(
            image: MemoryImage(bytes),
            fit: BoxFit.cover,
          ),
          borderRadius: BorderRadius.circular(20),
        ),
        child: Container(
          height: SizeConfig.height! * 0.4,
          width: SizeConfig.width,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20),
            gradient: LinearGradient(
              colors: [Colors.transparent, Colors.black.withOpacity(0.9)],
              begin: Alignment.topCenter,
              stops: const [0.5, 1],
              end: Alignment.bottomCenter,
            ),
          ),
          padding: EdgeInsets.symmetric(
              horizontal: SizeConfig.width! * 0.05,
              vertical: SizeConfig.height! * 0.01),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Spacer(),
              Text(
                widget.program.description!,
                maxLines: 2,
                overflow: TextOverflow.fade,
                style: const TextStyle(
                  color: Colors.white,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  convertUnit6List() {
    bytes = const Base64Decoder().convert(widget.program.projectLogo!);
    setState(() {});
  }
}
