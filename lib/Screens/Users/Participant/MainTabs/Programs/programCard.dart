import 'dart:convert';

import 'package:bdc/core/modal/program/Program.dart';
import 'package:flutter/material.dart';

import 'Program_Details.dart';

class ProgramCard extends StatefulWidget {
  const ProgramCard({Key? key, required this.program, this.viewMode = false})
      : super(key: key);
  final bool viewMode;
  final Programs program;

  @override
  _ProgramCardState createState() => _ProgramCardState();
}

class _ProgramCardState extends State<ProgramCard> {
  @override
  void initState() {
    // TODO: implement initState
    // convertUnit6List();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 10, horizontal: 16),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          border: Border.all(color: Colors.grey)),
      height: 160,
      child: GestureDetector(
        onTap: () {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (ctx) => ProgramDetailsScreen(
                program: widget.program,
                viewMode: widget.viewMode,
              ),
            ),
          );
        },
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Expanded(
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: const BorderRadius.only(
                    topLeft: Radius.circular(10),
                    topRight: Radius.circular(10),
                  ),
                  image: widget.program.projectLogo!.isNotEmpty
                      ? DecorationImage(
                          image: MemoryImage(const Base64Decoder()
                              .convert(widget.program.projectLogo!)),
                          fit: BoxFit.fill,
                        )
                      : const DecorationImage(
                          image: ExactAssetImage('assets/images/image.png'),
                          fit: BoxFit.fill,
                        ),
                ),
              ),
            ),
            Container(
              padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 5),
              decoration: const BoxDecoration(
                borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(10),
                  bottomRight: Radius.circular(10),
                ),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: [
                  const SizedBox(height: 3),
                  Flexible(
                    child: FittedBox(
                      child: Text(
                        widget.program.description!,
                        maxLines: 2,
                        overflow: TextOverflow.fade,
                        style: const TextStyle(
                          color: Colors.black,
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(height: 5),
                  // Text(
                  //   widget.program.projectBriefDescription!,
                  //   maxLines: 2,
                  //   overflow: TextOverflow.clip,
                  //   style: TextStyle(
                  //     color: Colors.black,
                  //   ),
                  // ),
                  // SizedBox(height: 5),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  // convertUnit6List() {
  //   Base64Decoder().convert(widget.program.projectLogo!);
  //   setState(() {});
  // }
}
