import 'package:bdc/Screens/LoginScreen/program_list_screen.dart';
import 'package:bdc/Screens/Users/Participant/MainTabs/Programs/programCard.dart';
import 'package:bdc/components/loading.dart';
import 'package:bdc/core/modal/LoginModal/Participant.dart';
import 'package:bdc/core/provider/programs_provider.dart';
import 'package:bdc/core/theme/app_Theme.dart';
import 'package:bdc/core/theme/sizeConfig.dart';
import 'package:bdc/core/utils/Navigation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:provider/provider.dart';

class ProgramsScreen extends StatefulWidget {
  const ProgramsScreen({Key? key}) : super(key: key);

  @override
  _ProgramsScreenState createState() => _ProgramsScreenState();
}

class _ProgramsScreenState extends State<ProgramsScreen> {
  int selected = 0;
  late ProgramsProvider programsProvider;

  @override
  void initState() {
    programsProvider = Provider.of<ProgramsProvider>(context, listen: false);
    programsProvider.init();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Container(
        width: SizeConfig.width,
        height: SizeConfig.height,
        decoration: const BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/bg.png"),
            fit: BoxFit.cover,
          ),
        ),
        child: RefreshIndicator(
          onRefresh: programsProvider.fetchProject,
          child: Column(
            children: [
              Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 5, vertical: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    InkWell(
                      onTap: () {
                        setState(() {
                          selected = 0;
                        });
                      },
                      child: Container(
                        height: SizeConfig.height! * 0.05,
                        width: SizeConfig.width! * 0.3,
                        decoration: BoxDecoration(
                            border: Border.all(
                                color: selected != 0
                                    ? kPrimaryColor
                                    : kPrimaryColorGreen),
                            color: selected != 0
                                ? Colors.transparent
                                : kPrimaryColorGreen,
                            borderRadius: BorderRadius.circular(10)),
                        child: Center(
                            child:
                                Text(AppLocalizations.of(context)!.portfolios)),
                      ),
                    ),
                    InkWell(
                      onTap: () {
                        setState(() {
                          selected = 1;
                        });
                      },
                      child: Container(
                        height: SizeConfig.height! * 0.05,
                        width: SizeConfig.width! * 0.3,
                        decoration: BoxDecoration(
                            border: Border.all(
                                color: selected != 1
                                    ? kPrimaryColor
                                    : kPrimaryColorGreen),
                            color: selected != 1
                                ? Colors.transparent
                                : kPrimaryColorGreen,
                            borderRadius: BorderRadius.circular(10)),
                        child: Center(
                            child:
                                Text(AppLocalizations.of(context)!.programs)),
                      ),
                    ),
                    InkWell(
                      onTap: () {
                        setState(() {
                          selected = 2;
                        });
                      },
                      child: Container(
                        height: SizeConfig.height! * 0.05,
                        width: SizeConfig.width! * 0.3,
                        decoration: BoxDecoration(
                            border: Border.all(
                                color: selected != 2
                                    ? kPrimaryColor
                                    : kPrimaryColorGreen),
                            color: selected != 2
                                ? Colors.transparent
                                : kPrimaryColorGreen,
                            borderRadius: BorderRadius.circular(10)),
                        child: Center(
                            child:
                                Text(AppLocalizations.of(context)!.myPrograms)),
                      ),
                    ),
                  ],
                ),
              ),
              Consumer<ProgramsProvider>(
                builder: (context, programsProvider, child) {
                  if (programsProvider.isLoading) {
                    return Loading.loading(context);
                  }

                  if (programsProvider.errorMessage.isNotEmpty) {
                    return Center(child: Text(programsProvider.errorMessage));
                  }

                  if (selected == 0) {
                    return Expanded(
                      child: ListView.separated(
                        itemCount: programsProvider
                            .projectPortfolios!.portfolios!.length,
                        itemBuilder: (context, index) {
                          return ListTile(
                            key: UniqueKey(),
                            onTap: () {
                              Navigation.next(
                                  context,
                                  ProgramListScreen(
                                    portfolios: programsProvider
                                        .projectPortfolios!.portfolios![index],
                                    mainAppBArTheme: true,
                                    viewMode: false,
                                  ));
                            },
                            title: Text(programsProvider.projectPortfolios!
                                .portfolios![index].description!),
                            trailing:
                                const Icon(Icons.arrow_forward_ios_rounded),
                          );
                        },
                        separatorBuilder: (BuildContext context, int index) =>
                            const Divider(),
                      ),
                    );
                  } else if (selected == 1) {
                    return Expanded(
                      child: ListView.builder(
                        shrinkWrap: true,
                        itemCount: programsProvider
                            .projectPortfolios!.programs!.length,
                        itemBuilder: (context, index) => ProgramCard(
                            program: programsProvider
                                .projectPortfolios!.programs![index]),
                      ),
                    );
                  } else {
                    return Expanded(
                      child: ListView.builder(
                        shrinkWrap: true,
                        itemCount: programsProvider.projectPortfolios!.programs!
                            .where((element) {
                          int index = mainParticipant!.projectList.indexWhere(
                              (project) =>
                                  project.projectID == element.projectID);
                          return index > -1;
                        }).length,
                        itemBuilder: (context, index) => ProgramCard(
                            program: programsProvider
                                .projectPortfolios!.programs!
                                .where((element) {
                          int index = mainParticipant!.projectList.indexWhere(
                              (project) =>
                                  project.projectID == element.projectID);
                          return index > -1;
                        }).toList()[index]),
                      ),
                    );
                  }
                },
              ),
            ],
          ),
        ));
  }
}
