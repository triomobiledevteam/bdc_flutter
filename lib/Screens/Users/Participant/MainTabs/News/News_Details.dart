import 'dart:convert';
import 'dart:typed_data';

import 'package:bdc/core/modal/News.dart';
import 'package:bdc/core/theme/app_Theme.dart';
import 'package:bdc/core/theme/sizeConfig.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:html/dom.dart' as dom;

import '../../../../webViewScreen.dart';

class NewsDetailsScreen extends StatefulWidget {
  final News? news;

  const NewsDetailsScreen({Key? key, this.news}) : super(key: key);

  @override
  _NewsDetailsScreenState createState() => _NewsDetailsScreenState();
}

class _NewsDetailsScreenState extends State<NewsDetailsScreen> {
  Uint8List bytes = Uint8List(0);

  @override
  void initState() {
    // TODO: implement initState
    convertUnit6List();
    super.initState();
  }

  convertUnit6List() {
    // List<int> list = widget.news.featuredImage.codeUnits;
    bytes = const Base64Decoder().convert(widget.news!.featuredImage!);
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: NestedScrollView(
            floatHeaderSlivers: true,
            headerSliverBuilder:
                (BuildContext context, bool innerBoxIsScrolled) {
              return [
                SliverAppBar(
                  backgroundColor: kPrimaryColor,
                  expandedHeight: SizeConfig.height! * 0.3,
                  // floating: true,
                  pinned: true,
                  flexibleSpace: FlexibleSpaceBar(
                    title: Text(
                      widget.news!.newsTitle!,
                      maxLines: 2,
                      style: TextStyle(
                          fontSize: SizeConfig.blockSizeVertical! * 2.5,
                          color: whiteColor),
                    ),
                    background: Stack(
                      children: [
                        SizedBox(
                          width: SizeConfig.width,
                          height: SizeConfig.height,
                          child: bytes.isNotEmpty
                              ? Image.memory(
                                  bytes,
                                  fit: BoxFit.fill,
                                )
                              : Image.asset(
                                  'assets/images/image.png',
                                  fit: BoxFit.fill,
                                ),
                        ),
                        Container(
                          decoration: BoxDecoration(
                            gradient: LinearGradient(
                              colors: [
                                blackColor.withOpacity(0.8),
                                Colors.transparent,
                                Colors.transparent,
                                blackColor.withOpacity(0.8)
                              ],
                              begin: Alignment.topCenter,
                              end: Alignment.bottomCenter,
                              stops: const [0, 0.2, 0.8, 1],
                            ),
                          ),
                          // width: SizeConfig.width,
                          padding: const EdgeInsets.only(top: 30, bottom: 10),
                        ),
                      ],
                    ),
                  ),
                )
              ];
            },
            body: Container(
              width: SizeConfig.width,
              padding:
                  EdgeInsets.symmetric(horizontal: SizeConfig.height! * 0.02),
              child: SingleChildScrollView(
                // child: WebviewScaffold(
                //   url: Uri.dataFromString('<html><body>hello world</body></html>',
                //           mimeType: 'text/html')
                //       .toString(),
                // ),
                child: Html(
                    data: widget.news!.newsBody,
                    onLinkTap: (String? url, RenderContext rContext,
                        Map<String, String> attributes, dom.Element? element) {
                      print(url);
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => WebViewClass(
                                    url: url,
                                  )));
                    }),
              ),
            )));
  }
}
