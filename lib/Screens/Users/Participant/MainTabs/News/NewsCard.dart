import 'dart:convert';
import 'dart:typed_data';

import 'package:bdc/Screens/Users/Participant/MainTabs/News/News_Details.dart';
import 'package:bdc/core/modal/News.dart';
import 'package:bdc/core/theme/sizeConfig.dart';
import 'package:flutter/material.dart';

class NewsCard extends StatefulWidget {
  const NewsCard({Key? key, this.news}) : super(key: key);
  final News? news;

  @override
  _NewsCardState createState() => _NewsCardState();
}

class _NewsCardState extends State<NewsCard> {
  Uint8List bytes = Uint8List(0);

  @override
  void initState() {
    // TODO: implement initState
    convertUnit6List();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (ctx) => NewsDetailsScreen(
              news: widget.news,
            ),
          ),
        );
      },
      child: Container(
        margin: const EdgeInsets.symmetric(vertical: 10),
        padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 5),
        child: ListTile(
          title: Text(
            widget.news!.newsName!,
            textAlign: TextAlign.start,
            maxLines: 2,
            style: Theme.of(context)
                .textTheme
                .bodyText1!
                .copyWith(fontWeight: FontWeight.bold),
          ),
          subtitle: Text(widget.news!.shortDescription!,
              style: Theme.of(context).textTheme.subtitle2),
          leading: bytes.isNotEmpty
              ? Image.memory(
                  const Base64Decoder().convert(widget.news!.featuredImage!),
                  width: SizeConfig.width! * 0.2,
                )
              : Image.asset(
                  'assets/images/image.png',
                  width: SizeConfig.width! * 0.2,
                ),
        ),
      ),
    );
  }

  convertUnit6List() {
    bytes = const Base64Decoder().convert(widget.news!.featuredImage!);
    setState(() {});
  }
}
