import 'dart:async';

import 'package:bdc/Screens/Users/Participant/MainTabs/News/NewsCard.dart';
import 'package:bdc/components/loading.dart';
import 'package:bdc/core/modal/News.dart';
import 'package:bdc/core/repository/main_repository/main_repository.dart';
import 'package:bdc/core/theme/sizeConfig.dart';
import 'package:flutter/material.dart';

class NewsScreen extends StatefulWidget {
  const NewsScreen({Key? key}) : super(key: key);

  @override
  _NewsScreenState createState() => _NewsScreenState();
}

class _NewsScreenState extends State<NewsScreen> {
  bool isLoading = false;
  String? errorMassage = '';

  Timer? timer;

  @override
  void initState() {
    if (newsList.isEmpty) load();
    // TODO: implement initState
    timer = Timer.periodic(const Duration(seconds: 15), (Timer t) => getNews());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if (isLoading) {
      return Center(
          child: Loading.loading(context,
              size: SizeConfig.blockSizeVertical! * 5));
    }
    if (errorMassage!.isEmpty) {
      return RefreshIndicator(
        onRefresh: load,
        child: ListView.separated(
          itemCount: newsList.length,
          addAutomaticKeepAlives: true,
          separatorBuilder: (context, index) => const Divider(thickness: 1),
          itemBuilder: (context, index) {
            return NewsCard(
              news: newsList[index],
            );
          },
        ),
      );
    }
    return Center(
      child: Text(errorMassage!),
    );
  }

  Future<void> getNews() async {
    newsList.clear();
    await GetMainRepository().getNews(context: context).then((value) {
      if (value is List<News>) {
        newsList.addAll(value);
      } else {
        errorMassage = value;
      }
    });
    setState(() {});
  }

  Future<void> load() async {
    isLoading = true;
    setState(() {});
    await getNews();
    isLoading = false;
    setState(() {});
  }

  @override
  void dispose() {
    // TODO: implement dispose
    timer?.cancel();
    super.dispose();
  }
}
