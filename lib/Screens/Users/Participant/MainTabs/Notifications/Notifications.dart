import 'dart:async';

import 'package:bdc/components/loading.dart';
import 'package:bdc/core/modal/BDCNotification.dart';
import 'package:bdc/core/repository/main_repository/main_repository.dart';
import 'package:bdc/core/theme/app_Theme.dart';
import 'package:bdc/core/theme/sizeConfig.dart';
import 'package:flutter/material.dart';

class Notifications extends StatefulWidget {
  const Notifications({Key? key}) : super(key: key);

  @override
  _NotificationsState createState() => _NotificationsState();
}

class _NotificationsState extends State<Notifications> {
  bool isLoading = false;
  String? errorMassage = '';

  Timer? timer;

  @override
  void initState() {
    // TODO: implement initState
    if (notificationList.isEmpty) {
      load();
    }
    timer = Timer.periodic(
        const Duration(seconds: 15), (Timer t) => fetchNotification());

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if (isLoading) {
      return Center(
          child: Loading.loading(context,
              size: SizeConfig.blockSizeVertical! * 5));
    }
    if (errorMassage!.isEmpty) {
      return RefreshIndicator(
        onRefresh: load,
        child: ListView.builder(
          itemCount: notificationList.length,
          itemBuilder: (context, index) {
            return Column(
              children: [
                Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 8, vertical: 5),
                  child: Container(
                    padding:
                        const EdgeInsets.symmetric(horizontal: 16, vertical: 5),
                    child: ListTile(
                      title: Text(
                        notificationList[index].description!,
                        textAlign: TextAlign.justify,
                        maxLines: 3,
                        style: TextStyle(color: blackColor),
                      ),
                      leading: Container(
                        width: 15,
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          color: kPrimaryColorGreen,
                        ),
                        // child: Text(
                        //     notificationList[index].description!.isNotEmpty
                        //         ? notificationList[index]
                        //             .description![0]
                        //             .toUpperCase()
                        //         : '#'),
                      ),
                    ),
                  ),
                ),
                const Divider(thickness: 1),
              ],
            );
          },
        ),
      );
    }
    return Center(
      child: Text(errorMassage!),
    );
  }

  Future<void> load() async {
    isLoading = true;
    setState(() {});
    await fetchNotification();
    isLoading = false;
    setState(() {});
  }

  Future<void> fetchNotification() async {
    notificationList.clear();
    await GetMainRepository().getNotifications(context: context).then((value) {
      if (value is List<BDCNotification>) {
        return notificationList = value;
      } else {
        errorMassage = value;
      }
    });
    setState(() {});
  }

  @override
  void dispose() {
    // TODO: implement dispose
    timer?.cancel();
    super.dispose();
  }
}
