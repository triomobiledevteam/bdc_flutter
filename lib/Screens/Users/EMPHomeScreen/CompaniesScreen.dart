import 'package:bdc/Screens/Forms/CompanyForm/CompanyInfoScreen.dart';
import 'package:bdc/core/modal/AplicationForm.dart';
import 'package:bdc/core/modal/LoginModal/Company.dart';
import 'package:bdc/core/modal/applicationFormTrans.dart';
import 'package:bdc/core/modal/constraints/global_values.dart';
import 'package:bdc/core/provider/CompanyProvider.dart';
import 'package:bdc/core/repository/company_repository/company_repository.dart';
import 'package:bdc/core/storage/db_client.dart';
import 'package:bdc/core/theme/app_Theme.dart';
import 'package:bdc/core/theme/sizeConfig.dart';
import 'package:bdc/core/utils/Navigation.dart';
import 'package:bdc/core/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:provider/provider.dart';
import 'package:square_percent_indicater/square_percent_indicater.dart';
import 'package:toggle_switch/toggle_switch.dart';

import '../../../components/loading.dart';
import '../../../core/helper/error_handler/errorhandler.dart';

class AddedCompanies extends StatefulWidget {
  const AddedCompanies({Key? key}) : super(key: key);

  @override
  _AddedCompaniesState createState() => _AddedCompaniesState();
}

class _AddedCompaniesState extends State<AddedCompanies> {
  final _db = DatabaseClient.instance;
  int initIndex = 0;
  List<Company> companyList = [];

  late CompanyProvider companyProvider;
  bool loading = false;
  @override
  void initState() {
    loading = true;
    companyProvider = Provider.of<CompanyProvider>(context, listen: false);
    fetchLocalData();
    fetchData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        await companyProvider.companyInit();
        return true;
      },
      child: Scaffold(
        appBar: AppBar(
          title: Text(AppLocalizations.of(context)!.addCompany),
        ),
        body: Column(
          mainAxisSize: MainAxisSize.max,
          children: [
            const SizedBox(height: 20),
            Center(
              child: ToggleSwitch(
                minWidth: 90.0,
                initialLabelIndex: initIndex,
                cornerRadius: 10.0,
                activeFgColor: Colors.white,
                inactiveBgColor: Colors.grey,
                inactiveFgColor: Colors.white,
                labels: [
                  AppLocalizations.of(context)!.sysNew,
                  AppLocalizations.of(context)!.all
                ],
                icons: const [Icons.new_releases, Icons.done_all_sharp],
                totalSwitches: 2,
                activeBgColors: const [
                  [Color(0xff3b5998), Color(0xff8b9dc3)],
                  [Color(0xff00aeff), Color(0xff0077f2)]
                ],
                onToggle: (index) {
                  setState(() {
                    if (index == 0) {
                      initIndex = 0;
                    } else {
                      initIndex = 1;
                    }
                  });
                },
              ),
            ),
            const SizedBox(height: 20),
            initIndex == 0
                ? Expanded(
                    child: RefreshIndicator(
                      onRefresh: () async {
                        await fetchLocalData();
                      },
                      child: SizedBox(
                        width: SizeConfig.width,
                        child: ListView.separated(
                          shrinkWrap: true,
                          separatorBuilder: (context, index) =>
                              const Divider(thickness: 2),
                          itemCount: companyList.length,
                          itemBuilder: (ctx, index) => InkWell(
                            key: UniqueKey(),
                            onTap: () async {
                              await companyProvider
                                  .getCompanyData(companyList[index].iD);

                              Navigation.next(
                                  context, const CompanyInfoScreen());
                              // companyProvider.companyInit();
                            },
                            child: Column(
                              children: [
                                Padding(
                                  padding:
                                      EdgeInsets.all(SizeConfig.width! * 0.05),
                                  child: Row(
                                    children: [
                                      SizedBox(
                                        width: SizeConfig.width! * 0.6,
                                        child: Expanded(
                                          flex: 4,
                                          child: Text(
                                            getCompanyDescription(
                                                companyList[index], languageID),
                                            style: const TextStyle(
                                                fontSize: 14,
                                                color: Colors.black,
                                                fontWeight: FontWeight.bold),
                                          ),
                                        ),
                                      ),
                                      const Spacer(),
                                      SquarePercentIndicator(
                                        width: SizeConfig.width! * 0.1,
                                        height: SizeConfig.width! * 0.1,
                                        startAngle: StartAngle.topRight,
                                        reverse: true,
                                        borderRadius: 12,
                                        shadowWidth: 4,
                                        progressWidth: 4,
                                        shadowColor: Colors.grey.shade300,
                                        progressColor: kPrimaryColorGreen,
                                        progress: companyList[index]
                                                .percentageOfCompletion /
                                            100,
                                        child: Center(
                                            child: Text(
                                          "${companyList[index].percentageOfCompletion}%",
                                          style: TextStyle(
                                              fontSize: SizeConfig
                                                      .blockSizeVertical! *
                                                  1.3),
                                        )),
                                      ),
                                      SizedBox(width: SizeConfig.width! * 0.05),
                                      Icon(
                                        Icons.upload_outlined,
                                        size: SizeConfig.width! * 0.07,
                                        color: Colors.red,
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                  )
                : loading
                    ? Expanded(child: Loading.loading(context, size: 50))
                    : Expanded(
                        child: RefreshIndicator(
                          onRefresh: () async {
                            setState(() {
                              loading = true;
                            });

                            await GetCompanyRepository()
                                .getCompanies(context)
                                .then((value) => setState(() {
                                      loading = false;
                                    }))
                                .catchError((onError) {
                              setState(() {
                                loading = false;
                              });
                            });
                          },
                          child: SizedBox(
                            width: SizeConfig.width,
                            child: ListView.separated(
                              itemCount: mainCompanyList.length,
                              separatorBuilder: (context, index) =>
                                  const Divider(thickness: 2),
                              itemBuilder: (ctx, index) => InkWell(
                                key: UniqueKey(),
                                onTap: () async {
                                  await getCompany(mainCompanyList[index]);

                                  // companyProvider.companyInit();
                                },
                                child: Column(
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 16, vertical: 10),
                                      child: Row(
                                        children: [
                                          Expanded(
                                            flex: 3,
                                            child: Text(
                                              getCompanyDescription(
                                                  mainCompanyList[index],
                                                  languageID),
                                              style: TextStyle(
                                                  fontSize: 14,
                                                  color: blackColor,
                                                  fontWeight: FontWeight.bold),
                                            ),
                                          ),
                                          const Spacer(),
                                          SquarePercentIndicator(
                                            width: SizeConfig.width! * 0.1,
                                            height: SizeConfig.width! * 0.1,
                                            startAngle: StartAngle.topRight,
                                            reverse: true,
                                            borderRadius: 12,
                                            shadowWidth: 4,
                                            progressWidth: 4,
                                            shadowColor: Colors.grey.shade300,
                                            progressColor: kPrimaryColorGreen,
                                            progress: mainCompanyList[index]
                                                    .percentageOfCompletion /
                                                100,
                                            child: Center(
                                                child: Text(
                                              "${mainCompanyList[index].percentageOfCompletion}%",
                                              style:
                                                  const TextStyle(fontSize: 12),
                                            )),
                                          ),
                                          const SizedBox(width: 20),
                                          Icon(
                                              mainCompanyList[index]
                                                      .code!
                                                      .isNotEmpty
                                                  ? Icons.done_all_sharp
                                                  : Icons.upload_outlined,
                                              color: mainCompanyList[index]
                                                      .code!
                                                      .isNotEmpty
                                                  ? kPrimaryColorGreen
                                                  : Colors.red,
                                              size: SizeConfig.width! * 0.07),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ),
                      )
          ],
        ),
      ),
    );
  }

  fetchLocalData() async {
    companyList.clear();
    companyList.addAll(await _db.getAllCompany());
    setState(() {});
  }

  fetchData() async {
    if (mainCompanyList.isEmpty) {
      await GetCompanyRepository().getCompanies(context);
    }

    setState(() {
      loading = false;
    });
  }

  getCompany(Company company) async {
    try {
      progressDialogue(context);

      await GetCompanyRepository()
          .getCompanyJO(context, company.trioID!)
          .then((value) => company.jobOpportunityList = value);
      await GetCompanyRepository()
          .getCompanyContact(context, company.trioID!)
          .then((value) => company.contactList = value);
      await GetCompanyRepository()
          .getCompanyBranch(context, company.trioID!)
          .then((value) => company.branchList = value);
      await GetCompanyRepository()
          .getCompanyEmployeeForm(company.trioID!)
          .then((value) async {
        List<ApplicationForm> applicationFormList = [];

        applicationFormList.addAll(
            await _db.getApplicationForm(ApplicationFormType.institution));

        for (var application in applicationFormList) {
          ApplicationFormTrans trans = ApplicationFormTrans();
          trans = value
              .where((element) => element.applicationFromID == application.iD)
              .first;
          application.yes = trans.yes;
          application.no = trans.no;
        }
        company.applicationFormList = [...applicationFormList];
      });

      await companyProvider.setCompany(company);

      Navigation.back(context);

      Navigation.next(context, const CompanyInfoScreen());
    } catch (e) {
      print(e.toString());
      Navigation.back(context);
      ErrorHandler.errorMassage = e.toString();
      ErrorHandler.showErrorMassageDialog(context);
    }
  }
}
