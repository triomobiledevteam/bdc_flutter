import 'package:bdc/Screens/Forms/ParticipantForm/UserInfoScreen.dart';
import 'package:bdc/core/helper/error_handler/errorhandler.dart';
import 'package:bdc/core/modal/AplicationForm.dart';
import 'package:bdc/core/modal/LoginModal/Participant.dart';
import 'package:bdc/core/modal/applicationFormTrans.dart';
import 'package:bdc/core/modal/constraints/global_values.dart';
import 'package:bdc/core/provider/ParticipantProvider.dart';
import 'package:bdc/core/repository/particiapnt_repository/particiapnt_repository.dart';
import 'package:bdc/core/storage/db_client.dart';
import 'package:bdc/core/theme/app_Theme.dart';
import 'package:bdc/core/theme/sizeConfig.dart';
import 'package:bdc/core/utils/Navigation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:provider/provider.dart';
import 'package:square_percent_indicater/square_percent_indicater.dart';
import 'package:toggle_switch/toggle_switch.dart';

import '../../../components/loading.dart';
import '../../../core/utils/utils.dart';

class ParticipantsInfoScreen extends StatefulWidget {
  const ParticipantsInfoScreen({Key? key}) : super(key: key);

  @override
  _ParticipantsInfoScreenState createState() => _ParticipantsInfoScreenState();
}

class _ParticipantsInfoScreenState extends State<ParticipantsInfoScreen> {
  int initIndex = 0;
  List<Participant> participantList = [];
  final _db = DatabaseClient.instance;
  late ParticipantProvider participantProvider;
  bool loading = false;
  @override
  void initState() {
    loading = true;
    participantProvider =
        Provider.of<ParticipantProvider>(context, listen: false);
    fetchLocalData();
    fetchData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        // You can do some work here.
        // Returning true allows the pop to happen, returning false prevents it.
        await participantProvider.participantInit();
        return true;
      },
      child: Scaffold(
        appBar: AppBar(
          title: const Text('Added users'),
        ),
        body: Column(
          children: [
            const SizedBox(height: 20),
            Center(
              child: ToggleSwitch(
                minWidth: 90.0,
                initialLabelIndex: initIndex,
                cornerRadius: 10.0,
                activeFgColor: Colors.white,
                inactiveBgColor: Colors.grey,
                inactiveFgColor: Colors.white,
                labels: [
                  AppLocalizations.of(context)!.sysNew,
                  AppLocalizations.of(context)!.all
                ],
                icons: const [Icons.new_releases, Icons.done_all_sharp],
                totalSwitches: 2,
                activeBgColors: const [
                  [Color(0xff3b5998), Color(0xff8b9dc3)],
                  [Color(0xff00aeff), Color(0xff0077f2)],
                  [
                    Color(0xfffeda75),
                    Color(0xfffa7e1e),
                    Color(0xffd62976),
                    Color(0xff962fbf),
                    Color(0xff4f5bd5)
                  ]
                ],
                onToggle: (index) {
                  setState(() {
                    if (index == 0) {
                      initIndex = 0;
                      // userName = 'Trio account';
                    } else {
                      initIndex = 1;
                      // userName = 'Mobile';
                    }
                  });
                },
              ),
            ),
            const SizedBox(height: 20),
            Builder(builder: (context) {
              if (initIndex == 0) {
                return Expanded(
                  child: RefreshIndicator(
                    onRefresh: () async {
                      await fetchLocalData();
                    },
                    child: SizedBox(
                      width: SizeConfig.width,
                      child: ListView.separated(
                        shrinkWrap: true,
                        itemCount: participantList.length,
                        separatorBuilder: (context, index) =>
                            const Divider(thickness: 2),
                        itemBuilder: (ctx, index) => InkWell(
                          key: UniqueKey(),
                          onTap: () async {
                            await participantProvider
                                .getParticipantData(participantList[index].iD);
                            Navigation.next(context, const UserInfoScreen());

                            // Navigator.push(
                            //     context,
                            //     MaterialPageRoute(
                            //         builder: (context) => UserInfoScreen()));
                          },
                          child: Column(
                            children: [
                              Padding(
                                padding:
                                    EdgeInsets.all(SizeConfig.width! * 0.05),
                                child: Row(
                                  children: [
                                    Flexible(
                                      child: Text(
                                        participantList[index].firstNameEn! +
                                            ' ' +
                                            participantList[index]
                                                .familyNameEn!,
                                        style: TextStyle(
                                            fontSize:
                                                SizeConfig.blockSizeVertical! *
                                                    2.5,
                                            color: blackColor,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ),
                                    const Spacer(),
                                    SquarePercentIndicator(
                                      width: SizeConfig.width! * 0.1,
                                      height: SizeConfig.width! * 0.1,
                                      startAngle: StartAngle.topRight,
                                      reverse: true,
                                      borderRadius: 12,
                                      shadowWidth: 4,
                                      progressWidth: 4,
                                      shadowColor: Colors.grey.shade300,
                                      progressColor: kPrimaryColorGreen,
                                      progress: participantList[index]
                                              .percentageOfCompletion /
                                          100,
                                      child: Center(
                                          child: Text(
                                        "${participantList[index].percentageOfCompletion}%",
                                        style: TextStyle(
                                            fontSize:
                                                SizeConfig.blockSizeVertical! *
                                                    1.5),
                                      )),
                                    ),
                                    SizedBox(width: SizeConfig.width! * 0.05),
                                    Icon(
                                      Icons.upload_outlined,
                                      size: SizeConfig.width! * 0.07,
                                      color: Colors.red,
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                );
              } else {
                if (loading) {
                  return Expanded(child: Loading.loading(context, size: 40));
                }
                return Expanded(
                  child: RefreshIndicator(
                    onRefresh: () async {
                      setState(() {
                        loading = true;
                      });

                      await GetParticipantRepository().getParticipants(context);
                      setState(() {
                        setState(() {
                          loading = false;
                        });
                      });
                    },
                    child: SizedBox(
                      width: SizeConfig.width,
                      child: ListView.separated(
                        shrinkWrap: true,
                        itemCount: mainParticipantList.length,
                        separatorBuilder: (context, index) =>
                            const Divider(thickness: 2),
                        itemBuilder: (ctx, index) => InkWell(
                          key: UniqueKey(),
                          onTap: () async {
                            //TODO:
                            await getParticipant(mainParticipantList[index]);
                          },
                          child: Column(
                            children: [
                              Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 16, vertical: 5),
                                child: Row(
                                  children: [
                                    Expanded(
                                      flex: 4,
                                      child: Text(
                                        getParticipantDescription(
                                            mainParticipantList[index],
                                            languageID),
                                        style: const TextStyle(
                                            fontSize: 14,
                                            color: Colors.black,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ),
                                    const Spacer(),
                                    SquarePercentIndicator(
                                      width: SizeConfig.width! * 0.1,
                                      height: SizeConfig.width! * 0.1,
                                      startAngle: StartAngle.topRight,
                                      reverse: true,
                                      borderRadius: 12,
                                      shadowWidth: 4,
                                      progressWidth: 4,
                                      shadowColor: Colors.grey.shade300,
                                      progressColor: kPrimaryColorGreen,
                                      progress: mainParticipantList[index]
                                              .percentageOfCompletion /
                                          100,
                                      child: Center(
                                          child: Text(
                                        "${mainParticipantList[index].percentageOfCompletion}%",
                                        style: const TextStyle(fontSize: 14),
                                      )),
                                    ),
                                    const SizedBox(width: 20),
                                    Icon(
                                        mainParticipantList[index]
                                                .code!
                                                .isNotEmpty
                                            ? Icons.done_all_sharp
                                            : Icons.upload_outlined,
                                        color: mainParticipantList[index]
                                                .code!
                                                .isNotEmpty
                                            ? kPrimaryColorGreen
                                            : Colors.red,
                                        size: SizeConfig.width! * 0.07),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                );
              }
            })
          ],
        ),
      ),
    );
  }

  fetchLocalData() async {
    participantList.clear();
    participantList.addAll(await _db.getAllParticipant());
    participantList.reversed;
    setState(() {});
  }

  fetchData() async {
    if (mainParticipantList.isEmpty) {
      await GetParticipantRepository().getParticipants(context);
    }
    setState(() {
      setState(() {
        loading = false;
      });
    });
  }

  getParticipant(Participant participant) async {
    try {
      progressDialogue(context);

      await GetParticipantRepository()
          .getParticipantExperience(context, participant.trioID!)
          .then((value) => participant.experienceList = value);
      await GetParticipantRepository()
          .getParticipantFamilyInfo(context, participant.trioID!)
          .then((value) => participant.familyInfoList = value);
      await GetParticipantRepository()
          .getParticipantApplicationForm(participant.trioID!)
          .then((value) async {
        List<ApplicationForm> applicationFormList = [];

        applicationFormList.addAll(
            await _db.getApplicationForm(ApplicationFormType.participant));
        if (value.isNotEmpty) {
          for (var application in applicationFormList) {
            ApplicationFormTrans trans = ApplicationFormTrans();
            trans = value
                .where((element) => element.applicationFromID == application.iD)
                .first;
            application.notApplicable = trans.notApplicable;
            application.yes = trans.yes;
            application.no = trans.no;
          }
          participant.applicationFormList = [...applicationFormList];
        }
      });

      await participantProvider.setParticipant(participant);
      Navigation.back(context);

      Navigation.next(context, const UserInfoScreen());
    } catch (e) {
      print(e.toString());
      Navigation.back(context);
      ErrorHandler.errorMassage = e.toString();
      ErrorHandler.showErrorMassageDialog(context);
    }
  }
}
