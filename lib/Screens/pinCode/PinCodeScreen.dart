import 'dart:async';

import 'package:bdc/Screens/LoginScreen/widget//cancel_button.dart';
import 'package:bdc/components/buttons/rounded_button.dart';
import 'package:bdc/components/loading.dart';
import 'package:bdc/core/helper/error_handler/errorhandler.dart';
import 'package:bdc/core/modal/enums/accountType.dart';
import 'package:bdc/core/modal/otp_modal/sms_body.dart';
import 'package:bdc/core/repository/main_repository/otp_repository.dart';
import 'package:bdc/core/theme/app_Theme.dart';
import 'package:bdc/core/theme/sizeConfig.dart';
import 'package:bdc/core/utils/Navigation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:pin_code_text_field/pin_code_text_field.dart';

import '../../core/helper/login.dart';
import '../../core/modal/LoginModal/certificate.dart';
import '../../core/modal/registration_modal/registrationModal.dart';
import '../../core/repository/main_repository/main_repository.dart';

class PinCodeScreen extends StatefulWidget {
  const PinCodeScreen({Key? key, required this.registration}) : super(key: key);
  final Registration registration;

  @override
  _PinCodeScreenState createState() => _PinCodeScreenState();
}

class _PinCodeScreenState extends State<PinCodeScreen>
    with SingleTickerProviderStateMixin {
  bool isLogin = true;
  Animation<double>? containerSize;
  AnimationController? animationController;
  Duration animationDuration = const Duration(milliseconds: 270);
  SmsBody? smsBody;

  bool isTimerActive = false;
  @override
  void initState() {
    super.initState();
    // if (!resendCode) {
    //   GetOtpRepository().registrationCode(
    //       widget.registration.registrationModal.mobile, onError, onDone);
    // }
    // SystemChrome.setEnabledSystemUIMode(SystemUiMode.manual, overlays: []);

    animationController =
        AnimationController(vsync: this, duration: animationDuration);
  }

  late Timer timer;
  @override
  void dispose() {
    animationController!.dispose();

    super.dispose();
  }

  TextEditingController controller = TextEditingController(text: '');

  @override
  Widget build(BuildContext context) {
    double defaultRegisterSize = MediaQuery.of(context).size.height -
        (MediaQuery.of(context).size.height * 0.1);
    containerSize = Tween<double>(
            begin: MediaQuery.of(context).size.height * 0.1,
            end: defaultRegisterSize)
        .animate(CurvedAnimation(
            parent: animationController!, curve: Curves.linear));
    return Scaffold(
      body: SafeArea(
        child: Stack(
          children: [
            Positioned(
                top: 100,
                right: -50,
                child: Container(
                  width: 100,
                  height: 100,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(50),
                      color: kPrimaryColor.withOpacity(0.5)),
                )),
            Positioned(
                top: -50,
                left: -50,
                child: Container(
                  width: 200,
                  height: 200,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(100),
                      color: kPrimaryColorGreen.withOpacity(0.5)),
                )),
            CancelButton(
              isLogin: isLogin,
              animationDuration: animationDuration,
              height: SizeConfig.height,
              width: SizeConfig.width,
              animationController: animationController,
              tapEvent: () {
                animationController!.reverse();
                setState(() {
                  isLogin = !isLogin;
                });
              },
            ),
            Center(
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    const Text(
                      'Verification code',
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 24),
                    ),
                    const SizedBox(height: 40),
                    PinCodeTextField(
                      autofocus: true,
                      controller: controller,
                      highlight: true,
                      highlightColor: Colors.transparent,
                      pinBoxColor: Colors.transparent,
                      defaultBorderColor: Colors.transparent,
                      hasTextBorderColor: Colors.transparent,
                      highlightPinBoxColor: kPrimaryColorGreen.withOpacity(0.5),
                      maxLength: 4,
                      onTextChanged: (text) {
                        setState(() {
                          text = text;
                        });
                      },
                      onDone: (text) async {
                        if (smsBody!.code.compareTo(text) == 0) {
                          await GetMainRepository().registerUser(
                              widget.registration, onError, (value) async {
                            progressDialogue(context);
                            await login(
                                Certificate(
                                    email: widget
                                        .registration.registrationModal.mobile,
                                    password: widget.registration
                                        .registrationModal.password,
                                    accountType: AccountType.GenUsers.name),
                                (value) {
                              Navigation.back(context);
                              onError(value);
                            }, context);
                          });
                        } else {
                          onAppError(context,
                              AppLocalizations.of(context)!.wrongCodeTryAgain);
                        }
                      },
                      pinBoxWidth: 60,
                      pinBoxHeight: 64,
                      wrapAlignment: WrapAlignment.spaceAround,
                      pinTextStyle: const TextStyle(fontSize: 22.0),
                      highlightAnimationBeginColor: Colors.black,
                      keyboardType: TextInputType.emailAddress,
                    ),
                    const SizedBox(height: 20),
                    RoundedButton(
                        title: AppLocalizations.of(context)!.send,
                        isLoading: isTimerActive,
                        onTap: () async {
                          isTimerActive = !isTimerActive;
                          setState(() {});

                          startTimer();
                          GetOtpRepository().registrationCode(
                              widget.registration.registrationModal.mobile,
                              onError,
                              onDone);
                        }),
                    const SizedBox(
                      height: 20,
                    ),
                    RoundedButton(
                        title: AppLocalizations.of(context)!.verify,
                        onTap: () async {
                          if (smsBody!.code.compareTo(controller.text) == 0) {
                            await GetMainRepository().registerUser(
                                widget.registration, onError, (value) async {
                              progressDialogue(context);
                              await login(
                                  Certificate(
                                      email: widget.registration
                                          .registrationModal.mobile,
                                      password: widget.registration
                                          .registrationModal.password,
                                      accountType: AccountType.GenUsers.name),
                                  (value) {
                                Navigation.back(context);
                                onError(value);
                              }, context);
                            });
                          } else {
                            onAppError(
                                context,
                                AppLocalizations.of(context)!
                                    .wrongCodeTryAgain);
                          }
                        }),
                    const SizedBox(
                      height: 20,
                    ),
                    if (isTimerActive)
                      Center(
                        child: Text(
                          AppLocalizations.of(context)!.tryAfter +
                              " " +
                              _start.toString(),
                          style: Theme.of(context).textTheme.headline6,
                        ),
                      )
                  ],
                ),
              ),
            ),
            const PositionedDirectional(child: BackButton()),
          ],
        ),
      ),
    );
  }

  int _start = 30;

  void startTimer() {
    const oneSec = Duration(seconds: 1);
    if (_start == 0) {
      _start = 30;
    }

    timer = Timer.periodic(
      oneSec,
      (Timer timer) {
        if (_start == 0) {
          setState(() {
            isTimerActive = !isTimerActive;
            timer.cancel();
          });
        } else {
          setState(() {
            _start--;
          });
        }
      },
    );
  }

  onError(String massage) {
    // resendCode = !resendCode;
    setState(() {});
    ErrorHandler.errorMassage = massage;
    ErrorHandler.showErrorMassageDialog(context);
  }

  onDone(value) {
    smsBody = SmsBody.fromJson(value);
    GetOtpRepository().sendSmsRequest(smsBody!, onError, (value) async {});
  }
}
