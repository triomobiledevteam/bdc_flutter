import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:bdc/Screens/Forms/CompanyForm/CompanyInfoScreen.dart';
import 'package:bdc/Screens/Forms/ParticipantForm/UserInfoScreen.dart';
import 'package:bdc/Screens/Users/Participant/MainTabs/News/NewsScreen.dart';
import 'package:bdc/Screens/Users/Participant/MainTabs/Notifications/Notifications.dart';
import 'package:bdc/Screens/Users/Participant/MainTabs/Programs/ProgramsScreen.dart';
import 'package:bdc/Screens/drawer/Drawer.dart';
import 'package:bdc/core/modal/LoginModal/Company.dart';
import 'package:bdc/core/modal/LoginModal/Participant.dart';
import 'package:bdc/core/modal/LoginModal/Trainer.dart';
import 'package:bdc/core/modal/constraints/global_values.dart';
import 'package:bdc/core/modal/enums/UsersType.dart';
import 'package:bdc/core/provider/CompanyProvider.dart';
import 'package:bdc/core/provider/ParticipantProvider.dart';
import 'package:bdc/core/theme/app_Theme.dart';
import 'package:bdc/core/theme/sizeConfig.dart';
import 'package:bdc/core/utils/Navigation.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:provider/provider.dart';

import '../Forms/trainer_form/trainer_info_screen.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  late ParticipantProvider participantProvider;
  late CompanyProvider companyProvider;
  // late ParticipantProvider participantProvider;
  Map source = {ConnectivityResult.none: true};

  List<Widget> homeWidgets = [];

  List<Tab> homeTab = [];

  @override
  void initState() {
    // TODO: implement initState

    participantProvider =
        Provider.of<ParticipantProvider>(context, listen: false);
    participantProvider.participantInit();

    companyProvider = Provider.of<CompanyProvider>(context, listen: false);
    companyProvider.companyInit();
    Future.delayed(const Duration(seconds: 3), () {
      checkProgress();
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    tabsWidget();
    return Scaffold(
      resizeToAvoidBottomInset: false,
      drawer: const MainDrawer(),
      appBar: AppBar(
        elevation: 0,
      ),
      body: DefaultTabController(
          length: homeTab.length,
          child: Scaffold(
            appBar: AppBar(
              automaticallyImplyLeading: false,
              // backgroundColor: kPrimaryColor,
              title: TabBar(
                  indicatorSize: TabBarIndicatorSize.label,
                  labelColor: kPrimaryColorGreen,
                  unselectedLabelColor: Colors.white,
                  indicatorColor: kPrimaryColorGreen,
                  indicatorWeight: 5,
                  tabs: homeTab),
              elevation: 0,
            ),
            body: TabBarView(children: homeWidgets),
          )),
    );
  }

  tabsWidget() {
    if (UserType.Participant.name == userType) {
      homeTab = [
        Tab(
          child: Align(
            alignment: Alignment.center,
            child: Text(AppLocalizations.of(context)!.programs),
          ),
        ),
        Tab(
          child: Align(
            alignment: Alignment.center,
            child: Text(AppLocalizations.of(context)!.news),
          ),
        ),
        Tab(
          child: Align(
            alignment: Alignment.center,
            child: Text(AppLocalizations.of(context)!.notification),
          ),
        )
      ];

      homeWidgets = [
        const ProgramsScreen(),
        const NewsScreen(),
        const Notifications(),
      ];
    } else {
      homeWidgets = [
        const NewsScreen(),
        const Notifications(),
      ];

      homeTab = [
        Tab(
          child: Align(
            alignment: Alignment.center,
            child: Text(AppLocalizations.of(context)!.news),
          ),
        ),
        Tab(
          child: Align(
            alignment: Alignment.center,
            child: Text(AppLocalizations.of(context)!.notification),
          ),
        ),
      ];
    }
  }

  checkProgress() {
    if (UserType.Participant.name == userType) {
      if (mainParticipant!.percentageOfCompletion < 100) {
        AwesomeDialog(
          context: context,
          dialogType: DialogType.INFO,
          animType: AnimType.BOTTOMSLIDE,
          title: '',
          desc: AppLocalizations.of(context)!.enrollProcessMassage,
          btnCancelText: AppLocalizations.of(context)!.later,
          btnOkText: AppLocalizations.of(context)!.confirm,
          btnCancelOnPress: () {},
          btnOkOnPress: () {
            Navigation.next(context, const UserInfoScreen());
          },
        ).show();
      }
    } else if (UserType.CBO.name == userType ||
        UserType.Company.name == userType) {
      if (mainCompany!.percentageOfCompletion < 100) {
        AwesomeDialog(
          context: context,
          dialogType: DialogType.INFO,
          animType: AnimType.BOTTOMSLIDE,
          title: '',
          desc: AppLocalizations.of(context)!.companyProcessMassage,
          btnCancelText: AppLocalizations.of(context)!.later,
          btnOkText: AppLocalizations.of(context)!.confirm,
          btnCancelOnPress: () {},
          btnOkOnPress: () {
            Navigation.next(context, const CompanyInfoScreen());
          },
        ).show();
      }
    } else if (UserType.Trainer.name == userType) {
      if (mainTrainer!.percentageOfCompletion < 100) {
        AwesomeDialog(
          context: context,
          dialogType: DialogType.INFO,
          animType: AnimType.BOTTOMSLIDE,
          title: '',
          desc: AppLocalizations.of(context)!.companyProcessMassage,
          btnCancelText: AppLocalizations.of(context)!.later,
          btnOkText: AppLocalizations.of(context)!.confirm,
          btnCancelOnPress: () {},
          btnOkOnPress: () {
            Navigation.next(context, const TrainerInfoScreen());
          },
        ).show();
      }
    }
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }
}
