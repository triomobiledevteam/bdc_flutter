import 'package:bdc/Screens/Forms/CompanyForm.dart';
import 'package:bdc/Screens/Forms/UserInfoForm.dart';
import 'package:bdc/Screens/drawer/Drawer.dart';
import 'package:bdc/core/helper/data_management/post_data.dart';
import 'package:bdc/core/provider/CompanyProvider.dart';
import 'package:bdc/core/provider/ParticipantProvider.dart';
import 'package:bdc/core/theme/app_Theme.dart';
import 'package:bdc/core/theme/sizeConfig.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:provider/provider.dart';

class EmpHomeScreen extends StatefulWidget {
  const EmpHomeScreen({Key? key}) : super(key: key);

  @override
  _EmpHomeScreenState createState() => _EmpHomeScreenState();
}

class _EmpHomeScreenState extends State<EmpHomeScreen> {
  late ParticipantProvider participantProvider;
  late CompanyProvider companyProvider;
  @override
  void initState() {
    companyProvider = Provider.of<CompanyProvider>(context, listen: false);
    companyProvider.companyInit();
    participantProvider =
        Provider.of<ParticipantProvider>(context, listen: false);

    participantProvider.participantInit();
    // TODO: implement initState
    postData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Colors.orangeAccent,
      drawer: const MainDrawer(),
      appBar: AppBar(
        elevation: 0,
        // title: Text('Home'),
      ),
      body: DefaultTabController(
          length: 2,
          child: Scaffold(
            appBar: AppBar(
              automaticallyImplyLeading: false,
              // backgroundColor: kPrimaryColor,
              title: TabBar(
                  indicatorSize: TabBarIndicatorSize.label,
                  labelColor: kPrimaryColorGreen,
                  unselectedLabelColor: Colors.white,
                  indicatorColor: kPrimaryColorGreen,
                  indicatorWeight: 5,
                  tabs: [
                    Tab(
                      child: Align(
                        alignment: Alignment.center,
                        child:
                            Text(AppLocalizations.of(context)!.newParticipant),
                      ),
                    ),
                    Tab(
                      child: Align(
                        alignment: Alignment.center,
                        child: Text(AppLocalizations.of(context)!.newCompany),
                      ),
                    ),
                  ]),
              elevation: 0,
            ),
            body: TabBarView(
              children: [
                UserInfoForm(
                  setScreen: setScreen,
                ),
                NewCompanyInfoForm(
                  setScreen: setScreen,
                ),
              ],
            ),
          )),
    );
  }

  setScreen() {
    setState(() {});
  }

  postData() async {
    print('compute Begin');
    PostData post = PostData();
    await post.post();
  }
}
