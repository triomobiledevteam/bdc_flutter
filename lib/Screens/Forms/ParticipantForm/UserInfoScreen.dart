import 'package:bdc/components/buttons/bottom_button.dart';
import 'package:bdc/components/tabs/virtical_tabs.dart';
import 'package:bdc/core/helper/error_handler/errorhandler.dart';
import 'package:bdc/core/modal/LoginModal/Participant.dart';
import 'package:bdc/core/modal/constraints/global_values.dart';
import 'package:bdc/core/modal/enums/UsersType.dart';
import 'package:bdc/core/provider/ParticipantProvider.dart';
import 'package:bdc/core/theme/app_Theme.dart';
import 'package:bdc/core/theme/sizeConfig.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';
import 'package:provider/provider.dart';

import '../../../core/utils/Navigation.dart';
import '../TabList.dart';

class UserInfoScreen extends StatefulWidget {
  const UserInfoScreen({Key? key}) : super(key: key);

  @override
  _UserInfoScreenState createState() => _UserInfoScreenState();
}

class _UserInfoScreenState extends State<UserInfoScreen> {
  late ParticipantProvider participantProvider;

  @override
  void initState() {
    // TODO: implement initState
    participantProvider =
        Provider.of<ParticipantProvider>(context, listen: false);

    if (UserType.Participant.name == userType) {
      participantProvider.setParticipant(mainParticipant!);
    }
    super.initState();
  }

  int initialIndex = 0;

  final keyForm = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      resizeToAvoidBottomInset: false,
      bottomNavigationBar: BottomButton(
        text: AppLocalizations.of(context)!.save,
        color: kPrimaryColorGreen,
        onPressed: () async {
          if (keyForm.currentState!.validate()) {
            await participantProvider.checkParticipantData(onError, () {
              setState(() {});
            }, context,
                backFlag: true,
                submitLocally: userType == UserType.Employee.name);
          }
        },
      ),
      appBar: AppBar(
        backgroundColor: kPrimaryColor,
        title: Text(AppLocalizations.of(context)!.edit),
      ),
      body: Column(
        children: [
          Container(
            padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 5),
            child: Consumer<ParticipantProvider>(
              builder: (context, participantProvider, child) =>
                  LinearPercentIndicator(
                // width: MediaQuery.of(context).size.width,
                animation: true,
                lineHeight: SizeConfig.height! * 0.02,
                animationDuration: 300,
                percent:
                    participantProvider.participant.percentageOfCompletion /
                        100,
                center: Text(
                    "${participantProvider.participant.percentageOfCompletion}%"),
                barRadius: const Radius.circular(20),
                // linearStrokeCap: LinearStrokeCap.roundAll,
                progressColor: Colors.green,
              ),
            ),
          ),
          Expanded(
            child: Form(
              key: keyForm,
              child: Consumer<ParticipantProvider>(
                builder: (context, participantProvider, child) => SizedBox(
                  height: SizeConfig.height,
                  child: VerticalTabs(
                    tabsElevation: 10,
                    indicatorColor: kPrimaryColorGreen,
                    tabTextStyle: TextStyle(
                        fontSize: SizeConfig.blockSizeVertical! * 1.5),
                    tabsWidth: SizeConfig.width! * 0.12,
                    tabs: userFormTabs,
                    contents: userFormWidgets,
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  onError(String massage) {
    Navigation.back(context);
    ErrorHandler.errorMassage = massage;
    ErrorHandler.showErrorMassageDialog(context);
  }
}
