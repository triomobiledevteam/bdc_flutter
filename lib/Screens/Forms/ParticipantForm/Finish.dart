import 'package:bdc/Screens/HomeScreen/HomeScreen.dart';
import 'package:bdc/components/form_components/FormsbottomButton.dart';
import 'package:bdc/core/theme/app_Theme.dart';
import 'package:bdc/core/theme/sizeConfig.dart';
import 'package:flutter/material.dart';

class FinishUser extends StatefulWidget {
  const FinishUser({Key? key}) : super(key: key);

  @override
  _FinishUserState createState() => _FinishUserState();
}

class _FinishUserState extends State<FinishUser> {
  bool? check = false;

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Container(
      padding: EdgeInsets.all(SizeConfig.width! * 0.02),
      child: Column(
        children: [
          SizedBox(height: SizeConfig.height! * 0.05),
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Checkbox(
                  checkColor: Colors.white,
                  activeColor: kPrimaryColor,
                  value: check,
                  onChanged: (value) {
                    setState(() {
                      check = value;
                    });
                  }),
              SizedBox(
                width: SizeConfig.width! * 0.51,
                child: const Text(
                    'I take responsibility for the accuracy and correctness of all the information that has been filled out by me'),
              )
            ],
          ),
          SizedBox(height: SizeConfig.height! * 0.05),
          FormsBottomButton(
            text: 'Done',
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (ctx) => const HomeScreen(),
                ),
              );
            },
          )
          // GestureDetector(
          //   onTap: () {
          //     Navigator.push(
          //       context,
          //       MaterialPageRoute(
          //         builder: (ctx) => HomeScreen(),
          //       ),
          //     );
          //   },
          //   child:
          //   Container(
          //     padding: EdgeInsets.all(SizeConfig.width * 0.02),
          //     decoration: BoxDecoration(
          //         color: kPrimaryColorGreen,
          //         borderRadius: BorderRadius.circular(10)),
          //     child: Center(
          //       child: Text(
          //         'Done',
          //         style: TextStyle(
          //             color: Colors.white,
          //             fontSize: SizeConfig.blockSizeVertical * 2,
          //             fontWeight: FontWeight.bold),
          //       ),
          //     ),
          //   ),
          // ),
        ],
      ),
    );
  }
}
