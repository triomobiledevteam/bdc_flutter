import 'package:bdc/Screens/attachment_screen/attachment_screen.dart';
import 'package:bdc/components/form_components/StickyHeaderForm.dart';
import 'package:bdc/components/form_components/TextFormWithPadding.dart';
import 'package:bdc/core/modal/Lookups.dart';
import 'package:bdc/core/modal/constraints/LookUpTypes.dart';
import 'package:bdc/core/provider/ParticipantProvider.dart';
import 'package:bdc/core/utils/Navigation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

import '../../../components/dialog/map_dialog.dart';
import '../../../core/utils/string_validation_extension.dart';
import '../../attachment_screen/attachment/attachment_bloc.dart';
import '../../map_layout/bloc/map_layout_cubit.dart';

class MainUser extends StatefulWidget {
  const MainUser({Key? key}) : super(key: key);

  @override
  _MainUserState createState() => _MainUserState();
}

class _MainUserState extends State<MainUser> {
  late ParticipantProvider participantProvider;

  TextEditingController? fNameEnController;
  TextEditingController? sNameEnController;
  TextEditingController? thNameEnController;
  TextEditingController? familyNameEnController;
  TextEditingController? fNameArController;
  TextEditingController? sNameArController;
  TextEditingController? thNameArController;
  TextEditingController? familyNameArController;
  TextEditingController? nationalIDController;
  TextEditingController? securityNIDController;
  TextEditingController? nationalityController;
  TextEditingController? mobileController;
  TextEditingController? genderController;
  TextEditingController? aMobileController;
  TextEditingController? dateOBirthController;
  TextEditingController? faceNameController;
  TextEditingController? mStatusController;
  TextEditingController? emailController;
  TextEditingController? religionController;
  TextEditingController? addressController;
  late TextEditingController locationController;

  @override
  void initState() {
    participantProvider =
        Provider.of<ParticipantProvider>(context, listen: false);
    // participantProvider.clearMain = clear;
    // participantProvider.setState = () {
    //   setState(() {});
    // };
    declareValue();
    fNameEnController!.addListener(() {
      participantProvider.percentageOfCompletion();
      participantProvider.participant.firstNameEn = fNameEnController!.text;
    });
    sNameEnController!.addListener(() {
      participantProvider.percentageOfCompletion();
      participantProvider.participant.secondNameEn = sNameEnController!.text;
    });
    thNameEnController!.addListener(() {
      participantProvider.percentageOfCompletion();
      participantProvider.participant.thirdNameEn = thNameEnController!.text;
    });
    familyNameEnController!.addListener(() {
      participantProvider.percentageOfCompletion();
      participantProvider.participant.familyNameEn =
          familyNameEnController!.text;
    });
    fNameArController!.addListener(() {
      participantProvider.percentageOfCompletion();
      participantProvider.participant.firstNameAr = fNameArController!.text;
    });
    sNameArController!.addListener(() {
      participantProvider.percentageOfCompletion();
      participantProvider.participant.secondNameAr = sNameArController!.text;
    });
    thNameArController!.addListener(() {
      participantProvider.percentageOfCompletion();
      participantProvider.participant.thirdNameAr = thNameArController!.text;
    });
    familyNameArController!.addListener(() {
      participantProvider.percentageOfCompletion();
      participantProvider.participant.familyNameAr =
          familyNameArController!.text;
    });
    nationalIDController!.addListener(() {
      participantProvider.participant.nationalID = nationalIDController!.text;
    });
    securityNIDController!.addListener(() {
      participantProvider.participant.securityNumberID =
          securityNIDController!.text;
    });
    mobileController!.addListener(() {
      participantProvider.participant.mobile = mobileController!.text;
      participantProvider.percentageOfCompletion();
    });
    aMobileController!.addListener(() {
      participantProvider.participant.alternateMobile = aMobileController!.text;
    });

    faceNameController!.addListener(() {
      participantProvider.participant.facebookName = faceNameController!.text;
    });
    emailController!.addListener(() {
      participantProvider.participant.email = emailController!.text;
      participantProvider.percentageOfCompletion();
    });

    addressController!.addListener(() {
      participantProvider.participant.address = addressController!.text;
    });
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return StickyHeaderForm(
      title: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          FittedBox(
            child: Text(
              AppLocalizations.of(context)!.main,
              style: Theme.of(context).textTheme.headline6!.copyWith(
                    color: Colors.white,
                  ),
            ),
          ),
        ],
      ),
      body: Padding(
        padding:
            EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
        child: SingleChildScrollView(
          child: Container(
            margin: const EdgeInsets.symmetric(
              vertical: 5,
            ),
            child: Consumer<ParticipantProvider>(
              builder: (context, participant, child) {
                print("participant.errorFlag ${participant.errorFlag}");
                return Column(
                  children: [
                    TextFormWithPadding(
                      labelText: AppLocalizations.of(context)!.firstNameEn,
                      controller: fNameEnController,
                      errorText: "First Name can't be empty",
                      errorFlag: participant.errorFlag,
                    ),
                    TextFormWithPadding(
                      labelText: AppLocalizations.of(context)!.secondNameEn,
                      controller: sNameEnController,
                    ),
                    TextFormWithPadding(
                      labelText: AppLocalizations.of(context)!.thirdNameEn,
                      controller: thNameEnController,
                    ),
                    TextFormWithPadding(
                      labelText: AppLocalizations.of(context)!.familyNameEn,
                      errorText: "Family Name can't be empty",
                      controller: familyNameEnController,
                      errorFlag: participant.errorFlag,
                    ),
                    TextFormWithPadding(
                      labelText: AppLocalizations.of(context)!.firstNameAr,
                      controller: fNameArController,
                      errorText: "First Name can't be empty",
                      errorFlag: participant.errorFlag,
                    ),
                    TextFormWithPadding(
                      labelText: AppLocalizations.of(context)!.secondNameAr,
                      controller: sNameArController,
                    ),
                    TextFormWithPadding(
                      labelText: AppLocalizations.of(context)!.thirdNameAr,
                      controller: thNameArController,
                    ),
                    TextFormWithPadding(
                      labelText: AppLocalizations.of(context)!.familyNameAr,
                      controller: familyNameArController,
                      errorText: "Family Name can't be empty",
                      errorFlag: participant.errorFlag,
                    ),
                    TextFormWithPadding(
                      labelText: AppLocalizations.of(context)!.nationalityID,
                      controller: nationalityController,
                      errorText: "Nationality can't be empty",
                      errorFlag: participant.errorFlag,
                      getData: getNationality,
                      recordID: participant.participant.nationalityID,
                      readOnly: true,
                      isDefaultDialog: true,
                      dialogType: LookUpTypes.nationality,
                    ),
                    TextFormWithPadding(
                      labelText: AppLocalizations.of(context)!.nationalID,
                      keyboardType: TextInputType.number,
                      controller: nationalIDController,
                      errorFlag: participant.errorFlag,
                      validator: (value) {
                        if (value!.isNotEmpty &&
                            securityNIDController!.text.isNotEmpty) {
                          return AppLocalizations.of(context)!
                              .pleaseEnterTheNationalNumberOrTheSecurityNumberAndNotBoth;
                        }
                        if (value.isEmpty &&
                            securityNIDController!.text.isEmpty) {
                          return AppLocalizations.of(context)!
                              .pleaseEnterYourNationalNumber;
                        }
                        return null;
                      },
                      suffixIcon: IconButton(
                        icon: const Icon(Icons.image),
                        onPressed: () {
                          BlocProvider.of<AttachmentBloc>(context)
                              .add(EmptyAttachmentEvent());

                          Navigation.next(
                              context,
                              AttachmentScreen(
                                participantID:
                                    " ${participant.participant.trioID ?? 0}",
                                participantLocalID:
                                    " ${participant.participant.iD ?? 0}",
                              ));
                        },
                      ),
                    ),
                    TextFormWithPadding(
                      labelText: AppLocalizations.of(context)!.securityNumberID,
                      controller: securityNIDController,
                      errorFlag: participant.errorFlag,
                      validator: (value) {
                        if (value!.isNotEmpty &&
                            nationalIDController!.text.isNotEmpty) {
                          return AppLocalizations.of(context)!
                              .pleaseEnterTheNationalNumberOrTheSecurityNumberAndNotBoth;
                        }
                        if (value.isEmpty &&
                            nationalIDController!.text.isEmpty) {
                          return AppLocalizations.of(context)!
                              .pleaseEnterYourSecurityNumberForNonJordanian;
                        }
                        return null;
                      },
                      suffixIcon: IconButton(
                        icon: const Icon(Icons.image),
                        onPressed: () {
                          BlocProvider.of<AttachmentBloc>(context)
                              .add(EmptyAttachmentEvent());

                          Navigation.next(
                              context,
                              AttachmentScreen(
                                participantID:
                                    " ${participant.participant.trioID ?? 0}",
                                participantLocalID:
                                    " ${participant.participant.iD ?? 0}",
                              ));
                        },
                      ),
                    ),

                    TextFormWithPadding(
                      labelText: AppLocalizations.of(context)!.mobile,
                      controller: mobileController,
                      validator: (value) {
                        if (!isValidMobile(value ?? '')) {
                          return AppLocalizations.of(context)!.invalidEmail;
                        }
                        return null;
                      },
                      errorText: AppLocalizations.of(context)!.invalidMobile,
                      keyboardType: TextInputType.phone,
                      errorFlag: participant.errorFlag,
                    ),
                    //TODO:Dialog
                    TextFormWithPadding(
                      labelText: AppLocalizations.of(context)!.genderID,
                      controller: genderController,
                      readOnly: true,
                      recordID: participant.participant.genderID.toString(),
                      getData: getGender,
                      isDefaultDialog: true,
                      dialogType: LookUpTypes.gender,
                    ),
                    TextFormWithPadding(
                      labelText: AppLocalizations.of(context)!.alternateMobile,
                      controller: aMobileController,
                      keyboardType: TextInputType.phone,
                    ),
                    TextFormWithPadding(
                      controller: dateOBirthController,
                      labelText: AppLocalizations.of(context)!.birthDate,
                      isDateDialog: true,
                      getDate: getDOB,
                      readOnly: true,
                    ),
                    TextFormWithPadding(
                      labelText: AppLocalizations.of(context)!.facebookName,
                      controller: faceNameController,
                    ),
                    //TODO:Dialog
                    TextFormWithPadding(
                      labelText: AppLocalizations.of(context)!.maritalStatusID,
                      controller: mStatusController,
                      getData: getMStatus,
                      recordID:
                          participant.participant.maritalStatusID.toString(),
                      readOnly: true,
                      isDefaultDialog: true,
                      dialogType: LookUpTypes.marital,
                    ),
                    TextFormWithPadding(
                      labelText: AppLocalizations.of(context)!.email,
                      controller: emailController,
                      keyboardType: TextInputType.emailAddress,
                      errorText: AppLocalizations.of(context)!.invalidEmail,
                      errorFlag: participant.errorFlag,
                      validator: (value) {
                        if (!isValidEmail(value ?? '')) {
                          return AppLocalizations.of(context)!.invalidEmail;
                        }
                        return null;
                      },
                    ),
                    TextFormWithPadding(
                      labelText: AppLocalizations.of(context)!.religionID,
                      controller: religionController,
                      getData: getReligion,
                      recordID: participant.participant.religionID.toString(),
                      readOnly: true,
                      isDefaultDialog: true,
                      dialogType: LookUpTypes.religion,
                    ),
                    TextFormWithPadding(
                      labelText: AppLocalizations.of(context)!.address,
                      controller: addressController,
                      keyboardType: TextInputType.streetAddress,
                      suffixIcon: IconButton(
                        icon: const Icon(Icons.pin_drop_rounded),
                        onPressed: () async {
                          await MapLayoutCubit.get(context)
                              .getCurrentLocation();
                          MapDialog.show(context, (value) {
                            setState(() {
                              addressController =
                                  TextEditingController(text: value);
                              participant.participant.address =
                                  addressController!.text;
                            });
                          });
                        },
                      ),
                    ),
                  ],
                );
              },
            ),
          ),
        ),
      ),
    );
  }

  void getMStatus(LookUps lookUp) {
    participantProvider.participant.maritalStatusID = int.parse(lookUp.iD!);
    participantProvider.percentageOfCompletion();
  }

  getReligion(LookUps lookUp) {
    participantProvider.participant.religionID = int.parse(lookUp.iD!);
  }

  getGender(LookUps lookUp) {
    participantProvider.participant.genderID = int.parse(lookUp.iD!);
    participantProvider.percentageOfCompletion();
  }

  getNationality(LookUps lookUp) {
    print('lookUp ${lookUp.iD}');
    participantProvider.participant.nationalityID = lookUp.iD;

    participantProvider.percentageOfCompletion();
  }

  getDOB(String date) {
    participantProvider.participant.birthDate = date;
    print('date $date');
  }

  declareValue() {
    fNameEnController = TextEditingController(
        text: participantProvider.participant.firstNameEn);
    sNameEnController = TextEditingController(
        text: participantProvider.participant.secondNameEn);
    thNameEnController = TextEditingController(
        text: participantProvider.participant.thirdNameEn);
    familyNameEnController = TextEditingController(
        text: participantProvider.participant.familyNameEn);
    fNameArController = TextEditingController(
        text: participantProvider.participant.firstNameAr);
    sNameArController = TextEditingController(
        text: participantProvider.participant.secondNameAr);
    thNameArController = TextEditingController(
        text: participantProvider.participant.thirdNameAr);
    familyNameArController = TextEditingController(
        text: participantProvider.participant.familyNameAr);
    nationalIDController = TextEditingController(
        text: '${participantProvider.participant.nationalID}');
    securityNIDController = TextEditingController(
        text: '${participantProvider.participant.securityNumberID}');
    nationalityController = TextEditingController(text: '');
    mobileController =
        TextEditingController(text: participantProvider.participant.mobile);
    genderController = TextEditingController(text: '');
    aMobileController = TextEditingController(
        text: participantProvider.participant.alternateMobile);
    dateOBirthController = TextEditingController(
        text: participantProvider.participant.birthDate!.isNotEmpty
            ? DateFormat.yMMMd().format(
                DateTime.parse(participantProvider.participant.birthDate!))
            : '');
    faceNameController = TextEditingController(
        text: participantProvider.participant.facebookName);
    mStatusController = TextEditingController(text: '');
    emailController =
        TextEditingController(text: participantProvider.participant.email);
    religionController = TextEditingController(text: '');
    addressController =
        TextEditingController(text: participantProvider.participant.address);
    locationController =
        TextEditingController(text: participantProvider.participant.location);
  }

  clear() {
    fNameEnController!.clear();
    sNameEnController!.clear();
    thNameEnController!.clear();
    familyNameEnController!.clear();
    fNameArController!.clear();
    sNameArController!.clear();
    thNameArController!.clear();
    familyNameArController!.clear();
    nationalIDController!.clear();
    securityNIDController!.clear();
    nationalityController!.clear();
    mobileController!.clear();
    genderController!.clear();
    aMobileController!.clear();
    dateOBirthController!.clear();
    faceNameController!.clear();
    mStatusController!.clear();
    emailController!.clear();
    religionController!.clear();
    addressController!.clear();
    locationController.clear();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    fNameEnController!.dispose();
    sNameEnController!.dispose();
    thNameEnController!.dispose();
    familyNameEnController!.dispose();
    fNameArController!.dispose();
    sNameArController!.dispose();
    thNameArController!.dispose();
    familyNameArController!.dispose();
    nationalIDController!.dispose();
    securityNIDController!.dispose();
    nationalityController!.dispose();
    mobileController!.dispose();
    genderController!.dispose();
    aMobileController!.dispose();
    dateOBirthController!.dispose();
    faceNameController!.dispose();
    mStatusController!.dispose();
    emailController!.dispose();
    religionController!.dispose();
    addressController!.dispose();
    locationController.dispose();
    super.dispose();
  }
}
