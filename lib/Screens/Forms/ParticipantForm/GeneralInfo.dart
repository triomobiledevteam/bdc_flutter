import 'package:bdc/components/form_components/StickyHeaderForm.dart';
import 'package:bdc/components/form_components/TextFormWithPadding.dart';
import 'package:bdc/core/modal/Lookups.dart';
import 'package:bdc/core/modal/constraints/LookUpTypes.dart';
import 'package:bdc/core/provider/ParticipantProvider.dart';
import 'package:bdc/core/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:provider/provider.dart';

class GeneralInfoUser extends StatefulWidget {
  const GeneralInfoUser({Key? key}) : super(key: key);

  @override
  _GeneralInfoUserState createState() => _GeneralInfoUserState();
}

class _GeneralInfoUserState extends State<GeneralInfoUser> {
  late ParticipantProvider participantProvider;

  TextEditingController? nOFMemberController;
  TextEditingController? countryController;
  TextEditingController? mPStatusController;
  TextEditingController? governorateController;
  TextEditingController? nOWFMController;

  @override
  void initState() {
    // TODO: implement initState
    participantProvider =
        Provider.of<ParticipantProvider>(context, listen: false);
    // participantProvider.clearGeneralInfo = clear;

    declareValue();
    nOWFMController!.addListener(() {
      participantProvider.participant.familyWorkMember =
          int.parse(nOWFMController!.text);
      participantProvider.percentageOfCompletion();
    });
    nOFMemberController!.addListener(() {
      participantProvider.participant.familyNumber =
          int.parse(nOFMemberController!.text);
      participantProvider.percentageOfCompletion();
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return StickyHeaderForm(
      title: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          FittedBox(
            child: Text(
              AppLocalizations.of(context)!.generalInfo,
              style: Theme.of(context).textTheme.headline6!.copyWith(
                    color: Colors.white,
                  ),
            ),
          ),
        ],
      ),
      body: Padding(
        padding:
            EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
        child: SingleChildScrollView(
          child: Container(
            margin: const EdgeInsets.symmetric(
              vertical: 5,
            ),
            child: Column(
              children: [
                TextFormWithPadding(
                  labelText:
                      AppLocalizations.of(context)!.numberOfFamilyMembers,
                  controller: nOFMemberController,
                  keyboardType: TextInputType.number,
                ),
                //TODO:Dialog
                TextFormWithPadding(
                  labelText:
                      AppLocalizations.of(context)!.parentsCountryResidence,
                  getData: getCountry,
                  recordID: participantProvider.participant.countryID,
                  controller: countryController,
                  readOnly: true,
                  isDefaultDialog: true,
                  dialogType: LookUpTypes.country,
                ),
                //TODO:Dialog
                TextFormWithPadding(
                  labelText:
                      AppLocalizations.of(context)!.maritalParentStatuesID,
                  recordID: participantProvider
                      .participant.maritalParentStatuesID
                      .toString(),
                  controller: mPStatusController,
                  getData: getMStatus,
                  readOnly: true,
                  isDefaultDialog: true,
                  dialogType: LookUpTypes.marital,
                ),
                //TODO:Dialog
                TextFormWithPadding(
                  labelText: AppLocalizations.of(context)!.governorate,
                  controller: governorateController,
                  parentRecordID:
                      participantProvider.participant.countryID ?? '',
                  readOnly: true,
                  recordID: participantProvider.participant.cityID.toString(),
                  getData: getCity,
                  isDefaultDialog: true,
                  dialogType: LookUpTypes.city,
                ),

                TextFormWithPadding(
                  labelText: AppLocalizations.of(context)!.familyWorkMember,
                  controller: nOWFMController,
                  keyboardType: TextInputType.number,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  getMStatus(LookUps lookUp) {
    participantProvider.participant.maritalParentStatuesID =
        int.parse(lookUp.iD!);
    participantProvider.percentageOfCompletion();
  }

  getCity(LookUps lookUp) {
    participantProvider.participant.cityID = parseInt(lookUp.iD!);
    participantProvider.percentageOfCompletion();
  }

  getCountry(LookUps lookUp) {
    participantProvider.participant.countryID = lookUp.iD;
    participantProvider.percentageOfCompletion();
  }

  declareValue() {
    nOFMemberController = TextEditingController(
        text: '${participantProvider.participant.familyNumber ?? ''}');
    countryController = TextEditingController(text: '');
    mPStatusController = TextEditingController(text: '');
    governorateController = TextEditingController(text: '');
    nOWFMController = TextEditingController(
        text: ' ${participantProvider.participant.familyWorkMember ?? ''}');
  }

  clear() {
    nOFMemberController!.clear();
    countryController!.clear();
    mPStatusController!.clear();
    governorateController!.clear();
    nOWFMController!.clear();
  }

  @override
  void dispose() {
    // TODO: implement dispose.
    nOFMemberController!.dispose();
    countryController!.dispose();
    mPStatusController!.dispose();
    governorateController!.dispose();
    nOWFMController!.dispose();

    super.dispose();
  }
}
