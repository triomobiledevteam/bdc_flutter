import 'package:bdc/components/form_components/CheckBoxTile.dart';
import 'package:bdc/components/form_components/FormsbottomButton.dart';
import 'package:bdc/components/form_components/TextFormWithPadding.dart';
import 'package:bdc/core/modal/Lookups.dart';
import 'package:bdc/core/modal/constraints/LookUpTypes.dart';
import 'package:bdc/core/modal/participant_modal/ParticipantFamilyInfo.dart';
import 'package:bdc/core/provider/ParticipantProvider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:provider/provider.dart';

class FamilyInfoForm extends StatefulWidget {
  const FamilyInfoForm(
      {Key? key, required this.familyInfo, required this.showList})
      : super(key: key);
  final ParticipantFamilyInfo familyInfo;
  final void Function() showList;
  @override
  _FamilyInfoFormState createState() => _FamilyInfoFormState();
}

class _FamilyInfoFormState extends State<FamilyInfoForm> {
  late ParticipantProvider participantProvider;

  TextEditingController? fullNameArController;
  TextEditingController? fullNameEnController;
  TextEditingController? genderController;
  TextEditingController? dateOfBirthController;
  TextEditingController? disabilityDescriptionController;
  ParticipantFamilyInfo? info;
  bool showList = true;

  @override
  void initState() {
    // TODO: implement initState
    participantProvider =
        Provider.of<ParticipantProvider>(context, listen: false);

    declareValue();

    fullNameArController!.addListener(() {
      print(fullNameArController!.text);
      info!.fullNameAr = fullNameArController!.text;
    });
    fullNameEnController!.addListener(() {
      print(fullNameArController!.text);
      info!.fullNameEn = fullNameEnController!.text;
    });
    dateOfBirthController!.addListener(() {
      info!.dateOfBirth = dateOfBirthController!.text;
    });
    disabilityDescriptionController!.addListener(() {
      info!.disabilityDescription = disabilityDescriptionController!.text;
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding:
          EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
      child: SingleChildScrollView(
        child: Container(
          margin: const EdgeInsets.symmetric(
            vertical: 5,
          ),
          child: Column(
            children: [
              TextFormWithPadding(
                labelText: AppLocalizations.of(context)!.fullNameAr,
                controller: fullNameArController,
              ),
              TextFormWithPadding(
                labelText: AppLocalizations.of(context)!.fullNameEn,
                controller: fullNameEnController,
              ),
              //TODO:Dialog
              TextFormWithPadding(
                labelText: AppLocalizations.of(context)!.genderID,
                controller: genderController,
                readOnly: true,
                getData: getGender,
                isDefaultDialog: true,
                recordID: info!.genderID.toString(),
                dialogType: LookUpTypes.gender,
              ),

              TextFormWithPadding(
                labelText: AppLocalizations.of(context)!.birthDate,
                controller: dateOfBirthController,
                isDateDialog: true,
                getDate: getDOB,
                readOnly: true,
              ),
              CheckBoxTilWithPadding(
                  value: info!.disability,
                  text: AppLocalizations.of(context)!.disability,
                  onchange: (value) {
                    info!.disability = value;
                    setState(() {});
                  }),
              TextFormWithPadding(
                controller: disabilityDescriptionController,
                labelText: AppLocalizations.of(context)!.disabilityDescription,
              ),
              FormsBottomButton(
                text: AppLocalizations.of(context)!.add,
                onPressed: () {
                  print('info.name ${info!.fullNameEn}');

                  ParticipantFamilyInfo familyInfo = info!;
                  print('familyInfo.name ${familyInfo.fullNameEn}');
                  participantProvider.addFamilyInfo(familyInfo);
                  clear();
                  widget.showList();
                },
              )
            ],
          ),
        ),
      ),
    );
  }

  getGender(LookUps lookUp) {
    info!.genderID = int.parse(lookUp.iD!);
  }

  getDOB(String date) {
    info!.dateOfBirth = date;
    print('date $date');
  }

  declareValue() {
    info = widget.familyInfo;

    print(info!.dateOfBirth!);
    fullNameEnController = TextEditingController(text: info!.fullNameEn);
    fullNameArController = TextEditingController(text: info!.fullNameAr);
    genderController = TextEditingController(text: '');
    dateOfBirthController =
        TextEditingController(text: info!.dateOfBirth ?? '');
    disabilityDescriptionController =
        TextEditingController(text: info!.disabilityDescription);
  }

  clear() {
    info = ParticipantFamilyInfo();
    fullNameEnController!.clear();
    fullNameArController!.clear();
    genderController!.clear();
    dateOfBirthController!.clear();
    disabilityDescriptionController!.clear();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    fullNameArController!.dispose();
    dateOfBirthController!.dispose();
    disabilityDescriptionController!.dispose();
    super.dispose();
  }
}
