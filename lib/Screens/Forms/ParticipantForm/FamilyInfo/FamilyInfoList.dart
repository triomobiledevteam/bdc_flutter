import 'package:bdc/Screens/Forms/ParticipantForm/FamilyInfo/FamilyInfoCard.dart';
import 'package:bdc/core/modal/participant_modal/ParticipantFamilyInfo.dart';
import 'package:bdc/core/provider/ParticipantProvider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class FamilyInfoList extends StatefulWidget {
  const FamilyInfoList({Key? key, required this.displayFamilyInfo})
      : super(key: key);
  final void Function(ParticipantFamilyInfo) displayFamilyInfo;

  @override
  _FamilyInfoListState createState() => _FamilyInfoListState();
}

class _FamilyInfoListState extends State<FamilyInfoList> {
  ParticipantFamilyInfo? info;

  @override
  void initState() {
    // TODO: implement initState

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: const EdgeInsets.symmetric(
          vertical: 5,
        ),
        child: Consumer<ParticipantProvider>(
          builder: (context, participantProvider, child) {
            return ListView.builder(
                itemCount:
                    participantProvider.participant.familyInfoList.length,
                itemBuilder: (context, index) => Dismissible(
                      onDismissed: (value) {
                        participantProvider.removeFamilyInfo(index);
                      },
                      key: UniqueKey(),
                      child: FamilyInfoCard(
                          key: UniqueKey(),
                          onTap: () {
                            widget.displayFamilyInfo(participantProvider
                                .participant.familyInfoList[index]);
                          },
                          info: participantProvider
                              .participant.familyInfoList[index]),
                    ));
          },
        ));
  }
}
