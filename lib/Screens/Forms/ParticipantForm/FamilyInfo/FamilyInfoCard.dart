import 'package:bdc/core/modal/participant_modal/ParticipantFamilyInfo.dart';
import 'package:bdc/core/theme/app_Theme.dart';
import 'package:bdc/core/theme/sizeConfig.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class FamilyInfoCard extends StatelessWidget {
  const FamilyInfoCard({Key? key, this.info, required this.onTap})
      : super(key: key);
  final ParticipantFamilyInfo? info;
  final void Function() onTap;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        decoration: BoxDecoration(
          color: kBackgroundColor,
          borderRadius: BorderRadius.circular(12),
        ),
        margin: const EdgeInsets.symmetric(vertical: 5),
        padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 5),
        // color: kBackgroundColor,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Row(
              children: [
                const Icon(Icons.person),
                Text(
                  info!.fullNameEn!,
                  style: basicTextStyle,
                ),
              ],
            ),
            Visibility(
                visible: info!.disability!,
                child: Row(children: [
                  Icon(
                    Icons.check,
                    size: SizeConfig.blockSizeVertical! * 2,
                    color: kPrimaryColorGreen,
                  ),
                  Text(
                    AppLocalizations.of(context)!.disability,
                    style: greenSmallTextStyle,
                  )
                ]))
          ],
        ),
      ),
    );
  }
}
