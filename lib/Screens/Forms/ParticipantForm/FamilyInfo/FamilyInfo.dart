import 'package:bdc/Screens/Forms/ParticipantForm/FamilyInfo/FamilyInfoForm.dart';
import 'package:bdc/Screens/Forms/ParticipantForm/FamilyInfo/FamilyInfoList.dart';
import 'package:bdc/components/form_components/StickyHeaderForm.dart';
import 'package:bdc/core/modal/participant_modal/ParticipantFamilyInfo.dart';
import 'package:bdc/core/provider/ParticipantProvider.dart';
import 'package:bdc/core/theme/app_Theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:provider/provider.dart';

class FamilyInfoUser extends StatefulWidget {
  const FamilyInfoUser({Key? key}) : super(key: key);

  @override
  _FamilyInfoUserState createState() => _FamilyInfoUserState();
}

class _FamilyInfoUserState extends State<FamilyInfoUser> {
  late ParticipantProvider participantProvider;

  ParticipantFamilyInfo? info;
  bool showList = true;

  @override
  void initState() {
    // TODO: implement initState
    participantProvider =
        Provider.of<ParticipantProvider>(context, listen: false);

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return StickyHeaderForm(
      title: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          FittedBox(
            child: Text(
              AppLocalizations.of(context)!.familyInfo,
              style: Theme.of(context).textTheme.headline6!.copyWith(
                    color: Colors.white,
                  ),
            ),
          ),
          IconButton(
              onPressed: () {
                showList = !showList;
                info = ParticipantFamilyInfo();
                setState(() {});
              },
              icon: Icon(
                showList ? Icons.add_circle_outline : Icons.view_list_sharp,
                color: whiteColor,
              ))
        ],
      ),
      body: showList
          ? FamilyInfoList(
              displayFamilyInfo: displayFamilyInfo,
            )
          : FamilyInfoForm(
              familyInfo: info!,
              showList: show,
            ),
    );
  }

  displayFamilyInfo(ParticipantFamilyInfo? familyInfo) {
    info = familyInfo;
    showList = false;
    setState(() {});
  }

  show() {
    showList = true;
    setState(() {});
  }

  @override
  void dispose() {
    // TODO: implement dispose

    super.dispose();
  }
}
