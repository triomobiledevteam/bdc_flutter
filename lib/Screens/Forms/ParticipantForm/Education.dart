import 'package:bdc/components/form_components/StickyHeaderForm.dart';
import 'package:bdc/components/form_components/TextFormWithPadding.dart';
import 'package:bdc/core/modal/Lookups.dart';
import 'package:bdc/core/modal/constraints/LookUpTypes.dart';
import 'package:bdc/core/provider/ParticipantProvider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:provider/provider.dart';

class EducationUser extends StatefulWidget {
  const EducationUser({Key? key}) : super(key: key);

  @override
  _EducationUserState createState() => _EducationUserState();
}

class _EducationUserState extends State<EducationUser> {
  late ParticipantProvider participantProvider;

  TextEditingController? educationLevelController;
  TextEditingController? pSectorController;
  TextEditingController? iNameController;
  TextEditingController? majorController;
  TextEditingController? graduationDateController;

  @override
  void initState() {
    // TODO: implement initState
    participantProvider =
        Provider.of<ParticipantProvider>(context, listen: false);
    declareValue();
    iNameController!.addListener(() {
      participantProvider.participant.universityName = iNameController!.text;
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return StickyHeaderForm(
      title: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          FittedBox(
            child: Text(
              AppLocalizations.of(context)!.education,
              style: Theme.of(context).textTheme.headline6!.copyWith(
                    color: Colors.white,
                  ),
            ),
          ),
          //TODO:Button
        ],
      ),
      body: Padding(
        padding:
            EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
        child: SingleChildScrollView(
          child: Container(
            margin: const EdgeInsets.symmetric(
              vertical: 5,
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                //TODO:Dialog
                TextFormWithPadding(
                  labelText: AppLocalizations.of(context)!.educationalLevel,
                  controller: educationLevelController,
                  isDefaultDialog: true,
                  recordID: participantProvider.participant.educationalLevel
                      .toString(),
                  getData: getEducationalLevel,
                  dialogType: LookUpTypes.eduLevel,
                  readOnly: true,
                ),
                //TODO:Dialog

                TextFormWithPadding(
                  labelText: AppLocalizations.of(context)!.professionalSector,
                  getData: getSector,
                  recordID: participantProvider.participant.professionalSector
                      .toString(),
                  controller: pSectorController,
                  isDefaultDialog: true,
                  dialogType: LookUpTypes.sector,
                  readOnly: true,
                  // errorText: "Professional Sector can't be empty",
                  // errorFlag: participantProvider.errorFlag,
                ),
                TextFormWithPadding(
                  labelText: AppLocalizations.of(context)!.universityName,
                  controller: iNameController,
                ),
                //TODO:Dialog

                TextFormWithPadding(
                  labelText: AppLocalizations.of(context)!.majorID,
                  recordID: participantProvider.participant.majorID.toString(),
                  getData: getMajor,
                  controller: majorController,
                  isDefaultDialog: true,
                  dialogType: LookUpTypes.major,
                  readOnly: true,
                ),

                TextFormWithPadding(
                  labelText: AppLocalizations.of(context)!.graduationDate,
                  controller: graduationDateController,
                  getDate: getGraduationDate,
                  readOnly: true,
                  isDateDialog: true,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  getEducationalLevel(LookUps lookUp) {
    participantProvider.participant.educationalLevel = int.parse(lookUp.iD!);
    participantProvider.percentageOfCompletion();
  }

  getSector(LookUps lookUp) {
    participantProvider.participant.professionalSector = int.parse(lookUp.iD!);
    participantProvider.percentageOfCompletion();
  }

  getMajor(LookUps lookUp) {
    participantProvider.participant.majorID = int.parse(lookUp.iD!);
  }

  declareValue() {
    educationLevelController = TextEditingController(text: '');
    pSectorController = TextEditingController(text: '');
    iNameController = TextEditingController(
        text: participantProvider.participant.universityName ?? '');
    majorController = TextEditingController(text: '');
    graduationDateController = TextEditingController(
        text: participantProvider.participant.graduationDate ?? '');
  }

  getGraduationDate(String date) {
    participantProvider.participant.graduationDate = date;
  }

  clear() {
    educationLevelController!.clear();
    pSectorController!.clear();
    iNameController!.clear();
    majorController!.clear();
    graduationDateController!.clear();
  }

  @override
  void dispose() {
    // TODO: implement dispose

    educationLevelController!.dispose();
    pSectorController!.dispose();
    iNameController!.dispose();
    majorController!.dispose();
    graduationDateController!.dispose();
    super.dispose();
  }
}
