import 'package:bdc/core/modal/participant_modal/ParticipantExperience.dart';
import 'package:bdc/core/theme/app_Theme.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class ExperienceInfoCard extends StatelessWidget {
  const ExperienceInfoCard({Key? key, this.info, required this.onTap})
      : super(key: key);
  final ParticipantExperience? info;
  final void Function() onTap;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        decoration: BoxDecoration(
          color: kBackgroundColor,
          borderRadius: BorderRadius.circular(12),
        ),
        margin: const EdgeInsets.symmetric(vertical: 5),
        padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 5),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Row(
              children: [
                const Icon(Icons.business_rounded),
                Text(
                  info!.companyName!,
                  style: basicTextStyle,
                ),
              ],
            ),
            Row(
              children: [
                const Icon(Icons.date_range_sharp),
                Text(
                  info!.startDate!.isNotEmpty
                      ? DateFormat('yyyy-MM-dd')
                          .format(DateTime.parse(info!.startDate!))
                      : '',
                  style: basicTextStyle,
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
