import 'package:bdc/components/form_components/FormsbottomButton.dart';
import 'package:bdc/components/form_components/TextFormWithPadding.dart';
import 'package:bdc/core/modal/constraints/LookUpTypes.dart';
import 'package:bdc/core/modal/participant_modal/ParticipantExperience.dart';
import 'package:bdc/core/provider/ParticipantProvider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:provider/provider.dart';

import '../../../../components/dialog/map_dialog.dart';
import '../../../map_layout/bloc/map_layout_cubit.dart';

class UserExperienceForm extends StatefulWidget {
  const UserExperienceForm(
      {Key? key, required this.participantExperience, required this.showList})
      : super(key: key);
  final ParticipantExperience participantExperience;
  final void Function() showList;
  @override
  _UserExperienceFormState createState() => _UserExperienceFormState();
}

class _UserExperienceFormState extends State<UserExperienceForm> {
  late ParticipantProvider participantProvider;

  TextEditingController? descriptionEnController;
  TextEditingController? descriptionArController;
  TextEditingController? startDateController;
  TextEditingController? endDateController;
  TextEditingController? locationController;
  TextEditingController? companyIndustryController;
  TextEditingController? companyNameController;
  TextEditingController? noteController;

  ParticipantExperience? experience;

  @override
  void initState() {
    participantProvider =
        Provider.of<ParticipantProvider>(context, listen: false);

    declareValue();
    descriptionEnController!.addListener(() {
      experience!.descriptionEn = descriptionEnController!.text;
    });
    descriptionArController!.addListener(() {
      experience!.descriptionAr = descriptionArController!.text;
    });
    locationController!.addListener(() {
      experience!.location = locationController!.text;
    });
    companyNameController!.addListener(() {
      experience!.companyName = companyNameController!.text;
    });
    noteController!.addListener(() {
      experience!.note = noteController!.text;
    });
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding:
          EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
      child: SingleChildScrollView(
        child: Container(
          margin: const EdgeInsets.symmetric(
            vertical: 5,
          ),
          child: Column(
            children: [
              TextFormWithPadding(
                labelText: AppLocalizations.of(context)!.descriptionEn,
                controller: descriptionEnController,
                minLines: 2,
                maxLines: 2,
              ),
              TextFormWithPadding(
                labelText: AppLocalizations.of(context)!.descriptionAr,
                controller: descriptionArController,
                minLines: 2,
                maxLines: 2,
              ),
              TextFormWithPadding(
                labelText: AppLocalizations.of(context)!.startDate,
                controller: startDateController,
                isDateDialog: true,
                getDate: getStartDate,
                readOnly: true,
              ),
              TextFormWithPadding(
                labelText: AppLocalizations.of(context)!.endDate,
                controller: endDateController,
                isDateDialog: true,
                readOnly: true,
                getDate: getEndDate,
              ),
              TextFormWithPadding(
                labelText: AppLocalizations.of(context)!.location,
                controller: locationController,
                suffixIcon: IconButton(
                  icon: const Icon(Icons.pin_drop_rounded),
                  onPressed: () async {
                    await MapLayoutCubit.get(context).getCurrentLocation();
                    MapDialog.show(context, (value) {
                      setState(() {
                        locationController = TextEditingController(text: value);
                        experience!.location = locationController!.text;
                      });
                    });
                  },
                ),
              ),
              //TODO:Dialog
              TextFormWithPadding(
                labelText: AppLocalizations.of(context)!.companyIndustry,
                controller: companyIndustryController,
                getData: getIndustry,
                recordID: experience!.companyIndustry.toString(),
                readOnly: true,
                isDefaultDialog: true,
                dialogType: LookUpTypes.industry,
              ),
              TextFormWithPadding(
                labelText: AppLocalizations.of(context)!.companyName,
                controller: companyNameController,
              ),
              TextFormWithPadding(
                controller: noteController,
                labelText: AppLocalizations.of(context)!.notes,
                minLines: 3,
                maxLines: 3,
              ),
              FormsBottomButton(
                text: AppLocalizations.of(context)!.add,
                onPressed: () {
                  ParticipantExperience? experienceInfo = experience;
                  participantProvider.addExperience(experienceInfo);
                  clear();

                  widget.showList();
                },
              ),
            ],
          ),
        ),
      ),
    );
  }

  getIndustry(lookUp) {
    experience!.companyIndustry = int.parse(lookUp.iD!);
  }

  getEndDate(String date) {
    experience!.endDate = date;
    print('date $date');
  }

  getStartDate(String date) {
    experience!.startDate = date;
    print('date $date');
  }

  declareValue() {
    experience = widget.participantExperience;

    descriptionEnController =
        TextEditingController(text: experience!.descriptionEn);
    descriptionArController =
        TextEditingController(text: experience!.descriptionAr);

    startDateController =
        TextEditingController(text: experience!.startDate ?? '');
    endDateController = TextEditingController(text: experience!.endDate ?? '');
    locationController = TextEditingController(text: experience!.location);
    companyIndustryController = TextEditingController(text: '');
    companyNameController =
        TextEditingController(text: experience!.companyName);
    noteController = TextEditingController(text: experience!.note);
  }

  clear() {
    experience = ParticipantExperience();
    descriptionEnController!.clear();
    descriptionArController!.clear();
    startDateController!.clear();
    endDateController!.clear();
    locationController!.clear();
    companyIndustryController!.clear();
    companyNameController!.clear();
    noteController!.clear();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    descriptionEnController!.dispose();
    startDateController!.dispose();
    endDateController!.dispose();
    locationController!.dispose();
    companyNameController!.dispose();
    noteController!.dispose();
    super.dispose();
  }
}
