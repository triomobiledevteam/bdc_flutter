import 'package:bdc/Screens/Forms/ParticipantForm/Experience/ExpirienceCard.dart';
import 'package:bdc/core/modal/participant_modal/ParticipantExperience.dart';
import 'package:bdc/core/modal/participant_modal/ParticipantFamilyInfo.dart';
import 'package:bdc/core/provider/ParticipantProvider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ExperienceList extends StatefulWidget {
  const ExperienceList({
    Key? key,
    required this.displayExperience,
  }) : super(key: key);
  final void Function(ParticipantExperience?) displayExperience;

  @override
  _ExperienceListState createState() => _ExperienceListState();
}

class _ExperienceListState extends State<ExperienceList> {
  ParticipantFamilyInfo? info;

  @override
  void initState() {
    // TODO: implement initState

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: const EdgeInsets.symmetric(
          vertical: 5,
        ),
        child: Consumer<ParticipantProvider>(
          builder: (context, participantProvider, child) {
            return ListView.builder(
                itemCount:
                    participantProvider.participant.experienceList.length,
                itemBuilder: (context, index) => Dismissible(
                      onDismissed: (value) {
                        participantProvider.removeExperience(index);
                      },
                      key: UniqueKey(),
                      child: ExperienceInfoCard(
                          key: UniqueKey(),
                          onTap: () {
                            widget.displayExperience(participantProvider
                                .participant.experienceList[index]);
                          },
                          info: participantProvider
                              .participant.experienceList[index]),
                    ));
          },
        ));
  }
}
