import 'package:bdc/Screens/Forms/ParticipantForm/Experience/ExperienceForm.dart';
import 'package:bdc/components/form_components/StickyHeaderForm.dart';
import 'package:bdc/core/modal/participant_modal/ParticipantExperience.dart';
import 'package:bdc/core/provider/ParticipantProvider.dart';
import 'package:bdc/core/theme/app_Theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:provider/provider.dart';

import 'ExperinecneList.dart';

class UserExperienceInfo extends StatefulWidget {
  const UserExperienceInfo({Key? key}) : super(key: key);

  @override
  _UserExperienceInfoState createState() => _UserExperienceInfoState();
}

class _UserExperienceInfoState extends State<UserExperienceInfo> {
  late ParticipantProvider participantProvider;

  ParticipantExperience? experience;
  bool showList = true;

  @override
  void initState() {
    participantProvider =
        Provider.of<ParticipantProvider>(context, listen: false);
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return StickyHeaderForm(
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            FittedBox(
              child: Text(
                AppLocalizations.of(context)!.experience,
                style: Theme.of(context).textTheme.headline6!.copyWith(
                      color: Colors.white,
                    ),
              ),
            ),
            IconButton(
                onPressed: () {
                  showList = !showList;
                  experience = ParticipantExperience();
                  setState(() {});
                },
                icon: Icon(
                  showList ? Icons.add_circle_outline : Icons.view_list_sharp,
                  color: whiteColor,
                ))
          ],
        ),
        body: showList
            ? ExperienceList(
                displayExperience: displayExperience,
              )
            : UserExperienceForm(
                participantExperience: experience!,
                showList: show,
              ));
  }

  void displayExperience(selectedExperience) {
    experience = selectedExperience;
    showList = false;
    setState(() {});
  }

  show() {
    showList = true;
    setState(() {});
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }
}
