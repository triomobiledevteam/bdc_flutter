import 'package:bdc/core/modal/Lookups.dart';
import 'package:bdc/core/modal/company_modal/Contact.dart';
import 'package:bdc/core/storage/db_client.dart';
import 'package:bdc/core/theme/app_Theme.dart';
import 'package:bdc/core/theme/sizeConfig.dart';
import 'package:flutter/material.dart';

class ContactInfoCard extends StatefulWidget {
  const ContactInfoCard({Key? key, this.info, required this.onTap})
      : super(key: key);
  final Contact? info;
  final void Function() onTap;

  @override
  _ContactInfoCardState createState() => _ContactInfoCardState();
}

class _ContactInfoCardState extends State<ContactInfoCard> {
  final _db = DatabaseClient.instance;

  @override
  void initState() {
    // TODO: implement initState
    getData();
    super.initState();
  }

  LookUps jobRole = LookUps();

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: widget.onTap,
      child: Container(
        decoration: BoxDecoration(
          color: kBackgroundColor,
          borderRadius: BorderRadius.circular(12),
        ),
        margin: EdgeInsets.symmetric(vertical: SizeConfig.height! * 0.01),
        padding: EdgeInsets.symmetric(
            horizontal: SizeConfig.width! * 0.03,
            vertical: SizeConfig.height! * 0.01),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Row(
              children: [
                const Icon(Icons.person),
                Text(
                  widget.info!.name!,
                  style: basicTextStyle,
                ),
              ],
            ),
            Row(
              children: [
                const Icon(Icons.business),
                Text(
                  jobRole.descriptionEn!,
                  style: basicTextStyle,
                ),
              ],
            )
          ],
        ),
      ),
    );
  }

  getData() async {
    jobRole = await _db.getWhereJobRole(widget.info!.roleID.toString());
    print(jobRole.descriptionEn);
    setState(() {});
  }
}
