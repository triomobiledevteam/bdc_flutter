import 'package:bdc/components/form_components/FormsbottomButton.dart';
import 'package:bdc/components/form_components/TextFormWithPadding.dart';
import 'package:bdc/core/modal/Lookups.dart';
import 'package:bdc/core/modal/company_modal/Contact.dart';
import 'package:bdc/core/modal/constraints/LookUpTypes.dart';
import 'package:bdc/core/provider/CompanyProvider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:provider/provider.dart';

import '../../../../core/utils/string_validation_extension.dart';

class ContactForm extends StatefulWidget {
  const ContactForm({Key? key, required this.contact}) : super(key: key);
  final Contact contact;

  @override
  _ContactFormState createState() => _ContactFormState();
}

class _ContactFormState extends State<ContactForm> {
  TextEditingController? nameController;
  TextEditingController? roleController;
  TextEditingController? mobileController;
  TextEditingController? emailController;
  Contact? contact;
  final keyForm = GlobalKey<FormState>();
  late CompanyProvider companyProvider;

  @override
  void initState() {
    // TODO: implement initState
    companyProvider = Provider.of<CompanyProvider>(context, listen: false);
    declareValue();

    nameController!.addListener(() {
      contact!.name = nameController!.text;
    });
    mobileController!.addListener(() {
      contact!.mobile = mobileController!.text;
    });
    emailController!.addListener(() {
      contact!.email = emailController!.text;
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Form(
        key: keyForm,
        child: Container(
          margin: const EdgeInsets.symmetric(
            vertical: 5,
          ),
          child: Column(
            children: [
              TextFormWithPadding(
                labelText: AppLocalizations.of(context)!.name,
                controller: nameController,
                validator: (value) {
                  if (value!.isEmpty) {
                    return AppLocalizations.of(context)!.thisFieldIsEmpty;
                  }
                  return null;
                },
              ),
              TextFormWithPadding(
                labelText: AppLocalizations.of(context)!.roleID,
                controller: roleController,
                recordID: contact!.roleID.toString(),
                readOnly: true,
                getData: getRole,
                isDefaultDialog: true,
                dialogType: LookUpTypes.jobRole,
              ),
              TextFormWithPadding(
                labelText: AppLocalizations.of(context)!.mobile,
                validator: (value) {
                  if (!isValidMobile(value ?? '')) {
                    return AppLocalizations.of(context)!.invalidEmail;
                  }
                  return null;
                },
                controller: mobileController,
                keyboardType: TextInputType.phone,
              ),
              TextFormWithPadding(
                labelText: AppLocalizations.of(context)!.email,
                validator: (value) {
                  if (!isValidEmail(value ?? '')) {
                    return AppLocalizations.of(context)!.invalidEmail;
                  }
                  return null;
                },
                controller: emailController,
                keyboardType: TextInputType.emailAddress,
              ),
              FormsBottomButton(
                text: AppLocalizations.of(context)!.add,
                onPressed: () {
                  if (keyForm.currentState!.validate()) {
                    Contact? contactInfo = contact;
                    companyProvider.addContact(contactInfo);
                    clear();
                  }
                },
              )
            ],
          ),
        ),
      ),
    );
  }

  getRole(LookUps lookUp) {
    contact!.roleID = int.parse(lookUp.iD!);
  }

  declareValue() {
    contact = widget.contact;
    nameController = TextEditingController(text: contact!.name);
    roleController = TextEditingController(text: '');
    mobileController = TextEditingController(text: contact!.mobile);
    emailController = TextEditingController(text: contact!.email);
  }

  clear() {
    contact = Contact();
    nameController!.clear();
    roleController!.clear();
    mobileController!.clear();
    emailController!.clear();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    nameController!.dispose();
    roleController!.dispose();
    mobileController!.dispose();
    emailController!.dispose();
    super.dispose();
  }
}
