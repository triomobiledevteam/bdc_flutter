import 'package:bdc/Screens/Forms/CompanyForm/Contact/ContactForm.dart';
import 'package:bdc/Screens/Forms/CompanyForm/Contact/ContactInfoList.dart';
import 'package:bdc/components/form_components/StickyHeaderForm.dart';
import 'package:bdc/core/modal/company_modal/Contact.dart';
import 'package:bdc/core/provider/CompanyProvider.dart';
import 'package:bdc/core/theme/app_Theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:provider/provider.dart';

class ContactInfo extends StatefulWidget {
  const ContactInfo({Key? key}) : super(key: key);

  @override
  _ContactInfoState createState() => _ContactInfoState();
}

class _ContactInfoState extends State<ContactInfo> {
  Contact? contact;

  late CompanyProvider companyProvider;

  bool showList = true;

  @override
  void initState() {
    // TODO: implement initState
    companyProvider = Provider.of<CompanyProvider>(context, listen: false);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return StickyHeaderForm(
      title: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          FittedBox(
            child: Text(
              AppLocalizations.of(context)!.contacts,
              style: Theme.of(context).textTheme.headline6!.copyWith(
                    color: Colors.white,
                  ),
            ),
          ),
          IconButton(
              onPressed: () {
                showList = !showList;
                contact = Contact();
                setState(() {});
              },
              icon: Icon(
                showList ? Icons.add_circle_outline : Icons.view_list_sharp,
                color: whiteColor,
              ))
        ],
      ),
      body: showList
          ? ContactListInfo(
              displayContact: displayContact,
            )
          : ContactForm(
              contact: contact!,
            ),
    );
  }

  void displayContact(Contact? selectedContact) {
    showList = false;
    contact = selectedContact;
    setState(() {});
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }
}
