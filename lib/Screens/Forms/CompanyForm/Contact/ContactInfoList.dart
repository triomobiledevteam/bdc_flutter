import 'package:bdc/Screens/Forms/CompanyForm/Contact/ContactInfoCard.dart';
import 'package:bdc/core/modal/company_modal/Contact.dart';
import 'package:bdc/core/provider/CompanyProvider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ContactListInfo extends StatefulWidget {
  const ContactListInfo({Key? key, required this.displayContact})
      : super(key: key);
  final void Function(Contact?) displayContact;

  @override
  _ContactListInfoState createState() => _ContactListInfoState();
}

class _ContactListInfoState extends State<ContactListInfo> {
  @override
  void initState() {
    // TODO: implement initState

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: const EdgeInsets.symmetric(
          vertical: 5,
        ),
        child: Consumer<CompanyProvider>(
          builder: (context, companyProvider, child) {
            return ListView.builder(
                itemCount: companyProvider.company.contactList.length,
                itemBuilder: (context, index) => ContactInfoCard(
                    key: UniqueKey(),
                    onTap: () {
                      widget.displayContact(
                          companyProvider.company.contactList[index]);
                    },
                    info: companyProvider.company.contactList[index]));
          },
        ));
  }
}
