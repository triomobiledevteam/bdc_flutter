import 'package:bdc/components/buttons/bottom_button.dart';
import 'package:bdc/components/tabs/virtical_tabs.dart';
import 'package:bdc/core/helper/error_handler/errorhandler.dart';
import 'package:bdc/core/modal/LoginModal/Company.dart';
import 'package:bdc/core/modal/constraints/global_values.dart';
import 'package:bdc/core/modal/enums/UsersType.dart';
import 'package:bdc/core/provider/CompanyProvider.dart';
import 'package:bdc/core/theme/app_Theme.dart';
import 'package:bdc/core/theme/sizeConfig.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';
import 'package:provider/provider.dart';

import '../../../core/utils/Navigation.dart';
import '../TabList.dart';

class CompanyInfoScreen extends StatefulWidget {
  const CompanyInfoScreen({Key? key}) : super(key: key);

  @override
  _CompanyInfoScreenState createState() => _CompanyInfoScreenState();
}

class _CompanyInfoScreenState extends State<CompanyInfoScreen> {
  late CompanyProvider companyProvider;

  @override
  void initState() {
    // TODO: implement initState
    companyProvider = Provider.of<CompanyProvider>(context, listen: false);
    if (UserType.Company.name == userType || UserType.CBO.name == userType) {
      companyProvider.setCompany(mainCompany!);
    }

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      resizeToAvoidBottomInset: false,
      bottomNavigationBar: BottomButton(
        text: AppLocalizations.of(context)!.edit,
        color: kPrimaryColorGreen,
        onPressed: () async {
          await companyProvider.checkCompanyData(onError, () {
            setState(() {});
          }, context,
              backFlag: true,
              resetFlag: true,
              submitLocally: userType == UserType.Employee.name);
        },
      ),
      appBar: AppBar(
        backgroundColor: kPrimaryColor,
        title: Text(AppLocalizations.of(context)!.edit),
      ),
      body: Column(
        children: [
          Container(
            padding: EdgeInsets.symmetric(horizontal: SizeConfig.width! * 0.02),
            child: Consumer<CompanyProvider>(
                builder: (context, companyProvider, child) =>
                    LinearPercentIndicator(
                      animation: true,
                      lineHeight: SizeConfig.height! * 0.02,
                      animationDuration: 300,
                      percent:
                          companyProvider.company.percentageOfCompletion / 100,
                      center: Text(
                          "${companyProvider.company.percentageOfCompletion}%"),
                      barRadius: const Radius.circular(20),
                      progressColor: Colors.green,
                    )),
          ),
          Expanded(
            child: Consumer<CompanyProvider>(
              builder: (context, companyProvider, child) => SizedBox(
                height: SizeConfig.height,
                child: VerticalTabs(
                  tabsElevation: 10,
                  indicatorColor: kPrimaryColorGreen,
                  tabTextStyle:
                      TextStyle(fontSize: SizeConfig.blockSizeVertical! * 2),
                  tabsWidth: SizeConfig.width! * 0.12,
                  tabs: companyFormTabs,
                  contents: companyFormWidgets,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  onError(String massage) {
    Navigation.back(context);
    ErrorHandler.errorMassage = massage;
    ErrorHandler.showErrorMassageDialog(context);
  }
}
