import 'package:bdc/components/form_components/StickyHeaderForm.dart';
import 'package:bdc/components/form_components/TextFormWithPadding.dart';
import 'package:bdc/core/modal/Lookups.dart';
import 'package:bdc/core/modal/constraints/LookUpTypes.dart';
import 'package:bdc/core/modal/constraints/global_values.dart';
import 'package:bdc/core/modal/enums/UsersType.dart';
import 'package:bdc/core/provider/CompanyProvider.dart';
import 'package:bdc/core/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:provider/provider.dart';

import '../../../components/dialog/map_dialog.dart';
import '../../../core/utils/string_validation_extension.dart';
import '../../map_layout/bloc/map_layout_cubit.dart';

class MainCompanyInfo extends StatefulWidget {
  const MainCompanyInfo({Key? key}) : super(key: key);

  @override
  _MainCompanyInfoState createState() => _MainCompanyInfoState();
}

class _MainCompanyInfoState extends State<MainCompanyInfo> {
  late CompanyProvider companyProvider;

  TextEditingController? aCNameController;
  TextEditingController? eCNameController;
  TextEditingController? cEmailController;
  TextEditingController? cCommercialRegistrationNoController;
  TextEditingController? cSectorController;
  TextEditingController? cFaxController;
  TextEditingController? cCountryController;
  TextEditingController? cPhoneController;
  TextEditingController? cGovernorateController;
  TextEditingController? cAddressController;

  @override
  void initState() {
    // TODO: implement initState
    companyProvider = Provider.of<CompanyProvider>(context, listen: false);
    // companyProvider.clearMain = clear;
    // companyProvider.setState = () {
    //   setState(() {});
    // };
    declareValue();

    aCNameController!.addListener(() {
      companyProvider.company.institutionNameAr = aCNameController!.text;
      companyProvider.percentageOfCompletion();
    });
    eCNameController!.addListener(() {
      companyProvider.company.institutionNameEn = eCNameController!.text;
      companyProvider.percentageOfCompletion();
    });
    cEmailController!.addListener(() {
      companyProvider.company.institutionEmail = cEmailController!.text;
      companyProvider.percentageOfCompletion();
    });
    cCommercialRegistrationNoController!.addListener(() {
      companyProvider.company.institutionID =
          cCommercialRegistrationNoController!.text;
      companyProvider.percentageOfCompletion();
    });

    cFaxController!.addListener(() {
      companyProvider.company.institutionFax = cFaxController!.text;
    });

    cPhoneController!.addListener(() {
      companyProvider.company.institutionPhone = cPhoneController!.text;
      companyProvider.percentageOfCompletion();
    });

    cAddressController!.addListener(() {
      companyProvider.company.institutionAddress = cAddressController!.text;
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return StickyHeaderForm(
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            FittedBox(
              child: Text(
                AppLocalizations.of(context)!.main,
                style: Theme.of(context).textTheme.headline6!.copyWith(
                      color: Colors.white,
                    ),
              ),
            ),
          ],
        ),
        body: Padding(
          padding:
              EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
          child: SingleChildScrollView(
            child: Container(
              margin: const EdgeInsets.symmetric(
                vertical: 5,
              ),
              child: Consumer<CompanyProvider>(
                builder: (context, participant, child) {
                  return Column(
                    children: [
                      TextFormWithPadding(
                        labelText:
                            AppLocalizations.of(context)!.institutionNameEn,
                        controller: eCNameController,
                        errorText: "Company Name can't be empty",
                        errorFlag: companyProvider.errorFlag,
                      ),

                      TextFormWithPadding(
                        labelText:
                            AppLocalizations.of(context)!.institutionNameAr,
                        controller: aCNameController,
                        errorText:
                            AppLocalizations.of(context)!.thisFieldIsEmpty,
                        errorFlag: companyProvider.errorFlag,
                      ),

                      TextFormWithPadding(
                        labelText: AppLocalizations.of(context)!.email,
                        errorText: AppLocalizations.of(context)!.invalidEmail,
                        errorFlag: companyProvider.errorFlag,
                        validator: (value) {
                          if (!isValidEmail(value ?? '')) {
                            return AppLocalizations.of(context)!.invalidEmail;
                          }
                          return null;
                        },
                        controller: cEmailController,
                      ),
                      //TODO:Dialog
                      if (UserType.CBO.name != userType)
                        TextFormWithPadding(
                          labelText: AppLocalizations.of(context)!.sectorTypeID,
                          controller: cSectorController,
                          recordID:
                              companyProvider.company.sectorTypeID.toString(),
                          readOnly: true,
                          getData: getSector,
                          isDefaultDialog: true,
                          dialogType: LookUpTypes.sector,
                        ),

                      TextFormWithPadding(
                        labelText: AppLocalizations.of(context)!.institutionID,
                        controller: cCommercialRegistrationNoController,
                        keyboardType: TextInputType.phone,
                        validator: (value) {
                          if (!isValidMobile(value ?? '')) {
                            return AppLocalizations.of(context)!.invalidEmail;
                          }
                          return null;
                        },
                        errorText:
                            AppLocalizations.of(context)!.thisFieldIsEmpty,
                        errorFlag: companyProvider.errorFlag,
                      ),

                      TextFormWithPadding(
                        labelText: AppLocalizations.of(context)!.institutionFax,
                        keyboardType: TextInputType.phone,
                        controller: cFaxController,
                      ),

                      TextFormWithPadding(
                        labelText: AppLocalizations.of(context)!.countryID,
                        controller: cCountryController,
                        readOnly: true,
                        getData: getCountry,
                        isDefaultDialog: true,
                        recordID: companyProvider.company.countryID,
                        dialogType: LookUpTypes.country,
                      ),

                      TextFormWithPadding(
                        labelText:
                            AppLocalizations.of(context)!.institutionPhone,
                        controller: cPhoneController,
                        validator: (value) {
                          if (!isValidMobile(value ?? '')) {
                            return AppLocalizations.of(context)!.invalidEmail;
                          }
                          return null;
                        },
                        errorText: AppLocalizations.of(context)!.invalidMobile,
                        keyboardType: TextInputType.phone,
                      ),

                      TextFormWithPadding(
                        labelText: AppLocalizations.of(context)!.governorate,
                        controller: cGovernorateController,
                        parentRecordID: companyProvider.company.countryID ?? '',
                        readOnly: true,
                        recordID: companyProvider.company.cityID.toString(),
                        getData: getCity,
                        isDefaultDialog: true,
                        dialogType: LookUpTypes.city,
                      ),

                      TextFormWithPadding(
                        labelText: AppLocalizations.of(context)!.address,
                        controller: cAddressController,
                        keyboardType: TextInputType.streetAddress,
                        suffixIcon: IconButton(
                          icon: const Icon(Icons.pin_drop_rounded),
                          onPressed: () async {
                            await MapLayoutCubit.get(context)
                                .getCurrentLocation();
                            MapDialog.show(context, (value) {
                              setState(() {
                                cAddressController =
                                    TextEditingController(text: value);

                                companyProvider.company.institutionAddress =
                                    cAddressController!.text;
                              });
                            });
                          },
                        ),
                      ),
                    ],
                  );
                },
              ),
            ),
          ),
        ));
  }

  getSector(LookUps lookUp) {
    companyProvider.company.sectorTypeID = int.parse(lookUp.iD!);
    companyProvider.percentageOfCompletion();
  }

  getCity(LookUps lookUp) {
    companyProvider.company.cityID = parseInt(lookUp.iD!);
    companyProvider.percentageOfCompletion();
  }

  getCountry(LookUps lookUp) {
    companyProvider.company.countryID = lookUp.iD;
    companyProvider.percentageOfCompletion();
  }

  declareValue() {
    aCNameController = TextEditingController(
        text: companyProvider.company.institutionNameAr ?? '');
    eCNameController = TextEditingController(
        text: companyProvider.company.institutionNameEn ?? '');
    cCommercialRegistrationNoController = TextEditingController(
        text: companyProvider.company.institutionID ?? "");
    cEmailController = TextEditingController(
        text: companyProvider.company.institutionEmail ?? '');
    cSectorController = TextEditingController(text: '');
    cFaxController =
        TextEditingController(text: companyProvider.company.institutionFax);
    cCountryController = TextEditingController(text: '');
    cPhoneController = TextEditingController(
        text: companyProvider.company.institutionPhone ?? '');
    cGovernorateController = TextEditingController(text: '');
    cAddressController =
        TextEditingController(text: companyProvider.company.institutionAddress);
  }

  clear() {
    aCNameController!.clear();
    cCommercialRegistrationNoController!.clear();
    eCNameController!.clear();
    cEmailController!.clear();
    cFaxController!.clear();
    cCountryController!.clear();
    cPhoneController!.clear();
    cGovernorateController!.clear();
    cAddressController!.clear();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    aCNameController!.dispose();
    cCommercialRegistrationNoController!.dispose();
    eCNameController!.dispose();
    cEmailController!.dispose();
    cSectorController!.dispose();
    cFaxController!.dispose();
    cCountryController!.dispose();
    cPhoneController!.dispose();
    cGovernorateController!.dispose();
    cAddressController!.dispose();
    super.dispose();
  }
}
