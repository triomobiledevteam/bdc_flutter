import 'package:bdc/Screens/Forms/CompanyForm/Branch/BranchCard.dart';
import 'package:bdc/core/modal/company_modal/Branch.dart';
import 'package:bdc/core/provider/CompanyProvider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class BranchListInfo extends StatefulWidget {
  const BranchListInfo({Key? key, required this.displayBranch})
      : super(key: key);
  final void Function(Branch?) displayBranch;

  @override
  _BranchListInfoState createState() => _BranchListInfoState();
}

class _BranchListInfoState extends State<BranchListInfo> {
  Branch? branch;

  @override
  void initState() {
    // TODO: implement initState

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: const EdgeInsets.symmetric(
          vertical: 5,
        ),
        child: Consumer<CompanyProvider>(
          builder: (context, companyProvider, child) {
            return ListView.builder(
                itemCount: companyProvider.company.branchList.length,
                itemBuilder: (context, index) => BranchCard(
                    onTap: () {
                      widget.displayBranch(
                          companyProvider.company.branchList[index]);
                    },
                    key: UniqueKey(),
                    info: companyProvider.company.branchList[index]));
          },
        ));
  }
}
