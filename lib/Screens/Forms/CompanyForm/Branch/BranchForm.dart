import 'package:bdc/components/form_components/FormsbottomButton.dart';
import 'package:bdc/components/form_components/TextFormWithPadding.dart';
import 'package:bdc/core/modal/Lookups.dart';
import 'package:bdc/core/modal/company_modal/Branch.dart';
import 'package:bdc/core/modal/constraints/LookUpTypes.dart';
import 'package:bdc/core/provider/CompanyProvider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:provider/provider.dart';

import '../../../../components/dialog/map_dialog.dart';
import '../../../../core/utils/utils.dart';
import '../../../map_layout/bloc/map_layout_cubit.dart';

class BranchForm extends StatefulWidget {
  const BranchForm({Key? key, this.branch}) : super(key: key);
  final Branch? branch;

  @override
  _BranchFormState createState() => _BranchFormState();
}

class _BranchFormState extends State<BranchForm> {
  late CompanyProvider companyProvider;

  TextEditingController? bCityController;
  TextEditingController? bNameController;
  TextEditingController? bAddressController;
  TextEditingController? bCountryController;

  Branch? branch;
  bool showList = true;

  @override
  void initState() {
    // TODO: implement initState
    print(widget.branch!.branchesName);
    print(widget.branch!.branchCity);
    print(widget.branch!.branchCountry);
    branch = widget.branch;
    companyProvider = Provider.of<CompanyProvider>(context, listen: false);
    declareValue();
    bNameController!.addListener(() {
      branch!.branchesName = bNameController!.text;
    });
    bAddressController!.addListener(() {
      branch!.branchAddress = bAddressController!.text;
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        margin: const EdgeInsets.symmetric(
          vertical: 5,
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            //TODO:Dialog
            TextFormWithPadding(
              labelText: AppLocalizations.of(context)!.branchName,
              controller: bNameController,
            ),
            TextFormWithPadding(
              labelText: AppLocalizations.of(context)!.branchCountry,
              controller: bCountryController,
              recordID: branch!.branchCountry,
              getData: getCountry,
              readOnly: true,
              isDefaultDialog: true,
              dialogType: LookUpTypes.country,
            ),

            TextFormWithPadding(
              labelText: AppLocalizations.of(context)!.branchCity,
              controller: bCityController,
              readOnly: true,
              recordID: branch!.branchCity.toString(),
              getData: getCity,
              isDefaultDialog: true,
              dialogType: LookUpTypes.city,
            ),
            TextFormWithPadding(
              labelText: AppLocalizations.of(context)!.branchAddress,
              keyboardType: TextInputType.streetAddress,
              controller: bAddressController,
              suffixIcon: IconButton(
                icon: const Icon(Icons.pin_drop_rounded),
                onPressed: () async {
                  await MapLayoutCubit.get(context).getCurrentLocation();
                  MapDialog.show(context, (value) {
                    setState(() {
                      bAddressController = TextEditingController(text: value);
                      branch!.branchAddress = bAddressController!.text;
                    });
                  });
                },
              ),
            ),
            FormsBottomButton(
              text: AppLocalizations.of(context)!.add,
              onPressed: () {
                Branch? branchInfo = branch;
                companyProvider.addBranchInfo(branchInfo);
                clear();
              },
            )
          ],
        ),
      ),
    );
  }

  getCity(LookUps lookUp) {
    branch!.branchCity = parseInt(lookUp.iD!);
  }

  getCountry(LookUps lookUp) {
    branch!.branchCountry = lookUp.iD;
  }

  declareValue() {
    // branch = trainer_experience_form();
    bCityController = TextEditingController(text: '');
    bNameController = TextEditingController(text: branch!.branchesName);
    bAddressController = TextEditingController(text: branch!.branchAddress);
    bCountryController = TextEditingController(text: '');
  }

  clear() {
    branch = Branch();
    bCityController!.clear();
    bNameController!.clear();
    bAddressController!.clear();
    bCountryController!.clear();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    bCityController!.dispose();
    bNameController!.dispose();
    bAddressController!.dispose();
    bCountryController!.dispose();
    super.dispose();
  }
}
