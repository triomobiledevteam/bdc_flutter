import 'package:bdc/core/modal/Lookups.dart';
import 'package:bdc/core/modal/company_modal/Branch.dart';
import 'package:bdc/core/storage/db_client.dart';
import 'package:bdc/core/theme/app_Theme.dart';
import 'package:bdc/core/theme/sizeConfig.dart';
import 'package:flutter/material.dart';

class BranchCard extends StatefulWidget {
  const BranchCard({Key? key, this.info, required this.onTap})
      : super(key: key);
  final Branch? info;
  final void Function() onTap;

  @override
  _BranchCardState createState() => _BranchCardState();
}

class _BranchCardState extends State<BranchCard> {
  final _db = DatabaseClient.instance;

  @override
  void initState() {
    // TODO: implement initState
    getData();
    super.initState();
  }

  LookUps city = LookUps();

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: widget.onTap,
      child: Container(
        decoration: BoxDecoration(
          color: kBackgroundColor,
          borderRadius: BorderRadius.circular(12),
        ),
        margin: EdgeInsets.symmetric(vertical: SizeConfig.height! * 0.01),
        padding: EdgeInsets.symmetric(
            horizontal: SizeConfig.width! * 0.03,
            vertical: SizeConfig.height! * 0.01),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Row(
              children: [
                const Icon(Icons.business_rounded),
                Text(
                  widget.info!.branchesName!,
                  style: basicTextStyle,
                ),
              ],
            ),
            Row(
              children: [
                const Icon(Icons.date_range_sharp),
                Text(
                  city.descriptionEn!,
                  style: basicTextStyle,
                ),
              ],
            )
          ],
        ),
      ),
    );
  }

  getData() async {
    city = await _db.getWhereCity(widget.info!.branchCity.toString());
    setState(() {});
  }
}
