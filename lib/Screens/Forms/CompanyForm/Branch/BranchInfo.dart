import 'package:bdc/Screens/Forms/CompanyForm/Branch/BranchList.dart';
import 'package:bdc/components/form_components/StickyHeaderForm.dart';
import 'package:bdc/core/modal/company_modal/Branch.dart';
import 'package:bdc/core/provider/CompanyProvider.dart';
import 'package:bdc/core/theme/app_Theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:provider/provider.dart';

import 'BranchForm.dart';

class BranchInfo extends StatefulWidget {
  const BranchInfo({Key? key}) : super(key: key);

  @override
  _BranchInfoState createState() => _BranchInfoState();
}

class _BranchInfoState extends State<BranchInfo> {
  late CompanyProvider companyProvider;

  Branch? branch;
  bool showList = true;

  @override
  void initState() {
    // TODO: implement initState
    companyProvider = Provider.of<CompanyProvider>(context, listen: false);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return StickyHeaderForm(
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            FittedBox(
              child: Text(
                AppLocalizations.of(context)!.branches,
                style: Theme.of(context).textTheme.headline6!.copyWith(
                      color: Colors.white,
                    ),
              ),
            ),
            IconButton(
                onPressed: () {
                  showList = !showList;
                  branch = Branch();
                  setState(() {});
                },
                icon: Icon(
                  showList ? Icons.add_circle_outline : Icons.view_list_sharp,
                  color: whiteColor,
                ))
          ],
        ),
        body: showList
            ? BranchListInfo(
                displayBranch: displayBranch,
              )
            : BranchForm(
                branch: branch,
              ));
  }

  void displayBranch(Branch? selectedBranch) {
    showList = false;
    branch = selectedBranch;
    setState(() {});
  }

  @override
  void dispose() {
    super.dispose();
  }
}
