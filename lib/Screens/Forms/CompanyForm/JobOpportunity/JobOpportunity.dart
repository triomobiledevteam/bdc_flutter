import 'package:bdc/Screens/Forms/CompanyForm/JobOpportunity/JobOpportunityForm.dart';
import 'package:bdc/Screens/Forms/CompanyForm/JobOpportunity/JobOppourtunityList.dart';
import 'package:bdc/components/form_components/StickyHeaderForm.dart';
import 'package:bdc/core/modal/company_modal/JobOpportunity.dart';
import 'package:bdc/core/provider/CompanyProvider.dart';
import 'package:bdc/core/theme/app_Theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:provider/provider.dart';

class JobOpportunityInfo extends StatefulWidget {
  const JobOpportunityInfo({Key? key}) : super(key: key);

  @override
  _JobOpportunityInfoState createState() => _JobOpportunityInfoState();
}

class _JobOpportunityInfoState extends State<JobOpportunityInfo> {
  late CompanyProvider companyProvider;

  bool showList = true;
  JobOpportunities? jobOpportunity;

  @override
  void initState() {
    // TODO: implement initState
    companyProvider = Provider.of<CompanyProvider>(context, listen: false);

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return StickyHeaderForm(
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            FittedBox(
              child: Text(
                AppLocalizations.of(context)!.jobOpportunity,
                style: Theme.of(context).textTheme.headline6!.copyWith(
                      color: Colors.white,
                    ),
              ),
            ),
            IconButton(
                onPressed: () {
                  showList = !showList;
                  jobOpportunity = JobOpportunities();
                  setState(() {});
                },
                icon: Icon(
                  showList ? Icons.add_circle_outline : Icons.view_list_sharp,
                  color: whiteColor,
                ))
          ],
        ),
        body: showList
            ? JobOpportunityList(
                displayJO: displayJO,
              )
            : JobOpportunityForm(jobOpportunity: jobOpportunity!));
  }

  void displayJO(JobOpportunities? selectedJO) {
    print('tap');
    showList = false;
    jobOpportunity = selectedJO;
    setState(() {});
  }

  @override
  void dispose() {
    // TODO: implement dispose

    super.dispose();
  }
}
