import 'package:bdc/Screens/Forms/CompanyForm/JobOpportunity/JobOpportunityCard.dart';
import 'package:bdc/core/modal/company_modal/JobOpportunity.dart';
import 'package:bdc/core/provider/CompanyProvider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class JobOpportunityList extends StatefulWidget {
  const JobOpportunityList({Key? key, required this.displayJO})
      : super(key: key);
  final void Function(JobOpportunities?) displayJO;

  @override
  _JobOpportunityListState createState() => _JobOpportunityListState();
}

class _JobOpportunityListState extends State<JobOpportunityList> {
  @override
  void initState() {
    // TODO: implement initState

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: const EdgeInsets.symmetric(
          vertical: 5,
        ),
        child: Consumer<CompanyProvider>(
          builder: (context, companyProvider, child) {
            return ListView.builder(
                itemCount: companyProvider.company.jobOpportunityList.length,
                itemBuilder: (context, index) => JobOpportunityCard(
                    key: UniqueKey(),
                    onTap: () {
                      print(companyProvider
                          .company.jobOpportunityList[index].salaryRangeEnd);
                      widget.displayJO(
                          companyProvider.company.jobOpportunityList[index]);
                    },
                    info: companyProvider.company.jobOpportunityList[index]));
          },
        ));
  }
}
