import 'package:bdc/core/modal/company_modal/JobOpportunity.dart';
import 'package:bdc/core/theme/app_Theme.dart';
import 'package:bdc/core/theme/sizeConfig.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class JobOpportunityCard extends StatefulWidget {
  const JobOpportunityCard({Key? key, this.info, required this.onTap})
      : super(key: key);
  final JobOpportunities? info;
  final void Function() onTap;

  @override
  _JobOpportunityCardState createState() => _JobOpportunityCardState();
}

class _JobOpportunityCardState extends State<JobOpportunityCard> {
  @override
  Widget build(BuildContext context) {
    // print(widget.info!.jobRoleID);
    return GestureDetector(
      onTap: widget.onTap,
      child: Container(
        decoration: BoxDecoration(
          color: kBackgroundColor,
          borderRadius: BorderRadius.circular(12),
        ),
        margin: EdgeInsets.symmetric(vertical: SizeConfig.height! * 0.01),
        padding: EdgeInsets.symmetric(
            horizontal: SizeConfig.width! * 0.03,
            vertical: SizeConfig.height! * 0.01),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Row(
              children: [
                // Icon(Icons.business_rounded),
                Text(
                  widget.info!.exactProfessionDescription!,
                  style: basicTextStyle,
                ),
              ],
            ),
            Row(
              children: [
                // Icon(Icons.t),
                Text(
                  widget.info!.totalTrainerRecruitment.toString(),
                  style: basicTextStyle,
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
