import 'package:bdc/components/form_components/CheckBoxTile.dart';
import 'package:bdc/components/form_components/FormsbottomButton.dart';
import 'package:bdc/components/form_components/TextFormWithPadding.dart';
import 'package:bdc/core/modal/Lookups.dart';
import 'package:bdc/core/modal/constraints/LookUpTypes.dart';
import 'package:bdc/core/modal/company_modal/JobOpportunity.dart';
import 'package:bdc/core/provider/CompanyProvider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:provider/provider.dart';

class JobOpportunityForm extends StatefulWidget {
  const JobOpportunityForm({Key? key, required this.jobOpportunity})
      : super(key: key);
  final JobOpportunities jobOpportunity;

  @override
  _JobOpportunityFormState createState() => _JobOpportunityFormState();
}

class _JobOpportunityFormState extends State<JobOpportunityForm> {
  late CompanyProvider companyProvider;

  TextEditingController? jobRoleController;
  TextEditingController? ePDescriptionController;
  TextEditingController? tNOAVacanciesController;
  TextEditingController? genderController;
  TextEditingController? wHoursController;
  TextEditingController? nationalityController;
  TextEditingController? numberController;
  TextEditingController? tDurationController;
  TextEditingController? sRStartController;
  TextEditingController? sREndController;

  bool showList = true;
  JobOpportunities? jobOpportunity;

  @override
  void initState() {
    // TODO: implement initState
    companyProvider = Provider.of<CompanyProvider>(context, listen: false);
    declareValue();
    ePDescriptionController!.addListener(() {
      jobOpportunity!.exactProfessionDescription =
          ePDescriptionController!.text;
    });
    tNOAVacanciesController!.addListener(() {
      jobOpportunity!.totalTrainerRecruitment =
          int.parse(tNOAVacanciesController!.text);
    });
    wHoursController!.addListener(() {
      jobOpportunity!.workingHours = double.parse(wHoursController!.text);
    });
    numberController!.addListener(() {
      jobOpportunity!.number = int.parse(numberController!.text);
    });
    tDurationController!.addListener(() {
      jobOpportunity!.trainingDuration =
          double.parse(tDurationController!.text);
    });
    sRStartController!.addListener(() {
      jobOpportunity!.salaryRangeStart = double.parse(sRStartController!.text);
    });
    sREndController!.addListener(() {
      jobOpportunity!.salaryRangeEnd = double.parse(sREndController!.text);
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
        child: Container(
            margin: const EdgeInsets.symmetric(
              vertical: 5,
            ),
            child: Column(
              children: [
                //TODO:Dialog
                TextFormWithPadding(
                  labelText: AppLocalizations.of(context)!.roleID,
                  controller: jobRoleController,
                  getData: getJobRole,
                  recordID: jobOpportunity!.jobRoleID.toString(),
                  readOnly: true,
                  isDefaultDialog: true,
                  dialogType: LookUpTypes.jobRole,
                ),

                TextFormWithPadding(
                  labelText:
                      AppLocalizations.of(context)!.exactProfessionDescription,
                  controller: ePDescriptionController,
                ),

                TextFormWithPadding(
                  labelText: AppLocalizations.of(context)!
                      .totalNumberOfAvailableVacancies,
                  controller: tNOAVacanciesController,
                  keyboardType: const TextInputType.numberWithOptions(),
                ),
                //TODO:Dialog
                TextFormWithPadding(
                  labelText: AppLocalizations.of(context)!.genderID,
                  readOnly: true,
                  getData: getGender,
                  isDefaultDialog: true,
                  dialogType: LookUpTypes.gender,
                  recordID: jobOpportunity!.genderID.toString(),
                  controller: genderController,
                ),
                TextFormWithPadding(
                  labelText: AppLocalizations.of(context)!.workingHours,
                  controller: wHoursController,
                  keyboardType: const TextInputType.numberWithOptions(),
                ),
                //TODO:Dialog
                TextFormWithPadding(
                  labelText: AppLocalizations.of(context)!.nationalityID,
                  getData: getNationality,
                  controller: nationalityController,
                  readOnly: true,
                  isDefaultDialog: true,
                  recordID: jobOpportunity!.nationalityID.toString(),
                  dialogType: LookUpTypes.nationality,
                ),
                TextFormWithPadding(
                  labelText: AppLocalizations.of(context)!.number,
                  controller: numberController,
                  keyboardType: const TextInputType.numberWithOptions(),
                ),
                TextFormWithPadding(
                  labelText: AppLocalizations.of(context)!.trainingDuration,
                  controller: tDurationController,
                  keyboardType: const TextInputType.numberWithOptions(),
                ),

                //TODO:CheckBox
                CheckBoxTilWithPadding(
                    value: jobOpportunity!.socialSecurity,
                    text: AppLocalizations.of(context)!.socialSecurity,
                    onchange: (value) {
                      jobOpportunity!.socialSecurity = value;
                      setState(() {});
                    }),
                CheckBoxTilWithPadding(
                    value: jobOpportunity!.insurance,
                    text: AppLocalizations.of(context)!.insurance,
                    onchange: (value) {
                      jobOpportunity!.insurance = value;
                      setState(() {});
                    }),

                TextFormWithPadding(
                  labelText: AppLocalizations.of(context)!.salaryRangeStart,
                  controller: sRStartController,
                  keyboardType: TextInputType.number,
                ),

                TextFormWithPadding(
                  labelText: AppLocalizations.of(context)!.salaryRangeEnd,
                  controller: sREndController,
                  keyboardType: TextInputType.number,
                ),
                FormsBottomButton(
                  text: AppLocalizations.of(context)!.add,
                  onPressed: () {
                    JobOpportunities? jobO = jobOpportunity;
                    companyProvider.addJO(jobO);
                    clear();
                  },
                )
              ],
            )));
  }

  declareValue() {
    jobOpportunity = widget.jobOpportunity;

    jobRoleController = TextEditingController(text: '');
    ePDescriptionController =
        TextEditingController(text: jobOpportunity!.exactProfessionDescription);
    tNOAVacanciesController = TextEditingController(
        text: "${jobOpportunity!.totalTrainerRecruitment ?? ''}");
    wHoursController =
        TextEditingController(text: "${jobOpportunity!.workingHours ?? ''}");
    numberController =
        TextEditingController(text: "${jobOpportunity!.number ?? ''}");
    tDurationController = TextEditingController(
        text: "${jobOpportunity!.trainingDuration ?? ''}");
    nationalityController = TextEditingController(text: '');
    sRStartController = TextEditingController(
        text: "${jobOpportunity!.salaryRangeStart ?? ''}");
    genderController = TextEditingController(text: '');
    sREndController =
        TextEditingController(text: "${jobOpportunity!.salaryRangeEnd ?? ''}");
  }

  clear() {
    jobOpportunity = JobOpportunities();

    jobRoleController!.clear();
    ePDescriptionController!.clear();
    tNOAVacanciesController!.clear();
    genderController!.clear();
    wHoursController!.clear();
    nationalityController!.clear();
    numberController!.clear();
    tDurationController!.clear();
    sRStartController!.clear();
    sREndController!.clear();
  }

  getJobRole(LookUps lookUp) {
    jobOpportunity!.jobRoleID = int.parse(lookUp.iD!);
  }

  getNationality(LookUps lookUp) {
    jobOpportunity!.nationalityID = lookUp.iD;
  }

  getGender(LookUps lookUp) {
    jobOpportunity!.genderID = int.parse(lookUp.iD!);
  }

  @override
  void dispose() {
    // TODO: implement dispose
    jobRoleController!.dispose();
    ePDescriptionController!.dispose();
    tNOAVacanciesController!.dispose();
    genderController!.dispose();
    wHoursController!.dispose();
    nationalityController!.dispose();
    numberController!.dispose();
    tDurationController!.dispose();
    sRStartController!.dispose();
    sREndController!.dispose();
    super.dispose();
  }
}
