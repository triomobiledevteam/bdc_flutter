import 'package:bdc/components/form_components/StickyHeaderForm.dart';
import 'package:bdc/components/form_components/TextFormWithPadding.dart';
import 'package:bdc/core/modal/Lookups.dart';
import 'package:bdc/core/modal/constraints/LookUpTypes.dart';
import 'package:bdc/core/provider/CompanyProvider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:provider/provider.dart';

class GeneralCompanyInfo extends StatefulWidget {
  const GeneralCompanyInfo({Key? key}) : super(key: key);

  @override
  _GeneralCompanyInfoState createState() => _GeneralCompanyInfoState();
}

class _GeneralCompanyInfoState extends State<GeneralCompanyInfo> {
  late CompanyProvider companyProvider;

  TextEditingController? nOfEmployeeController;
  TextEditingController? recruitmentController;
  TextEditingController? nOfPartTimeController;
  TextEditingController? contractTypeController;
  TextEditingController? nOfWWomanController;
  TextEditingController? nOfSNeedController;

  @override
  void initState() {
    // TODO: implement initState
    companyProvider = Provider.of<CompanyProvider>(context, listen: false);
    // companyProvider.clearGeneralInfo = clear;

    declareValue();

    nOfEmployeeController!.addListener(() {
      companyProvider.company.institutionEmployeeNumber =
          double.parse(nOfEmployeeController!.text);
      companyProvider.percentageOfCompletion();
    });

    nOfPartTimeController!.addListener(() {
      companyProvider.company.institutionEmployeeNumberPartTime =
          double.parse(nOfPartTimeController!.text);
      companyProvider.percentageOfCompletion();
    });

    nOfWWomanController!.addListener(() {
      companyProvider.company.womanWorkingNumber =
          double.parse(nOfWWomanController!.text);
      companyProvider.percentageOfCompletion();
    });
    nOfSNeedController!.addListener(() {
      companyProvider.company.employeeSpecialNeeds =
          double.parse(nOfSNeedController!.text);
      companyProvider.percentageOfCompletion();
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return StickyHeaderForm(
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            FittedBox(
              child: Text(
                AppLocalizations.of(context)!.generalInfo,
                style: Theme.of(context).textTheme.headline6!.copyWith(
                      color: Colors.white,
                    ),
              ),
            ),
          ],
        ),
        body: Padding(
          padding:
              EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
          child: SingleChildScrollView(
            child: Container(
              margin: const EdgeInsets.symmetric(
                vertical: 5,
              ),
              child: Column(
                children: [
                  TextFormWithPadding(
                    labelText:
                        AppLocalizations.of(context)!.institutionEmployeeNumber,
                    controller: nOfEmployeeController,
                    keyboardType: const TextInputType.numberWithOptions(),
                  ),
                  TextFormWithPadding(
                    labelText:
                        AppLocalizations.of(context)!.recruitTypesInCompany,
                    dialogType: LookUpTypes.recruitment,
                    controller: recruitmentController,
                    recordID: companyProvider.company.recruitTypesInCompany
                        .toString(),
                    isDefaultDialog: true,
                    getData: getRecruitment,
                    readOnly: true,
                  ),
                  TextFormWithPadding(
                    labelText: AppLocalizations.of(context)!
                        .institutionEmployeeNumberPartTime,
                    controller: nOfPartTimeController,
                    keyboardType: const TextInputType.numberWithOptions(),
                  ),
                  TextFormWithPadding(
                    labelText:
                        AppLocalizations.of(context)!.contractTypesInCompany,
                    controller: contractTypeController,
                    recordID: companyProvider.company.contractTypesInCompany
                        .toString(),
                    readOnly: true,
                    dialogType: LookUpTypes.contractTypes,
                    isDefaultDialog: true,
                    getData: getContractTypes,
                  ),
                  TextFormWithPadding(
                    labelText: AppLocalizations.of(context)!.womanWorkingNumber,
                    controller: nOfWWomanController,
                    keyboardType: const TextInputType.numberWithOptions(),
                  ),
                  TextFormWithPadding(
                    labelText:
                        AppLocalizations.of(context)!.employeeSpecialNeeds,
                    controller: nOfSNeedController,
                    keyboardType: const TextInputType.numberWithOptions(),
                  ),
                ],
              ),
            ),
          ),
        ));
  }

  void getRecruitment(LookUps lookUp) {
    companyProvider.company.recruitTypesInCompany = int.parse(lookUp.iD!);
  }

  getContractTypes(LookUps lookUp) {
    companyProvider.company.contractTypesInCompany = int.parse(lookUp.iD!);
  }

  declareValue() {
    nOfEmployeeController = TextEditingController(
        text: '${companyProvider.company.institutionEmployeeNumber ?? ''}');
    recruitmentController = TextEditingController(text: '');
    nOfPartTimeController = TextEditingController(
        text:
            '${companyProvider.company.institutionEmployeeNumberPartTime ?? ''}');
    contractTypeController = TextEditingController(text: '');
    nOfWWomanController = TextEditingController(
        text: '${companyProvider.company.womanWorkingNumber ?? ''}');
    nOfSNeedController = TextEditingController(
        text: '${companyProvider.company.employeeSpecialNeeds ?? ''}');
  }

  clear() {
    nOfEmployeeController!.clear();
    recruitmentController!.clear();
    nOfPartTimeController!.clear();
    contractTypeController!.clear();
    nOfWWomanController!.clear();
    nOfSNeedController!.clear();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    nOfEmployeeController!.dispose();
    recruitmentController!.dispose();
    nOfPartTimeController!.dispose();
    contractTypeController!.dispose();
    nOfWWomanController!.dispose();
    nOfSNeedController!.dispose();
    super.dispose();
  }
}
