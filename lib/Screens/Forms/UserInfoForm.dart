import 'package:bdc/components/buttons/bottom_button.dart';
import 'package:bdc/components/tabs/virtical_tabs.dart';
import 'package:bdc/core/helper/error_handler/errorhandler.dart';
import 'package:bdc/core/provider/ParticipantProvider.dart';
import 'package:bdc/core/theme/app_Theme.dart';
import 'package:bdc/core/theme/sizeConfig.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';
import 'package:provider/provider.dart';

import '../../core/utils/Navigation.dart';
import '../HomeScreen/EmpHomeScreen.dart';
import 'TabList.dart';

class UserInfoForm extends StatefulWidget {
  const UserInfoForm({Key? key, required this.setScreen}) : super(key: key);
  final void Function() setScreen;

  @override
  _UserInfoFormState createState() => _UserInfoFormState();
}

class _UserInfoFormState extends State<UserInfoForm> {
  late ParticipantProvider participantProvider;
  int initialIndex = 0;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  final keyForm = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      bottomNavigationBar: BottomButton(
        text: AppLocalizations.of(context)!.addParticipant,
        color: kPrimaryColorGreen,
        onPressed: () async {
          if (keyForm.currentState!.validate()) {
            participantProvider =
                Provider.of<ParticipantProvider>(context, listen: false);
            await participantProvider.checkParticipantData(onError, () {
              Navigation.nextAndResetStack(context, const EmpHomeScreen());
            }, context, clearFlag: true, resetFlag: true, submitLocally: true);
            initialIndex = 0;
          }
        },
      ),
      body: Column(
        children: [
          Container(
            padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 5),
            child: Consumer<ParticipantProvider>(
              builder: (context, participantProvider, child) =>
                  LinearPercentIndicator(
                animation: true,
                lineHeight: SizeConfig.height! * 0.02,
                animationDuration: 300,
                percent:
                    participantProvider.participant.percentageOfCompletion /
                        100,
                center: Text(
                    "${participantProvider.participant.percentageOfCompletion}%"),
                barRadius: const Radius.circular(20),
                progressColor: Colors.green,
              ),
            ),
          ),
          Expanded(
            child: Form(
              key: keyForm,
              child: SizedBox(
                height: SizeConfig.height,
                child: VerticalTabs(
                    tabsElevation: 10,
                    // indicatorColor: kPrimaryColorGreen,
                    initialIndex: initialIndex,
                    tabTextStyle:
                        TextStyle(fontSize: SizeConfig.blockSizeVertical! * 2),
                    tabsWidth: SizeConfig.width! * 0.15,
                    tabs: userFormTabs,
                    contents: userFormWidgets),
              ),
            ),
          ),
        ],
      ),
    );
  }

  onError(String massage) {
    Navigation.back(context);
    ErrorHandler.errorMassage = massage;
    ErrorHandler.showErrorMassageDialog(context);
  }
}
