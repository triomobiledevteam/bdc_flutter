import 'package:bdc/Screens/Forms/trainer_form/trainer_certificate_form/certificate_List.dart';
import 'package:bdc/Screens/Forms/trainer_form/trainer_certificate_form/certificate_form.dart';
import 'package:bdc/components/form_components/StickyHeaderForm.dart';
import 'package:bdc/core/modal/trainer_modal/trainer_certificate.dart';
import 'package:bdc/core/provider/trainer_provider.dart';
import 'package:bdc/core/theme/app_Theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:provider/provider.dart';

class CertificateInfo extends StatefulWidget {
  const CertificateInfo({Key? key}) : super(key: key);

  @override
  _CertificateInfoState createState() => _CertificateInfoState();
}

class _CertificateInfoState extends State<CertificateInfo> {
  TrainerCertificate? certificate;
  int? index;
  late TrainerProvider trainerProvider;

  bool showList = true;

  @override
  void initState() {
    // TODO: implement initState
    trainerProvider = Provider.of<TrainerProvider>(context, listen: false);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return StickyHeaderForm(
      title: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          FittedBox(
            child: Text(
              AppLocalizations.of(context)!.certificates,
              style: Theme.of(context).textTheme.headline6!.copyWith(
                    color: Colors.white,
                  ),
            ),
          ),
          IconButton(
              onPressed: () {
                showList = !showList;
                certificate = TrainerCertificate();
                index = -1;
                setState(() {});
              },
              icon: Icon(
                showList ? Icons.add_circle_outline : Icons.view_list_sharp,
                color: whiteColor,
              ))
        ],
      ),
      body: showList
          ? CertificateListInfo(
              displayContact: displayContact,
            )
          : CertificationForm(certificate: certificate!, index: index!),
    );
  }

  void displayContact(TrainerCertificate? selectedCertificate, int index) {
    showList = false;
    certificate = selectedCertificate;
    this.index = index;
    setState(() {});
  }
}
