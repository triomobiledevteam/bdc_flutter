import 'package:bdc/Screens/Forms/trainer_form/trainer_certificate_form/certificate_card.dart';
import 'package:bdc/core/modal/trainer_modal/trainer_certificate.dart';
import 'package:bdc/core/provider/trainer_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class CertificateListInfo extends StatefulWidget {
  const CertificateListInfo({Key? key, required this.displayContact})
      : super(key: key);
  final void Function(TrainerCertificate, int) displayContact;

  @override
  _CertificateListInfoState createState() => _CertificateListInfoState();
}

class _CertificateListInfoState extends State<CertificateListInfo> {
  @override
  void initState() {
    // TODO: implement initState

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: const EdgeInsets.symmetric(
          vertical: 5,
        ),
        child: Consumer<TrainerProvider>(
          builder: (context, trainerProvider, child) {
            return ListView.builder(
                itemCount: trainerProvider.trainer.certificate.length,
                itemBuilder: (context, index) => CertificateCard(
                    key: UniqueKey(),
                    onTap: () {
                      widget.displayContact(
                          trainerProvider.trainer.certificate[index], index);
                    },
                    certificate: trainerProvider.trainer.certificate[index]));
          },
        ));
  }
}
