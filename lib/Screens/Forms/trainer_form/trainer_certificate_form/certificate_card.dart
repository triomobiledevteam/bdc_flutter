import 'package:bdc/core/modal/Lookups.dart';
import 'package:bdc/core/modal/constraints/global_values.dart';
import 'package:bdc/core/theme/app_Theme.dart';
import 'package:flutter/material.dart';

import '../../../../core/modal/trainer_modal/trainer_certificate.dart';

class CertificateCard extends StatefulWidget {
  const CertificateCard({Key? key, this.certificate, required this.onTap})
      : super(key: key);
  final TrainerCertificate? certificate;
  final void Function() onTap;

  @override
  _CertificateCardState createState() => _CertificateCardState();
}

class _CertificateCardState extends State<CertificateCard> {
  @override
  void initState() {
    // TODO: implement initState
    getData();
    super.initState();
  }

  LookUps skillLevel = LookUps();

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: widget.onTap,
      child: Container(
        decoration: BoxDecoration(
          color: kBackgroundColor,
          borderRadius: BorderRadius.circular(12),
        ),
        margin: const EdgeInsets.symmetric(vertical: 5),
        padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 5),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Row(
              children: [
                const Icon(Icons.person),
                Text(
                  widget.certificate!.descriptionEn!,
                  style: basicTextStyle,
                ),
              ],
            ),
            Row(
              children: [
                const Icon(Icons.elevator),
                Text(
                  skillLevel.descriptionEn!,
                  style: basicTextStyle,
                ),
              ],
            )
          ],
        ),
      ),
    );
  }

  getData() async {
    skillLevel = await databaseClient
        .getWhereJobRole(widget.certificate!.skillLevelID.toString());
    setState(() {});
  }
}
