import 'package:bdc/components/form_components/FormsbottomButton.dart';
import 'package:bdc/components/form_components/TextFormWithPadding.dart';
import 'package:bdc/core/modal/Lookups.dart';
import 'package:bdc/core/modal/constraints/LookUpTypes.dart';
import 'package:bdc/core/modal/trainer_modal/trainer_certificate.dart';
import 'package:bdc/core/provider/trainer_provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:provider/provider.dart';

class CertificationForm extends StatefulWidget {
  const CertificationForm(
      {Key? key, required this.certificate, required this.index})
      : super(key: key);
  final TrainerCertificate certificate;
  final int index;

  @override
  _CertificationFormState createState() => _CertificationFormState();
}

class _CertificationFormState extends State<CertificationForm> {
  late TextEditingController nameArController;
  late TextEditingController nameEnController;
  late TextEditingController skillLevelController;
  late TextEditingController experienceYearsController;
  late TextEditingController lastUsedController;

  TrainerCertificate? certificate;

  late TrainerProvider trainerProvider;

  @override
  void initState() {
    // TODO: implement initState
    trainerProvider = Provider.of<TrainerProvider>(context, listen: false);
    declareValue();

    nameArController.addListener(() {
      certificate!.descriptionAr = nameArController.text;
    });

    nameEnController.addListener(() {
      certificate!.descriptionEn = nameEnController.text;
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        margin: const EdgeInsets.symmetric(
          vertical: 5,
        ),
        child: Column(
          children: [
            TextFormWithPadding(
              labelText: AppLocalizations.of(context)!.descriptionAr,
              controller: nameArController,
            ),
            TextFormWithPadding(
              labelText: AppLocalizations.of(context)!.descriptionEn,
              controller: nameEnController,
            ),
            TextFormWithPadding(
              labelText: AppLocalizations.of(context)!.skillsLevel,
              controller: skillLevelController,
              recordID: certificate!.skillLevelID.toString(),
              readOnly: true,
              getData: getSkillsLevel,
              isDefaultDialog: true,
              dialogType: LookUpTypes.skillsLevel,
            ),
            TextFormWithPadding(
              labelText: AppLocalizations.of(context)!.experienceYears,
              controller: experienceYearsController,
              recordID: certificate!.experienceYearsID.toString(),
              readOnly: true,
              getData: getExperienceYear,
              isDefaultDialog: true,
              dialogType: LookUpTypes.experienceYears,
            ),
            TextFormWithPadding(
              labelText: AppLocalizations.of(context)!.lastUsed,
              controller: lastUsedController,
              recordID: certificate!.skillLevelID.toString(),
              readOnly: true,
              getData: getLastUSed,
              isDefaultDialog: true,
              dialogType: LookUpTypes.lastUsed,
            ),
            FormsBottomButton(
              text: AppLocalizations.of(context)!.add,
              onPressed: () {
                TrainerCertificate certificateInfo = certificate!;
                trainerProvider.addCertificate(certificateInfo, widget.index);
                clear();
              },
            )
          ],
        ),
      ),
    );
  }

  getSkillsLevel(LookUps lookUp) {
    certificate!.skillLevelID = int.parse(lookUp.iD!);
  }

  getExperienceYear(LookUps lookUp) {
    certificate!.experienceYearsID = int.parse(lookUp.iD!);
  }

  getLastUSed(LookUps lookUp) {
    certificate!.lastUsedID = int.parse(lookUp.iD!);
  }

  declareValue() {
    certificate = widget.certificate;

    nameArController =
        TextEditingController(text: certificate!.descriptionAr ?? '');
    nameEnController =
        TextEditingController(text: certificate!.descriptionEn ?? '');
    skillLevelController = TextEditingController(text: '');
    experienceYearsController = TextEditingController(text: '');
    lastUsedController = TextEditingController(text: '');
  }

  clear() {
    certificate = TrainerCertificate();
    nameArController.clear();
    nameEnController.clear();
    skillLevelController.clear();
    experienceYearsController.clear();
    lastUsedController.clear();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    nameArController.dispose();
    nameEnController.dispose();
    skillLevelController.dispose();
    experienceYearsController.dispose();
    lastUsedController.dispose();
    super.dispose();
  }
}
