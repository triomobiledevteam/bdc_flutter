import 'package:bdc/components/form_components/StickyHeaderForm.dart';
import 'package:bdc/components/form_components/TextFormWithPadding.dart';
import 'package:bdc/core/modal/Lookups.dart';
import 'package:bdc/core/modal/constraints/LookUpTypes.dart';
import 'package:bdc/core/provider/trainer_provider.dart';
import 'package:bdc/core/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:provider/provider.dart';

import '../../../core/utils/string_validation_extension.dart';

class TrainerMainInfo extends StatefulWidget {
  const TrainerMainInfo({Key? key}) : super(key: key);

  @override
  _TrainerMainInfoState createState() => _TrainerMainInfoState();
}

class _TrainerMainInfoState extends State<TrainerMainInfo> {
  late TrainerProvider trainerProvider;

  late TextEditingController nameArController;
  late TextEditingController nameEnController;
  late TextEditingController emailController;
  late TextEditingController trainingHoursController;
  late TextEditingController majorController;
  late TextEditingController genderController;
  late TextEditingController eduLevelController;
  late TextEditingController countryController;
  late TextEditingController phoneController;
  late TextEditingController governorateController;

  @override
  void initState() {
    // TODO: implement initState
    trainerProvider = Provider.of<TrainerProvider>(context, listen: false);

    declareValue();

    nameArController.addListener(() {
      trainerProvider.trainer.trainerNameAr = nameArController.text;
      trainerProvider.percentageOfCompletion();
    });
    nameEnController.addListener(() {
      trainerProvider.trainer.trainerNameEn = nameEnController.text;
      trainerProvider.percentageOfCompletion();
    });
    emailController.addListener(() {
      trainerProvider.trainer.email = emailController.text;
      trainerProvider.percentageOfCompletion();
    });

    trainingHoursController.addListener(() {
      trainerProvider.trainer.trainingHours =
          parseInt(trainingHoursController.text);
    });

    phoneController.addListener(() {
      trainerProvider.trainer.mobilePhone = phoneController.text;
      trainerProvider.percentageOfCompletion();
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return StickyHeaderForm(
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            FittedBox(
              child: Text(
                AppLocalizations.of(context)!.main,
                style: Theme.of(context).textTheme.headline6!.copyWith(
                      color: Colors.white,
                    ),
              ),
            ),
          ],
        ),
        body: SingleChildScrollView(
            child: Container(
          margin: const EdgeInsets.symmetric(
            vertical: 5,
          ),
          child:
              Consumer<TrainerProvider>(builder: (context, participant, child) {
            return Column(
              children: [
                TextFormWithPadding(
                  labelText: AppLocalizations.of(context)!.institutionNameEn,
                  controller: nameEnController,
                  errorText: "trainer Name can't be empty",
                  errorFlag: trainerProvider.errorFlag,
                ),

                TextFormWithPadding(
                  labelText: AppLocalizations.of(context)!.institutionNameAr,
                  controller: nameArController,
                  errorText: "trainer Name can't be empty",
                  errorFlag: trainerProvider.errorFlag,
                ),

                TextFormWithPadding(
                  labelText: AppLocalizations.of(context)!.email,
                  errorText: AppLocalizations.of(context)!.invalidEmail,
                  controller: emailController,
                ),
                //TODO:Dialog

                TextFormWithPadding(
                  labelText: AppLocalizations.of(context)!.majorID,
                  controller: majorController,
                  recordID: trainerProvider.trainer.majorID.toString(),
                  readOnly: true,
                  getData: getMajor,
                  isDefaultDialog: true,
                  dialogType: LookUpTypes.major,
                ),
                TextFormWithPadding(
                  labelText: AppLocalizations.of(context)!.countryID,
                  controller: countryController,
                  readOnly: true,
                  getData: getCountry,
                  isDefaultDialog: true,
                  recordID: trainerProvider.trainer.countryID,
                  dialogType: LookUpTypes.country,
                ),

                TextFormWithPadding(
                  labelText: AppLocalizations.of(context)!.mobile,
                  errorText: AppLocalizations.of(context)!.invalidMobile,
                  controller: phoneController,
                  keyboardType: TextInputType.phone,
                  validator: (value) {
                    if (!isValidMobile(value ?? '')) {
                      return AppLocalizations.of(context)!.invalidEmail;
                    }
                    return null;
                  },
                  errorFlag: trainerProvider.errorFlag,
                ),

                TextFormWithPadding(
                  labelText: AppLocalizations.of(context)!.cityID,
                  controller: governorateController,
                  readOnly: true,
                  recordID: trainerProvider.trainer.cityID.toString(),
                  getData: getCity,
                  isDefaultDialog: true,
                  dialogType: LookUpTypes.city,
                ),
                TextFormWithPadding(
                  labelText: AppLocalizations.of(context)!.genderID,
                  controller: genderController,
                  readOnly: true,
                  recordID: trainerProvider.trainer.genderID.toString(),
                  getData: getGender,
                  isDefaultDialog: true,
                  dialogType: LookUpTypes.gender,
                ),
                TextFormWithPadding(
                  labelText: AppLocalizations.of(context)!.educationalLevel,
                  controller: eduLevelController,
                  errorFlag: trainerProvider.errorFlag,
                  errorText: AppLocalizations.of(context)!.thisFieldIsEmpty,
                  readOnly: true,
                  recordID:
                      trainerProvider.trainer.educationalLevelID.toString(),
                  getData: getEduLevel,
                  isDefaultDialog: true,
                  dialogType: LookUpTypes.eduLevel,
                ),
              ],
            );
          }),
        )));
  }

  getMajor(LookUps lookUp) {
    trainerProvider.trainer.majorID = int.parse(lookUp.iD!);
    trainerProvider.percentageOfCompletion();
  }

  getCity(LookUps lookUp) {
    trainerProvider.trainer.cityID = parseInt(lookUp.iD!);
    trainerProvider.percentageOfCompletion();
  }

  getEduLevel(LookUps lookUp) {
    trainerProvider.trainer.educationalLevelID = parseInt(lookUp.iD!);
    trainerProvider.percentageOfCompletion();
  }

  getGender(LookUps lookUp) {
    trainerProvider.trainer.genderID = parseInt(lookUp.iD!);
    trainerProvider.percentageOfCompletion();
  }

  getCountry(LookUps lookUp) {
    trainerProvider.trainer.countryID = lookUp.iD;
    trainerProvider.percentageOfCompletion();
  }

  declareValue() {
    nameArController = TextEditingController(
        text: trainerProvider.trainer.trainerNameAr ?? '');
    nameEnController = TextEditingController(
        text: trainerProvider.trainer.trainerNameEn ?? '');
    trainingHoursController = TextEditingController(
        text: trainerProvider.trainer.trainingHours.toString());
    emailController =
        TextEditingController(text: trainerProvider.trainer.email ?? '');

    countryController = TextEditingController(text: '');
    phoneController =
        TextEditingController(text: trainerProvider.trainer.mobilePhone ?? '');

    majorController = TextEditingController(text: '');

    governorateController = TextEditingController(text: '');

    genderController = TextEditingController(text: '');
    countryController = TextEditingController(text: '');
    eduLevelController = TextEditingController(text: '');
  }

  clear() {
    nameArController.clear();
    nameEnController.clear();
    emailController.clear();
    trainingHoursController.clear();
    majorController.clear();
    genderController.clear();
    eduLevelController.clear();
    countryController.clear();
    phoneController.clear();
    governorateController.clear();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    nameArController.dispose();
    nameEnController.dispose();
    emailController.dispose();
    trainingHoursController.dispose();
    majorController.dispose();
    genderController.dispose();
    eduLevelController.dispose();
    countryController.dispose();
    phoneController.dispose();
    governorateController.dispose();
    super.dispose();
  }
}
