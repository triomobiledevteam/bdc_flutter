import 'package:bdc/components/buttons/bottom_button.dart';
import 'package:bdc/components/tabs/virtical_tabs.dart';
import 'package:bdc/core/helper/error_handler/errorhandler.dart';
import 'package:bdc/core/modal/LoginModal/Trainer.dart';
import 'package:bdc/core/provider/trainer_provider.dart';
import 'package:bdc/core/theme/app_Theme.dart';
import 'package:bdc/core/theme/sizeConfig.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';
import 'package:provider/provider.dart';

import '../../../core/utils/Navigation.dart';
import '../TabList.dart';

class TrainerInfoScreen extends StatefulWidget {
  const TrainerInfoScreen({Key? key}) : super(key: key);

  @override
  _TrainerInfoScreenState createState() => _TrainerInfoScreenState();
}

class _TrainerInfoScreenState extends State<TrainerInfoScreen> {
  late TrainerProvider trainerProvider;

  @override
  void initState() {
    // TODO: implement initState
    trainerProvider = Provider.of<TrainerProvider>(context, listen: false);
    trainerProvider.setTrainer(mainTrainer!);

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      resizeToAvoidBottomInset: false,
      bottomNavigationBar: BottomButton(
        text: AppLocalizations.of(context)!.edit,
        color: kPrimaryColorGreen,
        onPressed: () async {
          await trainerProvider.checkTrainer(onError, (value) {
            ErrorHandler.onSuccessSnackBar('Done', context);
            Navigation.back(context);
          }, context);
        },
      ),
      appBar: AppBar(
        backgroundColor: kPrimaryColor,
        title: Text(AppLocalizations.of(context)!.edit),
      ),
      body: Column(
        children: [
          Container(
            padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 5),
            child: Consumer<TrainerProvider>(
                builder: (context, trainerProvider, child) =>
                    LinearPercentIndicator(
                      animation: true,
                      lineHeight: SizeConfig.height! * 0.02,
                      animationDuration: 300,
                      percent:
                          trainerProvider.trainer.percentageOfCompletion / 100,
                      center: Text(
                          "${trainerProvider.trainer.percentageOfCompletion}%"),
                      barRadius: const Radius.circular(20),
                      progressColor: Colors.green,
                    )),
          ),
          Expanded(
            child: Consumer<TrainerProvider>(
              builder: (context, trainerProvider, child) => SizedBox(
                height: SizeConfig.height,
                child: VerticalTabs(
                  tabsElevation: 10,
                  indicatorColor: kPrimaryColorGreen,
                  tabTextStyle:
                      TextStyle(fontSize: SizeConfig.blockSizeVertical! * 2),
                  tabsWidth: SizeConfig.width! * 0.12,
                  tabs: trainerFormTabs,
                  contents: trainerFormWidgets,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  onError(String massage) {
    Navigation.back(context);
    ErrorHandler.errorMassage = massage;
    ErrorHandler.showErrorMassageDialog(context);
  }
}
