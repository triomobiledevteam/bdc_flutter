import 'package:bdc/Screens/Forms/trainer_form/trainer_experience_form/experience_card.dart';
import 'package:bdc/core/provider/trainer_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../../core/modal/trainer_modal/trainer_experience.dart';

class ExperienceListInfo extends StatefulWidget {
  const ExperienceListInfo({Key? key, required this.displayExperience})
      : super(key: key);
  final void Function(TrainerExperience, int) displayExperience;

  @override
  _ExperienceListInfoState createState() => _ExperienceListInfoState();
}

class _ExperienceListInfoState extends State<ExperienceListInfo> {
  TrainerExperience? experience;

  @override
  void initState() {
    // TODO: implement initState

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: const EdgeInsets.symmetric(
          vertical: 5,
        ),
        child: Consumer<TrainerProvider>(
          builder: (context, trainerProvider, child) {
            return ListView.builder(
                itemCount: trainerProvider.trainer.experience.length,
                itemBuilder: (context, index) => ExperienceCard(
                    onTap: () {
                      widget.displayExperience(
                          trainerProvider.trainer.experience[index], index);
                    },
                    key: UniqueKey(),
                    experience: trainerProvider.trainer.experience[index]));
          },
        ));
  }
}
