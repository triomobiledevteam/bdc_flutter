import 'package:bdc/core/theme/app_Theme.dart';
import 'package:flutter/material.dart';

import '../../../../core/modal/trainer_modal/trainer_experience.dart';

class ExperienceCard extends StatefulWidget {
  const ExperienceCard({Key? key, this.experience, required this.onTap})
      : super(key: key);
  final TrainerExperience? experience;
  final void Function() onTap;
  @override
  _ExperienceCardState createState() => _ExperienceCardState();
}

class _ExperienceCardState extends State<ExperienceCard> {
  @override
  void initState() {
    // TODO: implement initState
    // getData();
    super.initState();
  }

  // LookUps city = LookUps();

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: widget.onTap,
      child: Container(
        decoration: BoxDecoration(
          color: kBackgroundColor,
          borderRadius: BorderRadius.circular(12),
        ),
        margin: const EdgeInsets.symmetric(vertical: 5),
        padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 5),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Row(
              children: [
                const Icon(Icons.work),
                Text(
                  widget.experience!.descriptionEn!,
                  style: basicTextStyle,
                ),
              ],
            ),
            Row(
              children: [
                const Icon(Icons.business_rounded),
                Text(
                  widget.experience!.companyName!,
                  style: basicTextStyle,
                ),
              ],
            )
          ],
        ),
      ),
    );
  }

  getData() async {
    // city = await _db.getWhereCity(widget.experirence!.branchCity ?? '');
    setState(() {});
  }
}
