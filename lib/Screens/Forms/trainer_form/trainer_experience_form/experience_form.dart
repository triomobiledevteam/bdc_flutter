import 'package:bdc/components/form_components/FormsbottomButton.dart';
import 'package:bdc/components/form_components/TextFormWithPadding.dart';
import 'package:bdc/core/modal/Lookups.dart';
import 'package:bdc/core/modal/constraints/LookUpTypes.dart';
import 'package:bdc/core/provider/trainer_provider.dart';
import 'package:bdc/core/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

import '../../../../components/dialog/map_dialog.dart';
import '../../../../core/modal/trainer_modal/trainer_experience.dart';
import '../../../map_layout/bloc/map_layout_cubit.dart';

class ExperienceForm extends StatefulWidget {
  const ExperienceForm(
      {Key? key, required this.experience, required this.index})
      : super(key: key);
  final TrainerExperience experience;
  final int index;

  @override
  _ExperienceFormState createState() => _ExperienceFormState();
}

class _ExperienceFormState extends State<ExperienceForm> {
  late TrainerProvider trainerProvider;

  // descriptionAr
  // descriptionEn
  // startDate
  // endDate
  // address
  // companyIndustry
  // companyName

  late TextEditingController descriptionEnController;
  late TextEditingController descriptionArController;
  late TextEditingController startDateController;
  late TextEditingController endDateController;
  late TextEditingController addressController;
  late TextEditingController companyIndustryController;
  late TextEditingController companyNameController;

  late TrainerExperience experience;
  bool showList = true;

  @override
  void initState() {
    // TODO: implement initState

    experience = widget.experience;
    trainerProvider = Provider.of<TrainerProvider>(context, listen: false);
    declareValue();

    // descriptionEnController
    // descriptionArController
    // startDateController
    // endDateController
    // bAddressController
    // companyIndustryController
    // companyNameController

    descriptionEnController.addListener(() {
      experience.descriptionEn = descriptionEnController.text;
    });
    descriptionArController.addListener(() {
      experience.descriptionAr = descriptionArController.text;
    });
    startDateController.addListener(() {
      experience.startDate = startDateController.text;
    });
    endDateController.addListener(() {
      experience.endDate = endDateController.text;
    });

    addressController.addListener(() {
      experience.address = addressController.text;
    });

    companyNameController.addListener(() {
      experience.companyName = companyNameController.text;
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        margin: const EdgeInsets.symmetric(
          vertical: 5,
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            //TODO:Dialog
            TextFormWithPadding(
              labelText: AppLocalizations.of(context)!.descriptionAr,
              controller: descriptionArController,
            ),
            TextFormWithPadding(
              labelText: AppLocalizations.of(context)!.descriptionEn,
              controller: descriptionEnController,
            ),

            TextFormWithPadding(
              labelText: AppLocalizations.of(context)!.startDate,
              controller: startDateController,
              getDate: getStartDate,
              readOnly: true,
              isDateDialog: true,
            ),
            TextFormWithPadding(
              labelText: AppLocalizations.of(context)!.endDate,
              controller: endDateController,
              getDate: getEndDate,
              readOnly: true,
              isDateDialog: true,
            ),

            TextFormWithPadding(
              labelText: AppLocalizations.of(context)!.address,
              keyboardType: TextInputType.streetAddress,
              controller: addressController,
              prefixIcon: IconButton(
                icon: const Icon(Icons.pin_drop_rounded),
                onPressed: () {},
              ),
              suffixIcon: IconButton(
                icon: const Icon(Icons.pin_drop_rounded),
                onPressed: () async {
                  await MapLayoutCubit.get(context).getCurrentLocation();
                  MapDialog.show(context, (value) {
                    setState(() {
                      addressController = TextEditingController(text: value);
                      experience.address = addressController.text;
                    });
                  });
                },
              ),
            ),
            TextFormWithPadding(
              labelText: AppLocalizations.of(context)!.companyIndustry,
              controller: companyIndustryController,
              readOnly: true,
              recordID: experience.companyIndustry.toString(),
              getData: getCompanyIndustry,
              isDefaultDialog: true,
              dialogType: LookUpTypes.industry,
            ),
            TextFormWithPadding(
              labelText: AppLocalizations.of(context)!.companyName,
              controller: companyNameController,
            ),
            FormsBottomButton(
              text: AppLocalizations.of(context)!.add,
              onPressed: () {
                TrainerExperience branchInfo = experience;
                trainerProvider.addExperience(branchInfo, widget.index);
                clear();
              },
            )
          ],
        ),
      ),
    );
  }

  getEndDate(String date) {
    experience.endDate = date;
  }

  getStartDate(String date) {
    experience.startDate = date;
  }

  getCompanyIndustry(LookUps lookUps) {
    experience.companyIndustry = parseInt(lookUps.iD!);
  }

  declareValue() {
    descriptionEnController =
        TextEditingController(text: experience.descriptionEn ?? '');
    descriptionArController =
        TextEditingController(text: experience.descriptionAr ?? '');
    startDateController = TextEditingController(
        text: experience.startDate!.isNotEmpty
            ? DateFormat.yMMMd().format(DateTime.parse(experience.startDate!))
            : '');
    endDateController = TextEditingController(
        text: experience.endDate!.isNotEmpty
            ? DateFormat.yMMMd().format(DateTime.parse(experience.endDate!))
            : '');
    addressController = TextEditingController(text: experience.address!);
    companyIndustryController = TextEditingController(text: '');
    companyNameController =
        TextEditingController(text: experience.companyName ?? '');
  }

  clear() {
    experience = TrainerExperience();

    descriptionEnController.clear();
    descriptionArController.clear();
    startDateController.clear();
    endDateController.clear();
    addressController.clear();
    companyIndustryController.clear();
    companyNameController.clear();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    descriptionEnController.dispose();
    descriptionArController.dispose();
    startDateController.dispose();
    endDateController.dispose();
    addressController.dispose();
    companyIndustryController.dispose();
    companyNameController.dispose();

    super.dispose();
  }
}
