import 'package:bdc/components/form_components/StickyHeaderForm.dart';
import 'package:bdc/core/modal/trainer_modal/trainer_experience.dart';
import 'package:bdc/core/provider/trainer_provider.dart';
import 'package:bdc/core/theme/app_Theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:provider/provider.dart';

import 'experience_form.dart';
import 'experience_list.dart';

class ExperienceInfo extends StatefulWidget {
  const ExperienceInfo({Key? key}) : super(key: key);

  @override
  _ExperienceInfoState createState() => _ExperienceInfoState();
}

class _ExperienceInfoState extends State<ExperienceInfo> {
  late TrainerProvider trainerProvider;
  int? index;
  TrainerExperience? experience;
  bool showList = true;

  @override
  void initState() {
    // TODO: implement initState
    trainerProvider = Provider.of<TrainerProvider>(context, listen: false);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return StickyHeaderForm(
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            FittedBox(
              child: Text(
                AppLocalizations.of(context)!.experience,
                style: Theme.of(context).textTheme.headline6!.copyWith(
                      color: Colors.white,
                    ),
              ),
            ),
            IconButton(
                onPressed: () {
                  showList = !showList;
                  experience = TrainerExperience();
                  index = -1;
                  setState(() {});
                },
                icon: Icon(
                  showList ? Icons.add_circle_outline : Icons.view_list_sharp,
                  color: whiteColor,
                ))
          ],
        ),
        body: showList
            ? ExperienceListInfo(
                displayExperience: displayExperience,
              )
            : ExperienceForm(
                experience: experience!,
                index: index!,
              ));
  }

  void displayExperience(TrainerExperience? selectedExperience, int index) {
    showList = false;
    experience = selectedExperience;
    this.index = index;
    setState(() {});
  }

  @override
  void dispose() {
    super.dispose();
  }
}
