import 'package:bdc/Screens/Forms/ApplicationForm.dart';
import 'package:bdc/Screens/Forms/CompanyForm/Branch/BranchInfo.dart';
import 'package:bdc/Screens/Forms/CompanyForm/Contact/ContactInfo.dart';
import 'package:bdc/Screens/Forms/CompanyForm/GeneralInfo.dart';
import 'package:bdc/Screens/Forms/CompanyForm/JobOpportunity/JobOpportunity.dart';
import 'package:bdc/Screens/Forms/CompanyForm/MainCompanyInfo.dart';
import 'package:bdc/Screens/Forms/ParticipantForm/Education.dart';
import 'package:bdc/Screens/Forms/ParticipantForm/Experience/Experience.dart';
import 'package:bdc/Screens/Forms/ParticipantForm/FamilyInfo/FamilyInfo.dart';
import 'package:bdc/Screens/Forms/ParticipantForm/GeneralInfo.dart';
import 'package:bdc/Screens/Forms/ParticipantForm/MainUserInfo.dart';
import 'package:bdc/core/modal/constraints/FormType.dart';
import 'package:bdc/core/modal/constraints/global_values.dart';
import 'package:bdc/core/modal/enums/UsersType.dart';
import 'package:bdc/core/theme/sizeConfig.dart';
import 'package:flutter/material.dart';

import 'trainer_form/main_trainer_info.dart';
import 'trainer_form/trainer_certificate_form/certificate_info.dart';
import 'trainer_form/trainer_experience_form/experience_info.dart';

//Participant
List<Widget> get userFormWidgets => [
      const MainUser(),
      const EducationUser(),
      const GeneralInfoUser(),
      const FamilyInfoUser(),
      const UserExperienceInfo(),
      const ApplicationFormUser(
        formType: FormType.participant,
      ),
      // FinishUser(),
    ];

List<Tab> get userFormTabs => [
      Tab(
          child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 5),
        child: Center(
            child: Image.asset(
          'assets/main.png',
          width: SizeConfig.width! * 0.07,
        )),
      )),
      Tab(
          child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 5),
        child: Center(
            child: Image.asset(
          'assets/education.png',
          width: SizeConfig.width! * 0.07,
        )),
      )),
      Tab(
          child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 5),
        child: Center(
            child: Image.asset(
          'assets/general.png',
          width: SizeConfig.width! * 0.07,
        )),
      )),
      Tab(
          child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 5),
        child: Center(
            child: Image.asset(
          'assets/family.png',
          width: SizeConfig.width! * 0.07,
        )),
      )),
      Tab(
          child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 5),
        child: Center(
            child: Image.asset(
          'assets/experience.png',
          width: SizeConfig.width! * 0.07,
        )),
      )),
      Tab(
          child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 5),
        child: Center(
            child: Image.asset(
          'assets/form.png',
          width: SizeConfig.width! * 0.07,
        )),
      )),
    ];

//Employee
List<Widget> get companyFormWidgets => [
      const MainCompanyInfo(),
      const GeneralCompanyInfo(),
      const BranchInfo(),
      const ContactInfo(),
      if (userType != UserType.CBO.name)
        const ApplicationFormUser(
          formType: FormType.institution,
        ),
      if (userType != UserType.CBO.name) const JobOpportunityInfo()
    ];

List<Tab> get companyFormTabs => [
      Tab(
          child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 5),
        child: Center(
            child: Image.asset(
          'assets/main.png',
          width: SizeConfig.width! * 0.07,
        )),
      )),
      Tab(
          child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 5),
        child: Center(
            child: Image.asset(
          'assets/company.png',
          width: SizeConfig.width! * 0.07,
        )),
      )),
      Tab(
          child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 5),
        child: Center(
            child: Image.asset(
          'assets/branches.png',
          width: SizeConfig.width! * 0.07,
        )),
      )),
      Tab(
          child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 5),
        child: Center(
            child: Image.asset(
          'assets/contact.png',
          width: SizeConfig.width! * 0.07,
        )),
      )),
      if (userType != UserType.CBO.name)
        Tab(
            child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 5),
          child: Center(
              child: Image.asset(
            'assets/employee.png',
            width: SizeConfig.width! * 0.07,
          )),
        )),
      if (userType != UserType.CBO.name)
        Tab(
            child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 5),
          child: Center(
              child: Image.asset(
            'assets/job.png',
            width: SizeConfig.width! * 0.07,
          )),
        )),
    ];

//HomeTab

List<Tab> get trainerFormTabs => [
      Tab(
          child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 5),
        child: Center(
            child: Image.asset(
          'assets/main.png',
          width: SizeConfig.width! * 0.07,
        )),
      )),
      Tab(
          child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 5),
        child: Center(
            child: Image.asset(
          'assets/certificate.png',
          width: SizeConfig.width! * 0.07,
        )),
      )),
      Tab(
          child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 5),
        child: Center(
            child: Image.asset(
          'assets/experience.png',
          width: SizeConfig.width! * 0.07,
        )),
      )),
    ];

List<Widget> get trainerFormWidgets => [
      const TrainerMainInfo(),
      const CertificateInfo(),
      const ExperienceInfo(),
    ];
