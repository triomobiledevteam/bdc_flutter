import 'package:bdc/components/form_components/StickyHeaderForm.dart';
import 'package:bdc/components/form_components/YesNoQuestionTemplete.dart';
import 'package:bdc/core/modal/constraints/FormType.dart';
import 'package:bdc/core/provider/CompanyProvider.dart';
import 'package:bdc/core/provider/ParticipantProvider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:provider/provider.dart';

class ApplicationFormUser extends StatefulWidget {
  const ApplicationFormUser({Key? key, this.formType}) : super(key: key);
  final String? formType;

  @override
  _ApplicationFormUserState createState() => _ApplicationFormUserState();
}

class _ApplicationFormUserState extends State<ApplicationFormUser> {
  late CompanyProvider companyProvider;
  late ParticipantProvider participantProvider;

  @override
  void initState() {
    // TODO: implement initState
    // print(widget.formType);
    if (widget.formType == FormType.institution) {
      companyProvider = Provider.of<CompanyProvider>(context, listen: false);
    } else if (widget.formType == FormType.participant) {
      participantProvider =
          Provider.of<ParticipantProvider>(context, listen: false);
    }

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return StickyHeaderForm(
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            FittedBox(
              child: Text(
                widget.formType == FormType.institution
                    ? AppLocalizations.of(context)!.employeeForm
                    : AppLocalizations.of(context)!.applicationForm,
                style: Theme.of(context).textTheme.headline6!.copyWith(
                      color: Colors.white,
                    ),
              ),
            ),
          ],
        ),
        body: Container(
            margin: const EdgeInsets.symmetric(
              vertical: 5,
            ),
            child: widget.formType == FormType.institution
                ? ListView.builder(
                    addAutomaticKeepAlives: true,
                    itemBuilder: (context, index) => YesNoQuestionTemplate(
                          key: UniqueKey(),
                          applicationForm: companyProvider
                              .company.applicationFormList[index],
                        ),
                    itemCount:
                        companyProvider.company.applicationFormList.length)
                : ListView.builder(
                    addAutomaticKeepAlives: true,
                    itemBuilder: (context, index) => YesNoQuestionTemplate(
                          key: UniqueKey(),
                          applicationForm: participantProvider
                              .participant.applicationFormList[index],
                        ),
                    itemCount: participantProvider
                        .participant.applicationFormList.length)));
  }
}
