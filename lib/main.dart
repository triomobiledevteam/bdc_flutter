import 'package:bdc/Screens/forget_passowrd/bloc/otp_cubit.dart';
import 'package:bdc/Screens/map_layout/bloc/map_layout_cubit.dart';
import 'package:bdc/core/provider/programs_provider.dart';
import 'package:bdc/core/theme/app_Theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';

import 'Screens/attachment_screen/attachment/attachment_bloc.dart';
import 'Screens/splash_screen/splash_screen.dart';
import 'core/modal/constraints/global_values.dart';
import 'core/provider/CompanyProvider.dart';
import 'core/provider/ParticipantProvider.dart';
import 'core/provider/trainer_provider.dart';
import 'injection_container.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await init();

  runApp(MultiBlocProvider(
    providers: [
      BlocProvider<OtpCubit>(
        create: (context) => OtpCubit(),
      ),
      BlocProvider(
        create: (context) => MapLayoutCubit(),
      ),
      BlocProvider(
        create: (context) =>
            AttachmentBloc()..add(AttachmentInitialDatabaseEvent()),
      ),
    ],
    child: MultiProvider(providers: [
      ChangeNotifierProvider(create: (_) => CompanyProvider()),
      ChangeNotifierProvider(create: (_) => TrainerProvider()),
      ChangeNotifierProvider(create: (_) => ProgramsProvider()),
      ChangeNotifierProvider(create: (_) => ParticipantProvider()),
    ], child: const MyApp()),
  ));
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();

  static _MyAppState? of(BuildContext context) =>
      context.findAncestorStateOfType<_MyAppState>();
}

class _MyAppState extends State<MyApp> {
  late Locale _locale;

  @override
  void initState() {
    // TODO: implement initState
    switch (languageID) {
      case 1:
        print('en');
        _locale = const Locale('en', 'US');
        break;
      case 2:
        print('ar');
        _locale = const Locale('ar', '');
        break;
      default:
        _locale = const Locale('en', 'US');
        break;
    }
    super.initState();
  }

  void setLocale(Locale value) {
    setState(() {
      _locale = value;
    });
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIMode(SystemUiMode.edgeToEdge, overlays: []);

    return GestureDetector(
      onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
      child: MaterialApp(
        title: 'BDC',
        debugShowCheckedModeBanner: false,
        localizationsDelegates: AppLocalizations.localizationsDelegates,
        supportedLocales: AppLocalizations.supportedLocales,
        locale: _locale,
        theme: ThemeData(
            scaffoldBackgroundColor: mainBackgroundColor,
            appBarTheme: AppBarTheme.of(context).copyWith(
              backgroundColor: mainColorTheme,
            ),
            textTheme: GoogleFonts.robotoTextTheme(Theme.of(context).textTheme),
            colorScheme:
                ColorScheme.fromSwatch().copyWith(secondary: Colors.cyan[600])),
        home: const SplashScreen(),
      ),
    );
  }
}
