import 'package:bdc/core/theme/sizeConfig.dart';
import 'package:flutter/material.dart';

class CustomRowWithBorder extends StatelessWidget {
  final String? text_1;
  final String? text_2;
  final Color color;

  const CustomRowWithBorder(
      {Key? key, this.text_1, this.text_2, this.color = Colors.black})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(
        horizontal: SizeConfig.width! * 0.02,
        vertical: SizeConfig.height! * 0.005,
      ),
      // width: SizeConfig.width * 0.9,
      child: Row(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            '$text_1 :',
            style: TextStyle(fontSize: SizeConfig.blockSizeVertical! * 2.5),
          ),
          Container(
            padding: EdgeInsets.only(
              left: SizeConfig.width! * 0.01,
              right: SizeConfig.width! * 0.01,
            ),
            decoration: BoxDecoration(
              border: Border.all(
                color: color,
                width: 0.5,
              ),
            ),
            child: Text(
              '$text_2',
              style: TextStyle(fontSize: SizeConfig.blockSizeVertical! * 2.5),
            ),
          ),
        ],
      ),
    );
  }
}
