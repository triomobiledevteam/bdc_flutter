import 'package:bdc/core/theme/app_Theme.dart';
import 'package:bdc/core/theme/sizeConfig.dart';
import 'package:flutter/material.dart';

class FormsBottomButton extends StatelessWidget {
  const FormsBottomButton({Key? key, this.onPressed, this.text})
      : super(key: key);
  final void Function()? onPressed;
  final String? text;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: SizeConfig.height! * 0.05,
      decoration: BoxDecoration(
          color: kPrimaryColorGreen, borderRadius: BorderRadius.circular(10)),
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(backgroundColor: kPrimaryColorGreen),
        onPressed: onPressed,
        child: Center(
          child: Text(
            text!,
            style: TextStyle(
                color: Colors.white,
                fontSize: SizeConfig.blockSizeVertical! * 2,
                fontWeight: FontWeight.bold),
          ),
        ),
      ),
    );
  }
}
