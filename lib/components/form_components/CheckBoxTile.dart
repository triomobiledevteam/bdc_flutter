import 'package:bdc/core/theme/sizeConfig.dart';
import 'package:flutter/material.dart';

class CheckBoxTilWithPadding extends StatefulWidget {
  const CheckBoxTilWithPadding({Key? key, this.value, this.text, this.onchange})
      : super(key: key);
  final bool? value;
  final String? text;
  final void Function(bool?)? onchange;

  @override
  _CheckBoxTilWithPaddingState createState() => _CheckBoxTilWithPaddingState();
}

class _CheckBoxTilWithPaddingState extends State<CheckBoxTilWithPadding> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(
        vertical: SizeConfig.height! * 0.01,
      ),
      child: CheckboxListTile(
        title: Text(
          widget.text!,
          style: const TextStyle(
            fontFamily: "Poppins",
          ),
        ),
        value: widget.value,
        onChanged: widget.onchange,
      ),
    );
  }
}
