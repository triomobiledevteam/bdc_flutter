import 'package:bdc/core/theme/app_Theme.dart';
import 'package:flutter/material.dart';

class StickyHeaderForm extends StatelessWidget {
  const StickyHeaderForm({Key? key, this.title, this.body}) : super(key: key);
  final Widget? title;
  final Widget? body;

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: const EdgeInsets.symmetric(vertical: 5, horizontal: 8),
        child: Column(
          children: [
            Container(
                height: 50,
                color: stickyHeaderColor,
                padding: const EdgeInsets.symmetric(horizontal: 16),
                margin: const EdgeInsets.symmetric(vertical: 5),
                alignment: AlignmentDirectional.centerStart,
                child: title),
            Expanded(
                child: MediaQuery.removePadding(
                    removeTop: true,
                    removeBottom: true,
                    context: context,
                    child: body!)),
          ],
        ));
    // StickyHeader(
    //     overlapHeaders: true,
    //     header: Container(
    //         height: 50,
    //         color: stickyHeaderColor,
    //         padding: const EdgeInsets.symmetric(horizontal: 16),
    //         alignment: AlignmentDirectional.centerStart,
    //         child: title),
    //     content: body!));
  }
}
