import 'package:bdc/components/dialog/lookUpsWithSearch_dailog.dart';
import 'package:bdc/components/dialog/lookUps_dailog.dart';
import 'package:bdc/core/modal/Lookups.dart';
import 'package:bdc/core/modal/constraints/LookUpTypes.dart';
import 'package:bdc/core/storage/db_client.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import '../../core/modal/constraints/global_values.dart';

class TextFormWithPadding extends StatefulWidget {
  final String? labelText;
  final String dialogType;
  final bool isDefaultDialog;
  final String? recordID;
  final String errorText;
  final void Function(LookUps)? getData;
  final TextInputType? keyboardType;
  final void Function(String)? getDate;
  final String? Function(String?)? validator;
  final TextInputAction textInputAction;
  final Widget? prefix;
  final Widget? prefixIcon;
  final Widget? suffixIcon;
  final bool obscureText;
  final bool readOnly;
  final String parentRecordID;
  final int minLines;
  final int maxLines;
  final bool errorFlag;
  final TextEditingController? controller;
  final bool isDateDialog;

  const TextFormWithPadding(
      {Key? key,
      this.controller,
      this.labelText = '',
      this.prefix,
      this.prefixIcon,
      this.readOnly = false,
      this.getData,
      this.keyboardType,
      this.minLines = 1,
      this.maxLines = 1,
      this.isDateDialog = false,
      this.dialogType = '',
      this.isDefaultDialog = false,
      this.recordID = '',
      this.errorText = '',
      this.textInputAction = TextInputAction.next,
      this.errorFlag = false,
      this.getDate,
      this.obscureText = false,
      this.suffixIcon,
      this.validator,
      this.parentRecordID = ''})
      : super(key: key);

  @override
  _TextFormWithPaddingState createState() => _TextFormWithPaddingState();
}

class _TextFormWithPaddingState extends State<TextFormWithPadding> {
  DateTime? _selectedDate;
  LookUps? selectedLookup;

  final db = DatabaseClient.instance;
  List<LookUps> lookUpList = [];
  List<LookUps> tempLookUpList = [];

  @override
  void initState() {
    // TODO: implement initState
    if (widget.isDefaultDialog) {
      getDefaultData();
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // print("${widget.controller!.text} widget.errorFlag :${widget.errorFlag}");
    // print(
    //     "  widget.validator == null && widget.errorFlag :${widget.validator == null && widget.errorFlag}");
    // print("  widget.validator :${widget.validator == null}");
    //
    // print("  widget.validator :${widget.validator == null}");
    return Container(
      padding: const EdgeInsets.symmetric(
        vertical: 5,
      ),
      margin: const EdgeInsets.symmetric(
        vertical: 5,
      ),
      child: TextFormField(
        onTap: () async {
          if (widget.isDefaultDialog) {
            await defaultDialog();
          }
          if (widget.isDateDialog) {
            _selectDate(context);
          }
        },
        validator: widget.validator == null && widget.errorText.isNotEmpty
            ? (value) {
                if (value!.isEmpty) {
                  return widget.errorText;
                }
                return null;
              }
            : widget.validator != null
                ? widget.validator!
                : null,
        textInputAction: widget.textInputAction,
        controller: widget.controller,
        readOnly: widget.readOnly,
        minLines: widget.minLines,
        maxLines: widget.maxLines,
        obscureText: widget.obscureText,
        decoration: InputDecoration(
          labelText: widget.labelText,
          fillColor: Colors.white,
          prefix: widget.prefix,
          suffixIcon: widget.suffixIcon,
          prefixIcon: widget.prefixIcon,
          // errorText: widget.errorText.isNotEmpty &&
          //         widget.controller!.text.isEmpty &&
          //         widget.errorFlag
          //     ? widget.errorText
          //     : null,
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10.0),
            borderSide: const BorderSide(),
          ),
        ),
        keyboardType: widget.keyboardType,
        style: const TextStyle(
          fontFamily: "Poppins",
        ),
      ),
    );
  }

  _selectDate(BuildContext context) async {
    DateTime? newSelectedDate = await showDatePicker(
      context: context,
      initialDate: _selectedDate != null ? _selectedDate! : DateTime.now(),
      firstDate: DateTime(1920),
      lastDate: DateTime(2091),
    );

    if (newSelectedDate != null) {
      _selectedDate = newSelectedDate;
      widget.getDate!(_selectedDate.toString());
      widget.controller!
        ..text = DateFormat.yMMMd().format(_selectedDate!)
        ..selection = TextSelection.fromPosition(TextPosition(
            offset: widget.controller!.text.length,
            affinity: TextAffinity.upstream));
    }
  }

  defaultDialog() async {
    lookUpList = [];
    tempLookUpList = [];

    if (widget.dialogType == LookUpTypes.gender) {
      lookUpList.addAll(await db.getGender());
    } else if (widget.dialogType == LookUpTypes.marital) {
      lookUpList.addAll(await db.getMarital());
    } else if (widget.dialogType == LookUpTypes.eduLevel) {
      lookUpList.addAll(await db.getEduLevel());
    } else if (widget.dialogType == LookUpTypes.nationality) {
      lookUpList.addAll(await db.getNationality());
    } else if (widget.dialogType == LookUpTypes.sector) {
      lookUpList.addAll(await db.getSector());
    } else if (widget.dialogType == LookUpTypes.industry) {
      lookUpList.addAll(await db.getIndustry());
    } else if (widget.dialogType == LookUpTypes.country) {
      lookUpList.addAll(await db.getCountry());
    } else if (widget.dialogType == LookUpTypes.city) {
      lookUpList.addAll(await db.getCity());
    } else if (widget.dialogType == LookUpTypes.religion) {
      lookUpList.addAll(await db.getReligion());
    } else if (widget.dialogType == LookUpTypes.major) {
      lookUpList.addAll(await db.getMajor());
    } else if (widget.dialogType == LookUpTypes.jobRole) {
      lookUpList.addAll(await db.getJobRole());
    } else if (widget.dialogType == LookUpTypes.recruitment) {
      lookUpList.addAll(await db.getRecruitment());
    } else if (widget.dialogType == LookUpTypes.contractTypes) {
      lookUpList.addAll(await db.getContactTypes());
    } else if (widget.dialogType == LookUpTypes.skillsLevel) {
      lookUpList.addAll(await db.getSkillsLevel());
    } else if (widget.dialogType == LookUpTypes.experienceYears) {
      lookUpList.addAll(await db.getExperienceYears());
    } else if (widget.dialogType == LookUpTypes.lastUsed) {
      lookUpList.addAll(await db.getLastUsed());
    }
    if (widget.parentRecordID.isNotEmpty) {
      tempLookUpList.addAll(lookUpList.where(
          (element) => element.parentID!.contains(widget.parentRecordID)));

      lookUpList = tempLookUpList;
    }
    if (lookUpList.length < 15) {
      showDialog(
          context: context,
          builder: (context) => LookUpsDialog(
                lookupList: lookUpList,
                dialogType: widget.dialogType,
              )).then((value) {
        if (value is LookUps) {
          selectedLookup = value;
          widget.getData!(selectedLookup!);
          widget.controller!.text =
              languageID == 1 ? value.descriptionEn! : value.descriptionAr!;
          setState(() {});
        }
      });
    } else {
      showDialog(
          context: context,
          builder: (context) => LookUpsWithSearchDialog(
                lookupList: lookUpList,
                dialogType: widget.dialogType,
              )).then((value) {
        if (value is LookUps) {
          print(value.descriptionEn);
          selectedLookup = value;
          widget.getData!(selectedLookup!);
          widget.controller!.text =
              languageID == 1 ? value.descriptionEn! : value.descriptionAr!;
          setState(() {});
        }
      });
    }
  }

  getDefaultData() async {
    if (widget.dialogType == LookUpTypes.gender) {
      selectedLookup = await db.getWhereGender(widget.recordID);
    } else if (widget.dialogType == LookUpTypes.marital) {
      selectedLookup = await db.getWhereMarital(widget.recordID);
    } else if (widget.dialogType == LookUpTypes.eduLevel) {
      selectedLookup = await db.getWhereEduLevel(widget.recordID);
    } else if (widget.dialogType == LookUpTypes.nationality) {
      selectedLookup = await db.getWhereNationality(widget.recordID);
    } else if (widget.dialogType == LookUpTypes.sector) {
      selectedLookup = await db.getWhereSector(widget.recordID);
    } else if (widget.dialogType == LookUpTypes.industry) {
      selectedLookup = await db.getWhereIndustry(widget.recordID);
    } else if (widget.dialogType == LookUpTypes.country) {
      selectedLookup = await db.getWhereCountry(widget.recordID);
    } else if (widget.dialogType == LookUpTypes.city) {
      selectedLookup = await db.getWhereCity(widget.recordID);
    } else if (widget.dialogType == LookUpTypes.religion) {
      selectedLookup = await db.getWhereReligion(widget.recordID);
    } else if (widget.dialogType == LookUpTypes.major) {
      selectedLookup = await db.getWhereMajor(widget.recordID);
    } else if (widget.dialogType == LookUpTypes.jobRole) {
      selectedLookup = await db.getWhereJobRole(widget.recordID);
    } else if (widget.dialogType == LookUpTypes.recruitment) {
      selectedLookup = await db.getWhereRecruitment(widget.recordID);
    } else if (widget.dialogType == LookUpTypes.contractTypes) {
      selectedLookup = await db.getWhereContactTypes(widget.recordID);
    } else if (widget.dialogType == LookUpTypes.skillsLevel) {
      selectedLookup = await db.getWhereSkillsLevel(widget.recordID);
    } else if (widget.dialogType == LookUpTypes.experienceYears) {
      selectedLookup = await db.getWhereExperienceYears(widget.recordID);
    } else if (widget.dialogType == LookUpTypes.lastUsed) {
      selectedLookup = await db.getWhereLastUsed(widget.recordID);
    }

    widget.controller!.text = selectedLookup!.descriptionEn ?? '';
  }
}
