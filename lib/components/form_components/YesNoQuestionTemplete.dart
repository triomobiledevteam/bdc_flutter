import 'package:bdc/core/modal/AplicationForm.dart';
import 'package:bdc/core/theme/sizeConfig.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import '../../core/modal/constraints/global_values.dart';

class YesNoQuestionTemplate extends StatefulWidget {
  const YesNoQuestionTemplate({Key? key, required this.applicationForm})
      : super(key: key);
  final ApplicationForm applicationForm;

  @override
  _YesNoQuestionTemplateState createState() => _YesNoQuestionTemplateState();
}

class _YesNoQuestionTemplateState extends State<YesNoQuestionTemplate> {
  int? _radioYesNo;

  @override
  void initState() {
    // TODO: implement initState
    if (widget.applicationForm.yes!) {
      _radioYesNo = 0;
    } else if (widget.applicationForm.no!) {
      _radioYesNo = 1;
    } else {
      _radioYesNo = -1;
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: SizeConfig.height! * 0.01),
      child: Column(
        mainAxisSize: MainAxisSize.max,
        children: [
          Container(
            padding: EdgeInsets.symmetric(
              vertical: SizeConfig.height! * 0.01,
              horizontal: SizeConfig.width! * 0.01,
            ),
            child: Row(
              children: [
                Expanded(
                  child: Container(
                    padding: EdgeInsets.symmetric(
                        vertical: SizeConfig.height! * 0.02,
                        horizontal: SizeConfig.width! * 0.01),
                    // width: SizeConfig.width * 0.38,
                    child: Text(
                      languageID == 1
                          ? widget.applicationForm.descriptionEn!
                          : widget.applicationForm.descriptionAr!,
                    ),
                  ),
                ),
                Column(
                  children: [
                    Radio(
                      focusColor: Colors.amber,
                      value: 0,
                      onChanged: _handleRadioValueChange,
                      groupValue: _radioYesNo,
                    ),
                    Text(AppLocalizations.of(context)!.yes)
                  ],
                ),
                Column(
                  children: [
                    Radio(
                      focusColor: Colors.amber,
                      value: 1,
                      onChanged: _handleRadioValueChange,
                      groupValue: _radioYesNo,
                    ),
                    Text(AppLocalizations.of(context)!.no)
                  ],
                ),
              ],
            ),
          ),
          const Divider(thickness: 2)
        ],
      ),
    );
  }

  void _handleRadioValueChange(int? value) {
    setState(() {
      _radioYesNo = value!;

      switch (_radioYesNo) {
        case 0:
          widget.applicationForm.yes = true;
          widget.applicationForm.no = false;
          break;
        case 1:
          widget.applicationForm.yes = false;
          widget.applicationForm.no = true;
          break;
      }
    });
  }
}
