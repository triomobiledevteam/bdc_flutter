import 'package:flutter/material.dart';

import '../core/theme/sizeConfig.dart';

class CardDivider extends StatelessWidget {
  const CardDivider({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Divider(
      color: Colors.grey.withOpacity(0.5),
      height: 10,
      thickness: 2,
      indent: SizeConfig.width! * 0.15,
      endIndent: SizeConfig.width! * 0.15,
    );
  }
}
