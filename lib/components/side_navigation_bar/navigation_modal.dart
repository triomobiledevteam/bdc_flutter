import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class NavigationModel {
  String? title;
  IconData? icon;
  String? paidText;

  NavigationModel({this.title, this.icon, this.paidText});
}
