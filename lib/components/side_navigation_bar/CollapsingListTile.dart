import 'package:bdc/core/theme/sizeConfig.dart';
import 'package:flutter/material.dart';

class CollapsingListTile extends StatefulWidget {
  final String title;
  final String? appliedPay;
  final IconData? icon;
  final Function? onTap;
  final Color? textColor;
  final Color? cardColor;
  final bool? isUnderDelivery;

  const CollapsingListTile(
      {Key? key,
      required this.title,
      this.icon,
      this.onTap,
      this.textColor,
      this.cardColor,
      this.appliedPay,
      this.isUnderDelivery})
      : super(key: key);

  @override
  _CollapsingListTileState createState() => _CollapsingListTileState();
}

class _CollapsingListTileState extends State<CollapsingListTile> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap:
            widget.isUnderDelivery! ? () {} : widget.onTap as void Function()?,
        child: Container(
            decoration: BoxDecoration(
                borderRadius: const BorderRadius.all(Radius.circular(15.0)),
                color: widget.cardColor),
            margin: EdgeInsets.symmetric(horizontal: SizeConfig.width! * 0.02),
            padding: EdgeInsets.symmetric(
                horizontal: SizeConfig.width! * 0.06,
                vertical: SizeConfig.height! * 0.002),
            child: Column(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  padding:
                      EdgeInsets.symmetric(vertical: SizeConfig.height! * 0.01),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Container(
                        padding: EdgeInsets.symmetric(
                            vertical: SizeConfig.height! * 0.005,
                            horizontal: SizeConfig.width! * 0.01),
                        child: Icon(
                          widget.icon,
                          size: SizeConfig.blockSizeVertical! * 5,
                          color: widget.isUnderDelivery!
                              ? Colors.grey
                              : Colors.blueAccent,
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.symmetric(
                            vertical: SizeConfig.height! * 0.005,
                            horizontal: SizeConfig.width! * 0.01),
                        child: Text(
                          widget.title,
                          style: TextStyle(
                              color: widget.textColor,
                              fontSize: SizeConfig.blockSizeVertical! * 2.5,
                              fontWeight: FontWeight.w600),
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  padding:
                      EdgeInsets.symmetric(vertical: SizeConfig.height! * 0.01),
                  child: Text(
                    widget.appliedPay!,
                    style: TextStyle(
                        color: widget.textColor,
                        fontSize: SizeConfig.blockSizeVertical! * 2.5,
                        fontWeight: FontWeight.w600),
                  ),
                ),
              ],
            )));
  }
}
