import 'package:flutter/material.dart';

import '../../core/theme/sizeConfig.dart';

class CustomButtonConfirm extends StatelessWidget {
  final String? title;
  final Color? backColor;
  final Color? fontColor;
  final Function? onTap;

  const CustomButtonConfirm(
      {Key? key, this.title, this.backColor, this.fontColor, this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap as void Function()?,
      child: Container(
        padding: EdgeInsets.all(SizeConfig.width! * 0.03),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20),
          color: backColor,
        ),
        child: Text(
          title!,
          style: TextStyle(
            fontSize: SizeConfig.blockSizeVertical! * 3,
            fontWeight: FontWeight.bold,
            color: fontColor,
          ),
        ),
      ),
    );
  }
}
