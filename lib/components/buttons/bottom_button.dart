import 'package:bdc/core/theme/app_Theme.dart';
import 'package:flutter/material.dart';

class BottomButton extends StatefulWidget {
  final Function? onPressed;
  final String? text;
  final Color? color;

  const BottomButton({Key? key, this.onPressed, this.text, this.color})
      : super(key: key);

  @override
  _BottomButtonState createState() => _BottomButtonState();
}

class _BottomButtonState extends State<BottomButton> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 5),
      color: widget.color ?? mainColorTheme,
      width: double.infinity,
      child: MaterialButton(
        onPressed: widget.onPressed as void Function()?,
        child: Text(
          '${widget.text}',
          style: const TextStyle(
            fontSize: 18,
            color: Colors.white,
          ),
          textAlign: TextAlign.center,
        ),
      ),
    );
  }
}
