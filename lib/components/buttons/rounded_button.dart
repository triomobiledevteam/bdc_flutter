import 'package:bdc/components/loading.dart';
import 'package:bdc/core/theme/app_Theme.dart';
import 'package:flutter/material.dart';

class RoundedButton extends StatelessWidget {
  const RoundedButton({
    Key? key,
    required this.title,
    required this.onTap,
    this.isLoading = false,
    this.decoration,
    this.style,
  }) : super(key: key);
  final void Function() onTap;
  final String title;
  final bool isLoading;
  final Decoration? decoration;
  final TextStyle? style;

  @override
  Widget build(BuildContext context) {
    return isLoading
        ? Loading.loading(context)
        : InkWell(
            onTap: onTap,
            borderRadius: BorderRadius.circular(10),
            child: Container(
              decoration: decoration ??
                  BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: kPrimaryColor,
                  ),
              padding: const EdgeInsets.symmetric(vertical: 10),
              alignment: Alignment.center,
              child: Text(title,
                  style: style ??
                      Theme.of(context)
                          .textTheme
                          .bodyText1!
                          .copyWith(color: whiteColor, fontSize: 18)),
            ),
          );
  }
}
