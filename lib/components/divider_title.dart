import 'package:flutter/material.dart';

import '../core/theme/sizeConfig.dart';

class DividerTitle extends StatelessWidget {
  final String? title;

  const DividerTitle({Key? key, this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Padding(
      padding: EdgeInsets.only(
          left: SizeConfig.width! * 0.09, right: SizeConfig.width! * 0.09),
      child: Row(mainAxisSize: MainAxisSize.min, children: <Widget>[
        const Expanded(
            child: Divider(
          thickness: 0.5,
          color: Colors.black,
        )),
        SizedBox(
          width: SizeConfig.width! * 0.03,
        ),
        Text(
          title!, // "Issued",
          style: TextStyle(fontSize: SizeConfig.blockSizeVertical! * 2),
        ),
        SizedBox(
          width: SizeConfig.width! * 0.03,
        ),
        const Expanded(
            child: Divider(
          thickness: 0.5,
          color: Colors.black,
        )),
      ]),
    );
  }
}
