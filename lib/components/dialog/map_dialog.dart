import 'dart:async';

import 'package:bdc/Screens/map_layout/bloc/map_layout_cubit.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import "package:flutter_gen/gen_l10n/app_localizations.dart";
import 'package:google_maps_flutter/google_maps_flutter.dart';

import '../../Screens/map_layout/bloc/map_layout_state.dart';

class MapDialog extends StatefulWidget {
  const MapDialog({
    Key? key,
    required this.getAddress,
  }) : super(key: key);

  final void Function(String) getAddress;
  @override
  _MapDialogState createState() => _MapDialogState();
  static Future show(context, void Function(String) getAddress) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return MapDialog(getAddress: getAddress);
        });
  }
}

class _MapDialogState extends State<MapDialog> {
  late MapLayoutCubit mapLayoutCubit;
  @override
  void initState() {
    // TODO: implement initState

    mapLayoutCubit = MapLayoutCubit.get(context);
    mapLayoutCubit.init();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
      ),
      elevation: 1,
      backgroundColor: Colors.transparent,
      child: BlocBuilder<MapLayoutCubit, MapLayoutStates>(
        builder: (context, state) {
          return Stack(
            children: [
              Container(
                color: Colors.white,
                height: MediaQuery.of(context).size.height * 0.7,
                child: Column(
                  children: [
                    Expanded(
                      child: GoogleMap(
                        initialCameraPosition: mapLayoutCubit.cameraPosition,
                        onMapCreated: mapLayoutCubit.onMapCreate,
                        markers: mapLayoutCubit.markers,
                        myLocationEnabled: true,
                        rotateGesturesEnabled: false,
                        myLocationButtonEnabled: true,
                        mapType: MapType.terrain,
                        onTap: mapLayoutCubit.handleTap,
                      ),
                    ),
                  ],
                ),
              ),
              Positioned(
                bottom: 50,
                left: 10,
                right: 10,
                child: Card(
                  elevation: 9,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10),
                  ),
                  color: Colors.white,
                  child: Visibility(
                    visible: mapLayoutCubit.addressController.text.isNotEmpty,
                    child: Container(
                        padding: const EdgeInsets.all(5),
                        child: Column(
                          children: [
                            Center(
                                child: Text(
                              mapLayoutCubit.addressController.text,
                              style: const TextStyle(fontSize: 16),
                            )),
                            ElevatedButton(
                              child:
                                  Text(AppLocalizations.of(context)!.confirm),
                              onPressed: () {
                                widget.getAddress(
                                    mapLayoutCubit.addressController.text);
                                Navigator.of(context).pop();
                              },
                            )
                          ],
                        )),
                  ),
                ),
              ),
              PositionedDirectional(
                top: 10,
                start: 10,
                child: Row(
                  children: [
                    Card(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(50),
                        ),
                        elevation: 0,
                        color: Colors.transparent,
                        child: IconButton(
                          icon: const Icon(Icons.arrow_back),
                          onPressed: () {
                            Navigator.of(context).pop(false);
                          },
                        )),
                  ],
                ),
              )
            ],
          );
        },
      ),
    );
  }
}
