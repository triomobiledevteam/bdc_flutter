import 'package:bdc/core/helper/error_handler/errorhandler.dart';
import 'package:bdc/core/modal/Lookups.dart';
import 'package:bdc/core/modal/constraints/LookUpTypes.dart';
import 'package:bdc/core/theme/sizeConfig.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import '../../core/modal/constraints/global_values.dart';
import '../../core/theme/app_Theme.dart';

class LookUpsWithSearchDialog extends StatefulWidget {
  const LookUpsWithSearchDialog({Key? key, this.lookupList, this.dialogType})
      : super(key: key);
  final List<LookUps>? lookupList;
  final String? dialogType;

  @override
  _LookUpsWithSearchDialogState createState() =>
      _LookUpsWithSearchDialogState();
}

class _LookUpsWithSearchDialogState extends State<LookUpsWithSearchDialog> {
  List<LookUps> lookupList = [];
  List<LookUps> duplicateLookupList = [];
  LookUps? selectedLookUp = LookUps();
  // TextEditingController searchController = TextEditingController(text: '');
  String? query;
  @override
  void initState() {
    print(lookupList.length);
    lookupList.addAll(widget.lookupList!);
    duplicateLookupList = [...lookupList];

    // searchController.addListener(() {
    //   print(searchController.text);
    //
    //
    //   // filterSearchResults(
    //   //     searchController.text, lookupList, duplicateLookupList);
    //   setState(() {});
    // });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Dialog(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(5),
        ),
        elevation: 0,
        child: SizedBox(
          height: 350,
          width: SizeConfig.width! * 0.8,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Container(
                width: double.infinity,
                decoration: BoxDecoration(
                  border: Border(
                      bottom: BorderSide(width: 3, color: kPrimaryColor)),
                ),
                child: Padding(
                  padding:
                      const EdgeInsets.symmetric(vertical: 16, horizontal: 10),
                  child: Text(
                    defaultDialog(),
                    style: Theme.of(context)
                        .textTheme
                        .headline6!
                        .copyWith(color: mainColorTheme),
                  ),
                ),
              ),
              Flexible(
                child: Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 16, vertical: 5),
                  child: TextField(
                    // controller: searchController,
                    onChanged: (value) {
                      query = value;
                      duplicateLookupList.clear();
                      if (value.isNotEmpty) {
                        for (LookUps item in lookupList) {
                          if (item.descriptionEn!
                              .toLowerCase()
                              .trim()
                              .contains(value.toLowerCase())) {
                            print(item.descriptionEn);
                            duplicateLookupList.add(item);
                          }
                        }
                      } else {
                        duplicateLookupList.addAll(lookupList);
                      }
                      setState(() {});
                    },
                    autofocus: true,
                    decoration: InputDecoration(
                      icon: const Icon(Icons.search),
                      border: InputBorder.none,
                      hintText: AppLocalizations.of(context)!.search,
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 200,
                child: ListView.builder(
                  key: UniqueKey(),
                  itemCount: duplicateLookupList.length,
                  itemBuilder: (BuildContext context, int index) {
                    return Container(
                      padding: const EdgeInsets.symmetric(horizontal: 8),
                      child: CheckboxListTile(
                        title: Text(
                          languageID == 2
                              ? duplicateLookupList[index].descriptionAr!
                              : duplicateLookupList[index].descriptionEn!,
                          style: Theme.of(context)
                              .textTheme
                              .bodyText1!
                              .copyWith(color: Colors.black),
                        ),
                        activeColor: Colors.grey[200],
                        checkColor: Colors.blue,
                        value:
                            duplicateLookupList[index].iD == selectedLookUp!.iD,
                        onChanged: (value) {
                          if (value!) {
                            selectedLookUp = duplicateLookupList[index];
                          }
                          setState(() {});
                        },
                      ),
                    );
                  },
                ),
              ),
              SizedBox(
                height: 50,
                width: SizeConfig.width! * 0.8,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          backgroundColor: Colors.red,
                          minimumSize: const Size(120, 40),
                        ),
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        child: Text(
                          AppLocalizations.of(context)!.cancel,
                          style: TextStyle(
                              fontSize: SizeConfig.blockSizeVertical! * 2,
                              color: Colors.white),
                        )),
                    const SizedBox(
                      height: 25,
                    ),
                    ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          backgroundColor: kPrimaryColorGreen,
                          minimumSize: const Size(120, 40),
                        ),
                        onPressed: () {
                          if (selectedLookUp != null) {
                            Navigator.pop(context, selectedLookUp);
                          } else {
                            ErrorHandler.onErrorSnackBar(
                                'Please Select ${widget.dialogType}', context);
                          }
                        },
                        child: Text(
                          AppLocalizations.of(context)!.confirm,
                          style: TextStyle(
                              fontSize: SizeConfig.blockSizeVertical! * 2,
                              color: Colors.white),
                        )),
                  ],
                ),
              ),
            ],
          ),
        ));
  }

  String defaultDialog() {
    if (widget.dialogType == LookUpTypes.gender) {
      return AppLocalizations.of(context)!.genderID;
    } else if (widget.dialogType == LookUpTypes.marital) {
      return AppLocalizations.of(context)!.maritalStatusID;
    } else if (widget.dialogType == LookUpTypes.eduLevel) {
      return AppLocalizations.of(context)!.educationalLevel;
    } else if (widget.dialogType == LookUpTypes.nationality) {
      return AppLocalizations.of(context)!.nationalityID;
    } else if (widget.dialogType == LookUpTypes.sector) {
      return AppLocalizations.of(context)!.sectorTypeID;
    } else if (widget.dialogType == LookUpTypes.industry) {
      return AppLocalizations.of(context)!.companyIndustry;
    } else if (widget.dialogType == LookUpTypes.country) {
      return AppLocalizations.of(context)!.countryID;
    } else if (widget.dialogType == LookUpTypes.city) {
      return AppLocalizations.of(context)!.governorate;
    } else if (widget.dialogType == LookUpTypes.religion) {
      return AppLocalizations.of(context)!.religionID;
    } else if (widget.dialogType == LookUpTypes.major) {
      return AppLocalizations.of(context)!.majorID;
    } else if (widget.dialogType == LookUpTypes.jobRole) {
      return AppLocalizations.of(context)!.roleID;
    } else if (widget.dialogType == LookUpTypes.recruitment) {
      return AppLocalizations.of(context)!.recruitTypesInCompany;
    } else if (widget.dialogType == LookUpTypes.contractTypes) {
      return AppLocalizations.of(context)!.contractTypesInCompany;
    } else {
      return '';
    }
  }
}
