import 'package:bdc/Components/loading.dart';
import 'package:flutter/material.dart';

import '../../core/theme/sizeConfig.dart';

class LoadingDialog {
  static showLoadingDialog(BuildContext context, GlobalKey key) async {
    SizeConfig().init(context);
    return showDialog<void>(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return WillPopScope(
              onWillPop: () async => false,
              child: Dialog(
                  key: key,
                  // backgroundColor: Colors.transparent,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: Container(
                    color: Colors.transparent,
                    padding: EdgeInsets.symmetric(
                        vertical: SizeConfig.height! * 0.01),
                    child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Center(
                            child: Loading.loading(context,
                                size: SizeConfig.blockSizeVertical! * 4),
                          ),
                          Text(
                            "Please Wait....",
                            style: TextStyle(
                                color: Colors.blueAccent,
                                fontSize: SizeConfig.blockSizeVertical! * 2),
                          ),
                        ]),
                  )));
        });
  }

  static showLoadingDialog2(BuildContext context) async {
    SizeConfig().init(context);
    return showDialog<void>(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return Dialog(
              backgroundColor: Colors.white,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10),
              ),
              child: Expanded(
                child:
                    Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
                  Center(
                    child: Column(children: [
                      SizedBox(
                        height: SizeConfig.height! * 0.01,
                      ),
                      Loading.loading(context),
                      SizedBox(
                        height: SizeConfig.height! * 0.01,
                      ),
                      Text(
                        "Please Wait....",
                        style: TextStyle(
                            color: Colors.blueAccent,
                            fontSize: SizeConfig.blockSizeVertical! * 2.5),
                      )
                    ]),
                  )
                ]),
              ));
        });
  }
}
