import 'package:bdc/components/custom_row.dart';
import 'package:bdc/core/modal/constraints/global_values.dart';
import 'package:bdc/core/modal/languages.dart';
import 'package:bdc/core/theme/sizeConfig.dart';
import 'package:bdc/core/utils/Navigation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import '../../main.dart';

class LanguagePopup extends StatelessWidget {
  const LanguagePopup({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(12),
      ),
      child: Container(
        padding: EdgeInsets.symmetric(
            vertical: SizeConfig.height! * 0.02,
            horizontal: SizeConfig.width! * 0.01),
        child: SizedBox(
          width: SizeConfig.width! * 0.5,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(
                AppLocalizations.of(context)!.chooseTheLanguage,
                style: TextStyle(fontSize: SizeConfig.blockSizeVertical! * 3),
              ),
              SizedBox(
                height: SizeConfig.height! * 0.02,
              ),
              SizedBox(height: SizeConfig.height! * 0.02),
              Container(
                padding:
                    EdgeInsets.symmetric(horizontal: SizeConfig.width! * 0.05),
                child: ElevatedButton(
                    onPressed: () async {
                      languageID = 2;
                      saveLocale(languageID);

                      MyApp.of(context)!
                          .setLocale(await saveLocale(languageID));
                      Navigation.back(context);
                    },
                    child: CustomRow(
                      text_1: '🇯🇴',
                      text_2: 'العربية',
                      fontSize: SizeConfig.blockSizeVertical! * 2.8,
                      showDoubleDot: false,
                    )),
              ),
              SizedBox(
                height: SizeConfig.height! * 0.02,
              ),
              Container(
                padding: EdgeInsets.symmetric(
                    // vertical: SizeConfig.height * 0.02,
                    horizontal: SizeConfig.width! * 0.05),
                // width: SizeConfig.width * 0.5,
                child: ElevatedButton(
                    onPressed: () async {
                      languageID = 1;
                      saveLocale(languageID);

                      MyApp.of(context)!
                          .setLocale(await saveLocale(languageID));
                      Navigation.back(context);
                    },
                    child: CustomRow(
                      text_1: '🇺🇸',
                      text_2: 'English',
                      fontSize: SizeConfig.blockSizeVertical! * 2.8,
                      showDoubleDot: false,
                    )),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
