import 'package:bdc/core/helper/error_handler/errorhandler.dart';
import 'package:bdc/core/modal/Lookups.dart';
import 'package:bdc/core/theme/app_Theme.dart';
import 'package:bdc/core/theme/sizeConfig.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import '../../core/modal/constraints/LookUpTypes.dart';
import '../../core/modal/constraints/global_values.dart';

class LookUpsDialog extends StatefulWidget {
  const LookUpsDialog({Key? key, this.lookupList, this.dialogType})
      : super(key: key);
  final List<LookUps>? lookupList;
  final String? dialogType;

  @override
  _LookUpsDialogState createState() => _LookUpsDialogState();
}

class _LookUpsDialogState extends State<LookUpsDialog> {
  List<LookUps> lookupList = [];
  LookUps? selectedLookUp;

  @override
  void initState() {
    lookupList.addAll(widget.lookupList!);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Dialog(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(5),
        ),
        elevation: 0,
        child: SingleChildScrollView(
          child: SizedBox(
            width: SizeConfig.width! * 0.8,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                const SizedBox(
                  height: 10,
                ),
                Container(
                  width: SizeConfig.width,
                  decoration: BoxDecoration(
                    border: Border(
                        bottom: BorderSide(width: 3, color: kPrimaryColor)),
                    // borderRadius: BorderRadius.circular(12),
                  ),
                  child: Padding(
                    padding:
                        const EdgeInsets.symmetric(vertical: 5, horizontal: 16),
                    child: Text(
                      defaultDialog(),
                      style: TextStyle(
                          fontSize: SizeConfig.blockSizeVertical! * 2.7,
                          color: mainColorTheme),
                    ),
                  ),
                ),
                lookupList.isNotEmpty
                    ? SizedBox(
                        height: SizeConfig.height! * 0.3,
                        child: ListView.builder(
                          shrinkWrap: true,
                          itemCount: lookupList.length,
                          itemBuilder: (BuildContext context, int index) {
                            return Container(
                              key: UniqueKey(),
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 8),
                              child: CheckboxListTile(
                                contentPadding: const EdgeInsets.symmetric(
                                    vertical: 2, horizontal: 8),
                                title: Text(
                                  languageID == 2
                                      ? lookupList[index].descriptionAr!
                                      : lookupList[index].descriptionEn!,
                                  style: TextStyle(
                                      fontSize:
                                          SizeConfig.blockSizeVertical! * 2,
                                      fontWeight: FontWeight.w600),
                                ),
                                activeColor: Colors.grey[200],
                                checkColor: Colors.blue,
                                value: lookupList[index].isSelected,
                                onChanged: (value) {
                                  lookupList[index].isSelected = value;
                                  if (value!) {
                                    selectedLookUp = lookupList[index];
                                  }
                                  for (var element in lookupList) {
                                    if (element.iD != lookupList[index].iD) {
                                      element.isSelected = false;
                                    }
                                  }
                                  setState(() {});
                                },
                              ),
                            );
                          },
                          // separatorBuilder: (BuildContext context, int index) {
                          //   return const Divider(
                          //     thickness: 1,
                          //   );
                          // },
                        ),
                      )
                    : const Center(
                        child: Text('No Data'),
                      ),
                SizedBox(
                  width: SizeConfig.width! * 0.8,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            backgroundColor: Colors.red,
                            minimumSize: const Size(120, 40),
                          ),
                          onPressed: () {
                            Navigator.pop(context);
                          },
                          child: Text(
                            AppLocalizations.of(context)!.cancel,
                            style: TextStyle(
                                fontSize: SizeConfig.blockSizeVertical! * 2,
                                color: Colors.white),
                          )),
                      const SizedBox(
                        height: 25,
                      ),
                      ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            backgroundColor: kPrimaryColorGreen,
                            minimumSize: const Size(120, 40),
                          ),
                          onPressed: () {
                            if (selectedLookUp != null) {
                              Navigator.pop(context, selectedLookUp);
                            } else {
                              ErrorHandler.onErrorSnackBar(
                                  'Please Select ${widget.dialogType}',
                                  context);
                            }
                          },
                          child: Text(
                            AppLocalizations.of(context)!.confirm,
                            style: TextStyle(
                                fontSize: SizeConfig.blockSizeVertical! * 2,
                                color: Colors.white),
                          )),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ));
  }

  String defaultDialog() {
    if (widget.dialogType == LookUpTypes.gender) {
      return AppLocalizations.of(context)!.genderID;
    } else if (widget.dialogType == LookUpTypes.marital) {
      return AppLocalizations.of(context)!.maritalStatusID;
    } else if (widget.dialogType == LookUpTypes.eduLevel) {
      return AppLocalizations.of(context)!.educationalLevel;
    } else if (widget.dialogType == LookUpTypes.nationality) {
      return AppLocalizations.of(context)!.nationalityID;
    } else if (widget.dialogType == LookUpTypes.sector) {
      return AppLocalizations.of(context)!.sectorTypeID;
    } else if (widget.dialogType == LookUpTypes.industry) {
      return AppLocalizations.of(context)!.companyIndustry;
    } else if (widget.dialogType == LookUpTypes.country) {
      return AppLocalizations.of(context)!.countryID;
    } else if (widget.dialogType == LookUpTypes.city) {
      return AppLocalizations.of(context)!.governorate;
    } else if (widget.dialogType == LookUpTypes.religion) {
      return AppLocalizations.of(context)!.religionID;
    } else if (widget.dialogType == LookUpTypes.major) {
      return AppLocalizations.of(context)!.majorID;
    } else if (widget.dialogType == LookUpTypes.jobRole) {
      return AppLocalizations.of(context)!.roleID;
    } else if (widget.dialogType == LookUpTypes.recruitment) {
      return AppLocalizations.of(context)!.recruitTypesInCompany;
    } else if (widget.dialogType == LookUpTypes.contractTypes) {
      return AppLocalizations.of(context)!.contractTypesInCompany;
    } else {
      return '';
    }
  }
}
