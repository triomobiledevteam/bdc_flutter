import 'package:bdc/core/theme/sizeConfig.dart';
import 'package:flutter/material.dart';

class ConfirmDialog {
  static Future<bool?> onBackPressed(
      BuildContext context, String text, Function onConfirm) {
    return showDialog(
      context: context,
      builder: (context) => AlertDialog(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10),
        ),
        elevation: 0,
        title: Text(
          text,
          style: TextStyle(fontSize: SizeConfig.blockSizeVertical! * 2),
        ),
        actions: <Widget>[
          ElevatedButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              style: ElevatedButton.styleFrom(
                  foregroundColor: Colors.blueAccent, backgroundColor: Colors.blue[600], disabledForegroundColor: Colors.blueAccent.withOpacity(0.38), disabledBackgroundColor: Colors.blueAccent.withOpacity(0.12),
                  side: BorderSide(
                    width: 1.0,
                    color: Colors.blue[600]!,
                  )),
              child: Text(
                "No",
                style: TextStyle(
                    fontSize: SizeConfig.blockSizeVertical! * 2.2,
                    color: Colors.white),
              )),
          // TextButton(
          //   onPressed: () => Navigator.of(context).pop(),
          //   child: Text(
          //     "No",
          //     style: TextStyle(fontSize: SizeConfig.blockSizeVertical * 2.5),
          //   ),
          // ),
          ElevatedButton(
              onPressed: onConfirm as void Function()?,
              style: ElevatedButton.styleFrom(
                  foregroundColor: Colors.blueAccent, backgroundColor: Colors.blue[600], disabledForegroundColor: Colors.blueAccent.withOpacity(0.38), disabledBackgroundColor: Colors.blueAccent.withOpacity(0.12),
                  side: BorderSide(
                    width: 1.0,
                    color: Colors.blue[600]!,
                  )),
              child: Text(
                "Yes",
                style: TextStyle(
                    fontSize: SizeConfig.blockSizeVertical! * 2.2,
                    color: Colors.white),
              ))
          // TextButton(
          //   onPressed: onConfirm,
          //   child: Text(
          //     "Yes",
          //     style: TextStyle(fontSize: SizeConfig.blockSizeVertical * 2.5),
          //   ),
          // )
        ],
      ),
    );
  }
}
