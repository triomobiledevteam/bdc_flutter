import 'package:bdc/components/form_components/TextFormWithPadding.dart';
import 'package:bdc/components/loading.dart';
import 'package:bdc/core/helper/error_handler/errorhandler.dart';
import 'package:bdc/core/modal/constraints/global_values.dart';
import 'package:bdc/core/modal/enums/UsersType.dart';
import 'package:bdc/core/repository/main_repository/main_repository.dart';
import 'package:bdc/core/theme/app_Theme.dart';
import 'package:bdc/core/theme/sizeConfig.dart';
import 'package:bdc/core/utils/Navigation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import '../../core/utils/string_validation_extension.dart';

class ChangePasswordBottomSheet extends StatefulWidget {
  final void Function()? onPress;

  const ChangePasswordBottomSheet({Key? key, this.onPress}) : super(key: key);

  @override
  _ChangePasswordBottomSheetState createState() =>
      _ChangePasswordBottomSheetState();
}

class _ChangePasswordBottomSheetState extends State<ChangePasswordBottomSheet> {
  final formKey = GlobalKey<FormState>();
  bool isOldPasswordVisible = true;
  bool isNewPasswordVisible = true;
  bool isConfirmPasswordVisible = true;
  TextEditingController oldPasswordController = TextEditingController(text: '');
  TextEditingController newPasswordController = TextEditingController(text: '');
  TextEditingController confirmPasswordController =
      TextEditingController(text: '');

  @override
  void initState() {
    // TODO: implement initState

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding:
          EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
      child: Container(
        padding: const EdgeInsets.symmetric(horizontal: 16),
        child: Form(
          key: formKey,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            mainAxisSize: MainAxisSize.min,
            children: [
              const SizedBox(
                height: 10,
              ),
              Center(
                child: Text(
                  AppLocalizations.of(context)!.changePassword,
                  style: Theme.of(context).textTheme.headline6,
                ),
              ),
              const SizedBox(
                height: 10,
              ),
              TextFormWithPadding(
                obscureText: isOldPasswordVisible,
                validator: (value) {
                  if (value!.isEmpty) {
                    return AppLocalizations.of(context)!.thisFieldIsEmpty;
                  }
                  return null;
                },
                suffixIcon: IconButton(
                    onPressed: () {
                      isOldPasswordVisible = !isOldPasswordVisible;
                      setState(() {});
                    },
                    icon: isOldPasswordVisible
                        ? const Icon(Icons.visibility)
                        : const Icon(Icons.visibility_off)),
                controller: oldPasswordController,
                labelText: AppLocalizations.of(context)!.oldPassword,
              ),
              TextFormWithPadding(
                obscureText: isNewPasswordVisible,
                validator: (value) {
                  if (!isValidPassword(value ?? '')) {
                    onError(AppLocalizations.of(context)!.passwordValidation);
                    return '';
                  }
                  return null;
                },
                suffixIcon: IconButton(
                    onPressed: () {
                      isNewPasswordVisible = !isNewPasswordVisible;
                      setState(() {});
                    },
                    icon: isNewPasswordVisible
                        ? const Icon(Icons.visibility)
                        : const Icon(Icons.visibility_off)),
                controller: newPasswordController,
                labelText: AppLocalizations.of(context)!.newPassword,
              ),
              TextFormWithPadding(
                obscureText: isConfirmPasswordVisible,
                validator: (value) {
                  if (value!.isEmpty) {
                    return AppLocalizations.of(context)!.thisFieldIsEmpty;
                  }
                  return null;
                },
                controller: confirmPasswordController,
                labelText: AppLocalizations.of(context)!.confirmNewPassword,
              ),
              SizedBox(
                width: SizeConfig.width!,
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(backgroundColor: mainColorTheme),
                  onPressed: () async {
                    if (formKey.currentState!.validate()) {
                      progressDialogue(context);
                      if (userType == UserType.Employee.name) {
                        await GetMainRepository().employeeChangePassword(
                            oldPasswordController.text,
                            newPasswordController.text,
                            onError,
                            onDone);
                      } else {
                        await GetMainRepository().userChangePassword(
                            oldPasswordController.text,
                            newPasswordController.text,
                            onError,
                            onDone);
                      }
                    }
                  },
                  child: Text(AppLocalizations.of(context)!.update),
                ),
              ),
              const SizedBox(
                height: 20,
              )
            ],
          ),
        ),
      ),
    );
  }

  onError(String massage) {
    Navigation.back(context);
    ErrorHandler.errorMassage = massage;
    ErrorHandler.showErrorMassageDialog(context);
  }

  onDone() {
    Navigation.back(context);
    Navigation.back(context);
    ErrorHandler.onSuccessSnackBar("Done", context);
    print(AppLocalizations.of(context)!.done);
  }
}
