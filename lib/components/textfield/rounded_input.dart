import 'package:bdc/components/dialog/lookUpsWithSearch_dailog.dart';
import 'package:bdc/core/modal/Lookups.dart';
import 'package:bdc/core/modal/constraints/LookUpTypes.dart';
import 'package:bdc/core/modal/constraints/global_values.dart';
import 'package:bdc/core/storage/db_client.dart';
import 'package:bdc/core/theme/app_Theme.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import '../dialog/lookUps_dailog.dart';
import '../input_container.dart';

class RoundedInput extends StatefulWidget {
  const RoundedInput(
      {Key? key,
      required this.icon,
      required this.hint,
      required this.controller,
      this.dialogType,
      this.isDefaultDialog = false,
      this.readOnly = false,
      this.getData,
      this.recordID = '',
      this.autofocus = false,
      this.isDateDialog = false,
      this.getDate,
      this.validator,
      this.parentRecordID = ''})
      : super(key: key);
  final String? dialogType;
  final bool isDefaultDialog;
  final String? recordID;
  final void Function(LookUps)? getData;
  final void Function(String)? getDate;
  final IconData icon;
  final String hint;
  final String parentRecordID;
  final bool readOnly;
  final bool autofocus;
  final TextEditingController controller;
  final bool isDateDialog;
  final String? Function(String?)? validator;

  @override
  _RoundedInputState createState() => _RoundedInputState();
}

class _RoundedInputState extends State<RoundedInput> {
  DateTime? _selectedDate;

  @override
  void initState() {
    // TODO: implement initState
    if (widget.isDefaultDialog) {
      getDefaultData();
    }
    super.initState();
  }

  final db = DatabaseClient.instance;
  List<LookUps> lookUpList = [];
  List<LookUps> tempLookUpList = [];
  LookUps? selectedLookup;

  @override
  Widget build(BuildContext context) {
    return InputContainer(
        child: TextFormField(
      controller: widget.controller,
      onTap: () async {
        if (widget.isDefaultDialog) {
          await defaultDialog();
        }
        if (widget.isDateDialog) {
          _selectDate(context);
        }
      },
      validator: widget.validator,
      readOnly: widget.readOnly,
      cursorColor: kPrimaryColor,
      autofocus: widget.autofocus,
      decoration: InputDecoration(
          icon: Icon(widget.icon, color: kPrimaryColor),
          hintText: widget.hint,
          border: InputBorder.none),
    ));
  }

  defaultDialog() async {
    lookUpList = [];
    tempLookUpList = [];
    if (widget.dialogType == LookUpTypes.gender) {
      lookUpList.addAll(await db.getGender());
    } else if (widget.dialogType == LookUpTypes.marital) {
      lookUpList.addAll(await db.getMarital());
    } else if (widget.dialogType == LookUpTypes.eduLevel) {
      lookUpList.addAll(await db.getEduLevel());
    } else if (widget.dialogType == LookUpTypes.nationality) {
      lookUpList.addAll(await db.getNationality());
    } else if (widget.dialogType == LookUpTypes.sector) {
      lookUpList.addAll(await db.getSector());
    } else if (widget.dialogType == LookUpTypes.industry) {
      lookUpList.addAll(await db.getIndustry());
    } else if (widget.dialogType == LookUpTypes.country) {
      lookUpList.addAll(await db.getCountry());
    } else if (widget.dialogType == LookUpTypes.city) {
      lookUpList.addAll(await db.getCity());
    } else if (widget.dialogType == LookUpTypes.religion) {
      lookUpList.addAll(await db.getReligion());
    } else if (widget.dialogType == LookUpTypes.major) {
      lookUpList.addAll(await db.getMajor());
    } else if (widget.dialogType == LookUpTypes.jobRole) {
      lookUpList.addAll(await db.getJobRole());
    } else if (widget.dialogType == LookUpTypes.recruitment) {
      lookUpList.addAll(await db.getRecruitment());
    } else if (widget.dialogType == LookUpTypes.contractTypes) {
      lookUpList.addAll(await db.getContactTypes());
    } else if (widget.dialogType == LookUpTypes.skillsLevel) {
      lookUpList.addAll(await db.getSkillsLevel());
    } else if (widget.dialogType == LookUpTypes.experienceYears) {
      lookUpList.addAll(await db.getExperienceYears());
    } else if (widget.dialogType == LookUpTypes.lastUsed) {
      lookUpList.addAll(await db.getLastUsed());
    }
    if (widget.parentRecordID.isNotEmpty) {
      tempLookUpList.addAll(lookUpList
          .where((element) => element.parentID!.contains(widget.parentRecordID))
          .toList());
      lookUpList = tempLookUpList;
    }
    if (lookUpList.length < 15) {
      showDialog(
          context: context,
          builder: (context) => LookUpsDialog(
                lookupList: lookUpList,
                dialogType: widget.dialogType,
              )).then((value) {
        if (value is LookUps) {
          selectedLookup = value;
          widget.getData!(selectedLookup!);
          widget.controller.text =
              languageID == 1 ? value.descriptionEn! : value.descriptionAr!;
          setState(() {});
        }
      });
    } else {
      showDialog(
          context: context,
          builder: (context) => LookUpsWithSearchDialog(
                lookupList: lookUpList,
                dialogType: widget.dialogType,
              )).then((value) {
        if (value is LookUps) {
          print(value.descriptionEn);
          selectedLookup = value;
          widget.getData!(selectedLookup!);
          widget.controller.text =
              languageID == 1 ? value.descriptionEn! : value.descriptionAr!;

          setState(() {});
        }
      });
    }
  }

  getDefaultData() async {
    if (widget.dialogType == LookUpTypes.gender) {
      selectedLookup = await db.getWhereGender(widget.recordID);
    } else if (widget.dialogType == LookUpTypes.marital) {
      selectedLookup = await db.getWhereMarital(widget.recordID);
    } else if (widget.dialogType == LookUpTypes.eduLevel) {
      selectedLookup = await db.getWhereEduLevel(widget.recordID);
    } else if (widget.dialogType == LookUpTypes.nationality) {
      selectedLookup = await db.getWhereNationality(widget.recordID);
    } else if (widget.dialogType == LookUpTypes.sector) {
      selectedLookup = await db.getWhereSector(widget.recordID);
    } else if (widget.dialogType == LookUpTypes.industry) {
      selectedLookup = await db.getWhereIndustry(widget.recordID);
    } else if (widget.dialogType == LookUpTypes.country) {
      selectedLookup = await db.getWhereCountry(widget.recordID);
    } else if (widget.dialogType == LookUpTypes.city) {
      selectedLookup = await db.getWhereCity(widget.recordID);
    } else if (widget.dialogType == LookUpTypes.religion) {
      selectedLookup = await db.getWhereReligion(widget.recordID);
    } else if (widget.dialogType == LookUpTypes.major) {
      selectedLookup = await db.getWhereMajor(widget.recordID);
    } else if (widget.dialogType == LookUpTypes.jobRole) {
      selectedLookup = await db.getWhereJobRole(widget.recordID);
    } else if (widget.dialogType == LookUpTypes.recruitment) {
      selectedLookup = await db.getWhereRecruitment(widget.recordID);
    } else if (widget.dialogType == LookUpTypes.contractTypes) {
      selectedLookup = await db.getWhereContactTypes(widget.recordID);
    } else if (widget.dialogType == LookUpTypes.skillsLevel) {
      selectedLookup = await db.getWhereSkillsLevel(widget.recordID);
    } else if (widget.dialogType == LookUpTypes.experienceYears) {
      selectedLookup = await db.getWhereExperienceYears(widget.recordID);
    } else if (widget.dialogType == LookUpTypes.lastUsed) {
      selectedLookup = await db.getWhereLastUsed(widget.recordID);
    }

    widget.controller.text = selectedLookup!.descriptionEn!;
  }

  _selectDate(BuildContext context) async {
    DateTime? newSelectedDate = await showDatePicker(
      context: context,
      initialDate: _selectedDate != null ? _selectedDate! : DateTime.now(),
      firstDate: DateTime(1920),
      lastDate: DateTime(2091),
    );

    if (newSelectedDate != null) {
      _selectedDate = newSelectedDate;
      widget.getDate!(_selectedDate.toString());
      widget.controller
        ..text = DateFormat.yMMMd().format(_selectedDate!)
        ..selection = TextSelection.fromPosition(TextPosition(
            offset: widget.controller.text.length,
            affinity: TextAffinity.upstream));
    }
  }
}
