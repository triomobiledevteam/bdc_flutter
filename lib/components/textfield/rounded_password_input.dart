import 'package:bdc/core/theme/app_Theme.dart';
import 'package:flutter/material.dart';

import '../input_container.dart';

class RoundedPasswordInput extends StatelessWidget {
  const RoundedPasswordInput(
      {Key? key,
      required this.hint,
      required this.controller,
      this.textInputAction = TextInputAction.none,
      this.validator})
      : super(key: key);
  final TextEditingController controller;
  final String hint;
  final TextInputAction? textInputAction;
  final String? Function(String?)? validator;

  @override
  Widget build(BuildContext context) {
    return InputContainer(
        child: TextFormField(
      cursorColor: kPrimaryColor,
      controller: controller,
      obscureText: true,
      validator: validator,
      textInputAction: textInputAction!,
      decoration: InputDecoration(
          icon: Icon(Icons.lock, color: kPrimaryColor),
          hintText: hint,
          border: InputBorder.none),
    ));
  }
}
