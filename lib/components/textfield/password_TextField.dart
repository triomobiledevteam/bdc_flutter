import 'package:bdc/core/theme/app_Theme.dart';
import 'package:bdc/core/theme/sizeConfig.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class PasswordTextField extends StatefulWidget {
  const PasswordTextField(
      {Key? key,
      this.controller,
      this.obscureText,
      this.onIconPress,
      this.onSubmitted})
      : super(key: key);
  final TextEditingController? controller;
  final bool? obscureText;
  final void Function()? onIconPress;
  final void Function(String)? onSubmitted;

  @override
  _PasswordTextFieldState createState() => _PasswordTextFieldState();
}

class _PasswordTextFieldState extends State<PasswordTextField> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: EdgeInsets.symmetric(
              horizontal: SizeConfig.width! * 0.01,
              vertical: SizeConfig.height! * 0.01),
          child: Align(
              alignment: Alignment.centerLeft,
              child: Text(
                AppLocalizations.of(context)!.password,
                style: basicTextStyle,
              )),
        ),
        TextField(
          controller: widget.controller,
          obscureText: widget.obscureText!,
          onSubmitted: widget.onSubmitted,
          decoration: InputDecoration(
              border: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.grey[100]!),
                borderRadius: const BorderRadius.all(
                  Radius.circular(10.0),
                ),
              ),
              hoverColor: Colors.white24,

              // filled: true,
              suffixIcon: IconButton(
                  icon: Icon(widget.obscureText!
                      ? Icons.visibility_outlined
                      : Icons.visibility_off_outlined),
                  onPressed: widget.onIconPress),
              hintStyle: TextStyle(color: Colors.grey[800]),
              hintText: "******",
              fillColor: Colors.white70),
        ),
      ],
    );
  }
}
