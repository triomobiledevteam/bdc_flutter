import 'package:bdc/core/theme/app_Theme.dart';
import 'package:bdc/core/theme/sizeConfig.dart';
import 'package:flutter/material.dart';

class CustomTextField extends StatelessWidget {
  final String? label;
  final Widget? icon;
  final bool isPassword;
  final Function? onChanged;
  final Function? onTap;
  final double? width;
  final bool? enable;
  final TextInputType? type;
  final Color? color;
  final TextInputAction? textAction;
  final bool readOnly;
  final Function? onSubmit;
  final TextEditingController? controller;

  const CustomTextField(
      {Key? key,
      this.label,
      this.icon,
      this.isPassword = false,
      this.onChanged,
      this.width,
      this.enable,
      this.type,
      this.color,
      this.textAction,
      this.onSubmit,
      this.controller,
      this.readOnly = false,
      this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: EdgeInsets.symmetric(
              horizontal: SizeConfig.width! * 0.01,
              vertical: SizeConfig.height! * 0.01),
          child: Align(
              alignment: Alignment.centerLeft,
              child: Text(
                label!,
                style: basicTextStyle,
              )),
        ),
        TextFormField(
          controller: controller,
          validator: (value) {
            if (value == null) return "Enter customer name";
            return null;
          },
          keyboardType: type,
          enabled: enable,
          readOnly: readOnly,
          onTap: onTap as void Function()?,
          textInputAction: textAction,
          onChanged: onChanged as void Function(String)?,
          onFieldSubmitted: onSubmit as void Function(String)?,
          style: TextStyle(
            color: const Color(0xFF234253),
            fontSize: SizeConfig.blockSizeVertical! * 2,
            fontWeight: FontWeight.bold,
          ),
          obscureText: isPassword,
          decoration: InputDecoration(
              // labelText: label,
              border: OutlineInputBorder(
                borderSide: BorderSide(
                  color: Colors.grey[100]!,
                ),
                borderRadius: const BorderRadius.all(
                  Radius.circular(10.0),
                ),
              ),
              suffixIcon: icon),
        ),
      ],
    );
  }
}
