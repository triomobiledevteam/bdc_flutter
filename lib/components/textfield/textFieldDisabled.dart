import 'package:flutter/material.dart';

import '../../core/theme/sizeConfig.dart';

class TextFieldDisabled extends StatelessWidget {
  const TextFieldDisabled({
    Key? key,
    required TextEditingController controller,
    this.label,
  })  : _controller = controller,
        super(key: key);

  final TextEditingController _controller;
  final String? label;

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Expanded(
      child: TextFormField(
        enabled: false,
        controller: _controller,
        style: TextStyle(
          color: const Color(0xff234253),
          fontSize: SizeConfig.blockSizeVertical! * 3,
          fontWeight: FontWeight.bold,
        ),
        decoration: InputDecoration(
          focusedBorder: const OutlineInputBorder(
            borderSide: BorderSide(
              color: Colors.grey,
              width: 1.0,
            ),
          ),
          enabledBorder: const OutlineInputBorder(
            borderSide: BorderSide(
              color: Colors.grey,
              width: 1.0,
            ),
          ),
          labelText: label,
          labelStyle: TextStyle(
            color: const Color(0xff234253),
            fontSize: SizeConfig.blockSizeVertical! * 3,
            fontWeight: FontWeight.bold,
          ),
          // suffixIcon: widget.icon,
        ),
      ),
    );
  }
}
