import 'package:bdc/core/theme/app_Theme.dart';
import 'package:bdc/core/theme/sizeConfig.dart';
import 'package:flutter/material.dart';

class UserIDTextField extends StatelessWidget {
  const UserIDTextField({Key? key, this.controller}) : super(key: key);
  final TextEditingController? controller;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: EdgeInsets.symmetric(
              horizontal: SizeConfig.width! * 0.01,
              vertical: SizeConfig.height! * 0.01),
          child: Align(
              alignment: Alignment.centerLeft,
              child: Text(
                'User ID',
                style: basicTextStyle,
              )),
        ),
        TextField(
          autofocus: true,
          controller: controller,
          textInputAction: TextInputAction.next,
          decoration: InputDecoration(
              border: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.grey[100]!),
                borderRadius: const BorderRadius.all(
                  Radius.circular(10.0),
                ),
              ),
              // filled: true,
              hintStyle: TextStyle(color: Colors.grey[800]),
              hintText: "User",
              fillColor: Colors.white70),
        ),
      ],
    );
  }
}
