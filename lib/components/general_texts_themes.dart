import 'package:bdc/core/theme/sizeConfig.dart';
import 'package:flutter/material.dart';

TextStyle listTitleDefaultTextStyle = TextStyle(
    color: Colors.white70,
    fontSize: SizeConfig.blockSizeVertical! * 3,
    fontWeight: FontWeight.w600);

TextStyle footerTextStyle = TextStyle(
    fontSize: SizeConfig.blockSizeVertical! * 3,
    fontWeight: FontWeight.bold,
    color: Colors.white);
TextStyle footerPaymentTextStyle = TextStyle(
    fontSize: SizeConfig.blockSizeVertical! * 2.6,
    fontWeight: FontWeight.bold,
    color: Colors.white);
TextStyle footerNumberStyle =
    TextStyle(fontSize: SizeConfig.blockSizeVertical! * 3, color: Colors.white);
TextStyle footerEditNumberStyle = TextStyle(
  fontSize: SizeConfig.blockSizeVertical! * 2.6,
  color: Colors.blue,
  decoration: TextDecoration.underline,
);

TextStyle bottomSheetTitle = TextStyle(
    fontSize: SizeConfig.blockSizeVertical! * 3, fontWeight: FontWeight.bold);
