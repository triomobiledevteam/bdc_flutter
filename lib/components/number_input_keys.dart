import 'package:flutter/material.dart';

import '../core/theme/sizeConfig.dart';

class NumbersInputKey extends StatefulWidget {
  final String? label;
  final Widget? icon;
  final double? width;
  final bool focus;
  final bool enable;
  final TextEditingController? controller;
  final String defaultLabel;
  final String? hint;
  final Function? onChange;
  final Color? color;
  final Function? onSubmitData;
  final bool readOnly;
  final TextInputType textInputType;

  const NumbersInputKey({
    Key? key,
    this.label,
    this.icon,
    this.width,
    this.focus = false,
    this.enable = true,
    this.defaultLabel = '',
    this.onChange,
    this.color,
    this.onSubmitData,
    this.controller,
    this.readOnly = false,
    this.textInputType = TextInputType.number,
    this.hint,
  }) : super(key: key);

  @override
  _NumbersInputKeyState createState() => _NumbersInputKeyState();
}

class _NumbersInputKeyState extends State<NumbersInputKey> {
  @override
  Widget build(BuildContext context) {
    return TextField(
      readOnly: widget.readOnly,
      controller: widget.controller,
      enabled: widget.enable,
      autofocus: widget.focus,
      onChanged: widget.onChange as void Function(String)?,
      keyboardType: widget.textInputType,
      onSubmitted: widget.onSubmitData as void Function(String)?,
      style: TextStyle(
        color: widget.color ?? const Color(0xFF234253),
        fontSize: SizeConfig.blockSizeVertical! * 2,
        fontWeight: FontWeight.bold,
      ),
      decoration: InputDecoration(
        focusedBorder: const OutlineInputBorder(
          borderSide: BorderSide(
            color: Colors.grey,
            width: 1.0,
          ),
          borderRadius: BorderRadius.all(
            Radius.circular(10.0),
          ),
        ),
        enabledBorder: OutlineInputBorder(
          borderSide: BorderSide(
            color: widget.color == null ? Colors.grey : widget.color!,
            width: 1.0,
          ),
          borderRadius: const BorderRadius.all(
            Radius.circular(10.0),
          ),
        ),
        labelText: widget.label,
        hintText: widget.hint,
        hintStyle: TextStyle(
            color: Colors.grey, fontSize: SizeConfig.blockSizeVertical! * 2),
        labelStyle: TextStyle(
          color: widget.color ?? const Color(0xFF234253),
          fontSize: SizeConfig.blockSizeVertical! * 2,
          fontWeight: FontWeight.bold,
        ),
        suffixIcon: widget.icon,
      ),
    );
  }
}
