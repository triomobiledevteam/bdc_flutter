import 'package:bdc/core/theme/app_Theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

import '../core/theme/sizeConfig.dart';

class Loading {
  static loading(context, {double size = 30}) {
    SizeConfig().init(context);
    return Center(
      child: SpinKitFadingCircle(
        size: size,
        itemBuilder: (BuildContext context, int index) {
          return DecoratedBox(
            decoration: BoxDecoration(
              color: index.isEven ? kPrimaryColorGreen : mainColorTheme,
            ),
          );
        },
      ),
    );
  }
}

progressDialogue(BuildContext context) {
  AlertDialog alert = AlertDialog(
    backgroundColor: Colors.transparent,
    elevation: 0,
    content: CircleAvatar(
      radius: 30,
      backgroundColor: Colors.blue,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: CircularProgressIndicator(color: whiteColor),
      ),
    ),
  );
  showDialog(
    //prevent outside touch
    barrierDismissible: false,
    context: context,
    builder: (BuildContext context) {
      //prevent Back button press
      return WillPopScope(
          onWillPop: () async {
            return true;
          },
          child: alert);
    },
  );
}
