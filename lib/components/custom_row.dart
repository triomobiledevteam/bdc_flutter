import 'package:bdc/core/theme/sizeConfig.dart';
import 'package:flutter/material.dart';

class CustomRow extends StatelessWidget {
  final String? text_1;
  final String? text_2;
  final double? fontSize;
  final bool showDoubleDot;

  const CustomRow(
      {Key? key,
      this.text_1,
      this.text_2,
      this.fontSize,
      this.showDoubleDot = true})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(
          horizontal: SizeConfig.width! * 0.05,
          vertical: SizeConfig.height! * 0.01),

      // width: SizeConfig.width * 0.9,
      child: Row(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            '$text_1 ${showDoubleDot ? ':' : ''}',
            style: TextStyle(
                fontSize: fontSize ?? SizeConfig.blockSizeVertical! * 2.5),
          ),
          Text(
            '$text_2',
            style: TextStyle(
                fontSize: fontSize ?? SizeConfig.blockSizeVertical! * 2.5),
          ),
        ],
      ),
    );
  }
}
