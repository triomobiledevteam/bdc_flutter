import 'dart:convert';

import 'package:bdc/core/modal/LoginModal/Trainer.dart';
import 'package:bdc/core/modal/constraints/global_values.dart';
import 'package:bdc/core/exception/appexception.dart';
import 'package:bdc/core/network/httpAPILinks.dart';
import 'package:http/http.dart' as http;

class GetTrainerRepository {
  editTrainer(Trainer trainer, void Function(String) onError,
      void Function(dynamic) onDone) async {
    print('trainer.toJson() ${trainer.toJson()}');
    final response = await http.post(
        Uri.parse(
            '${HttpAPILinks.defaultProxy + HttpAPILinks.normalAPI}ERMS_Trainers_Create?'),
        body: {
          'CompanyID': "$companyID",
          'TrainerID': "${trainer.iD}",
          'Body': json.encode(trainer.toJson()),
          'LanguageID': "$languageID",
        });

    return returnResponse(response, onError, onDone);
  }
}
