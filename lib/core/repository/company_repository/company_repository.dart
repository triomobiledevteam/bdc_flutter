import 'dart:async';
import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:bdc/core/exception/appexception.dart';
import 'package:bdc/core/helper/error_handler/errorhandler.dart';
import 'package:bdc/core/modal/AplicationForm.dart';
import 'package:bdc/core/modal/LoginModal/Company.dart';
import 'package:bdc/core/modal/applicationFormTrans.dart';
import 'package:bdc/core/modal/company_modal/Branch.dart';
import 'package:bdc/core/modal/company_modal/Contact.dart';
import 'package:bdc/core/modal/company_modal/JobOpportunity.dart';
import 'package:bdc/core/modal/constraints/global_values.dart';
import 'package:bdc/core/network/api_msg.dart';
import 'package:bdc/core/network/confirmParticipant.dart';
import 'package:bdc/core/network/error_massage.dart';
import 'package:bdc/core/network/httpAPILinks.dart';
import 'package:bdc/core/storage/db_client.dart';
import 'package:http/http.dart' as http;

class GetCompanyRepository {
  final db = DatabaseClient.instance;

  Future getCompanies(context) async {
    try {
      final response = await http.post(
          Uri.parse(
              '${HttpAPILinks.defaultProxy + HttpAPILinks.normalAPI}ERMS_GetInstitutions?'),
          body: {
            'UserID': '$userID',
            'CompanyID': '$companyID',
            'LanguageID': '$languageID'
          });
      print(response.statusCode);
      print(response.reasonPhrase);

      print(response.request);
      if (response.statusCode == 200) {
        if (response.body.isNotEmpty) {
          log('Company body :${response.body}');
          Iterable map = json.decode(response.body);
          mainCompanyList =
              map.map((company) => Company.fromJson(company)).toList();
          return mainCompanyList;
          // =
          //     map.map((company) => Company.fromJson(company)).toList();
        }
        // ErrorMassage errorMessage = ErrorMassage(message: 'No Data');
        ErrorHandler.errorMassage = 'No Data';
        return ErrorHandler.showErrorMassageDialog(context);
        // return errorMessage.message;
      } else {
        ErrorMassage errorMessage =
            ErrorMassage.fromJson(json.decode(response.body));

        ErrorHandler.errorMassage = errorMessage.message;
        return ErrorHandler.showErrorMassageDialog(context);
      }
    } on SocketException {
      ErrorHandler.errorMassage = 'No Internet connection';
      FetchDataException('No Internet connection');
    }
  }

  Future submitCompanyToTrio(
      Company company, Function(String) onError, void Function() onDone) async {
    try {
      print(json.encode(company.toTrio()));

      print(json.encode({
        'ID': '${company.trioID ?? 0}',
        'CompanyID': '$companyID',
        'Code': company.code,
        'LanguageID': '$languageID'
      }).toString());
      final response = await http.post(
          Uri.parse(
              '${HttpAPILinks.defaultProxy + HttpAPILinks.normalAPI}ERMS_Institution_Create?'),
          body: {
            'ID': '${company.trioID ?? 0}',
            'CompanyID': '$companyID',
            'Code': company.code,
            "Data": json.encode(company.toTrio()).toString(),
            'LanguageID': '$languageID'
          });

      print(response.statusCode);
      print(response.body);
      if (response.statusCode == 200) {
        if (response.body.isNotEmpty) {
          Iterable map = json.decode(response.body);
          ConformationResponse conformCompany = map
              .map((company) => ConformationResponse.fromJsonCompany(company))
              .first;

          company.trioID = conformCompany.iD;
          company.code = conformCompany.code;

          for (var branch in company.branchList) {
            branch.institutionID = company.trioID;
            await GetCompanyRepository()
                .submitCompanyBranchToTrio(branch, onError);
          }
          for (var contact in company.contactList) {
            contact.institutionID = company.trioID;
            print(contact.institutionID);
            await GetCompanyRepository()
                .submitCompanyContactToTrio(contact, onError);
          }

          for (var jobO in company.jobOpportunityList) {
            jobO.institutionID = company.trioID;
            await GetCompanyRepository().submitCompanyJobOToTrio(jobO, onError);
          }
          for (ApplicationForm applicationForm in company.applicationFormList) {
            ApplicationFormTrans applicationFormTrans =
                ApplicationFormTrans.fromApplicationFormToTrans(
                    applicationForm, company.trioID!);

            await GetCompanyRepository()
                .submitCompanyEmployeeFormToTrio(applicationFormTrans, onError);
          }

          conformCompany;
          return onDone();
        }
        // ErrorHandler.errorMassage = 'No Data';
        // return ErrorHandler.showErrorMassageDialog(context);
      } else {
        ErrorMassage errorMessage =
            ErrorMassage.fromJson(json.decode(response.body));
        return onError(errorMessage.message!);

        // ErrorHandler.errorMassage = errorMessage.message;
        // return ErrorHandler.showErrorMassageDialog(context);
      }
    } on SocketException {
      ErrorHandler.errorMassage = 'No Internet connection';
      FetchDataException('No Internet connection');
    }
  }

  Future submitCompanyContactToTrio(
      Contact contact, Function(String) onError) async {
    try {
      print(contact.toTrio());
      final response = await http.post(
          Uri.parse(
              '${HttpAPILinks.defaultProxy + HttpAPILinks.normalAPI}ERMS_InstitutionContact_Create?'),
          body: contact.toTrio());
      print(response.statusCode);
      print(response.request);
      if (response.statusCode == 200) {
        if (response.body.isNotEmpty) {
          print(response.body);
          ApiMsg apiMsg = ApiMsg.toJsonID(json.decode(response.body));
          contact.trioID = apiMsg.iD;
          print('${contact.trioID}');
        }
      } else {
        ErrorMassage errorMessage =
            ErrorMassage.fromJson(json.decode(response.body));

        ErrorHandler.errorMassage = errorMessage.message;
        print(errorMessage.message!);
        // return onError(errorMessage.message!);

        // return ErrorHandler.showErrorMassageDialog(context);
      }
    } on SocketException {
      ErrorHandler.errorMassage = 'No Internet connection';
      FetchDataException('No Internet connection');
    }
  }

  Future submitCompanyBranchToTrio(
      Branch branch, Function(String) onError) async {
    try {
      print(branch.toTrio());
      final response = await http.post(
          Uri.parse(
              '${HttpAPILinks.defaultProxy + HttpAPILinks.normalAPI}ERMS_InstitutionBranch_Create?'),
          body: branch.toTrio());
      print(response.statusCode);
      print(response.request);
      if (response.statusCode == 200) {
        if (response.body.isNotEmpty) {
          print(response.body);
          ApiMsg apiMsg = ApiMsg.toJsonID(json.decode(response.body));
          branch.trioID = apiMsg.iD;
          print('${branch.trioID}');
        }
      } else {
        ErrorMassage errorMessage =
            ErrorMassage.fromJson(json.decode(response.body));
        print(errorMessage.message!);
      }
    } on SocketException {
      ErrorHandler.errorMassage = 'No Internet connection';
      FetchDataException('No Internet connection');
    }
  }

  Future submitCompanyJobOToTrio(
      JobOpportunities jobOpportunity, Function(String) onError) async {
    try {
      print("ERMS_InstitutionJobOpportunities_Create");
      print(jobOpportunity.toTrio().toString());
      final response = await http.post(
          Uri.parse(
              '${HttpAPILinks.defaultProxy + HttpAPILinks.normalAPI}ERMS_InstitutionJobOpportunities_Create?'),
          body: jobOpportunity.toTrio());
      print(response.statusCode);
      print(response.request);
      if (response.statusCode == 200) {
        if (response.body.isNotEmpty) {
          ApiMsg apiMsg = ApiMsg.toJsonID(json.decode(response.body));
          jobOpportunity.trioID = apiMsg.iD;
          print('${jobOpportunity.trioID}');
        }
      } else {
        ErrorMassage errorMessage =
            ErrorMassage.fromJson(json.decode(response.body));
        //
        // ErrorHandler.errorMassage = errorMessage.message;
        // return onError(errorMessage.message!);
        print(errorMessage.message!);
      }
    } on SocketException {
      ErrorHandler.errorMassage = 'No Internet connection';
      FetchDataException('No Internet connection');
    }
  }

  Future submitCompanyEmployeeFormToTrio(
      ApplicationFormTrans applicationFormTrans,
      Function(String) onError) async {
    try {
      final response = await http.post(
          Uri.parse(
              '${HttpAPILinks.defaultProxy + HttpAPILinks.normalAPI}ERMS_InstitutionEmployeeForm_Create?'),
          body: applicationFormTrans.employeeFormToTrio());
      print(response.statusCode);
      print(response.request);
      print(applicationFormTrans.employeeFormToTrio().toString());
      if (response.statusCode == 200) {
        if (response.body.isNotEmpty) {
          print('Done');
        }
      } else {
        ErrorMassage errorMessage =
            ErrorMassage.fromJson(json.decode(response.body));

        // ErrorHandler.errorMassage = errorMessage.message;
        // return onError(errorMessage.message!);
        print(errorMessage.message!);
        // return ErrorHandler.showErrorMassageDialog(context);
      }
    } on SocketException {
      ErrorHandler.errorMassage = 'No Internet connection';
      FetchDataException('No Internet connection');
    }
  }

  Future getCompanyBranch(context, int institutionID) async {
    try {
      final response = await http.post(
          Uri.parse(
              '${HttpAPILinks.defaultProxy + HttpAPILinks.normalAPI}ERMS_GetInstitutionBranch?'),
          body: {
            'InstitutionID': '$institutionID',
          });
      print(response.statusCode);
      if (response.statusCode == 200) {
        print('getParticipantExperience ${response.body}');
        Iterable map = json.decode(response.body);

        return map.map((info) => Branch.fromJson(info)).toList();
      } else {
        ErrorMassage errorMessage =
            ErrorMassage.fromJson(json.decode(response.body));

        ErrorHandler.errorMassage = errorMessage.message;
        return ErrorHandler.showErrorMassageDialog(context);
      }
    } on SocketException {
      ErrorHandler.errorMassage = 'No Internet connection';
      FetchDataException('No Internet connection');
    }
  }

  Future getCompanyContact(context, int institutionID) async {
    try {
      final response = await http.post(
          Uri.parse(
              '${HttpAPILinks.defaultProxy + HttpAPILinks.normalAPI}ERMS_GetInstitutionContact?'),
          body: {
            'InstitutionID': '$institutionID',
          });
      print(response.statusCode);
      if (response.statusCode == 200) {
        print('getCompanyContact ${response.body}');
        Iterable map = json.decode(response.body);

        return map.map((info) => Contact.fromJson(info)).toList();
      } else {
        ErrorMassage errorMessage =
            ErrorMassage.fromJson(json.decode(response.body));

        ErrorHandler.errorMassage = errorMessage.message;
        return ErrorHandler.showErrorMassageDialog(context);
      }
    } on SocketException {
      ErrorHandler.errorMassage = 'No Internet connection';
      FetchDataException('No Internet connection');
    }
  }

  Future getCompanyJO(context, int institutionID) async {
    try {
      final response = await http.post(
          Uri.parse(
              '${HttpAPILinks.defaultProxy + HttpAPILinks.normalAPI}ERMS_GetInstitutionJobOpportunities?'),
          body: {
            'InstitutionID': '$institutionID',
          });
      print(response.statusCode);
      if (response.statusCode == 200) {
        print('getCompanyJO ${response.body}');
        Iterable map = json.decode(response.body);
        return map.map((info) => JobOpportunities.fromJson(info)).toList();
      } else {
        ErrorMassage errorMessage =
            ErrorMassage.fromJson(json.decode(response.body));

        ErrorHandler.errorMassage = errorMessage.message;
        ErrorHandler.showErrorMassageDialog(context);
        throw (errorMessage.message!);
      }
    } on SocketException {
      ErrorHandler.errorMassage = 'No Internet connection';
      FetchDataException('No Internet connection');
    }
  }

  Future getCompanyEmployeeForm(int institutionID) async {
    try {
      final response = await http.post(
          Uri.parse(
              '${HttpAPILinks.defaultProxy + HttpAPILinks.normalAPI}ERMS_Institution_GetEmployeeForm?'),
          body: {
            'InstitutionID': '$institutionID',
          });
      print(response.statusCode);
      if (response.statusCode == 200) {
        print('getCompanyEmployeeForm ${response.body}');
        Iterable map = json.decode(response.body);
        List<ApplicationFormTrans> list = map
            .map((element) => ApplicationFormTrans.fromJson(element))
            .toList();

        return list;
      } else {
        ErrorMassage errorMessage =
            ErrorMassage.fromJson(json.decode(response.body));

        ErrorHandler.errorMassage = errorMessage.message;
        // return ErrorHandler.showErrorMassageDialog(context);
      }
    } on SocketException {
      ErrorHandler.errorMassage = 'No Internet connection';
      FetchDataException('No Internet connection');
    }
  }
}
