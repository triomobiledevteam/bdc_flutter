import 'package:http/http.dart' as http;

import '../exception/appexception.dart';
import '../modal/constraints/global_values.dart';
import '../network/httpAPILinks.dart';

class ForgetPasswordRepository {
  checkUser(String mobile, void Function(dynamic) onDone,
      void Function(String) onError) async {
    try {
      final response = await http.post(
          Uri.parse(
              '${HttpAPILinks.defaultProxy + HttpAPILinks.normalAPI}Gen_User_Check?'),
          body: {
            'CompanyID': companyID.toString(),
            'Mobile': mobile,
            'LanguageID': languageID.toString(),
          });
      return returnResponse(response, onError, onDone);
    } catch (error) {
      onError(error.toString());
    }
  }

  userPasswordReset(String newPassword, String userID, Function(dynamic) onDone,
      Function(String) onError) async {
    try {
      final response = await http.post(
          Uri.parse(
              '${HttpAPILinks.defaultProxy + HttpAPILinks.normalAPI}Gen_UserPassword_Reset?'),
          body: {
            'CompanyID': companyID.toString(),
            'UserID': userID,
            'NewPassword': newPassword,
            'LanguageID': languageID.toString(),
          });
      return returnResponse(response, onError, onDone);
    } catch (error) {
      onError(error.toString());
    }
  }
}
