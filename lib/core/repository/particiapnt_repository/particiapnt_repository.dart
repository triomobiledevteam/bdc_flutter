import 'dart:async';
import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:bdc/core/exception/appexception.dart';
import 'package:bdc/core/helper/error_handler/errorhandler.dart';
import 'package:bdc/core/modal/AplicationForm.dart';
import 'package:bdc/core/modal/LoginModal/Participant.dart';
import 'package:bdc/core/modal/applicationFormTrans.dart';
import 'package:bdc/core/modal/constraints/global_values.dart';
import 'package:bdc/core/modal/participant_modal/ParticipantFamilyInfo.dart';
import 'package:bdc/core/network/confirmParticipant.dart';
import 'package:bdc/core/network/error_massage.dart';
import 'package:bdc/core/network/httpAPILinks.dart';
import 'package:bdc/core/storage/db_client.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';

import '../../modal/participant_modal/ParticipantExperience.dart';

class GetParticipantRepository {
  DateFormat dateFormatter = DateFormat('yyyy-MM-dd');
  DateFormat timeFormatter = DateFormat('jm');
  final db = DatabaseClient.instance;

  Future getParticipants(context) async {
    try {
      final response = await http.post(
          Uri.parse(
              '${HttpAPILinks.defaultProxy + HttpAPILinks.normalAPI}ERMS_GetParticipants?'),
          body: {
            'UserID': '$userID',
            'CompanyID': '$companyID',
            'LanguageID': '$languageID'
          });

      if (response.statusCode == 200) {
        if (response.body.isNotEmpty) {
          log('participant body : ${response.body}');
          Iterable map = json.decode(response.body);
          mainParticipantList.clear();
          mainParticipantList = map
              .map((participant) => Participant.fromJson(participant))
              .toList();
          return mainParticipantList;
        }
        // ErrorMassage errorMessage = ErrorMassage(message: 'No Data');
        ErrorHandler.errorMassage = 'No Data';
        return ErrorHandler.showErrorMassageDialog(context);
        // return errorMessage.message;
      } else {
        ErrorMassage errorMessage =
            ErrorMassage.fromJson(json.decode(response.body));

        ErrorHandler.errorMassage = errorMessage.message;
        return ErrorHandler.showErrorMassageDialog(context);
      }
    } on SocketException {
      ErrorHandler.errorMassage = 'No Internet connection';
      FetchDataException('No Internet connection');
    }
  }

  Future submitParticipantToTrio(Participant participant,
      Function(String) onError, void Function() onDone) async {
    try {
      print(participant.assignToJson());
      final response = await http.post(
          Uri.parse(
              '${HttpAPILinks.defaultProxy + HttpAPILinks.normalAPI}ERMS_Participant_Create?'),
          body: {
            'CompanyID': '$companyID',
            'Code': participant.code,
            "Data": json.encode(participant.assignToJson()),
            'LanguageID': '$languageID'
          });

      print(response.statusCode);
      print(response.reasonPhrase);

      if (response.statusCode == 200) {
        if (response.body.isNotEmpty) {
          print(response.body);
          Iterable map = json.decode(response.body);
          ConformationResponse conformParticipant = map
              .map((participant) =>
                  ConformationResponse.fromJsonParticipant(participant))
              .first;
          participant.trioID = conformParticipant.iD;
          participant.code = conformParticipant.code;

          for (var experience in participant.experienceList) {
            experience.participantID = participant.trioID;
            experience.participantID = participant.trioID;
            print("${experience.participantID}${experience.descriptionEn}");

            await submitParticipantExperienceToTrio(experience, onError);
          }

          for (var familyInfo in participant.familyInfoList) {
            familyInfo.participantID = participant.trioID;
            print("${familyInfo.participantID}${familyInfo.fullNameEn}");
            await submitParticipantFamilyInfoToTrio(familyInfo, onError);
          }

          for (ApplicationForm applicationForm
              in participant.applicationFormList) {
            ApplicationFormTrans applicationFormTrans =
                ApplicationFormTrans.fromApplicationFormToTrans(
                    applicationForm, participant.trioID!);

            await submitParticipantApplicationFormToTrio(
                applicationFormTrans, onError);
          }
          conformParticipant;

          return onDone();
        }
        ErrorHandler.errorMassage = 'No Data';
        return onError(ErrorHandler.errorMassage!);

        // return ErrorHandler.showErrorMassageDialog(context);
      } else {
        ErrorMassage errorMessage =
            ErrorMassage.fromJson(json.decode(response.body));

        // ErrorHandler.errorMassage = errorMessage.message;
        return onError(errorMessage.message!);
      }
    } on SocketException {
      ErrorHandler.errorMassage = 'No Internet connection';
      FetchDataException('No Internet connection');
    }
  }

  Future submitParticipantFamilyInfoToTrio(
      ParticipantFamilyInfo familyInfo, Function(String) onError) async {
    try {
      final response = await http.post(
          Uri.parse(
              '${HttpAPILinks.defaultProxy + HttpAPILinks.normalAPI}ERMS_ParticipantFamilyInfo_Create?'),
          body: familyInfo.toTrio());
      print(response.statusCode);
      print(response.reasonPhrase);

      print(response.body);
      if (response.statusCode == 200) {
        // Iterable map = json.decode(response.body);
        // ApiMsg? msg = map.map((e) => ApiMsg.toJsonID(e)).first;
        // familyInfo.trioID = msg.iD;
        return familyInfo;
      } else {
        ErrorMassage errorMessage =
            ErrorMassage.fromJson(json.decode(response.body));

        // ErrorHandler.errorMassage = errorMessage.message;
        // return onError(errorMessage.message!);
        print(errorMessage.message!);
        // return ErrorHandler.showErrorMassageDialog(context);
      }
    } on SocketException {
      ErrorHandler.errorMassage = 'No Internet connection';
      FetchDataException('No Internet connection');
    }
  }

  Future submitParticipantExperienceToTrio(
      ParticipantExperience experience, Function(String) onError) async {
    try {
      final response = await http.post(
          Uri.parse(
              '${HttpAPILinks.defaultProxy + HttpAPILinks.normalAPI}ERMS_ParticipantExperience_Create?'),
          body: experience.toTrio());

      print(response.body);
      print(response.statusCode);
      print(response.reasonPhrase);

      if (response.statusCode == 200) {
        // Iterable map = json.decode(response.body);
        // ApiMsg? msg = map.map((e) => ApiMsg.toJsonID(e)).first;
        // experience.trioID = msg.iD;
        return experience;
      } else {
        ErrorMassage errorMessage =
            ErrorMassage.fromJson(json.decode(response.body));
        //
        // ErrorHandler.errorMassage = errorMessage.message;
        // return onError(errorMessage.message!);
        print(errorMessage.message!);
      }
    } on SocketException {
      ErrorHandler.errorMassage = 'No Internet connection';
      FetchDataException('No Internet connection');
    }
  }

  Future submitParticipantApplicationFormToTrio(
      ApplicationFormTrans applicationForm, Function(String) onError) async {
    print(applicationForm.applicationFormToTrio());
    try {
      final response = await http.post(
          Uri.parse(
              '${HttpAPILinks.defaultProxy + HttpAPILinks.normalAPI}ERMS_ParticipantApplicationForms_Create?'),
          body: applicationForm.applicationFormToTrio());
      print(response.statusCode);
      print(response.reasonPhrase);

      print(response.request);
      if (response.statusCode == 200) {
        if (response.body.isNotEmpty) {}
      } else {
        ErrorMassage errorMessage =
            ErrorMassage.fromJson(json.decode(response.body));
        //
        // ErrorHandler.errorMassage = errorMessage.message;
        // return onError(errorMessage.message!);
        print(errorMessage.message!);
        // return ErrorHandler.showErrorMassageDialog(context);
      }
    } on SocketException {
      ErrorHandler.errorMassage = 'No Internet connection';
      FetchDataException('No Internet connection');
    }
  }

  Future getParticipantFamilyInfo(context, int participantID) async {
    try {
      final response = await http.post(
          Uri.parse(
              '${HttpAPILinks.defaultProxy + HttpAPILinks.normalAPI}ERMS_GetParticipantFamilyInfo?'),
          body: {
            'ParticipantID': '$participantID',
          });
      print(response.statusCode);

      if (response.statusCode == 200) {
        print('getParticipantFamilyInfo ${response.body}');
        Iterable map = json.decode(response.body);
        // return mainParticipant!.familyInfoList =
        return map.map((info) => ParticipantFamilyInfo.fromJson(info)).toList();
      } else {
        ErrorMassage errorMessage =
            ErrorMassage.fromJson(json.decode(response.body));

        ErrorHandler.errorMassage = errorMessage.message;
        return ErrorHandler.showErrorMassageDialog(context);
      }
    } on SocketException {
      ErrorHandler.errorMassage = 'No Internet connection';
      FetchDataException('No Internet connection');
    }
  }

  Future getParticipantExperience(context, int participantID) async {
    try {
      final response = await http.post(
          Uri.parse(
              '${HttpAPILinks.defaultProxy + HttpAPILinks.normalAPI}ERMS_GetParticipantExperience?'),
          body: {
            'ParticipantID': '$participantID',
          });
      print(response.statusCode);
      if (response.statusCode == 200) {
        print('getParticipantExperience ${response.body}');
        Iterable map = json.decode(response.body);

        return map.map((info) => ParticipantExperience.fromJson(info)).toList();
      } else {
        ErrorMassage errorMessage =
            ErrorMassage.fromJson(json.decode(response.body));

        ErrorHandler.errorMassage = errorMessage.message;
        return ErrorHandler.showErrorMassageDialog(context);
      }
    } on SocketException {
      ErrorHandler.errorMassage = 'No Internet connection';
      FetchDataException('No Internet connection');
    }
  }

  Future getParticipantApplicationForm(int participantID) async {
    try {
      final response = await http.post(
          Uri.parse(
              '${HttpAPILinks.defaultProxy + HttpAPILinks.normalAPI}ERMS_Participant_GetApplicationForm?'),
          body: {
            'ParticipantID': '$participantID',
          });
      print(response.statusCode);
      if (response.statusCode == 200) {
        print('getParticipantApplicationForm ${response.body}');
        Iterable map = json.decode(response.body);
        return map
            .map((element) => ApplicationFormTrans.fromJson(element))
            .toList();
      } else {
        ErrorMassage errorMessage =
            ErrorMassage.fromJson(json.decode(response.body));

        ErrorHandler.errorMassage = errorMessage.message;
        // return ErrorHandler.showErrorMassageDialog(context);
      }
    } on SocketException {
      ErrorHandler.errorMassage = 'No Internet connection';
      FetchDataException('No Internet connection');
    }
  }

  Future participantProjectEnroll(int projectID, void Function(String) onError,
      void Function(String) onDone) async {
    try {
      final response = await http.post(
          Uri.parse(
              '${HttpAPILinks.defaultProxy + HttpAPILinks.normalAPI}ERMS_Participant_ProjectEnroll?'),
          body: {
            'CompanyID': '$companyID',
            'ParticipantID': '${mainParticipant!.trioID}',
            'ProjectID': '$projectID',
            'LanguageID': '$languageID',
          });
      print(response.statusCode);
      if (response.statusCode == 200) {
        if (response.body.isNotEmpty) {
          Iterable map = json.decode(response.body);
          ErrorMassage massage =
              map.map((map) => ErrorMassage.fromJson(map)).first;
          // ErrorMassage.fromJson(map);
          return onDone(massage.message!);
        } else {
          return onError("Try Again Later");
        }
      } else {
        ErrorMassage errorMessage =
            ErrorMassage.fromJson(json.decode(response.body));
        return onError(errorMessage.message!);
        // ErrorHandler.errorMassage = errorMessage.message;
        // return ErrorHandler.showErrorMassageDialog(context);
      }
    } on SocketException {
      ErrorHandler.errorMassage = 'No Internet connection';
      FetchDataException('No Internet connection');
    }
  }

  Future participantUploadFile(Participant trainer, File file,
      void Function(String) onError, void Function(dynamic) onDone) async {
    var length = await file.length();
    var uri = Uri.parse(
        '${HttpAPILinks.defaultProxy + HttpAPILinks.normalAPI}ERMS_ParticipantAttachments_UpLoad?');
    var request = http.MultipartRequest("POST", uri);
    request.files.add(http.MultipartFile(
        'AttachmentFile', file.readAsBytes().asStream(), length,
        filename: file.path.split('/').last));
    request.fields['ParticipantID'] = "${trainer.trioID!}";
    request.fields['FileName'] = file.path.split('/').last;
    request.fields['LanguageID'] = "$languageID";
    request.headers['Content-Type'] = 'multipart/form-data';

    print('fileDescription :${file.path.split('/').last}');

    var response = await request.send();
    returnStreamedResponse(response, onError, onDone);
  }

  Future participantAttachmentCheck(Participant participant,
      Function(String) onError, void Function(dynamic) onDone) async {
    try {
      final response = await http.post(
          Uri.parse(
              '${HttpAPILinks.defaultProxy + HttpAPILinks.normalAPI}ERMS_ParticipantAttachments_Check?'),
          body: {
            'CompanyID': '$companyID',
            'ParticipantID': '${participant.trioID}',
            'LanguageID': '$languageID',
          });
      return returnResponse(response, onError, onDone);
    } on SocketException {
      ErrorHandler.errorMassage = 'No Internet connection';
      FetchDataException('No Internet connection');
    }
  }
}
