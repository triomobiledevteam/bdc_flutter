import 'dart:convert';
import 'dart:developer';

import 'package:bdc/core/exception/appexception.dart';
import 'package:bdc/core/modal/constraints/global_values.dart';
import 'package:bdc/core/modal/otp_modal/sms_body.dart';
import 'package:bdc/core/network/httpAPILinks.dart';
import 'package:http/http.dart' as http;

class GetOtpRepository {
  registrationCode(String mobile, void Function(String) onError,
      void Function(dynamic) onDone,
      [bool resetPassword = false]) async {
    try {
      http.Response response = await http.post(
          Uri.parse(
              '${HttpAPILinks.defaultProxy + HttpAPILinks.normalAPI}ERMS_SendSMSCode?'),
          body: {
            "CompanyID": "$companyID",
            "Data":
                json.encode({'Mobile': mobile, 'ResetPassword': resetPassword}),
            "LanguageID": "$languageID",
          });
      return returnResponse(response, onError, onDone);
    } catch (error) {
      onError(error.toString());
    }
  }

  sendSmsRequest(SmsBody smsBody, void Function(String) onError,
      void Function(dynamic) onDone) async {
    try {
      log('${HttpAPILinks.defaultProxy + HttpAPILinks.smsAPI}?');
      final response = await http.post(
          Uri.parse('${HttpAPILinks.defaultProxy + HttpAPILinks.smsAPI}?'),
          body: {
            "CompanyID": "$companyID",
            "MessageContent": smsBody.massage,
            "SendToNumber": smsBody.mobile,
          });

      log(response.body.toString());
      return returnResponse(response, onError, onDone);
    } catch (error) {
      onError(error.toString());
    }
  }
}
