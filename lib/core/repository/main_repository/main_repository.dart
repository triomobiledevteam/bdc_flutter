import 'dart:async';
import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:bdc/core/exception/appexception.dart';
import 'package:bdc/core/modal/AplicationForm.dart';
import 'package:bdc/core/modal/BDCNotification.dart';
import 'package:bdc/core/modal/LoginModal/Employee.dart';
import 'package:bdc/core/modal/LoginModal/GenUser.dart';
import 'package:bdc/core/modal/LoginModal/Trainer.dart';
import 'package:bdc/core/modal/LoginModal/certificate.dart';
import 'package:bdc/core/modal/Lookups.dart';
import 'package:bdc/core/modal/News.dart';
import 'package:bdc/core/modal/constraints/global_values.dart';
import 'package:bdc/core/network/api_msg.dart';
import 'package:bdc/core/network/error_massage.dart';
import 'package:bdc/core/network/httpAPILinks.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import '../../helper/error_handler/errorhandler.dart';
import '../../modal/registration_modal/registrationModal.dart';

class GetMainRepository {
  // final db = DatabaseClient.instance;

  Future login(Certificate certificate, void Function(dynamic) onDone,
      Function(String error) onError) async {
    try {
      print(
          'password ${certificate.password}\n usercode ${certificate.email}\n userAccount ${certificate.accountType}\n');
      final response = await http.post(
          Uri.parse(
              '${HttpAPILinks.defaultProxy + HttpAPILinks.normalAPI}ERMS_Users_Login?'),
          body: {
            'UserCode': certificate.email,
            'Password': certificate.password,
            'UserType': certificate.accountType,
            'LanguageID': '$languageID'
          }).timeout(const Duration(seconds: 30));
      log('body ${response.body.toString()}');
      return returnResponse(response, onError, onDone);
    } on SocketException {
      return onError('No Internet connection');
    }
  }

  // Future userLogin(Certificate certificate) async {
  //   print(userCode + ' ' + password);
  //   try {
  //     final response = await http.post(
  //         Uri.parse(
  //             '${HttpAPILinks.defaultProxy + HttpAPILinks.normalAPI}ERMS_Users_Login?'),
  //         body: {
  //           'UserCode': '${certificate.email}',
  //           'Password': '${certificate.password}',
  //           'UserType': '${certificate.accountType}',
  //           'LanguageID': '$languageID'
  //         }).timeout(Duration(seconds: 30));
  //     print('body ${response.body.toString()}');
  //     if (response.statusCode == 200) {
  //       if (response.body.isNotEmpty) {
  //         print('body ${response.body.toString()}');
  //         Iterable map = json.decode(response.body);
  //         return map;
  //         // Employee employee = map.map((emp) => Employee.fromJson(emp)).first;
  //         // return employee;
  //       }
  //       ErrorMassage errorMessage = ErrorMassage(message: 'No Data');
  //       return errorMessage.message;
  //     } else {
  //       ErrorMassage errorMessage =
  //           ErrorMassage.fromJson(json.decode(response.body));
  //
  //       return errorMessage.message ?? '';
  //     }
  //   } on SocketException {
  //     ErrorHandler.errorMassage = 'No Internet connection';
  //     FetchDataException('No Internet connection');
  //   }
  // }

  userInfo(int languageID, int empID, String password, String deviceType,
      BuildContext context) async {
    try {
      final response = await http.post(
          Uri.parse(
              '${HttpAPILinks.defaultProxy + HttpAPILinks.normalAPI}POS_UserInfo?'),
          body: {
            'UserID': '$empID',
            'Password': password,
            'DeviceType': deviceType,
            'LanguageID': '$languageID'
          });
      return response;
    } on SocketException {
      ErrorHandler.errorMassage = 'No Internet connection';
      ErrorHandler.showErrorMassage(context);
      return FetchDataException('No Internet connection');
    }
  }

  Future getLookUps({int? companyID, BuildContext? context}) async {
    try {
      final response = await http.post(
          Uri.parse(
              '${HttpAPILinks.defaultProxy + HttpAPILinks.normalAPI}Gen_GetLookUps?'),
          body: {
            'CompanyID': '${companyID ?? 1}',
          });

      print('getLookUps ${response.statusCode}');
      if (response.statusCode == 200) {
        if (response.body.isNotEmpty) {
          Iterable map = json.decode(response.body);
          List<LookUps> lookUpList =
              map.map((item) => LookUps.fromJson(item)).toList();
          print('LookUps ${lookUpList.length}');
          await LookUps().fetchToLocalDB(lookUpList);
          return;
        }
        ErrorMassage errorMessage = ErrorMassage(message: 'No Data');
        return errorMessage.message;
      } else {
        ErrorMassage errorMessage =
            ErrorMassage.fromJson(json.decode(response.body));

        return errorMessage.message ?? '';
      }
    } on SocketException {
      ErrorHandler.errorMassage = 'No Internet connection';
      ErrorHandler.showErrorMassage(context);
      return FetchDataException('No Internet connection');
    }
  }

  Future getApplicationForm({required BuildContext context}) async {
    try {
      final response = await http.post(
          Uri.parse(
              '${HttpAPILinks.defaultProxy + HttpAPILinks.normalAPI}ERMS_GetApplicationForm?'),
          body: {
            'CompanyID': '$companyID',
          });

      debugPrint('getApplicationForm ${response.body}', wrapWidth: 10000);
      if (response.statusCode == 200) {
        if (response.body.isNotEmpty) {
          Iterable map = json.decode(response.body);
          List<ApplicationForm> appFormList =
              map.map((item) => ApplicationForm.fromJson(item)).toList();
          print('LookUps ${appFormList.length}');
          await ApplicationForm().fetchToLocalDB(appFormList);
          return;
        }
        ErrorMassage errorMessage = ErrorMassage(message: 'No Data');
        return errorMessage.message;
      } else {
        ErrorMassage errorMessage =
            ErrorMassage.fromJson(json.decode(response.body));

        return errorMessage.message ?? '';
      }
    } on SocketException {
      ErrorHandler.errorMassage = 'No Internet connection';
      ErrorHandler.showErrorMassage(context);
      return FetchDataException('No Internet connection');
    }
  }

  Future getNews({BuildContext? context}) async {
    try {
      final response = await http.post(
          Uri.parse(
              '${HttpAPILinks.defaultProxy + HttpAPILinks.normalAPI}ERMS_GetNews?'),
          body: {
            'CompanyID': '$companyID',
            'LanguageID': '$languageID',
          });

      print('getNews ${response.body}');
      if (response.statusCode == 200) {
        if (response.body.isNotEmpty) {
          Iterable map = json.decode(response.body);
          List<News> newsList = map.map((item) => News.fromJson(item)).toList();
          print('LookUps ${newsList.length}');
          return newsList;
        }
        ErrorMassage errorMessage = ErrorMassage(message: 'No Data');
        return errorMessage.message;
      } else {
        ErrorMassage errorMessage =
            ErrorMassage.fromJson(json.decode(response.body));

        return errorMessage.message ?? '';
      }
    } on SocketException {
      ErrorHandler.errorMassage = 'No Internet connection';
      ErrorHandler.showErrorMassage(context);
      return FetchDataException('No Internet connection');
    }
  }

  getPrograms(void Function(String) onError, Function(dynamic) onDone) async {
    try {
      final response = await http.post(
          Uri.parse(
              '${HttpAPILinks.defaultProxy + HttpAPILinks.normalAPI}ERMS_GetPrograms?'),
          body: {
            'CompanyID': '$companyID',
            'LanguageID': '$languageID',
          });
      return returnResponse(response, onError, onDone);
    } on SocketException {
      return onError('No Internet connection');
    }
  }

  Future getNotifications({BuildContext? context}) async {
    try {
      final response = await http.post(
          Uri.parse(
              '${HttpAPILinks.defaultProxy + HttpAPILinks.normalAPI}ERMS_GetUsersNotification?'),
          body: {
            'CompanyID': '$companyID',
            'UserID': '$userID',
            'LanguageID': '$languageID',
          });
      print('getNotifications ${response.body}');
      if (response.statusCode == 200) {
        if (response.body.isNotEmpty) {
          Iterable map = json.decode(response.body);
          List<BDCNotification> bdcNotificationList =
              map.map((item) => BDCNotification.fromJson(item)).toList();
          return bdcNotificationList;
        }
        ErrorMassage errorMessage = ErrorMassage(message: 'No Data');
        return errorMessage.message;
      } else {
        ErrorMassage errorMessage =
            ErrorMassage.fromJson(json.decode(response.body));

        return errorMessage.message ?? '';
      }
    } on SocketException {
      ErrorHandler.errorMassage = 'No Internet connection';
      ErrorHandler.showErrorMassage(context);
      return FetchDataException('No Internet connection');
    }
  }

  Future genUserUpdateInfo(
      String userName,
      String nationalityID,
      String mobile,
      String email,
      void Function(String) onError,
      void Function() onDone) async {
    try {
      final response = await http.post(
          Uri.parse(
              '${HttpAPILinks.defaultProxy + HttpAPILinks.normalAPI}ERMS_GenUsers_Update?'),
          body: {
            'UserID': "${mainUser.userID}",
            'UserName': userName,
            'NationalityID': nationalityID,
            'Mobile': mobile,
            'Email': email,
            'LanguageID': '$languageID'
          });
      if (response.statusCode == 200) {
        if (response.body.isNotEmpty) {
          print('body ${response.body.toString()}');
          Iterable map = json.decode(response.body);
          GenUser user = map.map((user) => GenUser.fromJson(user)).first;
          mainUser = user;
          return onDone();
        } else {
          return onError("No Data");
        }
      } else {
        ErrorMassage errorMessage =
            ErrorMassage.fromJson(json.decode(response.body));
        return onError(errorMessage.message!);
      }
    } on SocketException {
      ErrorHandler.errorMassage = 'No Internet connection';
      return onError(ErrorHandler.errorMassage!);
    }
  }

  Future checkEmployee(int userID, String password) async {
    print(userID.toString() + ' ' + password);
    try {
      final response = await http.post(
          Uri.parse(
              '${HttpAPILinks.defaultProxy + HttpAPILinks.normalAPI}ERMS_Employee_CheckInfo?'),
          // UserID	,  Password   , 	LanguageID
          body: {
            'UserID': '$userID',
            'Password': password,
            'LanguageID': '$languageID'
          }).timeout(const Duration(seconds: 30));
      print('body ${response.body.toString()}');
      if (response.statusCode == 200) {
        if (response.body.isNotEmpty) {
          print('body ${response.body.toString()}');
          Iterable map = json.decode(response.body);
          Employee employee = map.map((emp) => Employee.fromJson(emp)).first;
          return employee;
        }
        ErrorMassage errorMessage = ErrorMassage(message: 'No Data');
        return errorMessage.message;
      } else {
        ErrorMassage errorMessage =
            ErrorMassage.fromJson(json.decode(response.body));

        return errorMessage.message ?? '';
      }
    } on SocketException {
      ErrorHandler.errorMassage = 'No Internet connection';
      FetchDataException('No Internet connection');
    }
  }

  Future checkUser(int userID, String password) async {
    print(userID.toString() + ' ' + password);
    try {
      final response = await http.post(
          Uri.parse(
              '${HttpAPILinks.defaultProxy + HttpAPILinks.normalAPI}ERMS_GenUsers_CheckInfo?'),
          body: {
            'UserID': '$userID',
            'Password': password,
            'LanguageID': '$languageID'
          }).timeout(const Duration(seconds: 20));
      print('body ${response.body.toString()}');
      if (response.statusCode == 200) {
        if (response.body.isNotEmpty) {
          print('body ${response.body.toString()}');
          Iterable map = json.decode(response.body);
          return map;
        }
        ErrorMassage errorMessage = ErrorMassage(message: 'No Data');
        return errorMessage.message;
      } else {
        ErrorMassage errorMessage =
            ErrorMassage.fromJson(json.decode(response.body));

        return errorMessage.message ?? '';
      }
    } on SocketException {
      ErrorHandler.errorMassage = 'No Internet connection';
      FetchDataException('No Internet connection');
    }
  }

  Future userUploadImage(File imageFile, void Function(String) onError,
      void Function(dynamic) onDone) async {
    var length = await imageFile.length();
    print(length);
    var uri = Uri.parse(
        '${HttpAPILinks.defaultProxy + HttpAPILinks.normalAPI}Gen_GenUsers_ChangeImage?');
    var request = http.MultipartRequest("POST", uri);
    request.files.add(http.MultipartFile(
        'Image', imageFile.readAsBytes().asStream(), length,
        filename: imageFile.path.split('/').last));
    request.fields['LanguageID'] = "$languageID";
    request.fields['UserID'] = "$userID";
    request.headers['Content-Type'] = 'multipart/form-data';
    var response = await request.send();
    returnStreamedResponse(response, onError, onDone);
  }

  Future employeeUploadImage(File imageFile, void Function(String) onError,
      void Function(dynamic) onDone) async {
    var length = await imageFile.length();
    print(length);
    var uri = Uri.parse(
        '${HttpAPILinks.defaultProxy + HttpAPILinks.normalAPI}Gen_Employee_ChangeImage?');
    var request = http.MultipartRequest("POST", uri);
    request.files.add(http.MultipartFile(
        'Image', imageFile.readAsBytes().asStream(), length,
        filename: imageFile.path.split('/').last));
    request.fields['LanguageID'] = "$languageID";
    request.fields['UserID'] = "$userID";
    request.headers['Content-Type'] = 'multipart/form-data';
    var response = await request.send();
    returnStreamedResponse(response, onError, onDone);
  }

  Future trainerUploadFile(Trainer trainer, File file,
      void Function(String) onError, void Function(dynamic) onDone) async {
    var length = await file.length();
    var uri = Uri.parse(
        '${HttpAPILinks.defaultProxy + HttpAPILinks.normalAPI}ERMS_Trainer_UploadCV?');
    var request = http.MultipartRequest("POST", uri);
    request.files.add(http.MultipartFile(
        'File', file.readAsBytes().asStream(), length,
        filename: file.path.split('/').last));

    request.fields['UserID'] = "${trainer.iD!}";
    request.fields['FileName'] = file.path.split('/').last;
    request.fields['LanguageID'] = "$languageID";
    request.headers['Content-Type'] = 'multipart/form-data';

    var response = await request.send();
    returnStreamedResponse(response, onError, onDone);
  }

  Future registerUser(Registration registration, void Function(String) onError,
      void Function(dynamic) onDone) async {
    try {
      print(json.encode(registration.toJson()));
      final response = await http.post(
          Uri.parse(
              '${HttpAPILinks.defaultProxy + HttpAPILinks.normalAPI}ERMS_Registration?'),
          body: {
            "UserReference": 'ERMS',
            "Body": json.encode(registration.toJson()),
            "LanguageID": '$languageID',
          });
      return returnResponse(response, onError, onDone);
    } on SocketException {
      return onError('No Internet connection');
    }
  }

  Future checkData(String email, String mobile, void Function(String) onError,
      void Function(dynamic) onDone) async {
    try {
      final response = await http.post(
          Uri.parse(
              '${HttpAPILinks.defaultProxy + HttpAPILinks.normalAPI}Gen_EmailAndMobile_Check?'),
          body: {
            "Email": email,
            "Mobile": mobile,
            "LanguageID": '$languageID',
          });
      return returnResponse(response, onError, onDone);
    } on SocketException {
      return onError('No Internet connection');
    }
  }

  Future employeeChangePassword(String oldPassword, String newPassword,
      void Function(String) onError, void Function() onDone) async {
    try {
      final response = await http.post(
          Uri.parse(
              '${HttpAPILinks.defaultProxy + HttpAPILinks.normalAPI}Gen_Users_ChangePassword?'),
          body: {
            'UserID': '${mainEmployee.userID}',
            'OldPassword': oldPassword,
            'NewPassword': newPassword,
            'LanguageID': '$languageID'
          });
      if (response.statusCode == 200) {
        print('body ${response.body.toString()}');
        if (response.body.isNotEmpty) {
          Iterable map = json.decode(response.body);
          ApiMsg msg = map.map((element) => ApiMsg.toJsonID(element)).first;
          // UserPreferences().password = newPassword;
          print(msg.iD);
          return onDone();
        }
        return onError('Error');
      } else {
        ErrorMassage errorMessage =
            ErrorMassage.fromJson(json.decode(response.body));
        return onError(errorMessage.message!);
      }
    } on SocketException {
      ErrorHandler.errorMassage = 'No Internet connection';
      return onError(ErrorHandler.errorMassage!);
    }
  }

  Future userChangePassword(String oldPassword, String newPassword,
      void Function(String) onError, void Function() onDone) async {
    try {
      final response = await http.post(
          Uri.parse(
              '${HttpAPILinks.defaultProxy + HttpAPILinks.normalAPI}Gen_GenUsers_ChangePassword?'),
          body: {
            'UserID': '${mainUser.userID}',
            'OldPassword': oldPassword,
            'NewPassword': newPassword,
            'LanguageID': '$languageID'
          });
      if (response.statusCode == 200) {
        if (response.body.isNotEmpty) {
          Iterable map = json.decode(response.body);
          ApiMsg msg = map.map((element) => ApiMsg.toJsonID(element)).first;
          // UserPreferences().password = newPassword;
          print(msg.iD);

          return onDone();
        }
        return onError('Error');
      } else {
        ErrorMassage errorMessage =
            ErrorMassage.fromJson(json.decode(response.body));

        return onError(errorMessage.message!);
      }
    } on SocketException {
      ErrorHandler.errorMassage = 'No Internet connection';
      return onError(ErrorHandler.errorMassage!);
    }
  }
}
