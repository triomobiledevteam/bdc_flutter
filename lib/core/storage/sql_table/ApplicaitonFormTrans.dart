//--------------AppFormTrans--------------
const String tableAppFormTrans = 'AppFormTrans';

const String columnAppFormTransID = 'ID';
const String columnAppFormTransUserID = 'LocalUserID';
const String columnAppFormTransTrioUserID = 'UserID';
const String columnAppFormTransCheckListID = 'CheckListID';
const String columnAppFormTransFormType = 'FormType';
const String columnAppFormTransYes = 'Yes';
const String columnAppFormTransNo = 'No';
const String columnAppFormTransNotApplicable = 'NotApplicable';

//CREATE
String createTableAppFormTrans = '''
 CREATE TABLE $tableAppFormTrans (
                $columnAppFormTransID  INTEGER PRIMARY KEY AUTOINCREMENT,
                $columnAppFormTransUserID INTEGER,
                $columnAppFormTransTrioUserID INTEGER,
                $columnAppFormTransCheckListID INTEGER,
                $columnAppFormTransFormType TEXT,
                $columnAppFormTransYes INTEGER,
                $columnAppFormTransNo INTEGER,
                $columnAppFormTransNotApplicable INTEGER
               )
''';
