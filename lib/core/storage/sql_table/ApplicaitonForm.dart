//--------------AppForm--------------
const String tableAppForm = 'AppForm';

const String columnAppFormID = 'ID';
const String columnAppFormDescriptionEn = 'DescriptionEn';
const String columnAppFormDescriptionAr = 'DescriptionAr';
const String columnAppFormFormType = 'FormType';

//CREATE
String createTableAppFrom = '''
 CREATE TABLE $tableAppForm (
               $columnAppFormID  INTEGER,
               $columnAppFormDescriptionEn TEXT,
               $columnAppFormDescriptionAr TEXT,
               $columnAppFormFormType TEXT
               )
''';
