//--------------Nationality--------------
import 'table_parameters.dart';

const String tableNationality = 'Nationality';

//CREATE
String createTableNationality = '''
 CREATE TABLE $tableNationality (
               $columnID TEXT Primary Key,
               $columnDescriptionEn TEXT,
               $columnDescriptionAr TEXT,
               $columnParentID TEXT
               )
''';
