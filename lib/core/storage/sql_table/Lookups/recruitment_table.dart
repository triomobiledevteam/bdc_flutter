//--------------Recruitment--------------
import 'table_parameters.dart';

const String tableRecruitment = 'Recruitment';

//CREATE
String createTableRecruitment = '''
 CREATE TABLE $tableRecruitment (
              $columnID TEXT Primary Key,
               $columnDescriptionEn TEXT,
               $columnDescriptionAr TEXT,
               $columnParentID TEXT
               )
''';
