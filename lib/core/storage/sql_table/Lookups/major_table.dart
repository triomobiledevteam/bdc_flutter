//--------------Majors--------------
import 'table_parameters.dart';

const String tableMajor = 'Majors';

//CREATE
String createTableMajor = '''
 CREATE TABLE $tableMajor (
               $columnID TEXT Primary Key,
               $columnDescriptionEn TEXT,
               $columnDescriptionAr TEXT,
               $columnParentID TEXT
               )
''';
