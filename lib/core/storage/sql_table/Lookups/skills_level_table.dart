import 'table_parameters.dart';

//--------------SkillsLevel--------------

const String tableSkillsLevel = 'SkillsLevel';

//CREATE
String createTableSkillsLevel = '''
 CREATE TABLE $tableSkillsLevel (
               $columnID TEXT Primary Key,
               $columnDescriptionEn TEXT,
               $columnDescriptionAr TEXT,
               $columnParentID TEXT
               )
''';
