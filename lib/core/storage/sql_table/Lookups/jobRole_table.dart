//--------------JobRole--------------
import 'table_parameters.dart';

const String tableJobRole = 'JobRole';

//CREATE
String createTableJobRole = '''
 CREATE TABLE $tableJobRole (
               $columnID TEXT Primary Key,
               $columnDescriptionEn TEXT,
               $columnDescriptionAr TEXT,
               $columnParentID TEXT
               )
''';
