//--------------EducationalLevel--------------
import 'table_parameters.dart';

const String tableEduLevel = 'EducationalLevel';

//CREATE
String createTableEducationalLevel = '''
 CREATE TABLE $tableEduLevel (
               $columnID TEXT Primary Key,
               $columnDescriptionEn TEXT,
               $columnDescriptionAr TEXT,
               $columnParentID TEXT
               )
''';
