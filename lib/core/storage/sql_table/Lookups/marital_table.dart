//--------------Marital--------------
import 'table_parameters.dart';

const String tableMarital = 'Marital';

//CREATE
String createTableMarital = '''
 CREATE TABLE $tableMarital (
               $columnID TEXT Primary Key,
               $columnDescriptionEn TEXT,
               $columnDescriptionAr TEXT,
               $columnParentID TEXT
               )
''';
