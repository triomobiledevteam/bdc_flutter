//--------------LastUsed--------------
import 'table_parameters.dart';

const String tableLastUsed = 'LastUsed';

//CREATE
String createTableLastUsed = '''
 CREATE TABLE $tableLastUsed (
               $columnID TEXT Primary Key,
               $columnDescriptionEn TEXT,
               $columnDescriptionAr TEXT,
               $columnParentID TEXT
               )
''';
