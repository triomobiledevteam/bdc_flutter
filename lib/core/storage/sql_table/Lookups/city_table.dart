//--------------City--------------
import 'table_parameters.dart';

const String tableCity = 'City';

//CREATE
String createTableCity = '''
 CREATE TABLE $tableCity (
               $columnID TEXT Primary Key,
               $columnDescriptionEn TEXT,
               $columnDescriptionAr TEXT,
               $columnParentID TEXT
               )
''';
