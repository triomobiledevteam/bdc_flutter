import 'table_parameters.dart';

//--------------Sector--------------

const String tableSector = 'Sector';

//CREATE
String createTableSector = '''
 CREATE TABLE $tableSector (
               $columnID TEXT Primary Key,
               $columnDescriptionEn TEXT,
               $columnDescriptionAr TEXT,
               $columnParentID TEXT
               )
''';
