//--------------ExperienceYears--------------
import 'table_parameters.dart';

const String tableExperienceYears = 'ExperienceYears';

//CREATE
String createTableExperienceYears = '''
 CREATE TABLE $tableExperienceYears (
               $columnID TEXT Primary Key,
               $columnDescriptionEn TEXT,
               $columnDescriptionAr TEXT,
               $columnParentID TEXT
               )
''';
