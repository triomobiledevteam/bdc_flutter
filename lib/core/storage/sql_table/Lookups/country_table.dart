//--------------Country--------------
import 'table_parameters.dart';

const String tableCountry = 'Country';

//CREATE
String createTableCountry = '''
 CREATE TABLE $tableCountry (
               $columnID TEXT Primary Key,
               $columnDescriptionEn TEXT,
               $columnDescriptionAr TEXT,
               $columnParentID TEXT
               )
''';
