//--------------Gender--------------
import 'table_parameters.dart';

const String tableGender = 'Gender';

//CREATE
String createTableGender = '''
 CREATE TABLE $tableGender (
               $columnID TEXT Primary Key,
               $columnDescriptionEn TEXT,
               $columnDescriptionAr TEXT,
               $columnParentID TEXT
               )
''';
