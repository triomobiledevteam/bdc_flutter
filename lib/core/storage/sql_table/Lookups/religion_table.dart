//--------------Religion--------------
import 'table_parameters.dart';

const String tableReligion = 'Religion';

//CREATE
String createTableReligion = '''
 CREATE TABLE $tableReligion (
               $columnID TEXT Primary Key,
               $columnDescriptionEn TEXT,
               $columnDescriptionAr TEXT,
               $columnParentID TEXT
               )
''';
