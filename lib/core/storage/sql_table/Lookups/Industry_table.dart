//--------------Industry--------------
import 'table_parameters.dart';

const String tableIndustry = 'Industry';

//CREATE
String createTableIndustry = '''
 CREATE TABLE $tableIndustry (
               $columnID TEXT Primary Key,
               $columnDescriptionEn TEXT,
               $columnDescriptionAr TEXT,
               $columnParentID TEXT
               )
''';
