//--------------ContractTypes--------------
import 'table_parameters.dart';

const String tableContractTypes = 'ContractTypes';

//CREATE
String createTableContractTypes = '''
 CREATE TABLE $tableContractTypes (
               $columnID TEXT Primary Key,
               $columnDescriptionEn TEXT,
               $columnDescriptionAr TEXT,
               $columnParentID TEXT
               )
''';
