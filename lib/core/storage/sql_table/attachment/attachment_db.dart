import 'dart:developer';
import 'dart:io';
import 'dart:typed_data';

import 'package:bdc/core/modal/attachment.dart';
import 'package:bdc/core/storage/sql_table/attachment/attachment_table.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';

class AttachmentDatabase {
  late Database _database;

  static const databaseName = "attachmentBDC.db";
  // Increment this version when you need to change the schema.
  static const databaseVersion = 1;

  AttachmentDatabase._privateConstructor();
  static final AttachmentDatabase instance =
      AttachmentDatabase._privateConstructor();

  Future<Database> get database async {
    _database = await _initDatabase();
    return _database;
  }

  // deletePosDatabase() async {
  //   await _database.close();
  //   var databasesPath = await getDatabasesPath();
  //   String filePath = join(databasesPath, databaseName);
  //   // Delete the database
  //   await deleteDatabase(filePath);
  //   print('database Deleted');
  // }

  _initDatabase() async {
    Directory path = await getApplicationDocumentsDirectory();
    String databasePath = join(path.path, databaseName);

    return openDatabase(
      databasePath,
      version: databaseVersion,
      onCreate: _onCreate,
      onOpen: (database) async {
        _database = database;
      },
      onConfigure: (database) {},
      onUpgrade: (database, int oldVersion, int newVersion) {
        if (oldVersion < newVersion) {}
      },
    );
  }

//--------------OnCreate--------------
  Future _onCreate(Database _database, int version) async {
    //Category Table
    await _database.execute(createAdditionTable);
  }

//##############################################Insert##############################################
  //Attachment
  attachmentDB(AttachmentModal attachmentModal) async {
    print(
        'attachmentModal ${attachmentModal.localID},${attachmentModal.trioID}');

    var count = Sqflite.firstIntValue(await _database.rawQuery(
            "SELECT COUNT(*) FROM $tableAttachment  WHERE $columnAttachmentFile = ?",
            [attachmentModal.image])) ??
        0;
    int? cate;
    await _database.transaction((txn) async {
      if (count == 0) {
        cate = await txn.insert(tableAttachment, attachmentModal.toJson());
      } else {
        cate = await txn.update(tableAttachment, attachmentModal.toJson(),
            where: "$columnAttachmentFile = ?",
            whereArgs: [attachmentModal.image]);
      }
    });

    return cate;
  }

//##############################################GetWHERE##############################################
  //Attachment
  getParticipantAttachment(
    String trioID,
  ) async {
    List<Map> maps = await _database.query(tableAttachment,
        where: '$columnAttachmentTrioID=?', whereArgs: [trioID]);

    print('maps ${maps.toString()}');

    if (maps.isNotEmpty) {
      return maps
          .map((item) => AttachmentModal.fromJson(item as Map<String, dynamic>))
          .toList();
    }
    return [].cast<AttachmentModal>();
  }

  getAdminParticipantAttachment(
    String localID,
  ) async {
    List<Map> maps = await _database.query(tableAttachment,
        where: '$columnAttachmentLocalID=?', whereArgs: [localID]);

    print('maps ${maps.toString()}');

    if (maps.isNotEmpty) {
      return maps
          .map((item) => AttachmentModal.fromJson(item as Map<String, dynamic>))
          .toList();
    }
    return [].cast<AttachmentModal>();
  }
//##############################################GetData##############################################

  //Product
  getAttachments() async {
    List<Map> maps = await _database.query(tableAttachment);
    log('maps :  ${maps.toString()}');

    if (maps.isNotEmpty) {
      return maps.map(
          (item) => AttachmentModal.fromJson(item as Map<String, dynamic>));
    }
    return [].cast<AttachmentModal>();
  }

//##############################################GetAll##############################################

//   //Category
//   getAllCategory() async {
//     List<Map> maps = await _database.query(tableCategory);
//     if (maps.isNotEmpty) {
//       return maps
//           .map((item) => Category.fromJson(item as Map<String, dynamic>))
//           .toList();
//     }
//     return [].cast<Category>();
//   }

//##############################################Delete##############################################

//Attachment
  deleteAttachment(int iD) async {
    await _database.rawDelete(
        'DELETE FROM $tableAttachment WHERE $columnAttachmentLocalID = ? OR $columnAttachmentTrioID = ?',
        [iD, iD]);
  }

  deleteAttachmentImage(Uint8List image) async {
    await _database.rawDelete(
        'DELETE FROM $tableAttachment WHERE  $columnAttachmentFile = ?',
        [image]);
  }

  deleteParticipantAttachment(int iD) async {
    await _database.rawDelete(
        'DELETE FROM $tableAttachment WHERE $columnAttachmentTrioID = ?', [iD]);
  }

  deleteAdminAttachment(int iD) async {
    await _database.rawDelete(
        'DELETE FROM $tableAttachment WHERE $columnAttachmentLocalID = ?',
        [iD]);
  }

  Future<bool> isParticipantAttachmentEmpty(int trioID) async {
    var count = Sqflite.firstIntValue(await _database.rawQuery(
            "SELECT COUNT(*) FROM $tableAttachment  WHERE $columnAttachmentTrioID = ?",
            [trioID])) ??
        0;
    if (count > 0) return false;
    return true;
  }

  Future<bool> isAdminAttachmentEmpty(int localID) async {
    var count = Sqflite.firstIntValue(await _database.rawQuery(
            "SELECT COUNT(*) FROM $tableAttachment  WHERE $columnAttachmentLocalID = ?",
            [localID])) ??
        0;
    if (count > 0) return false;
    return true;
  }
// ##############################################Update##############################################

//##############################################ClearTable##############################################

// //Category
//   clearCategory() async {
//     List<Map> maps = await _database.query(tableCategory);
//     if (maps.toList().isNotEmpty) {
//       await _database.delete(tableCategory);
//     }
//   }
}
