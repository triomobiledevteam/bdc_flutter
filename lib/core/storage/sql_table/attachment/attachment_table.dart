//-------------- ADDITIONS--------------
const String tableAttachment = 'AttachmentTable';

const String columnAttachmentID = 'ID';
const String columnAttachmentLocalID = 'LocalID';
const String columnAttachmentTrioID = 'TrioID';
const String columnAttachmentFile = 'File';

const String createAdditionTable = '''
              CREATE TABLE $tableAttachment(             
                $columnAttachmentID INTEGER  PRIMARY KEY AUTOINCREMENT NOT NULL ,
                $columnAttachmentLocalID  INTEGER ,
                $columnAttachmentTrioID INTEGER  ,
                $columnAttachmentFile BLOB 
              )
              ''';
