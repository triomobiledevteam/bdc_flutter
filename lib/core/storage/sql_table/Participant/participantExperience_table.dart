//--------------ParticipantExperience--------------
const String tablePExperience = 'ParticipantExperience';

String columnPExperienceID = 'LocalID';
String columnPExperienceTrioID = 'ID';
String columnPExperienceDescriptionEn = 'DescriptionEn';
String columnPExperienceDescriptionAr = 'DescriptionAr';
String columnPExperienceStartDate = 'StartDate';
String columnPExperienceEndDate = 'EndDate';
String columnPExperienceLocation = 'Location';
String columnPExperienceCompanyIndustry = 'CompanyIndustry';
String columnPExperienceCompanyName = 'CompanyName';
String columnPExperienceNote = 'Note';
String columnPExperienceParticipantID = 'LocalParticipantID';
String columnPExperienceTrioParticipantID = 'ParticipantID';

//CREATE
String createTablePExperience = '''
 CREATE TABLE $tablePExperience (
                $columnPExperienceID INTEGER  PRIMARY KEY  AUTOINCREMENT,
                $columnPExperienceTrioID INTEGER,
                $columnPExperienceDescriptionEn TEXT  ,
                $columnPExperienceDescriptionAr TEXT  ,
                $columnPExperienceStartDate TEXT  , 
                $columnPExperienceEndDate TEXT ,
                $columnPExperienceLocation TEXT ,
                $columnPExperienceCompanyIndustry INTEGER ,
                $columnPExperienceCompanyName TEXT ,
                $columnPExperienceNote TEXT ,
                $columnPExperienceTrioParticipantID INTEGER ,
                $columnPExperienceParticipantID INTEGER 
                )

''';
