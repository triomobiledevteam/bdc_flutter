//--------------ParticipantFamilyInfo--------------
const String tablePFInfo = 'ParticipantFamilyInfo';

String columnFInfoID = 'LocalID';
String columnFInfoTrioID = 'ID';
String columnFInfoFullNameEn = 'FullNameEn';
String columnFInfoFullNameAr = 'FullNameAr';
String columnFInfoGenderID = 'GenderID';
String columnFInfoDisability = 'Disability';
String columnFInfoDisabilityDescription = 'DisabilityDescription';
String columnFInfoDateOfBirth = 'DateOfBirth';
String columnFInfoTrioParticipantID = 'ParticipantID';
String columnFInfoParticipantID = 'LocalParticipantID';

//CREATE
String createTablePFInfo = '''
 CREATE TABLE $tablePFInfo (
               $columnFInfoID  INTEGER  PRIMARY KEY  AUTOINCREMENT ,
               $columnFInfoTrioID  INTEGER  ,
               $columnFInfoFullNameEn  TEXT  ,
               $columnFInfoFullNameAr  TEXT  ,
               $columnFInfoGenderID  INTEGER  , 
               $columnFInfoDisability  INTEGER  ,
               $columnFInfoDisabilityDescription TEXT ,
               $columnFInfoDateOfBirth TEXT ,
               $columnFInfoTrioParticipantID INTEGER ,
               $columnFInfoParticipantID INTEGER 
               )
''';
