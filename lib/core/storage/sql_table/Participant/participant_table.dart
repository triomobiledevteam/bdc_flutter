//--------------Participant--------------
const String tableParticipant = 'Participant';

String columnParticipantTrioID = 'ID';
String columnParticipantID = 'LocalID';
String columnParticipantCode = 'Code';
String columnParticipantFullNameEn = 'FullNameEn';
String columnParticipantFullNameAr = 'FullNameAr';
String columnParticipantNationalityID = 'NationalityID';
String columnParticipantNationalID = 'NationalID';
String columnParticipantSecurityNumberID = 'SecurityNumberID';
String columnParticipantMaritalStatusID = 'MaritalStatusID';
String columnParticipantReligionID = 'ReligionID';
String columnParticipantGenderID = 'GenderID';
String columnParticipantCountryID = 'CountryID';
String columnParticipantCityID = 'CityID';
String columnParticipantBirthDate = 'BirthDate';
String columnParticipantAddress = 'Address';
String columnParticipantAlternateMobile = 'AlternateMobile';
String columnParticipantMobile = 'Mobile';
String columnParticipantFamilyWorkMember = 'FamilyWorkMember';
String columnParticipantMaritalParentStatuesID = 'MaritalParentStatuesID';
String columnParticipantEmail = 'Email';
String columnParticipantProfessionalSector = 'ProfessionalSector';
String columnParticipantEducationalLevel = 'EducationalLevel';
String columnParticipantUniversityName = 'UniversityName';
String columnParticipantMajorID = 'MajorID';
String columnParticipantRequestDate = 'ParticipantDate';
String columnParticipantFamilyNumber = 'FamilyNumber';
String columnParticipantParticipantStatuesID = 'ParticipantStatusID';
String columnParticipantFirstNameEn = 'FirstNameEn';
String columnParticipantSecondNameEn = 'SecondNameEn';
String columnParticipantThirdNameEn = 'ThirdNameEn';
String columnParticipantFamilyNameEn = 'FamilyNameEn';
String columnParticipantFirstNameAr = 'FirstNameAr';
String columnParticipantThirdNameAr = 'ThirdNameAr';
String columnParticipantSecondNameAr = 'SecondNameAr';
String columnParticipantFamilyNameAr = 'FamilyNameAr';
String columnParticipantGraduationDate = 'GraduationDate';
String columnParticipantLocation = 'Location';
String columnParticipantFacebookName = 'FacebookName';
String columnParticipantDisability = 'Disability';
String columnParticipantCreatedDate = 'CreatedDate';
String columnParticipantCreatedUser = 'CreatedUser';
String columnParticipantPercentageOfCompletion = 'PercentageOfCompletion';

//CREATE
String createTableParticipant = '''
 CREATE TABLE $tableParticipant (
                $columnParticipantID INTEGER  PRIMARY KEY  AUTOINCREMENT,
                $columnParticipantTrioID INTEGER   ,
                $columnParticipantCode  TEXT,
                $columnParticipantFullNameEn TEXT,
                $columnParticipantFullNameAr TEXT,
                $columnParticipantNationalityID TEXT,
                $columnParticipantNationalID TEXT,
                $columnParticipantSecurityNumberID  TEXT,
                $columnParticipantMaritalStatusID INTEGER,
                $columnParticipantReligionID INTEGER,
                $columnParticipantGenderID INTEGER,
                $columnParticipantCountryID TEXT,
                $columnParticipantCityID TEXT,
                $columnParticipantBirthDate TEXT,
                $columnParticipantAddress TEXT,
                $columnParticipantAlternateMobile TEXT,
                $columnParticipantMobile TEXT,
                $columnParticipantFamilyWorkMember INTEGER,
                $columnParticipantMaritalParentStatuesID INTEGER,
                $columnParticipantEmail TEXT,
                $columnParticipantProfessionalSector INTEGER,
                $columnParticipantEducationalLevel INTEGER,
                $columnParticipantUniversityName TEXT,
                $columnParticipantMajorID INTEGER,
                $columnParticipantRequestDate TEXT,
                $columnParticipantFamilyNumber INTEGER,
                $columnParticipantParticipantStatuesID TEXT,
                $columnParticipantFirstNameEn TEXT,
                $columnParticipantSecondNameEn TEXT,
                $columnParticipantThirdNameEn TEXT,
                $columnParticipantFamilyNameEn TEXT,
                $columnParticipantFirstNameAr TEXT,
                $columnParticipantThirdNameAr TEXT,
                $columnParticipantSecondNameAr TEXT,
                $columnParticipantFamilyNameAr TEXT,
                $columnParticipantGraduationDate TEXT,
                $columnParticipantLocation TEXT,
                $columnParticipantFacebookName TEXT,
                $columnParticipantDisability INTEGER,
                $columnParticipantCreatedDate TEXT,
                $columnParticipantCreatedUser INTEGER,
                $columnParticipantPercentageOfCompletion INTEGER
               )
''';
