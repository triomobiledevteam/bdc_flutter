//--------------Branches--------------
const String tableBranches = 'Branches';

const String columnBranchID = 'LocalID';
const String columnBranchTrioID = 'ID';
const String columnBranchBranchesName = 'BranchesName';
const String columnBranchBranchAddress = 'BranchAddress';
const String columnBranchBranchCountry = 'BranchCountry';
const String columnBranchBranchCity = 'BranchCity';
const String columnBranchTrioInstitutionID = 'InstitutionID';
const String columnBranchInstitutionID = 'LocalInstitutionID';

//CREATE
String createTableBranches = '''
 CREATE TABLE $tableBranches (
              $columnBranchID INTEGER  PRIMARY KEY  AUTOINCREMENT,
              $columnBranchTrioID INTEGER  ,
              $columnBranchBranchesName TEXT,
              $columnBranchBranchAddress TEXT,
              $columnBranchBranchCountry TEXT,
              $columnBranchBranchCity TEXT,
              $columnBranchTrioInstitutionID INTEGER,
              $columnBranchInstitutionID INTEGER
               )
               ''';
