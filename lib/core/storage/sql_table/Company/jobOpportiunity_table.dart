//--------------JobOpportunities--------------
const String tableJO = 'JobOpportunities';

String columnJOID = 'LocalID';
String columnJOTrioID = 'ID';
String columnJOJobRoleID = 'JobRoleID';
String columnJOInstitutionID = 'LocalInstitutionID';
String columnJOTrioInstitutionID = 'InstitutionID';
String columnJOExactProfessionDescription = 'ExactProfessionDescription';
String columnJOTotalTrainerRecruitment = 'TotalTraineerRecruitment';
String columnJOGenderID = 'GenderID';
String columnJONationalityID = 'NationalityID';
String columnJOWorkingHours = 'WorkingHours';
String columnJOSocialSecurity = 'SocialSecurity';
String columnJOTrainingDuration = 'TrainingDuration';
String columnJONumber = 'Number';
String columnJOInsurance = 'Insurance';
String columnJOSalaryRangeStart = 'SalaryRangeStart';
String columnJOSalaryRangeEnd = 'SalaryRangeEnd';
String columnJORemainingParticipants = 'RemainingParticipants';

//CREATE
String createTableJO = '''
 CREATE TABLE $tableJO (
              $columnJOID INTEGER  PRIMARY KEY  AUTOINCREMENT ,
              $columnJOTrioID INTEGER   ,
              $columnJOJobRoleID INTEGER,
              $columnJOInstitutionID INTEGER,
              $columnJOTrioInstitutionID INTEGER,
              $columnJOExactProfessionDescription TEXT,
              $columnJOTotalTrainerRecruitment INTEGER,
              $columnJOGenderID INTEGER,
              $columnJONationalityID TEXT,
              $columnJOWorkingHours INTEGER,
              $columnJOSocialSecurity INTEGER,
              $columnJOTrainingDuration INTEGER,
              $columnJONumber INTEGER,
              $columnJOInsurance INTEGER,
              $columnJOSalaryRangeStart REAL,
              $columnJOSalaryRangeEnd REAL,
              $columnJORemainingParticipants INTEGER
               )
''';

///$columnJOSocialSecurity INTEGER, //bool
///$columnJOInsurance INTEGER,//bool
