//--------------trainer_certificate_form--------------
const String tableContact = 'trainer_certificate_form';

const String columnContactID = 'LocalID';
const String columnContactTrioID = 'ID';
const String columnContactName = 'Name';
const String columnContactRoleID = 'RoleID';
const String columnContactMobile = 'Mobile';
const String columnContactEmail = 'Email';
const String columnContactTrioInstitutionID = 'InstitutionID';
const String columnContactInstitutionID = 'LocalInstitutionID';

//CREATE
String createTableContact = '''
 CREATE TABLE $tableContact (
              $columnContactID INTEGER  PRIMARY KEY  AUTOINCREMENT,
              $columnContactTrioID INTEGER   ,
              $columnContactName TEXT,
              $columnContactRoleID INTEGER,
              $columnContactMobile TEXT,
              $columnContactEmail TEXT,
              $columnContactTrioInstitutionID INTEGER,
              $columnContactInstitutionID INTEGER
               )
               ''';
