//--------------Company--------------
const String tableCompany = 'Company';

String columnCompanyID = 'LocalID';
String columnCompanyTrioID = 'ID';
String columnCompanyCode = 'Code';
String columnCompanyTransactionDate = 'TransactionDate';
String columnCompanyInstitutionNameAr = 'InstitutionNameAr';
String columnCompanyInstitutionNameEn = 'InstitutionNameEn';
String columnCompanyInstitutionID = 'InstitutionID';
String columnCompanySectorTypeID = 'SectorTypeID';
String columnCompanyInstitutionAddress = 'InstitutionAddress';
String columnCompanyCountryID = 'CountryID';
String columnCompanyCityID = 'CityID';
String columnCompanyInstitutionEmail = 'InstitutionEmail';
String columnCompanyInstitutionFax = 'InstitutionFax';
String columnCompanyInstitutionPhone = 'InstitutionPhone';
String columnCompanyInstitutionEmployeeNumber = 'InstitutionEmployeeNumber';
String columnCompanyInstitutionEmployeeNumberPartTime =
    'InstitutionEmployeeNumberPartTime';
String columnCompanyWomanWorkingNumber = 'WomanWorkingNumber';
String columnCompanyEmployeeSpecialNeeds = 'EmployeeSpecialNeeds';
String columnCompanyRecruitTypesInCompany = 'RecruitTypesInCompany';
String columnCompanyContractTypesInCompany = 'ContractTypesInComapny';
String columnCompanyLocation = 'Location';
String columnCompanyProjectID = 'ProjectID';
String columnCompanyCreatedUser = 'CreatedUser';
String columnCompanyCreatedDate = 'CreatedDate';
String columnCompanyNote = 'Note';
String columnCompanyPercentageOfCompletion = 'PercentageOfCompletion';

//CREATE
String createTableCompany = '''
 CREATE TABLE $tableCompany (
              $columnCompanyID INTEGER  PRIMARY KEY  AUTOINCREMENT,
              $columnCompanyTrioID INTEGER  ,
              $columnCompanyCode TEXT,
              $columnCompanyTransactionDate TEXT,
              $columnCompanyInstitutionNameAr TEXT,
              $columnCompanyInstitutionNameEn TEXT,
              $columnCompanyInstitutionID TEXT,
              $columnCompanySectorTypeID INTEGER,
              $columnCompanyInstitutionAddress TEXT,
              $columnCompanyCountryID TEXT,
              $columnCompanyCityID TEXT,
              $columnCompanyInstitutionEmail TEXT,
              $columnCompanyInstitutionFax TEXT,
              $columnCompanyInstitutionPhone TEXT,
              $columnCompanyInstitutionEmployeeNumber Real,
              $columnCompanyInstitutionEmployeeNumberPartTime Real,
              $columnCompanyWomanWorkingNumber Real,
              $columnCompanyEmployeeSpecialNeeds Real,
              $columnCompanyRecruitTypesInCompany INTEGER,
              $columnCompanyContractTypesInCompany INTEGER,
              $columnCompanyLocation TEXT,
              $columnCompanyProjectID INTEGER,
              $columnCompanyCreatedUser INTEGER,
              $columnCompanyCreatedDate TEXT,
              $columnCompanyNote TEXT,
              $columnCompanyPercentageOfCompletion INTEGER
               )
               ''';
