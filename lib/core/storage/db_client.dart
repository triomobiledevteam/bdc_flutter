import 'dart:io';

import 'package:bdc/core/modal/AplicationForm.dart';
import 'package:bdc/core/modal/LoginModal/Company.dart';
import 'package:bdc/core/modal/LoginModal/Participant.dart';
import 'package:bdc/core/modal/Lookups.dart';
import 'package:bdc/core/modal/company_modal/Branch.dart';
import 'package:bdc/core/modal/company_modal/Contact.dart';
import 'package:bdc/core/modal/company_modal/JobOpportunity.dart';
import 'package:bdc/core/modal/participant_modal/ParticipantExperience.dart';
import 'package:bdc/core/modal/participant_modal/ParticipantFamilyInfo.dart';
import 'package:bdc/core/storage/sql_table/ApplicaitonFormTrans.dart';
import 'package:bdc/core/storage/sql_table/Company/branch_table.dart';
import 'package:bdc/core/storage/sql_table/Company/company_table.dart';
import 'package:bdc/core/storage/sql_table/Company/contact_table.dart';
import 'package:bdc/core/storage/sql_table/Company/jobOpportiunity_table.dart';
import 'package:bdc/core/storage/sql_table/Lookups/experience_years_table.dart';
import 'package:bdc/core/storage/sql_table/Lookups/jobRole_table.dart';
import 'package:bdc/core/storage/sql_table/Lookups/major_table.dart';
import 'package:bdc/core/storage/sql_table/Lookups/recruitment_table.dart';
import 'package:bdc/core/storage/sql_table/Lookups/skills_level_table.dart';
import 'package:bdc/core/storage/sql_table/Participant/participantExperience_table.dart';
import 'package:bdc/core/storage/sql_table/Participant/participantFamilyInfo_table.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';

import '../modal/applicationFormTrans.dart';
import 'sql_table/ApplicaitonForm.dart';
import 'sql_table/Lookups/Industry_table.dart';
import 'sql_table/Lookups/city_table.dart';
import 'sql_table/Lookups/contractTypes_table.dart';
import 'sql_table/Lookups/country_table.dart';
import 'sql_table/Lookups/educationalLevel_table.dart';
import 'sql_table/Lookups/gender_table.dart';
import 'sql_table/Lookups/last_used_table.dart';
import 'sql_table/Lookups/marital_table.dart';
import 'sql_table/Lookups/nationality_table.dart';
import 'sql_table/Lookups/religion_table.dart';
import 'sql_table/Lookups/sector_table.dart';
import 'sql_table/Lookups/table_parameters.dart';
import 'sql_table/Participant/participant_table.dart';

class DatabaseClient {
  static Database? _database;

  static const _databaseName = "PosDB.db";

  // Increment this version when you need to change the schema.
  static const _databaseVersion = 1;

  DatabaseClient._privateConstructor();

  static final DatabaseClient instance = DatabaseClient._privateConstructor();

  Future<Database?> get database async {
    if (_database != null) return _database;
    _database = await _initDatabase();
    return _database;
  }

  _initDatabase() async {
    Directory path = await getApplicationDocumentsDirectory();
    String dbPath = join(path.path, _databaseName);

    _database = await openDatabase(dbPath,
        version: _databaseVersion, onCreate: _onCreate);
  }

//--------------OnCreate--------------

  Future _onCreate(Database db, int version) async {
    //1- Gender Table
    await db.execute(createTableGender);
    //2- Marital Table
    await db.execute(createTableMarital);
    //3- EducationalLevel Table
    await db.execute(createTableEducationalLevel);
    //4- Nationality Table
    await db.execute(createTableNationality);
    //5- Sector Table
    await db.execute(createTableSector);
    //6- Industry Table
    await db.execute(createTableIndustry);
    //7- Country Table
    await db.execute(createTableCountry);
    //8- City Table
    await db.execute(createTableCity);

    //9- Religion Table
    await db.execute(createTableReligion);
    //10- Majors Table
    await db.execute(createTableMajor);
    //11- JobRole Table
    await db.execute(createTableJobRole);
    //12- ContractTypes Table
    await db.execute(createTableContractTypes);

    //13- Recruitment Table
    await db.execute(createTableRecruitment);

    //13- SkillsLevel Table
    await db.execute(createTableSkillsLevel);

    //13- ExperienceYears Table
    await db.execute(createTableExperienceYears);

    //13- LastUsed Table
    await db.execute(createTableLastUsed);

    // ApplicationFrom Table
    await db.execute(createTableAppFrom);

    // ApplicationFromTrans Table
    await db.execute(createTableAppFormTrans);

//Participant
    //Participant Experience
    await db.execute(createTablePExperience);

    //Participant FamilyInfo
    await db.execute(createTablePFInfo);

    //Participant
    await db.execute(createTableParticipant);

//Company
    //Branches
    await db.execute(createTableBranches);

    //trainer_certificate_form
    await db.execute(createTableContact);

    //JobOpportunities
    await db.execute(createTableJO);

    //Company
    await db.execute(createTableCompany);
  }

//##############################################Insert##############################################

  //1- GenderDB
  genderDB(LookUps lookUp) async {
    Database db = _database!;
    await _initDatabase();
    var count = Sqflite.firstIntValue(await db.rawQuery(
        "SELECT COUNT($columnID) FROM $tableGender WHERE $columnID = ?",
        [lookUp.iD]));
    if (count == 0) {
      await db.transaction((txn) async =>
          await txn.insert(tableGender, lookUp.toLocalStorage()));
    } else {
      await _database!.update(tableGender, lookUp.toLocalStorage(),
          where: "$columnID = ?", whereArgs: [lookUp.iD]);
    }
  }

  //2- MaritalDB
  maritalDB(LookUps lookUp) async {
    Database db = _database!;
    await _initDatabase();
    var count = Sqflite.firstIntValue(await db.rawQuery(
        "SELECT COUNT($columnID) FROM $tableMarital WHERE $columnID = ?",
        [lookUp.iD]));
    if (count == 0) {
      await db.transaction((txn) async =>
          await txn.insert(tableMarital, lookUp.toLocalStorage()));
    } else {
      await _database!.update(tableMarital, lookUp.toLocalStorage(),
          where: "$columnID = ?", whereArgs: [lookUp.iD]);
    }
  }

  //3- EducationalLevelDB
  eduLevelDB(LookUps lookUp) async {
    Database db = _database!;
    await _initDatabase();
    var count = Sqflite.firstIntValue(await db.rawQuery(
        "SELECT COUNT($columnID) FROM $tableEduLevel WHERE $columnID = ?",
        [lookUp.iD]));
    if (count == 0) {
      await db.transaction((txn) async =>
          await txn.insert(tableEduLevel, lookUp.toLocalStorage()));
    } else {
      await _database!.update(tableEduLevel, lookUp.toLocalStorage(),
          where: "$columnID = ?", whereArgs: [lookUp.iD]);
    }
  }

  //4- NationalityDB
  nationalityDB(LookUps lookUp) async {
    Database db = _database!;
    await _initDatabase();
    var count = Sqflite.firstIntValue(await db.rawQuery(
        "SELECT COUNT($columnID) FROM $tableNationality WHERE $columnID = ?",
        [lookUp.iD]));
    if (count == 0) {
      await db.transaction((txn) async =>
          await txn.insert(tableNationality, lookUp.toLocalStorage()));
    } else {
      await _database!.update(tableNationality, lookUp.toLocalStorage(),
          where: "$columnID = ?", whereArgs: [lookUp.iD]);
    }
  }

  //5- SectorDB
  sectorDB(LookUps lookUp) async {
    Database db = _database!;
    await _initDatabase();
    var count = Sqflite.firstIntValue(await db.rawQuery(
        "SELECT COUNT($columnID) FROM $tableSector WHERE $columnID = ?",
        [lookUp.iD]));
    if (count == 0) {
      await db.transaction((txn) async =>
          await txn.insert(tableSector, lookUp.toLocalStorage()));
    } else {
      await _database!.update(tableSector, lookUp.toLocalStorage(),
          where: "$columnID = ?", whereArgs: [lookUp.iD]);
    }
  }

  //6- IndustryDB
  industryDB(LookUps lookUp) async {
    Database db = _database!;
    await _initDatabase();
    var count = Sqflite.firstIntValue(await db.rawQuery(
        "SELECT COUNT($columnID) FROM $tableIndustry WHERE $columnID = ?",
        [lookUp.iD]));
    if (count == 0) {
      await db.transaction((txn) async =>
          await txn.insert(tableIndustry, lookUp.toLocalStorage()));
    } else {
      await _database!.update(tableIndustry, lookUp.toLocalStorage(),
          where: "$columnID = ?", whereArgs: [lookUp.iD]);
    }
  }

  //7- CountryDB
  countryDB(LookUps lookUp) async {
    Database db = _database!;
    await _initDatabase();
    var count = Sqflite.firstIntValue(await db.rawQuery(
        "SELECT COUNT($columnID) FROM $tableCountry WHERE $columnID = ?",
        [lookUp.iD]));
    if (count == 0) {
      await db.transaction((txn) async =>
          await txn.insert(tableCountry, lookUp.toLocalStorage()));
    } else {
      await _database!.update(tableCountry, lookUp.toLocalStorage(),
          where: "$columnID = ?", whereArgs: [lookUp.iD]);
    }
  }

  //8- CityDB
  cityDB(LookUps lookUp) async {
    await _initDatabase();
    Database db = _database!;
    var count = Sqflite.firstIntValue(await db.rawQuery(
        "SELECT COUNT($columnID) FROM $tableCity WHERE $columnID = ?",
        [lookUp.iD]));
    if (count == 0) {
      await db.transaction(
          (txn) async => await txn.insert(tableCity, lookUp.toLocalStorage()));
    } else {
      await _database!.update(tableCity, lookUp.toLocalStorage(),
          where: "$columnID = ?", whereArgs: [lookUp.iD]);
    }
  }

  //9- ReligionDB
  religionDB(LookUps lookUp) async {
    Database db = _database!;
    await _initDatabase();
    var count = Sqflite.firstIntValue(await db.rawQuery(
        "SELECT COUNT($columnID) FROM $tableReligion WHERE $columnID = ?",
        [lookUp.iD]));
    if (count == 0) {
      await db.transaction((txn) async =>
          await txn.insert(tableReligion, lookUp.toLocalStorage()));
    } else {
      await _database!.update(tableReligion, lookUp.toLocalStorage(),
          where: "$columnID = ?", whereArgs: [lookUp.iD]);
    }
  }

  //10- MajorDB
  majorDB(LookUps lookUp) async {
    Database db = _database!;
    await _initDatabase();
    var count = Sqflite.firstIntValue(await db.rawQuery(
        "SELECT COUNT($columnID) FROM $tableMajor WHERE $columnID = ?",
        [lookUp.iD]));
    if (count == 0) {
      await db.transaction(
          (txn) async => await txn.insert(tableMajor, lookUp.toLocalStorage()));
    } else {
      await _database!.update(tableMajor, lookUp.toLocalStorage(),
          where: "$columnID = ?", whereArgs: [lookUp.iD]);
    }
  }

  //11- JobRoleDB
  jobRoleDB(LookUps lookUp) async {
    Database db = _database!;
    await _initDatabase();
    var count = Sqflite.firstIntValue(await db.rawQuery(
        "SELECT COUNT($columnID) FROM $tableJobRole WHERE $columnID = ?",
        [lookUp.iD]));
    if (count == 0) {
      await db.transaction((txn) async =>
          await txn.insert(tableJobRole, lookUp.toLocalStorage()));
    } else {
      await _database!.update(tableJobRole, lookUp.toLocalStorage(),
          where: "$columnID = ?", whereArgs: [lookUp.iD]);
    }
  }

  //12- ContractTypeDB
  contractTypeDB(LookUps lookUp) async {
    Database db = _database!;
    await _initDatabase();
    var count = Sqflite.firstIntValue(await db.rawQuery(
        "SELECT COUNT($columnID) FROM $tableContractTypes WHERE $columnID = ?",
        [lookUp.iD]));
    if (count == 0) {
      await db.transaction((txn) async =>
          await txn.insert(tableContractTypes, lookUp.toLocalStorage()));
    } else {
      await _database!.update(tableContractTypes, lookUp.toLocalStorage(),
          where: "$columnID = ?", whereArgs: [lookUp.iD]);
    }
  }

  //13- RecruitmentDB
  recruitmentDB(LookUps lookUp) async {
    Database db = _database!;
    await _initDatabase();
    var count = Sqflite.firstIntValue(await db.rawQuery(
        "SELECT COUNT($columnID) FROM $tableRecruitment WHERE $columnID = ?",
        [lookUp.iD]));
    if (count == 0) {
      await db.transaction((txn) async =>
          await txn.insert(tableRecruitment, lookUp.toLocalStorage()));
    } else {
      await _database!.update(tableRecruitment, lookUp.toLocalStorage(),
          where: "$columnID = ?", whereArgs: [lookUp.iD]);
    }
  }

  //14- SkillsLevelDB
  skillsLevelDB(LookUps lookUp) async {
    Database db = _database!;
    await _initDatabase();
    var count = Sqflite.firstIntValue(await db.rawQuery(
        "SELECT COUNT($columnID) FROM $tableSkillsLevel WHERE $columnID = ?",
        [lookUp.iD]));
    if (count == 0) {
      await db.transaction((txn) async =>
          await txn.insert(tableSkillsLevel, lookUp.toLocalStorage()));
    } else {
      await _database!.update(tableSkillsLevel, lookUp.toLocalStorage(),
          where: "$columnID = ?", whereArgs: [lookUp.iD]);
    }
  }

  //13- ExperienceYearsDB
  experienceYearsDB(LookUps lookUp) async {
    Database db = _database!;
    await _initDatabase();
    var count = Sqflite.firstIntValue(await db.rawQuery(
        "SELECT COUNT($columnID) FROM $tableExperienceYears WHERE $columnID = ?",
        [lookUp.iD]));
    if (count == 0) {
      await db.transaction((txn) async =>
          await txn.insert(tableExperienceYears, lookUp.toLocalStorage()));
    } else {
      await _database!.update(tableExperienceYears, lookUp.toLocalStorage(),
          where: "$columnID = ?", whereArgs: [lookUp.iD]);
    }
  }
  //13- RecruitmentDB

  lastUsedDB(LookUps lookUp) async {
    Database db = _database!;
    await _initDatabase();
    var count = Sqflite.firstIntValue(await db.rawQuery(
        "SELECT COUNT($columnID) FROM $tableLastUsed WHERE $columnID = ?",
        [lookUp.iD]));
    if (count == 0) {
      await db.transaction((txn) async =>
          await txn.insert(tableLastUsed, lookUp.toLocalStorage()));
    } else {
      await _database!.update(tableLastUsed, lookUp.toLocalStorage(),
          where: "$columnID = ?", whereArgs: [lookUp.iD]);
    }
  }

  //ApplicationFormDB
  applicationFormDB(ApplicationForm applicationForm) async {
    await _initDatabase();

    Database db = _database!;
    await _initDatabase();
    var count = Sqflite.firstIntValue(await db.rawQuery(
        "SELECT COUNT($columnAppFormID) FROM $tableAppForm WHERE $columnAppFormID = ? AND $columnAppFormFormType=?",
        [applicationForm.iD, applicationForm.formType]));
    if (count == 0) {
      await db.transaction((txn) async =>
          await txn.insert(tableAppForm, applicationForm.toJson()));
    } else {
      await _database!.update(tableAppForm, applicationForm.toJson(),
          where: "$columnAppFormID = ? AND $columnAppFormFormType=? ",
          whereArgs: [applicationForm.iD, applicationForm.formType]);
    }
  }

  //ApplicationFormTransDB
  applicationFormTransDB(ApplicationFormTrans appFormTrans) async {
    Database db = _database!;
    await _initDatabase();
    var count = Sqflite.firstIntValue(await db.rawQuery(
        "SELECT COUNT($columnAppFormTransID) FROM $tableAppFormTrans WHERE $columnAppFormTransID = ?",
        [appFormTrans.iD]));
    if (count == 0) {
      await db.transaction((txn) async =>
          await txn.insert(tableAppFormTrans, appFormTrans.toJson()));
      // await db.insert("$tableReligion", lookUp.toLocalStorage());
    } else {
      await _database!.update(tableAppFormTrans, appFormTrans.toJson(),
          where: "$columnAppFormTransID = ?", whereArgs: [appFormTrans.iD]);
    }
  }

  //Participant
  //1- ParticipantDB
  participantDB(Participant? participant) async {
    Database db = _database!;
    await _initDatabase();
    var count = Sqflite.firstIntValue(await db.rawQuery(
        "SELECT COUNT($columnParticipantID) FROM $tableParticipant WHERE $columnParticipantID = ?",
        [participant!.iD ?? 0]));
    if (count == 0) {
      await db.transaction((txn) async =>
          await txn.insert(tableParticipant, participant.toJson()));
      // await db.insert("$tableReligion", lookUp.toLocalStorage());
    } else {
      await _database!.update(tableParticipant, participant.toJson(),
          where: "$columnParticipantID = ?", whereArgs: [participant.iD]);
    }
  }

  //2- ParticipantFamilyInfoDB
  participantFInfoDB(ParticipantFamilyInfo? info) async {
    Database db = _database!;
    await _initDatabase();
    var count = Sqflite.firstIntValue(await db.rawQuery(
        "SELECT COUNT($columnFInfoID) FROM $tablePFInfo WHERE $columnFInfoID = ?",
        [info!.iD ?? 0]));
    if (count == 0) {
      await db.transaction(
          (txn) async => await txn.insert(tablePFInfo, info.toJson()));
      // await db.insert("$tableReligion", lookUp.toLocalStorage());
    } else {
      await _database!.update(tablePFInfo, info.toJson(),
          where: "$columnFInfoID = ?", whereArgs: [info.iD]);
    }
  }

  //3- ParticipantExperienceDB
  participantExperienceDB(ParticipantExperience info) async {
    Database db = _database!;
    await _initDatabase();
    var count = Sqflite.firstIntValue(await db.rawQuery(
        "SELECT COUNT($columnPExperienceID) FROM $tablePExperience WHERE $columnPExperienceID = ?",
        [info.iD]));
    if (count == 0) {
      await db.transaction(
          (txn) async => await txn.insert(tablePExperience, info.toJson()));
      // await db.insert("$tableReligion", lookUp.toLocalStorage());
    } else {
      await _database!.update(tablePExperience, info.toJson(),
          where: "$columnPExperienceID = ?", whereArgs: [info.iD]);
    }
  }

//Company
  //1- CompanyDB
  companyDB(Company? company) async {
    Database db = _database!;
    await _initDatabase();
    var count = Sqflite.firstIntValue(await db.rawQuery(
        "SELECT COUNT($columnCompanyID) FROM $tableCompany WHERE $columnCompanyID = ?",
        [company!.iD!]));
    if (count == 0) {
      await db.transaction(
          (txn) async => await txn.insert(tableCompany, company.toJson()));
    } else {
      await _database!.update(tableCompany, company.toJson(),
          where: "$columnCompanyID = ?", whereArgs: [company.iD]);
    }
  }

  //2- BranchDB
  branchDB(Branch info) async {
    Database db = _database!;
    await _initDatabase();
    var count = Sqflite.firstIntValue(await db.rawQuery(
        "SELECT COUNT($columnBranchID) FROM $tableBranches WHERE $columnBranchID = ?",
        [info.iD]));
    if (count == 0) {
      await db.transaction(
          (txn) async => await txn.insert(tableBranches, info.toJson()));
      // await db.insert("$tableReligion", lookUp.toLocalStorage());
    } else {
      await _database!.update(tableBranches, info.toJson(),
          where: "$columnBranchID = ?", whereArgs: [info.iD]);
    }
  }

  //3- ContactDB
  contactDB(Contact info) async {
    Database db = _database!;
    await _initDatabase();
    var count = Sqflite.firstIntValue(await db.rawQuery(
        "SELECT COUNT($columnContactID) FROM $tableContact WHERE $columnContactID = ?",
        [info.iD]));
    if (count == 0) {
      await db.transaction(
          (txn) async => await txn.insert(tableContact, info.toJson()));
      // await db.insert("$tableReligion", lookUp.toLocalStorage());
    } else {
      await _database!.update(tableContact, info.toJson(),
          where: "$columnContactID = ?", whereArgs: [info.iD]);
    }
  }

  //3- JobOpportunitiesDB
  jobOpportunitiesDB(JobOpportunities info) async {
    Database db = _database!;
    await _initDatabase();
    var count = Sqflite.firstIntValue(await db.rawQuery(
        "SELECT COUNT($columnJOID) FROM $tableJO WHERE $columnJOID = ?",
        [info.iD]));
    if (count == 0) {
      await db
          .transaction((txn) async => await txn.insert(tableJO, info.toJson()));
      // await db.insert("$tableReligion", lookUp.toLocalStorage());
    } else {
      await _database!.update(tableJO, info.toJson(),
          where: "$columnJOID = ?", whereArgs: [info.iD]);
    }
  }

//##############################################GetData##############################################
//Participant
  //1- getParticipant
  getParticipant(Participant participant) async {
    Database db = _database!;
    await _initDatabase();
    List<Map> maps = await db.query(tableGender);
    print('map: ${maps.toList().length}');
    print(await _database!.query(tableGender));
    if (maps.isNotEmpty) {
      return maps.map((item) => LookUps.fromJson(item as Map<String, dynamic>));
    }
  }

  // //Participant
  // //1- getParticipant
  // getParticipant(Participant participant) async {
  //   await _initDatabase();
  //   Database db = _database!;
  //   List<Map> maps = await db.query('$tableGender');
  //   print('map: ${maps.toList().length}');
  //   print(await _database!.query(tableGender));
  //   if (maps.length > 0) {
  //     return maps.map((item) => LookUps.fromJson(item as Map<String, dynamic>));
  //   }
  // }

// //##############################################GetAll##############################################
//Participant
  //1-getAllParticipant
  getAllParticipant() async {
    Database db = _database!;
    await _initDatabase();
    // final Database db = await _initDatabase();

    List<Map> maps = await db.query(tableParticipant);
    print('map: ${maps.toList().length}');
    print(await db.query(tableParticipant));
    if (maps.isNotEmpty) {
      return maps
          .map((item) => Participant.fromJson(item as Map<String, dynamic>))
          .toList();
    }
    return [].cast<Participant>();
  }

// 2-getAllParticipantFamilyInfo
  getAllPFInfo(int? participantID) async {
    await _initDatabase();
    Database db = _database!;
    List<Map> maps = await db.query(tablePFInfo,
        where: '$columnFInfoParticipantID=?', whereArgs: ['$participantID']);
    print('map: ${maps.toList().length}');
    print(await _database!.query(tablePFInfo));
    if (maps.isNotEmpty) {
      return maps
          .map((item) =>
              ParticipantFamilyInfo.fromJson(item as Map<String, dynamic>))
          .toList();
    }
    return [].cast<ParticipantFamilyInfo>().toList();
  }

// 3-getAllParticipantFamilyInfo
  getAllPExperience(int? participantID) async {
    await _initDatabase();
    Database db = _database!;
    List<Map> maps = await db.query(tablePExperience,
        where: '$columnPExperienceParticipantID=?',
        whereArgs: ['$participantID']);
    print('map: ${maps.toList().length}');
    print(await _database!.query(tablePFInfo));
    if (maps.isNotEmpty) {
      return maps
          .map((item) =>
              ParticipantExperience.fromJson(item as Map<String, dynamic>))
          .toList();
    }
    return [].cast<ParticipantExperience>();
  }

  //Company
  //1-getAllCompany
  getAllCompany() async {
    await _initDatabase();
    Database db = _database!;
    List<Map> maps = await db.query(tableCompany);
    print('map: ${maps.toList().length}');
    print(await _database!.query(tableCompany));
    if (maps.isNotEmpty) {
      return maps
          .map((item) => Company.fromJson(item as Map<String, dynamic>))
          .toList();
    }
    return [].cast<Company>();
  }

// 2-getAllCBranch
  getAllCBranch(int? institutionID) async {
    await _initDatabase();
    Database db = _database!;
    List<Map> maps = await db.query(tableBranches,
        where: '$columnBranchInstitutionID=?', whereArgs: ['$institutionID']);
    print('map: ${maps.toList().length}');
    print(await _database!.query(tableBranches));
    if (maps.isNotEmpty) {
      return maps
          .map((item) => Branch.fromJson(item as Map<String, dynamic>))
          .toList();
    }
    return [].cast<Branch>();
  }

// 3-getAllCContact
  getAllCContact(int? institutionID) async {
    await _initDatabase();
    Database db = _database!;
    List<Map> maps = await db.query(tableContact,
        where: '$columnContactInstitutionID=?', whereArgs: ['$institutionID']);
    print('map: ${maps.toList().length}');
    print(await _database!.query(tableContact));
    if (maps.isNotEmpty) {
      return maps
          .map((item) => Contact.fromJson(item as Map<String, dynamic>))
          .toList();
    }
    return [].cast<Contact>();
  }

// 4- getAllJobOpportunities
  getAllCJO(int? institutionID) async {
    await _initDatabase();
    Database db = _database!;
    List<Map> maps = await db.query(tableJO,
        where: '$columnJOInstitutionID=?', whereArgs: ['$institutionID']);
    print('map: ${maps.toList().length}');
    print(await _database!.query(tableJO));
    if (maps.isNotEmpty) {
      return maps
          .map(
              (item) => JobOpportunities.fromJson(item as Map<String, dynamic>))
          .toList();
    }
    return [].cast<JobOpportunities>();
  }

  //1- getGender
  getGender() async {
    await _initDatabase();
    Database db = _database!;
    late List<Map> maps;
    await db.transaction((txn) async => maps = await txn.query(tableGender));
    if (maps.isNotEmpty) {
      return maps
          .map((item) => LookUps.fromLocalStorage(item as Map<String, dynamic>))
          .toList();
    }
    return [].cast<LookUps>();
  }

  //2- getMarital
  getMarital() async {
    await _initDatabase();
    Database db = _database!;
    late List<Map> maps;
    await db.transaction((txn) async => maps = await txn.query(tableMarital));
    if (maps.isNotEmpty) {
      return maps
          .map((item) => LookUps.fromLocalStorage(item as Map<String, dynamic>))
          .toList();
    }
    return [].cast<LookUps>();
  }

  //3- getEduLevel
  getEduLevel() async {
    await _initDatabase();
    Database db = _database!;
    late List<Map> maps;
    await db.transaction((txn) async => maps = await txn.query(tableEduLevel));
    if (maps.isNotEmpty) {
      return maps
          .map((item) => LookUps.fromLocalStorage(item as Map<String, dynamic>))
          .toList();
    }
    return [].cast<LookUps>();
  }

  //4- getNationality
  getNationality() async {
    await _initDatabase();
    Database db = _database!;
    late List<Map> maps;
    await db
        .transaction((txn) async => maps = await txn.query(tableNationality));
    if (maps.isNotEmpty) {
      return maps
          .map((item) => LookUps.fromLocalStorage(item as Map<String, dynamic>))
          .toList();
    }
    return [].cast<LookUps>();
  }

  //5- getSector
  getSector() async {
    await _initDatabase();
    Database db = _database!;
    late List<Map> maps;
    await db.transaction((txn) async => maps = await txn.query(tableSector));
    if (maps.isNotEmpty) {
      return maps
          .map((item) => LookUps.fromLocalStorage(item as Map<String, dynamic>))
          .toList();
    }
    return [].cast<LookUps>();
  }

  //6- getIndustry
  getIndustry() async {
    await _initDatabase();
    Database db = _database!;
    late List<Map> maps;
    await db.transaction((txn) async => maps = await txn.query(tableIndustry));
    if (maps.isNotEmpty) {
      return maps
          .map((item) => LookUps.fromLocalStorage(item as Map<String, dynamic>))
          .toList();
    }
    return [].cast<LookUps>();
  }

  //7- getCountry
  getCountry() async {
    await _initDatabase();
    Database db = _database!;
    late List<Map> maps;
    await db.transaction((txn) async => maps = await txn.query(tableCountry));
    if (maps.isNotEmpty) {
      return maps
          .map((item) => LookUps.fromLocalStorage(item as Map<String, dynamic>))
          .toList();
    }
    return [].cast<LookUps>();
  }

  //8- getCity
  getCity() async {
    await _initDatabase();
    Database db = _database!;
    late List<Map> maps;
    await db.transaction((txn) async => maps = await txn.query(tableCity));

    if (maps.isNotEmpty) {
      print(maps
          .map((item) => LookUps.fromLocalStorage(item as Map<String, dynamic>))
          .where((element) => element.descriptionEn == 'Amman')
          .toList());
      return maps
          .map((item) => LookUps.fromLocalStorage(item as Map<String, dynamic>))
          .toList();
    }
    return [].cast<LookUps>();
  }

  //9- getReligion
  getReligion() async {
    await _initDatabase();
    Database db = _database!;
    late List<Map> maps;
    await db.transaction((txn) async => maps = await txn.query(tableReligion));
    if (maps.isNotEmpty) {
      return maps
          .map((item) => LookUps.fromLocalStorage(item as Map<String, dynamic>))
          .toList();
    }
    return [].cast<LookUps>();
  }

  //10- getMajor
  getMajor() async {
    await _initDatabase();
    Database db = _database!;
    late List<Map> maps;
    await db.transaction((txn) async => maps = await txn.query(tableMajor));
    if (maps.isNotEmpty) {
      return maps
          .map((item) => LookUps.fromLocalStorage(item as Map<String, dynamic>))
          .toList();
    }
    return [].cast<LookUps>();
  }

  //11- getJobRole
  getJobRole() async {
    await _initDatabase();
    Database db = _database!;
    late List<Map> maps;
    await db.transaction((txn) async => maps = await txn.query(tableJobRole));
    if (maps.isNotEmpty) {
      return maps
          .map((item) => LookUps.fromLocalStorage(item as Map<String, dynamic>))
          .toList();
    }
    return [].cast<LookUps>();
  }

  //12- getContactTypes
  getContactTypes() async {
    await _initDatabase();
    Database db = _database!;
    late List<Map> maps;
    await db
        .transaction((txn) async => maps = await txn.query(tableContractTypes));
    if (maps.isNotEmpty) {
      return maps
          .map((item) => LookUps.fromLocalStorage(item as Map<String, dynamic>))
          .toList();
    }
    return [].cast<LookUps>();
  }

  //13- getRecruitment
  getRecruitment() async {
    await _initDatabase();
    Database db = _database!;
    late List<Map> maps;
    await db
        .transaction((txn) async => maps = await txn.query(tableRecruitment));
    if (maps.isNotEmpty) {
      return maps
          .map((item) => LookUps.fromLocalStorage(item as Map<String, dynamic>))
          .toList();
    }
    return [].cast<LookUps>();
  }

  //13- getSkillsLevel
  getSkillsLevel() async {
    await _initDatabase();
    Database db = _database!;
    late List<Map> maps;
    await db
        .transaction((txn) async => maps = await txn.query(tableSkillsLevel));
    if (maps.isNotEmpty) {
      return maps
          .map((item) => LookUps.fromLocalStorage(item as Map<String, dynamic>))
          .toList();
    }
    return [].cast<LookUps>();
  }

  //13- getExperienceYears
  getExperienceYears() async {
    await _initDatabase();
    Database db = _database!;
    late List<Map> maps;
    await db.transaction(
        (txn) async => maps = await txn.query(tableExperienceYears));
    if (maps.isNotEmpty) {
      return maps
          .map((item) => LookUps.fromLocalStorage(item as Map<String, dynamic>))
          .toList();
    }
    return [].cast<LookUps>();
  }

  //13- getExperienceYears
  getLastUsed() async {
    await _initDatabase();
    Database db = _database!;
    late List<Map> maps;
    await db.transaction((txn) async => maps = await txn.query(tableLastUsed));
    if (maps.isNotEmpty) {
      return maps
          .map((item) => LookUps.fromLocalStorage(item as Map<String, dynamic>))
          .toList();
    }
    return [].cast<LookUps>();
  }

  //getApplicationForm
  getApplicationForm(String formType) async {
    await _initDatabase();
    Database db = _database!;
    late List<Map> maps;
    await db.transaction((txn) async => maps = await txn.query(tableAppForm,
        where: '$columnAppFormFormType=?', whereArgs: [formType]));
    print('maps.length ${maps.length}');
    if (maps.isNotEmpty) {
      return maps
          .map((item) => ApplicationForm.fromJson(item as Map<String, dynamic>))
          .toList();
    }
    return [].cast<ApplicationForm>();
  } //getApplicationFormTrans

  getApplicationFormTrans(String formType, int? userID) async {
    await _initDatabase();
    Database db = _database!;
    late List<Map> maps;
    await db.transaction((txn) async => maps = await txn.query(
        tableAppFormTrans,
        where: '$columnAppFormTransFormType=? AND $columnAppFormTransUserID=?',
        whereArgs: [formType, '$userID']));
    if (maps.isNotEmpty) {
      return maps
          .map((item) =>
              ApplicationFormTrans.fromJson(item as Map<String, dynamic>))
          .toList();
    }
    return [].cast<ApplicationFormTrans>();
  }

//
// //##############################################Get Where##############################################
  //1- getWhereGender
  getWhereGender(String? iD) async {
    await _initDatabase();
    Database db = _database!;
    late List<Map> maps;
    await db.transaction((txn) async => maps =
        await txn.query(tableGender, where: '$columnID =?', whereArgs: [iD]));
    if (maps.isNotEmpty) {
      return maps
          .map((item) => LookUps.fromLocalStorage(item as Map<String, dynamic>))
          .first;
    }
    return LookUps();
  }

  //2- getWhereMarital
  getWhereMarital(String? iD) async {
    await _initDatabase();
    Database db = _database!;
    late List<Map> maps;
    await db.transaction((txn) async => maps =
        await txn.query(tableMarital, where: '$columnID =?', whereArgs: [iD]));
    // List<Map> maps = await db.query('$tableMarital');
    // print('map: ${maps.toList().length}');
    // print(await _database.query(tableMarital));
    if (maps.isNotEmpty) {
      return maps
          .map((item) => LookUps.fromLocalStorage(item as Map<String, dynamic>))
          .first;
    }
    return LookUps();
  }

  //3- getWhereEduLevel
  getWhereEduLevel(String? iD) async {
    await _initDatabase();
    Database db = _database!;
    late List<Map> maps;
    await db.transaction((txn) async => maps =
        await txn.query(tableEduLevel, where: '$columnID =?', whereArgs: [iD]));
    // List<Map> maps = await db.query('$tableEduLevel');
    // print('map: ${maps.toList().length}');
    // print(await _database.query(tableEduLevel));
    if (maps.isNotEmpty) {
      return maps
          .map((item) => LookUps.fromLocalStorage(item as Map<String, dynamic>))
          .first;
    }
    return LookUps();
  }

  //4- getNationality
  getWhereNationality(String? iD) async {
    await _initDatabase();
    Database db = _database!;
    // List<Map> maps = await db.query('$tableNationality');
    // print('map: ${maps.toList().length}');
    // print(await _database.query(tableNationality));
    late List<Map> maps;
    await db.transaction((txn) async => maps = await txn
        .query(tableNationality, where: '$columnID =?', whereArgs: [iD]));
    if (maps.isNotEmpty) {
      return maps
          .map((item) => LookUps.fromLocalStorage(item as Map<String, dynamic>))
          .first;
    }
    return LookUps();
  }

  //5- getSector
  getWhereSector(String? iD) async {
    await _initDatabase();
    Database db = _database!;
    late List<Map> maps;
    await db.transaction((txn) async => maps =
        await txn.query(tableSector, where: '$columnID =?', whereArgs: [iD]));
    if (maps.isNotEmpty) {
      return maps
          .map((item) => LookUps.fromLocalStorage(item as Map<String, dynamic>))
          .first;
    }
    return LookUps();
  }

  //6- getWhereIndustry
  getWhereIndustry(String? iD) async {
    await _initDatabase();
    Database db = _database!;
    // List<Map> maps = await db.query('$tableIndustry');
    // print('map: ${maps.toList().length}');
    // print(await _database.query(tableIndustry));
    late List<Map> maps;
    await db.transaction((txn) async => maps =
        await txn.query(tableIndustry, where: '$columnID =?', whereArgs: [iD]));
    if (maps.isNotEmpty) {
      return maps
          .map((item) => LookUps.fromLocalStorage(item as Map<String, dynamic>))
          .first;
    }
    return LookUps();
  }

  //7- getCountry
  getWhereCountry(String? iD) async {
    await _initDatabase();
    Database db = _database!;
    // List<Map> maps = await db.query('$tableCountry');
    // print('map: ${maps.toList().length}');
    // print(await _database.query(tableCountry));
    late List<Map> maps;
    await db.transaction((txn) async => maps =
        await txn.query(tableCountry, where: '$columnID =?', whereArgs: [iD]));
    if (maps.isNotEmpty) {
      return maps
          .map((item) => LookUps.fromLocalStorage(item as Map<String, dynamic>))
          .first;
    }
    return LookUps();
  }

  //8- getWhereCity
  getWhereCity(String? iD) async {
    await _initDatabase();
    Database db = _database!;
    late List<Map> maps;
    await db.transaction((txn) async => maps =
        await txn.query(tableCity, where: '$columnID =?', whereArgs: [iD]));

    if (maps.isNotEmpty) {
      return maps
          .map((item) => LookUps.fromLocalStorage(item as Map<String, dynamic>))
          .first;
    }
    return LookUps();
  }

  //9- getWhereReligion
  getWhereReligion(String? iD) async {
    await _initDatabase();
    Database db = _database!;
    late List<Map> maps;
    await db.transaction((txn) async => maps =
        await txn.query(tableReligion, where: '$columnID =?', whereArgs: [iD]));

    if (maps.isNotEmpty) {
      return maps
          .map((item) => LookUps.fromLocalStorage(item as Map<String, dynamic>))
          .first;
    }
    return LookUps();
  }

  //10- getWhereMajor
  getWhereMajor(String? iD) async {
    await _initDatabase();
    Database db = _database!;
    // List<Map> maps = await db.query('$tableMajor');
    // print('map: ${maps.toList().length}');
    // print(await _database.query(tableMajor));
    late List<Map> maps;
    await db.transaction((txn) async => maps =
        await txn.query(tableMajor, where: '$columnID =?', whereArgs: [iD]));
    if (maps.isNotEmpty) {
      return maps
          .map((item) => LookUps.fromLocalStorage(item as Map<String, dynamic>))
          .first;
    }
    return LookUps();
  }

  //11- getWhereJobRole
  getWhereJobRole(String? iD) async {
    await _initDatabase();
    Database db = _database!;
    // List<Map> maps = await db.query('$tableJobRole');
    // print('map: ${maps.toList().length}');
    // print(await _database.query(tableJobRole));
    late List<Map> maps;
    await db.transaction((txn) async => maps =
        await txn.query(tableJobRole, where: '$columnID =?', whereArgs: [iD]));
    if (maps.isNotEmpty) {
      return maps
          .map((item) => LookUps.fromLocalStorage(item as Map<String, dynamic>))
          .first;
    }
    return LookUps();
  }

  //12- getWhereContactTypes
  getWhereContactTypes(String? iD) async {
    await _initDatabase();
    Database db = _database!;

    late List<Map> maps;
    await db.transaction((txn) async => maps = await txn
        .query(tableContractTypes, where: '$columnID =?', whereArgs: [iD]));
    if (maps.isNotEmpty) {
      return maps
          .map((item) => LookUps.fromLocalStorage(item as Map<String, dynamic>))
          .first;
    }
    return LookUps();
  }

  //13- getWhereRecruitment
  getWhereRecruitment(String? iD) async {
    await _initDatabase();
    Database db = _database!;
    late List<Map> maps;
    await db.transaction((txn) async => maps = await txn
        .query(tableRecruitment, where: '$columnID =?', whereArgs: [iD]));
    if (maps.isNotEmpty) {
      return maps
          .map((item) => LookUps.fromLocalStorage(item as Map<String, dynamic>))
          .first;
    }
    return LookUps();
  }

  //14- getWhereSkillsLevel
  getWhereSkillsLevel(String? iD) async {
    await _initDatabase();
    Database db = _database!;
    late List<Map> maps;
    await db.transaction((txn) async => maps = await txn
        .query(tableSkillsLevel, where: '$columnID =?', whereArgs: [iD]));
    if (maps.isNotEmpty) {
      return maps
          .map((item) => LookUps.fromLocalStorage(item as Map<String, dynamic>))
          .first;
    }
    return LookUps();
  }

  //15- getWhereExperienceYears
  getWhereExperienceYears(String? iD) async {
    await _initDatabase();
    Database db = _database!;
    late List<Map> maps;
    await db.transaction((txn) async => maps = await txn
        .query(tableExperienceYears, where: '$columnID =?', whereArgs: [iD]));
    if (maps.isNotEmpty) {
      return maps
          .map((item) => LookUps.fromLocalStorage(item as Map<String, dynamic>))
          .first;
    }
    return LookUps();
  }
  //16- getWhereLastUsed

  getWhereLastUsed(String? iD) async {
    await _initDatabase();
    Database db = _database!;
    late List<Map> maps;
    await db.transaction((txn) async => maps =
        await txn.query(tableLastUsed, where: '$columnID =?', whereArgs: [iD]));
    if (maps.isNotEmpty) {
      return maps
          .map((item) => LookUps.fromLocalStorage(item as Map<String, dynamic>))
          .first;
    }
    return LookUps();
  }

//Company
  getWhereCompany(int? iD) async {
    await _initDatabase();
    Database db = _database!;
    List<Map> map = await db
        .query(tableCompany, where: '$columnCompanyID=?', whereArgs: [iD]);
    print('map: ${map.toList().length}');
    if (map.isNotEmpty) {
      return map
          .map((item) => Company.fromJson(item as Map<String, dynamic>))
          .first;
    }
    return Company();
  }

//Participant
  getWhereParticipant(int? iD) async {
    await _initDatabase();
    Database db = _database!;
    List<Map> map = await db.query(tableParticipant,
        where: '$columnParticipantID=?', whereArgs: [iD]);
    print('map: ${map.toList().length}');
    if (map.isNotEmpty) {
      return map
          .map((item) => Participant.fromJson(item as Map<String, dynamic>))
          .first;
    }
    return Participant();
  }

//##############################################Get ID Last##############################################

//ParticipantID
  getLastParticipantID() async {
    await _initDatabase();
    Database db = _database!;
    var participantID = Sqflite.firstIntValue(await db.rawQuery(
          "SELECT $columnParticipantID FROM $tableParticipant ORDER BY $columnParticipantID DESC",
        )) ??
        0;
    print('participantID :$participantID');
    return ++participantID;
  }

  //CompanyID
  getLastCompanyID() async {
    await _initDatabase();
    Database db = _database!;
    var companyID = Sqflite.firstIntValue(await db.rawQuery(
          "SELECT $columnCompanyID FROM $tableCompany  ORDER BY $columnCompanyID DESC",
        )) ??
        0;
    print('companyID :$companyID');
    return ++companyID;
  }

//
// //##############################################Delete##############################################
//Participant
  deleteParticipant(int localID) async {
    await _initDatabase();
    Database db = _database!;
    //TODO:LocalParticipantID
    var count = Sqflite.firstIntValue(await db.rawQuery(
            "SELECT COUNT($columnParticipantID) FROM $tableParticipant WHERE $columnParticipantID = ?",
            ['$localID'])) ??
        0;
    if (count == 0) {
      await db.rawDelete(
          'DELETE FROM $tablePFInfo WHERE $columnFInfoParticipantID = ?',
          ['$localID']);
      await db.rawDelete(
          'DELETE FROM $tablePExperience WHERE $columnPExperienceParticipantID = ?',
          ['$localID']);
      await db.rawDelete(
          'DELETE FROM $tableAppFormTrans WHERE $columnAppFormTransUserID = ? AND $columnAppFormTransFormType=?',
          ['$localID', (ApplicationFormType.participant)]);
      await db.rawDelete(
          'DELETE FROM $tableParticipant WHERE $columnParticipantID = ?',
          ['$localID']);
    }
  }

  //Company
  deleteCompany(int localID) async {
    await _initDatabase();
    Database db = _database!;
    //TODO:LocalCompanyID
    var count = Sqflite.firstIntValue(await db.rawQuery(
            "SELECT COUNT($columnCompanyID) FROM $tableCompany WHERE $columnCompanyID = ?",
            ['$localID'])) ??
        0;
    if (count == 0) {
      await db.rawDelete(
          'DELETE FROM $tableBranches WHERE $columnBranchInstitutionID = ?',
          ['$localID']);
      await db.rawDelete(
          'DELETE FROM $tableContact WHERE $columnContactInstitutionID = ?',
          ['$localID']);
      await db.rawDelete(
          'DELETE FROM $tableJO WHERE $columnJOInstitutionID = ?',
          ['$localID']);
      await db.rawDelete(
          'DELETE FROM $tableAppFormTrans WHERE $columnAppFormTransUserID = ? AND $columnAppFormTransFormType=?',
          ['$localID', (ApplicationFormType.institution)]);
      await db.rawDelete(
          'DELETE FROM $tableCompany WHERE $columnCompanyID = ?', ['$localID']);
    }
  }

  clearLookups() async {
    await _initDatabase();
    Database db = _database!;

    await db.rawDelete('DELETE FROM $tableCity ');
    await db.rawDelete('DELETE FROM $tableNationality');
    await db.rawDelete('DELETE FROM $tableCountry ');

    // query(tableGender);
    //     .query(tableMarital);
    // b.query(tableEduLevel);
    // t db.query(tableNationality);
    // query(tableSector);
    // b.query(tableIndustry);
    //     .query(tableCountry);
    // ery(tableCity);
    // b.query(tableReligion);
    // uery(tableMajor);
    //     .query(tableJobRole);
    // t db.query(tableRecruitment);
    // ait db.query(tableContractTypes);
  }

  isLookupsEmpty() async {
    await _initDatabase();
    Database db = _database!;
    List<Map> genderMap = await db.query(tableGender);
    List<Map> maritalMap = await db.query(tableMarital);
    List<Map> eduLevelMap = await db.query(tableEduLevel);
    List<Map> nationalityMap = await db.query(tableNationality);
    List<Map> sectorMap = await db.query(tableSector);
    List<Map> industryMap = await db.query(tableIndustry);
    List<Map> countryMap = await db.query(tableCountry);
    List<Map> cityMap = await db.query(tableCity);
    List<Map> religionMap = await db.query(tableReligion);
    List<Map> majorMap = await db.query(tableMajor);
    List<Map> jobRoleMap = await db.query(tableJobRole);
    List<Map> recruitmentMap = await db.query(tableRecruitment);
    List<Map> contractTypesMap = await db.query(tableContractTypes);

    if ((genderMap.isEmpty || maritalMap.isEmpty) ||
        (eduLevelMap.isEmpty || nationalityMap.isEmpty) ||
        (sectorMap.isEmpty || industryMap.isEmpty) ||
        (countryMap.isEmpty || cityMap.isEmpty) ||
        (religionMap.isEmpty || majorMap.isEmpty) ||
        (recruitmentMap.isEmpty || contractTypesMap.isEmpty) ||
        jobRoleMap.isEmpty) {
      print('isLookupsEmpty :true');
      print('genderMap.length         ${genderMap.length}');
      print('maritalMap.length        ${maritalMap.length}');
      print('eduLevelMap.length       ${eduLevelMap.length}');
      print('nationalityMap.length    ${nationalityMap.length}');
      print('sectorMap.length         ${sectorMap.length}');
      print('industryMap.length       ${industryMap.length}');
      print('countryMap.length        ${countryMap.length}');
      print('cityMap.length           ${cityMap.length}');
      print('religionMap.length       ${religionMap.length}');
      print('majorMap.length          ${majorMap.length}');
      print('jobRoleMap.length        ${jobRoleMap.length}');
      print('recruitmentMap.length    ${recruitmentMap.length}');
      print('contractTypesMap.length  ${contractTypesMap.length}');
      return true;
    }
    print('isLookupsEmpty :false');

    print('genderMap.length         ${genderMap.length}');
    print('maritalMap.length        ${maritalMap.length}');
    print('eduLevelMap.length       ${eduLevelMap.length}');
    print('nationalityMap.length    ${nationalityMap.length}');
    print('sectorMap.length         ${sectorMap.length}');
    print('industryMap.length       ${industryMap.length}');
    print('countryMap.length        ${countryMap.length}');
    print('cityMap.length           ${cityMap.length}');
    print('religionMap.length       ${religionMap.length}');
    print('majorMap.length          ${majorMap.length}');
    print('jobRoleMap.length        ${jobRoleMap.length}');
    print('recruitmentMap.length    ${recruitmentMap.length}');
    print('contractTypesMap.length  ${contractTypesMap.length}');
    return false;
  }
}
