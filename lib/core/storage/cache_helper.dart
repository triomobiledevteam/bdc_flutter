// ignore_for_file: constant_identifier_names

import 'package:shared_preferences/shared_preferences.dart';

class CacheHelper {
  static late SharedPreferences _sharedPreferences;

  static init() async {
    _sharedPreferences = await SharedPreferences.getInstance();
  }

  static dynamic getData({required String key}) {
    return _sharedPreferences.get(key);
  }

  static Future<void> remove() async {
    _sharedPreferences.clear();
  }

  static Future<bool> saveData(
      {required String key, required dynamic value}) async {
    if (value is int) return await _sharedPreferences.setInt(key, value);
    if (value is double) return await _sharedPreferences.setDouble(key, value);
    if (value is bool) return await _sharedPreferences.setBool(key, value);
    if (value is List<String>) {
      return await _sharedPreferences.setStringList(key, value);
    }
    return await _sharedPreferences.setString(key, value);
  }
}

//Sheared Preferences keys
const USER_DATA = 'USER_DATA';

const CERTIFICATE = 'CERTIFICATE';

const USER_TYPE = 'USER_TYPE';

const ACCOUNT_TYPE = 'ACCOUNT_TYPE';

const USER_CERTIFICATE = 'USER_CERTIFICATE';

const LANGUAGE_ID = 'LANGUAGE_ID';

const COMPANY_ID = 'COMPANY_ID';
const USER_ID = 'USER_ID';
