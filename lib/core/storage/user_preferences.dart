// import 'package:shared_preferences/shared_preferences.dart';
//
// class UserPreferences {
//   static final UserPreferences _instance = UserPreferences._ctor();
//
//   factory UserPreferences() {
//     return _instance;
//   }
//
//   UserPreferences._ctor();
//
//   late SharedPreferences _prefs;
//
//   init() async {
//     _prefs = await SharedPreferences.getInstance();
//   }
//
//   clear() async {
//     userType = '';
//     userID = -1;
//     languageID = 1;
//     companyID = 1;
//
//     password = '';
//     employee = '';
//     company = '';
//     participant = '';
//   }
//
//   String get userType {
//     return _prefs.getString('UserType') ?? '';
//   }
//
//   set userType(String value) {
//     _prefs.setString('UserType', value);
//   }
//
//   int get userID {
//     return _prefs.getInt('UserID') ?? -1;
//   }
//
//   set userID(int value) {
//     _prefs.setInt('UserID', value);
//   }
//
//   String get password {
//     return _prefs.getString('Pass') ?? '';
//   }
//
//   set password(String value) {
//     _prefs.setString('Pass', value);
//   }
//
//   int get languageID {
//     return _prefs.getInt('LanguageID') ?? 1;
//   }
//
//   set languageID(int value) {
//     _prefs.setInt('LanguageID', value);
//   }
//
//   int get companyID {
//     return _prefs.getInt('CompanyID') ?? 1;
//   }
//
//   set companyID(int value) {
//     _prefs.setInt('CompanyID', value);
//   }
//
//   String get employee {
//     return _prefs.getString('Employee') ?? '';
//   }
//
//   set employee(String value) {
//     _prefs.setString('Employee', value);
//   }
//
//   String get participant {
//     return _prefs.getString('Participant') ?? '';
//   }
//
//   set participant(String value) {
//     _prefs.setString('Participant', value);
//   }
//
//   String get trainer {
//     return _prefs.getString('Trainer') ?? '';
//   }
//
//   set trainer(String value) {
//     _prefs.setString('Trainer', value);
//   }
//
//   String get company {
//     return _prefs.getString('Company') ?? '';
//   }
//
//   set company(String value) {
//     _prefs.setString('Company', value);
//   }
// }
