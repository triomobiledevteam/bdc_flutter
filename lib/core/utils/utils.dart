import 'dart:convert';
import 'dart:typed_data';

import 'package:bdc/core/helper/error_handler/errorhandler.dart';
import 'package:flutter/cupertino.dart';
import 'package:intl/intl.dart';

import '../modal/LoginModal/Company.dart';
import '../modal/LoginModal/Participant.dart';

int parseInt(String value) {
  try {
    return int.parse(value);
  } catch (error) {
    return 0;
  }
}

onAppError(BuildContext context, String massage) {
  ErrorHandler.errorMassage = massage;
  ErrorHandler.showErrorMassage(context);
}

String formatDate(String format, String date, [String onError = '']) {
  try {
    String formatDate = DateFormat(format).format(DateTime.parse(date));
    print(formatDate);
    return formatDate;
  } catch (e) {
    return onError;
  }
}

Uint8List dataFromBase64String(String? base64String) {
  return base64Decode(base64String ?? '');
}

String getParticipantDescription(Participant participant, int languageID) {
  String name = '';

  String firstName = languageID == 1
      ? participant.firstNameEn ?? participant.firstNameAr ?? ''
      : participant.firstNameAr ?? participant.firstNameEn ?? '';

  String lastname = languageID == 1
      ? participant.familyNameEn ?? participant.familyNameEn ?? ''
      : participant.familyNameEn ?? participant.familyNameEn ?? '';

  name = firstName + ' ' + lastname;

  if (name.trim().isEmpty) {
    name = languageID == 1
        ? participant.fullNameEn ??
            participant.familyNameAr ??
            participant.code ??
            ''
        : participant.fullNameAr ??
            participant.familyNameEn ??
            participant.code ??
            '';
  }
  return name;
}

String getCompanyDescription(Company company, int languageID) {
  String description = '';

  if (languageID == 2) {
    description =
        (company.institutionNameAr != null && company.institutionNameAr != ''
                ? company.institutionNameAr
                : company.institutionNameEn) ??
            '';
  } else {
    description =
        (company.institutionNameEn != null && company.institutionNameEn != ''
                ? company.institutionNameEn
                : company.institutionNameAr) ??
            '';
  }

  if (description.trim().isEmpty) {
    description = company.code ?? '';
  }
  return description;
}
