// extension EmailValidator on String {
bool isValidEmail(String value) {
  return RegExp(
          r"^[a-zA-Z0-9.!#$%&\'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?)*$")
      .hasMatch(value);
}
// }

// extension PhoneValidator on String {
bool isValidMobile(String value) {
  return RegExp(r'^(?:07)?[0-9]{10}$').hasMatch(value);
}

bool isValidPassword(String value) {
  return RegExp(r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{5,}$').hasMatch(value);
}
