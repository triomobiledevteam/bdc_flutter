import 'package:bdc/core/theme/sizeConfig.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:top_snackbar_flutter/custom_snack_bar.dart';
import 'package:top_snackbar_flutter/top_snack_bar.dart';

void onAppError(BuildContext context, String massage) {
  ErrorHandler.errorMassage = massage;
  ErrorHandler.showErrorMassage(context);
}

class ErrorHandler {
  static String? errorMassage = '';

  static showErrorMassageDialog(BuildContext context) {
    return showDialog<void>(
        context: context,
        builder: (context) => AlertDialog(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10),
              ),
              elevation: 0,
              title: Text(errorMassage!),
              actions: <Widget>[
                ElevatedButton(
                  onPressed: () async {
                    errorMassage = '';
                    Navigator.pop(context, true);
                  },
                  child: Text(AppLocalizations.of(context)!.confirm),
                ),
              ],
            ));
  }

  static showErrorMassage(BuildContext? context) {
    return Center(child: Text(errorMassage!));
  }

  static onErrorMassageWidget(Function onPress) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          ElevatedButton(
              onPressed: onPress as void Function()?,
              child: const Text('Try Again')),
          Text(ErrorHandler.errorMassage!),
        ],
      ),
    );
  }

  static noConnectionSnackBar(context) {
    showTopSnackBar(
      context,
      CustomSnackBar.error(
        message: "No Internet Check Connection",
        textStyle: TextStyle(
          fontWeight: FontWeight.w600,
          fontSize: SizeConfig.width! * 0.04,
          color: Colors.black,
        ),
      ),
    );
  }

  static onErrorSnackBar(String text, context) {
    showTopSnackBar(
      context,
      CustomSnackBar.error(
        message: text,
        textStyle: TextStyle(
          fontWeight: FontWeight.w600,
          fontSize: SizeConfig.width! * 0.04,
          color: Colors.black,
        ),
      ),
    );
  }

  static onSuccessSnackBar(String text, context) {
    showTopSnackBar(
      context,
      CustomSnackBar.success(
        message: text,
        textStyle: TextStyle(
          fontWeight: FontWeight.w600,
          fontSize: SizeConfig.width! * 0.04,
          color: Colors.black,
        ),
      ),
    );
  }

  static noConnectionDialog(BuildContext context) {
    return showDialog<void>(
        context: context,
        builder: (context) => AlertDialog(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10),
              ),
              elevation: 0,
              title: const Text('No Internet Check Connection'),
              actions: <Widget>[
                ElevatedButton(
                  onPressed: () async {
                    errorMassage = '';
                    Navigator.pop(context, true);
                  },
                  child: const Text("Ok"),
                ),
              ],
            ));
  }

  static noPermissionDialog(BuildContext context) {
    return showDialog<void>(
        context: context,
        builder: (context) => AlertDialog(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10),
              ),
              elevation: 0,
              title: const Text('You Don"t Have Permission'),
              actions: <Widget>[
                ElevatedButton(
                  onPressed: () async {
                    Navigator.pop(context, true);
                  },
                  child: const Text("Ok"),
                ),
              ],
            ));
  }
}
