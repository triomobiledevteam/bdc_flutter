import 'package:encrypt/encrypt.dart';

import '../utils/utils.dart';

class EncryptData {
  static String encryptAES(plainText) {
    final key = Key.fromUtf8('my 32 length key................');
    final iv = IV.fromLength(16);
    final encrypt = Encrypter(AES(key));
    final encrypted = encrypt.encrypt(plainText, iv: iv);
    return encrypted.base64;
  }

  static String decryptAES(plainText) {
    final key = Key.fromUtf8('my 32 length key................');
    final iv = IV.fromLength(16);
    final encrypt = Encrypter(AES(key));
    final decrypted =
        encrypt.decrypt(Encrypted(dataFromBase64String(plainText)), iv: iv);
    return decrypted;
  }
}
