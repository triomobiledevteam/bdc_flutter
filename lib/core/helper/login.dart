import 'dart:convert';
import 'dart:developer';

import 'package:bdc/Screens/HomeScreen/EmpHomeScreen.dart';
import 'package:bdc/Screens/HomeScreen/HomeScreen.dart';
import 'package:bdc/Screens/LoginScreen/LoginScreen.dart';
import 'package:bdc/core/modal/AplicationForm.dart';
import 'package:bdc/core/modal/LoginModal/Company.dart';
import 'package:bdc/core/modal/LoginModal/Employee.dart';
import 'package:bdc/core/modal/LoginModal/GenUser.dart';
import 'package:bdc/core/modal/LoginModal/Participant.dart';
import 'package:bdc/core/modal/LoginModal/Trainer.dart';
import 'package:bdc/core/modal/LoginModal/certificate.dart';
import 'package:bdc/core/modal/applicationFormTrans.dart';
import 'package:bdc/core/modal/constraints/global_values.dart';
import 'package:bdc/core/modal/enums/UsersType.dart';
import 'package:bdc/core/modal/program/project.dart';
import 'package:bdc/core/repository/main_repository/main_repository.dart';
import 'package:bdc/core/repository/particiapnt_repository/particiapnt_repository.dart';
import 'package:bdc/core/repository/trainer_repository/trainer_repository.dart';
import 'package:bdc/core/storage/cache_helper.dart';
import 'package:bdc/core/utils/Navigation.dart';
import 'package:flutter/cupertino.dart';

import '../repository/company_repository/company_repository.dart';

late Certificate _certificate;
late BuildContext _context;

login(Certificate certificate, void Function(String) onError, context) async {
  _certificate = certificate;
  _context = context;
  await GetMainRepository().login(certificate, onDoneLogin, onError);
}

onDoneLogin(dynamic value) async {
  userType = value['UserType'];
  CacheHelper.saveData(key: USER_TYPE, value: userType);
  // EncryptData
  CacheHelper.saveData(
      key: CERTIFICATE, value: json.encode(_certificate.toMap()));

  dynamic body = (value['Body']);
  print(body);
  if (userType == UserType.Employee.name) {
    await employeeFetch(json.decode(body));
  } else if (userType == UserType.Participant.name) {
    await participantFetch(json.decode(body));
  } else if (userType == UserType.Trainer.name) {
    await trainerFetch(json.decode(body));
  } else if (userType == UserType.CBO.name) {
    await cBOFetch(json.decode(body));
  } else if (userType == UserType.Company.name) {
    await companyFetch(json.decode(body));
  }
}

employeeFetch(dynamic body) async {
  print(body);
  mainEmployee = Employee.fromJson(body);
  saveUserData(body);
  _saveData(mainEmployee.userID, mainEmployee.companyID);
  Navigation.nextAndResetStack(_context, const EmpHomeScreen());
}

participantFetch(dynamic body) async {
  mainUser = GenUser.fromJson(body);
  saveUserData(body);

  mainParticipant = Participant.fromJson(body);
  ProjectList projectList = ProjectList.fromJson(body);
  mainParticipant?.projectList = projectList.project!;
  _saveData(mainUser.userID!, mainUser.companyID!);
  await GetParticipantRepository()
      .getParticipantFamilyInfo(_context, mainParticipant!.trioID!)
      .then((value) => mainParticipant!.familyInfoList = value);
  await GetParticipantRepository()
      .getParticipantExperience(_context, mainParticipant!.trioID!)
      .then((value) => mainParticipant!.experienceList = value);
  await GetParticipantRepository()
      .getParticipantApplicationForm(mainParticipant!.trioID!)
      .then((value) async {
    List<ApplicationForm> applicationFormList = [];

    applicationFormList.addAll(await databaseClient
        .getApplicationForm(ApplicationFormType.participant));
    if (value.isNotEmpty) {
      for (var application in applicationFormList) {
        ApplicationFormTrans trans = ApplicationFormTrans();
        trans = value
            .where((element) => element.applicationFromID == application.iD)
            .first;
        application.notApplicable = trans.notApplicable;
        application.yes = trans.yes;
        application.no = trans.no;
      }
      mainParticipant!.applicationFormList = [...applicationFormList];
    }
  });

  Navigation.nextAndResetStack(_context, const HomeScreen());
}

companyFetch(
  dynamic body,
) async {
  mainUser = GenUser.fromJson(body);
  saveUserData(body);
  mainCompany = Company.fromJson(body);
  _saveData(mainUser.userID!, mainUser.companyID!);
  Navigation.nextAndResetStack(_context, const HomeScreen());
}

cBOFetch(
  dynamic body,
) async {
  mainUser = GenUser.fromJson(body);
  saveUserData(body);
  mainCompany = Company.fromJson(body);
  _saveData(mainUser.userID!, mainUser.companyID!);
  Navigation.nextAndResetStack(_context, const HomeScreen());
}

trainerFetch(
  dynamic body,
) async {
  log(body.toString());
  try {
    mainUser = GenUser.fromJson(body);
    saveUserData(body);
    mainTrainer = Trainer.fromJson(body);
    _saveData(mainUser.userID!, mainUser.companyID!);
    Navigation.nextAndResetStack(_context, const HomeScreen());
  } catch (error) {
    Navigation.nextAndResetStack(_context, const LoginScreen());
  }
}

_saveData(int userId, int companyId) {
  CacheHelper.saveData(key: COMPANY_ID, value: companyId);
  CacheHelper.saveData(key: USER_ID, value: userId);
  companyID = companyId;
  userID = userId;
}

saveUserData(dynamic value) {
  CacheHelper.saveData(key: USER_DATA, value: json.encode(value));
}

checkLocalUser(context) {
  String data = CacheHelper.getData(key: USER_DATA) ?? '';

  if (data.isNotEmpty) {
    if (userType == UserType.Employee.name) {
      mainEmployee = Employee.fromJson(json.decode(data));
      Navigation.nextAndResetStack(context, const EmpHomeScreen());
    } else if (userType == UserType.Participant.name) {
      mainUser = GenUser.fromJson(json.decode(data));
      mainParticipant = Participant.fromJson(json.decode(data));

      ProjectList projectList = ProjectList.fromJson(json.decode(data));
      mainParticipant?.projectList = projectList.project!;
      Navigation.nextAndResetStack(context, const HomeScreen());
    } else if (userType == UserType.Trainer.name) {
      mainUser = GenUser.fromJson(json.decode(data));
      mainTrainer = Trainer.fromJson(json.decode(data));
      Navigation.nextAndResetStack(context, const HomeScreen());
    } else if (userType == UserType.CBO.name) {
      mainUser = GenUser.fromJson(json.decode(data));
      mainCompany = Company.fromJson(json.decode(data));
      Navigation.nextAndResetStack(context, const HomeScreen());
    } else if (userType == UserType.Company.name) {
      mainUser = GenUser.fromJson(json.decode(data));
      mainCompany = Company.fromJson(json.decode(data));
      Navigation.nextAndResetStack(context, const HomeScreen());
    }
  } else {
    Navigation.nextAndResetStack(context, const LoginScreen());
  }
}
