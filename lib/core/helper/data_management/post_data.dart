import 'package:bdc/core/modal/AplicationForm.dart';
import 'package:bdc/core/modal/LoginModal/Company.dart';
import 'package:bdc/core/modal/LoginModal/Participant.dart';
import 'package:bdc/core/modal/applicationFormTrans.dart';
import 'package:bdc/core/repository/company_repository/company_repository.dart';
import 'package:bdc/core/repository/particiapnt_repository/particiapnt_repository.dart';
import 'package:bdc/core/storage/db_client.dart';

import '../checkConnection.dart';

class PostData {
  final _db = DatabaseClient.instance;

  Future post() async {
    await CheckConnection().checkConnection(() async {
      await _postParticipant();
      await _postCompany();
    });
  }

  _postParticipant() async {
    List<Participant> participantList = [];
    participantList.addAll(await _db.getAllParticipant());
    if (participantList.isNotEmpty) {
      for (var participant in participantList) {
        participant.experienceList
            .addAll(await _db.getAllPExperience(participant.iD));
        participant.familyInfoList
            .addAll(await _db.getAllPFInfo(participant.iD));

        participant.applicationFormTransList.addAll(
            await _db.getApplicationFormTrans(
                ApplicationFormType.participant, participant.iD));
        participant.applicationFormList.addAll(
            await _db.getApplicationForm(ApplicationFormType.participant));
        for (var application in participant.applicationFormList) {
          ApplicationFormTrans trans = ApplicationFormTrans();
          trans = participant.applicationFormTransList
              .where((element) => element.applicationFromID == application.iD)
              .first;
          application.notApplicable = trans.notApplicable;
          application.yes = trans.yes;
          application.no = trans.no;
        }
        GetParticipantRepository()
            .submitParticipantToTrio(participant, onError, () {});
        if (participant.trioID != null && participant.trioID! > 0) {
          _db.deleteParticipant(participant.iD!);
        }
      }
    }
  }

  _postCompany() async {
    List<Company> companyList = [];
    companyList.addAll(await _db.getAllCompany());
    if (companyList.isNotEmpty) {
      for (var company in companyList) {
        company.branchList.addAll(await _db.getAllCBranch(company.iD));
        company.contactList.addAll(await _db.getAllPFInfo(company.iD));
        company.jobOpportunityList.addAll(await _db.getAllPFInfo(company.iD));

        company.applicationFormTransList.addAll(
            await _db.getApplicationFormTrans(
                ApplicationFormType.institution, company.iD));

        company.applicationFormList.addAll(
            await _db.getApplicationForm(ApplicationFormType.institution));
        for (var application in company.applicationFormList) {
          ApplicationFormTrans trans = ApplicationFormTrans();
          trans = company.applicationFormTransList
              .where((element) => element.applicationFromID == application.iD)
              .first;
          application.notApplicable = trans.notApplicable;
          application.yes = trans.yes;
          application.no = trans.no;
        }
        GetCompanyRepository().submitCompanyToTrio(company, onError, () {});
        if (company.trioID != null && company.trioID! > 0) {
          await _db.deleteCompany(company.iD!);
        }
      }
    }
  }

  onError(String massage) {
    print(massage);
  }
}
