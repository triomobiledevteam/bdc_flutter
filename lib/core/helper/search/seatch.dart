import 'package:bdc/core/modal/Lookups.dart';

filterSearchResults(
  String query,
  List<LookUps> mainList,
  List<LookUps> list,
) {
  list.clear();
  if (query.isNotEmpty) {
    for (var item in mainList) {
      if (item.descriptionEn!.toLowerCase().contains(query.toLowerCase()) ||
          item.descriptionAr!.toLowerCase().contains(query.toLowerCase())) {
        list.add(item);
      }
    }
  } else {
    list.addAll(mainList);
  }
}
