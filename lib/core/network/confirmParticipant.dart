class ConformationResponse {
  int? iD;
  String? code;

  ConformationResponse({this.iD, this.code});

  ConformationResponse.fromJsonParticipant(Map<String, dynamic> json) {
    iD = json['ParticipantID'];
    code = json['Code'];
  }

  ConformationResponse.fromJsonCompany(Map<String, dynamic> json) {
    iD = json['InstitutionID'];
    code = json['Code'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['ParticipantID'] = iD;
    data['Code'] = code;
    return data;
  }
}
