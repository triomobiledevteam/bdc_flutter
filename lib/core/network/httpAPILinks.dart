class HttpAPILinks {
  // static String defaultProxy = 'https://ProxyPos.trio365.com';
  static String defaultProxy = 'https://bdc.nutrodigital.com';
  //TODO:BDC URL

  static String normalAPI = '/api/ext/';
  static String secureAPI = '/api/sec/';
  static String smsAPI = '/api/sms/send';

  static const String appSource = 'BDC';
}
