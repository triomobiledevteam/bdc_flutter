import 'package:bdc/core/utils/utils.dart';

class ApiMsg {
  String? msg;
  int? iD;

  ApiMsg({this.msg});

  ApiMsg.codeFromJson(Map<String, dynamic> json) {
    msg = json['Code'];
  }

  ApiMsg.toJsonDone(Map<String, dynamic> json) {
    msg = json['Done'];
  }

  ApiMsg.toJsonCode(Map<String, dynamic> json) {
    msg = json['Code'];
  }

  ApiMsg.toJsonID(Map<String, dynamic> json) {
    iD = json['ID'];
  }
  ApiMsg.toJsonUserID(Map<String, dynamic> json) {
    iD = parseInt(json['UserID']);
  }
}
