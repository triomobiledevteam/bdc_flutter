class ErrorMassage {
  String? message;
  String? status;

  ErrorMassage({this.message});

  ErrorMassage.fromJson(Map<String, dynamic> json) {
    message = json['Message'];
  }

  ErrorMassage.fromJsonWithStatus(Map<String, dynamic> json) {
    message = json['Message'];
    status = json['Status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['Message'] = message;
    return data;
  }
}
