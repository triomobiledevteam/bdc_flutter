import 'package:bdc/core/modal/program/Program.dart';
import 'package:bdc/core/modal/program/program_portofolio.dart';
import 'package:bdc/core/repository/main_repository/main_repository.dart';
import 'package:flutter/material.dart';

class ProgramsProvider with ChangeNotifier {
  ProjectPortfolios? projectPortfolios;
  List<Programs> tempProgramList = [];
  String errorMessage = '';
  bool isLoading = false;
  getProgramsWhere(int portfolioID) {
    tempProgramList.clear();
    tempProgramList.addAll(projectPortfolios!.programs!
        .where((element) => element.projectPortfolioID! == portfolioID)
        .toList());
    notifyListeners();
  }

  init() {
    if (projectPortfolios == null) {
      fetchProject();
    }
  }

  setLoading() {
    isLoading = !isLoading;
  }

  Future fetchProject() async {
    errorMessage = '';
    setLoading();
    notifyListeners();
    await GetMainRepository().getPrograms(onError, onDone);
  }

  onDone(dynamic value) {
    setLoading();
    projectPortfolios = ProjectPortfolios.fromJson(value);
    notifyListeners();
  }

  onError(String massage) {
    print(massage);
    setLoading();
    errorMessage = massage;
  }
}
