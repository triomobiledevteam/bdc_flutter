import 'package:bdc/core/helper/error_handler/errorhandler.dart';
import 'package:bdc/core/modal/LoginModal/Trainer.dart';
import 'package:bdc/core/modal/trainer_modal/trainer_experience.dart';
import 'package:bdc/core/repository/trainer_repository/trainer_repository.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import '../../components/loading.dart';
import '../utils/Navigation.dart';
import '../utils/string_validation_extension.dart';

class TrainerProvider with ChangeNotifier {
  late Trainer trainer;
  bool errorFlag = false;

  setTrainer(Trainer trainer) {
    this.trainer = trainer;
  }

  addCertificate(trainerCertificate, int index) {
    if (index == -1) {
      trainer.certificate.add(trainerCertificate);
    } else {
      trainer.certificate[index] = trainerCertificate;
    }
  }

  addExperience(TrainerExperience trainerExperience, index) {
    if (index == -1) {
      trainer.experience.add(trainerExperience);
    } else {
      trainer.experience[index] = trainerExperience;
    }
  }

  checkTrainer(
    void Function(String) onError,
    void Function(dynamic) onDone,
    context,
  ) async {
    progressDialogue(context);

    print(trainer.trainerNameAr!);
    print(trainer.trainerNameEn!);
    print(trainer.educationalLevelID!);
    print(isValidMobile(trainer.mobilePhone ?? ''));
    print(trainer.email!);
    if (trainer.trainerNameAr!.isNotEmpty &&
        trainer.trainerNameEn!.isNotEmpty &&
        trainer.educationalLevelID!.toString().isNotEmpty &&
        isValidMobile(trainer.mobilePhone ?? '') &&
        isValidEmail(trainer.email ?? '')) {
      await GetTrainerRepository().editTrainer(trainer, onError, onDone);
    } else {
      errorFlag = true;
      Navigation.back(context);
      ErrorHandler.onErrorSnackBar(
          AppLocalizations.of(context)!.thisFieldIsEmpty, context);
      notifyListeners();
    }
  }

  percentageOfCompletion() {
    trainer.checkPercentageOfCompletion();
    notifyListeners();
  }
}
