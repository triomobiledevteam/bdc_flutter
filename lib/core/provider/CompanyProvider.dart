import 'package:bdc/core/helper/checkConnection.dart';
import 'package:bdc/core/helper/error_handler/errorhandler.dart';
import 'package:bdc/core/modal/AplicationForm.dart';
import 'package:bdc/core/modal/LoginModal/Company.dart';
import 'package:bdc/core/modal/company_modal/Branch.dart';
import 'package:bdc/core/modal/company_modal/Contact.dart';
import 'package:bdc/core/modal/company_modal/JobOpportunity.dart';
import 'package:bdc/core/modal/constraints/FormType.dart';
import 'package:bdc/core/modal/constraints/global_values.dart';
import 'package:bdc/core/repository/company_repository/company_repository.dart';
import 'package:bdc/core/utils/Navigation.dart';
import 'package:flutter/material.dart';

import '../../components/loading.dart';

class CompanyProvider with ChangeNotifier {
  late Company company;
  bool errorFlag = false;

  setCompany(Company company) {
    this.company = company;
    if (company.applicationFormList.isEmpty) {
      getCompanyForm();
    }
  }

  companyInit() async {
    company = Company();
    await getCompanyForm();
    company.iD = await databaseClient.getLastCompanyID();
    print('company ${company.iD}');
    notifyListeners();
  }

  getCompanyData(int? iD) async {
    company = await databaseClient.getWhereCompany(iD);
    await getCompanyJO();
    await getCompanyBranch();
    await getCompanyContact();
    notifyListeners();
  }

  List<ApplicationForm> getApplicationFormList() {
    return company.applicationFormList;
  }

  getCompanyForm() async {
    company.applicationFormList
        .addAll(await databaseClient.getApplicationForm(FormType.institution));
  }

  addBranchInfo(Branch? branch) {
    int iD = 1;
    if (company.branchList.isEmpty && branch!.iD == null) {
      branch.iD = iD;
    } else {
      iD = company.branchList.last.iD! + 1;

      if (company.branchList
          .where((element) => element.iD == branch!.iD)
          .isNotEmpty) {
        company.branchList
            .where((element) => element.iD == branch!.iD)
            .first
            .iD!;
        company.branchList.removeWhere((element) => element.iD == branch!.iD);
      }
    }
    company.branchList.add(branch!);
  }

  addContact(Contact? contact) {
    int iD = 1;
    if (company.contactList.isEmpty && contact!.iD == null) {
      contact.iD = iD;
    } else {
      iD = company.contactList.last.iD! + 1;

      if (company.contactList
          .where((element) => element.iD == contact!.iD)
          .isNotEmpty) {
        company.contactList
            .where((element) => element.iD == contact!.iD)
            .first
            .iD!;
        company.contactList.removeWhere((element) => element.iD == contact!.iD);
      }
    }
    company.contactList.add(contact!);
  }

  addJO(JobOpportunities? jobOpportunities) {
    int iD = 1;
    if (company.jobOpportunityList.isEmpty && jobOpportunities!.iD == null) {
      jobOpportunities.iD = iD;
    } else {
      iD = company.jobOpportunityList.last.iD! + 1;

      if (company.jobOpportunityList
          .where((element) => element.iD == jobOpportunities!.iD)
          .isNotEmpty) {
        company.jobOpportunityList
            .where((element) => element.iD == jobOpportunities!.iD)
            .first
            .iD!;
        company.jobOpportunityList
            .removeWhere((element) => element.iD == jobOpportunities!.iD);
      }
    }
    company.jobOpportunityList.add(jobOpportunities!);
  }

  getCompanyJO() async {
    company.jobOpportunityList
        .addAll(await databaseClient.getAllCJO(company.iD));
  }

  getCompanyContact() async {
    company.contactList.addAll(await databaseClient.getAllCContact(company.iD));
  }

  getCompanyBranch() async {
    company.branchList.addAll(await databaseClient.getAllCBranch(company.iD));
  }

  //Submit to sql
  submitToSql() async {
    await databaseClient.companyDB(company);
    if (company.branchList.isNotEmpty) {
      for (var branch in company.branchList) {
        branch.institutionID = company.iD;

        await databaseClient.branchDB(branch);
      }
    }
    if (company.contactList.isNotEmpty) {
      for (var contact in company.contactList) {
        contact.institutionID = company.iD;
        await databaseClient.contactDB(contact);
      }
    }
    if (company.jobOpportunityList.isNotEmpty) {
      for (var jobO in company.jobOpportunityList) {
        jobO.institutionID = company.iD;
        await databaseClient.jobOpportunitiesDB(jobO);
      }
    }
  }

  checkCompanyData(
      void Function(String) onError, void Function() onDone, context,
      {bool clearFlag = false,
      resetFlag = false,
      backFlag = false,
      bool submitLocally = false}) async {
    progressDialogue(context);

    errorFlag = true;
    if (company.institutionNameEn!.isNotEmpty &&
        company.institutionNameAr!.isNotEmpty &&
        company.institutionID!.isNotEmpty) {
      await CheckConnection().checkConnectivity2(() async {
        await GetCompanyRepository().submitCompanyToTrio(company, onError,
            () async {
          if (resetFlag) {
            await companyInit();
          }
          ErrorHandler.onSuccessSnackBar('Done', context);
          notifyListeners();
          onDone();
          if (backFlag) {
            Navigation.back(context);
          }
          Navigation.back(context);
        });

        if (company.trioID != null && company.trioID! > 0) {
          await databaseClient.deleteCompany(company.iD ?? 0);
        }
      }, () async {
        if (submitLocally) {
          await submitToSql();
          if (resetFlag) {
            await companyInit();
          }
          ErrorHandler.onSuccessSnackBar('Done', context);
          notifyListeners();
          if (backFlag) {
            Navigation.back(context);
          }
          Navigation.back(context);
        }
      });
      onDone();
    } else {
      errorFlag = true;
      Navigation.back(context);
      ErrorHandler.onErrorSnackBar('Empty Field', context);
      notifyListeners();
    }
  }

  percentageOfCompletion() {
    company.checkPercentageOfCompletion();
    notifyListeners();
  }
}
