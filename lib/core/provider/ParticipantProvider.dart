import 'dart:io';
import 'dart:typed_data';

import 'package:bdc/core/helper/checkConnection.dart';
import 'package:bdc/core/helper/error_handler/errorhandler.dart';
import 'package:bdc/core/modal/AplicationForm.dart';
import 'package:bdc/core/modal/LoginModal/Participant.dart';
import 'package:bdc/core/modal/constraints/FormType.dart';
import 'package:bdc/core/modal/constraints/global_values.dart';
import 'package:bdc/core/modal/enums/UsersType.dart';
import 'package:bdc/core/modal/participant_modal/ParticipantExperience.dart';
import 'package:bdc/core/modal/participant_modal/ParticipantFamilyInfo.dart';
import 'package:bdc/core/repository/particiapnt_repository/particiapnt_repository.dart';
import 'package:bdc/core/storage/db_client.dart';
import 'package:bdc/core/storage/sql_table/attachment/attachment_db.dart';
import 'package:bdc/core/utils/Navigation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:path_provider/path_provider.dart';

import '../../components/loading.dart';
import '../utils/string_validation_extension.dart';

class ParticipantProvider with ChangeNotifier {
  final _db = DatabaseClient.instance;
  final _attachmentDB = AttachmentDatabase.instance;

  late Participant participant;

  setParticipant(Participant participant) {
    this.participant = participant;
    if (participant.applicationFormList.isEmpty) {
      getParticipantForm();
    }
  }

  bool errorFlag = false;

  getParticipantForm() async {
    participant.applicationFormList
        .addAll(await _db.getApplicationForm(FormType.participant));
  }

  participantInit() async {
    participant = Participant();
    errorFlag = false;
    await _attachmentDB.database;
    await getParticipantForm();
    participant.iD = await _db.getLastParticipantID();
    notifyListeners();
  }

  getParticipantData(int? iD) async {
    participant = await _db.getWhereParticipant(iD);
    print('getParticipantData ${participant.iD}iD');
    await getParticipantFamilyInfo();
    await getParticipantExperience();
    notifyListeners();
  }

  List<ApplicationForm> getApplicationFormList() {
    return participant.applicationFormList;
  }

  //Add to list
  addFamilyInfo(ParticipantFamilyInfo? info) {
    int iD = 1;
    if (participant.familyInfoList.isEmpty && info!.iD == null) {
      info.iD = iD;
    } else {
      if (info!.iD == null) {
        iD = participant.familyInfoList.last.iD! + 1;
        info.iD = iD;
      }
      if (participant.familyInfoList
          .where((element) => element.iD == info.iD)
          .isNotEmpty) {
        participant.familyInfoList
            .where((element) => element.iD == info.iD)
            .first
            .iD!;
        participant.familyInfoList
            .removeWhere((element) => element.iD == info.iD);
      }
    }
    participant.familyInfoList.add(info);
  }

  addExperience(ParticipantExperience? experience) {
    int iD = 1;
    if (participant.experienceList.isEmpty && experience!.iD == null) {
      experience.iD = iD;
    } else {
      if (experience!.iD == null) {
        iD = participant.experienceList.last.iD! + 1;
        experience.iD = iD;
      }

      if (participant.experienceList
          .where((element) => element.iD == experience.iD)
          .isNotEmpty) {
        participant.experienceList
            .where((element) => element.iD == experience.iD!)
            .first
            .iD!;
        participant.experienceList
            .removeWhere((element) => element.iD == experience.iD!);
      }
    }
    participant.experienceList.add(experience);
  }

  //get data
  getParticipantExperience() async {
    participant.experienceList
        .addAll(await _db.getAllPExperience(participant.iD));
  }

  getParticipantFamilyInfo() async {
    participant.familyInfoList.addAll(await _db.getAllPFInfo(participant.iD));
  }

  //Submit to sql
  submitToSql() async {
    await _db.participantDB(participant);

    for (var info in participant.familyInfoList) {
      info.participantID = participant.iD;
      await _db.participantFInfoDB(info);
    }

    for (var experience in participant.experienceList) {
      experience.participantID = participant.iD;
      await _db.participantExperienceDB(experience);
    }
  }

  checkParticipantData(
      void Function(String) onError, void Function() onDone, context,
      {bool clearFlag = false,
      resetFlag = false,
      backFlag = false,
      bool submitLocally = false}) async {
    errorFlag = false;

    bool isEmployee = userType == UserType.Employee.name;

    bool checkAttachment = false;

    if (isEmployee) {
      checkAttachment =
          await (_attachmentDB.isAdminAttachmentEmpty(participant.iD!))
              ? true
              : (participant.nationalityID!.isNotEmpty ||
                  participant.securityNumberID!.isNotEmpty);
    } else {
      checkAttachment = await (_attachmentDB
              .isParticipantAttachmentEmpty(participant.trioID!))
          ? true
          : (participant.nationalityID!.isNotEmpty ||
              participant.securityNumberID!.isNotEmpty);
    }
    if ( //Name
        participant.firstNameEn!.isNotEmpty &&
            participant.firstNameAr!.isNotEmpty &&
            //General
            (participant.securityNumberID!.isNotEmpty ^
                participant.nationalID!.isNotEmpty) &&
            // checkAttachment &&
            isValidMobile(participant.mobile ?? '') &&
            isValidEmail(participant.email ?? '')) {
      if (participant.code!.isNotEmpty) {
        progressDialogue(context);

        var list = userType == UserType.Employee.name
            ? await _attachmentDB.getAdminParticipantAttachment(
                participant.iD.toString()) //localID
            : await _attachmentDB
                .getParticipantAttachment(participant.trioID.toString());

        if (list.isNotEmpty) {
          await checkData(onError, onDone, context,
              clearFlag: clearFlag,
              resetFlag: resetFlag,
              backFlag: backFlag,
              submitLocally: submitLocally);
        } else {
          await GetParticipantRepository()
              .participantAttachmentCheck(participant, onError, (value) async {
            await checkData(onError, onDone, context,
                clearFlag: clearFlag,
                resetFlag: resetFlag,
                backFlag: backFlag,
                submitLocally: submitLocally);
          });
        }
      } else {
        var list = userType == UserType.Employee.name
            ? await _attachmentDB.getAdminParticipantAttachment(
                participant.iD.toString()) //localID
            : await _attachmentDB
                .getParticipantAttachment(participant.trioID.toString());

        if (list.isNotEmpty) {
          progressDialogue(context);

          await checkData(onError, onDone, context,
              clearFlag: clearFlag,
              resetFlag: resetFlag,
              backFlag: backFlag,
              submitLocally: submitLocally);
        } else {
          onError(AppLocalizations.of(context)!.attachmentValidation);
          ErrorHandler.onErrorSnackBar('Check data', context);
          notifyListeners();
        }
      }
    } else {
      errorFlag = true;
      ErrorHandler.onErrorSnackBar('Check data', context);
      notifyListeners();
    }
  }

  removeExperience(int index) {
    participant.experienceList.removeAt(index);
    notifyListeners();
  }

  removeFamilyInfo(int index) {
    participant.familyInfoList.removeAt(index);

    notifyListeners();
  }

  percentageOfCompletion() {
    participant.checkPercentageOfCompletion();
    notifyListeners();
  }

  submitAttachment(Participant participant) async {
    var list = userType == UserType.Employee.name
        ? await _attachmentDB
            .getAdminParticipantAttachment(participant.iD.toString()) //localID
        : await _attachmentDB
            .getParticipantAttachment(participant.trioID.toString());

    if (list.isNotEmpty) {
      for (var attachment in list) {
        Uint8List imageInUnit8List = attachment.image;
        // store unit8List image here ;
        final tempDir = await getTemporaryDirectory();
        File file = await File('${tempDir.path}/${attachment.id}.png').create();
        file.writeAsBytesSync(imageInUnit8List);
        GetParticipantRepository().participantUploadFile(
            participant, file, (String value) {}, (dynamic) async {
          if (userType == UserType.Employee.name) {
            await _attachmentDB.deleteAdminAttachment(participant.iD!);
          } else {
            await _attachmentDB.deleteAdminAttachment(participant.trioID!);
          }
        });
      }
    }
  }

  checkData(void Function(String) onError, void Function() onDone, context,
      {bool clearFlag = false,
      resetFlag = false,
      backFlag = false,
      bool submitLocally = false}) async {
    await CheckConnection().checkConnectivity2(() async {
      await GetParticipantRepository()
          .submitParticipantToTrio(participant, onError, () async {
        await submitAttachment(participant);

        if (resetFlag) {
          await participantInit();
        }

        ErrorHandler.onSuccessSnackBar('Done', context);

        if (userType != UserType.Participant.name) {
          await participantInit();
        }
        notifyListeners();

        if (backFlag) {
          Navigation.back(context);
        }
        Navigation.back(context);
      });

      if (participant.trioID != null && participant.trioID! > 0) {
        _db.deleteParticipant(participant.iD ?? 0);
      }
    }, () async {
      if (submitLocally && participant.trioID == null) {
        await submitToSql();
      }
      if (resetFlag) {
        await participantInit();
      }
      ErrorHandler.onSuccessSnackBar('Done', context);

      if (userType != UserType.Participant.name) {
        await participantInit();
      }
      notifyListeners();

      if (backFlag) {
        Navigation.back(context);
      }
      Navigation.back(context);
    });

    onDone();
  }
}
