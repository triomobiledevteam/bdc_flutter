import 'dart:convert';

import 'package:bdc/core/network/error_massage.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';

class AppException implements Exception {
  final String message;
  final dynamic prefix;

  AppException({required this.message, this.prefix});

  @override
  String toString() {
    return "$prefix$message";
  }
}

class FetchDataException extends AppException {
  FetchDataException([message])
      : super(message: message, prefix: "Error During Communication: ");
}

class BadRequestException extends AppException {
  BadRequestException([message])
      : super(message: message, prefix: "Invalid Request: ");
}

class UnauthorisedException extends AppException {
  UnauthorisedException([message])
      : super(message: message!, prefix: "Unauthorised: ");
}

class InvalidInputException extends AppException {
  InvalidInputException([message])
      : super(message: message, prefix: "Invalid Input: ");
}

dynamic returnResponse(Response response, void Function(String) onError,
    void Function(dynamic) onDone) {
  switch (response.statusCode) {
    case 200:
      var responseJson = json.decode(response.body);
      debugPrint(response.body);
      return onDone(responseJson);

    case 400:
      print(response.body);
      ErrorMassage errorMessage =
          ErrorMassage.fromJson(json.decode(response.body));
      return onError(BadRequestException(errorMessage.message).toString());
    case 401:
    case 403:
      return onError(
          UnauthorisedException(response.body.toString()).toString());
    case 500:
    default:
      return onError(FetchDataException(
              'Error occurred while Communication with Server with StatusCode : ${response.statusCode}')
          .toString());
  }
}

dynamic returnStreamedResponse(StreamedResponse response,
    void Function(String) onError, void Function(dynamic) onDone) async {
  final respStr = await response.stream.bytesToString();
  switch (response.statusCode) {
    case 200:
      var responseJson = json.decode(respStr);
      debugPrint(respStr);
      return onDone(responseJson);

    case 400:
      print(respStr);
      ErrorMassage errorMessage = ErrorMassage.fromJson(json.decode(respStr));
      return onError(BadRequestException(errorMessage.message).toString());
    case 401:
    case 403:
      return onError(UnauthorisedException(respStr).toString());
    case 500:
    default:
      return onError(FetchDataException(
              'Error occurred while Communication with Server with StatusCode : ${response.statusCode}')
          .toString());
  }
}
