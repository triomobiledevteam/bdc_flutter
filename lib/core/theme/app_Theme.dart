import 'package:bdc/core/theme/sizeConfig.dart';
import 'package:flutter/material.dart';

Color mainColorTheme = const Color(0XFF1B5681);

Color? mainBackgroundColor = Colors.grey[200];

Color? stickyHeaderColor = Colors.blueGrey[700];

Color kPrimaryColor = const Color(0XFF1B5681);
Color kPrimaryColorGreen = const Color(0XFF8CC640);
Color kBackgroundColor = const Color(0XFFE5E5E5);
const kSpacingUnit = 10;

Color whiteColor = Colors.white;
Color blackColor = Colors.black;

TextStyle mainTextStyle = TextStyle(
    fontSize: SizeConfig.blockSizeVertical! * 2, color: mainColorTheme);
TextStyle midTextStyle =
    TextStyle(fontSize: SizeConfig.blockSizeVertical! * 3, color: blackColor);
TextStyle largeTextStyle =
    TextStyle(fontSize: SizeConfig.blockSizeVertical! * 3.5, color: blackColor);

TextStyle basicTextStyle =
    TextStyle(fontSize: SizeConfig.blockSizeVertical! * 2, color: blackColor);

TextStyle whiteTextStyle =
    TextStyle(fontSize: SizeConfig.blockSizeVertical! * 2, color: Colors.white);

TextStyle greenSmallTextStyle = TextStyle(
    fontSize: SizeConfig.blockSizeVertical! * 1.5, color: kPrimaryColorGreen);

Widget textLogo = Text(
  'BDC',
  style: TextStyle(
      fontSize: SizeConfig.blockSizeVertical! * 10,
      color: whiteColor,
      fontWeight: FontWeight.w900,
      fontStyle: FontStyle.italic,
      fontFamily: 'Poppins'),
);

Widget midTextLogo = Text(
  'BDC',
  style: TextStyle(
      fontSize: SizeConfig.blockSizeVertical! * 6,
      color: whiteColor,
      fontWeight: FontWeight.w800,
      fontStyle: FontStyle.italic,
      fontFamily: 'Cantarell'),
);

const List<Color> signInGradients = [
  Color(0xFF0EDED2),
  Color(0xFF03A0FE),
];

const List<Color> signUpGradients = [
  Color(0xFFFF9945),
  Color(0xFFFc6076),
];

const List<Color> trioColorsGradients = [
  Color(0xFF0077cc),
  Color(0xFF5eba7d),
  Color(0xFFFc6076),
];

const List<Color> trioBlueColorGradients = [
  Color(0xFF5AEAF1),
  Color(0xFF0077cc),
];
