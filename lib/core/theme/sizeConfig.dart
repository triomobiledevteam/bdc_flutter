import 'package:flutter/widgets.dart';

class SizeConfig {
  static late MediaQueryData _mediaQueryData;
  static Orientation? orientation;
  static double? width;
  static double? height;
  static double? inButtonTxtPortrait;
  static double? inButtonTxtLandscape;
  static double? inTxtFieldPortrait;
  static double? inTxtFieldLandscape;
  static double? fullPadding;
  static double? sizeBoxH;
  static double? sizeBoxW;
  static double? mainScreenSmallTxt;
  static double? mainScreenVerySmallTxt;
  static double? mainScreenLargTxt;
  static double? containerHalfSize;
  static double? horizontalP;
  static double? verticalP;
  static late double blockSizeHorizontal;
  static double? blockSizeVertical;
  static late double _safeAreaHorizontal;
  static late double _safeAreaVertical;
  static double? safeBlockHorizontal;
  static double? safeBlockVertical;

  void init(BuildContext context) {
    _mediaQueryData = MediaQuery.of(context);
    orientation = _mediaQueryData.orientation;
    width = _mediaQueryData.size.width;
    height = _mediaQueryData.size.height;
    horizontalP = width! * 0.05;
    verticalP = height! * 0.1;
    blockSizeHorizontal = width! / 100;
    blockSizeVertical = height! / 100;
    mainScreenSmallTxt = (blockSizeVertical! * 4);
    mainScreenLargTxt = (blockSizeVertical! * 8);
    mainScreenVerySmallTxt = (blockSizeVertical! * 2);
    inTxtFieldPortrait = width! * 0.04;
    inTxtFieldLandscape = width! * 0.045;
    containerHalfSize = width! / 2 - width! * 0.15;
    _safeAreaHorizontal =
        _mediaQueryData.padding.left + _mediaQueryData.padding.right;
    _safeAreaVertical =
        _mediaQueryData.padding.top + _mediaQueryData.padding.bottom;
    safeBlockHorizontal = (width! - _safeAreaHorizontal) / 100;
    safeBlockVertical = (height! - _safeAreaVertical) / 100;
  }
}
