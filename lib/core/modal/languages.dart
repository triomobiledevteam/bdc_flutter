// ignore_for_file: constant_identifier_names

import 'dart:ui';

import 'package:bdc/core/storage/cache_helper.dart';

import 'constraints/global_values.dart';

const String ENGLISH = 'en';
const String ARABIC = "ar";

Future<Locale> saveLocale(int id) async {
  await CacheHelper.saveData(key: LANGUAGE_ID, value: id);

  languageID = id;
  return _locale(id);
}

Future<Locale> getLocale() async {
  languageID = CacheHelper.getData(key: LANGUAGE_ID) ?? 1;
  return _locale(languageID);
}

Locale _locale(int languageID) {
  Locale temp;
  switch (languageID) {
    case 1:
      temp = const Locale(ENGLISH, 'US');
      break;
    case 2:
      temp = const Locale(ARABIC, 'AE');
      break;
    default:
      temp = const Locale(ENGLISH, 'US');
  }
  print('returned temp $temp');
  return temp;
}
