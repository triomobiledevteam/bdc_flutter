import 'package:bdc/core/modal/constraints/global_values.dart';

import 'constraints/LookUpTypes.dart';

class LookUps {
  String? lookupType;
  String? iD;
  String? descriptionEn;
  String? descriptionAr;
  String? parentID;
  bool? isSelected = false;

  LookUps(
      {this.lookupType,
      this.iD,
      this.descriptionEn = '',
      this.descriptionAr = '',
      this.parentID});

  LookUps.fromJson(Map<String, dynamic> json) {
    lookupType = json['LookupType'];
    iD = json['ID'];
    descriptionEn = json['DescriptionEn'] ?? '';
    descriptionAr = json['DescriptionAr'] ?? '';
    parentID = json['ParentID'] ?? '';
  }

  LookUps.fromLocalStorage(Map<String, dynamic> json) {
    iD = json['ID'];
    descriptionEn = json['DescriptionEn'] ?? '';
    descriptionAr = json['DescriptionAr'] ?? '';
    parentID = json['ParentID'] ?? '';
  }

  Map<String, dynamic> toLocalStorage() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['ID'] = iD;
    data['DescriptionEn'] = descriptionEn ?? '';
    data['DescriptionAr'] = descriptionAr ?? '';
    data['ParentID'] = parentID ?? '';
    return data;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['LookupType'] = lookupType;
    data['ID'] = iD;
    data['DescriptionEn'] = descriptionEn ?? "";
    data['DescriptionAr'] = descriptionAr ?? '';
    data['ParentID'] = parentID ?? '';
    return data;
  }

  fetchToLocalDB(List<LookUps> lookUpList) async {
    List<LookUps> genderList = [];
    List<LookUps> maritalList = [];
    List<LookUps> eduLevelList = [];
    List<LookUps> nationalityList = [];
    List<LookUps> sectorList = [];
    List<LookUps> industryList = [];
    List<LookUps> countryList = [];
    List<LookUps> cityList = [];
    List<LookUps> religionList = [];
    List<LookUps> majorList = [];
    List<LookUps> jobRoleList = [];
    List<LookUps> recruitmentList = [];
    List<LookUps> contractTypeList = [];
    List<LookUps> skillsLevelList = [];
    List<LookUps> experienceYearsList = [];
    List<LookUps> lastUsedList = [];

    print('fetchToLocalDB.length ${lookUpList.length}');

    //City
    cityList = lookUpList
        .where((element) => element.lookupType == LookUpTypes.city)
        .toList();

    for (var element in cityList) {
      await databaseClient.cityDB(element);
    }

    //Country
    countryList = lookUpList
        .where((element) => element.lookupType == LookUpTypes.country)
        .toList();

    for (var element in countryList) {
      await databaseClient.countryDB(element);
    }

    //Nationality
    nationalityList = lookUpList
        .where((element) => element.lookupType == LookUpTypes.nationality)
        .toList();

    for (var element in nationalityList) {
      await databaseClient.nationalityDB(element);
    }

    //Gender
    genderList = lookUpList
        .where((element) => element.lookupType == LookUpTypes.gender)
        .toList();
    for (var element in genderList) {
      await databaseClient.genderDB(element);
    }

    //Marital
    maritalList = lookUpList
        .where((element) => element.lookupType == LookUpTypes.marital)
        .toList();
    for (var element in maritalList) {
      await databaseClient.maritalDB(element);
    }

    //EduLevel
    eduLevelList = lookUpList
        .where((element) => element.lookupType == LookUpTypes.eduLevel)
        .toList();

    for (var element in eduLevelList) {
      await databaseClient.eduLevelDB(element);
    }

    //Sector
    sectorList = lookUpList
        .where((element) => element.lookupType == LookUpTypes.sector)
        .toList();

    for (var element in sectorList) {
      await databaseClient.sectorDB(element);
    }

    //Industry
    industryList = lookUpList
        .where((element) => element.lookupType == LookUpTypes.industry)
        .toList();

    for (var element in industryList) {
      await databaseClient.industryDB(element);
    }

    //Religion
    religionList = lookUpList
        .where((element) => element.lookupType == LookUpTypes.religion)
        .toList();
    for (var element in religionList) {
      await databaseClient.religionDB(element);
    }

    //Major
    majorList = lookUpList
        .where((element) => element.lookupType == LookUpTypes.major)
        .toList();
    for (var element in majorList) {
      await databaseClient.majorDB(element);
    }

    //JobRole
    jobRoleList = lookUpList
        .where((element) => element.lookupType == LookUpTypes.jobRole)
        .toList();
    for (var element in jobRoleList) {
      await databaseClient.jobRoleDB(element);
    }

    //Recruitment
    recruitmentList = lookUpList
        .where((element) => element.lookupType == LookUpTypes.recruitment)
        .toList();
    for (var element in recruitmentList) {
      await databaseClient.recruitmentDB(element);
    }

    //ContractType
    contractTypeList = lookUpList
        .where((element) => element.lookupType == LookUpTypes.contractTypes)
        .toList();
    for (var element in contractTypeList) {
      await databaseClient.contractTypeDB(element);
    }

    //SkillsLevel
    skillsLevelList = lookUpList
        .where((element) => element.lookupType == LookUpTypes.skillsLevel)
        .toList();
    for (var element in skillsLevelList) {
      await databaseClient.skillsLevelDB(element);
    }

    //ExperienceYears
    experienceYearsList = lookUpList
        .where((element) => element.lookupType == LookUpTypes.experienceYears)
        .toList();
    for (var element in experienceYearsList) {
      await databaseClient.experienceYearsDB(element);
    }

    //LastUsed
    lastUsedList = lookUpList
        .where((element) => element.lookupType == LookUpTypes.lastUsed)
        .toList();
    for (var element in lastUsedList) {
      await databaseClient.lastUsedDB(element);
    }
  }
}
