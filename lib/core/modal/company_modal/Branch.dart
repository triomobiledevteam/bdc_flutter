import 'package:bdc/core/modal/constraints/global_values.dart';

List<Branch> branchList = [];

class Branch {
  int? iD;
  int? trioID;
  String? branchesName;
  String? branchAddress;
  String? branchCountry;
  int? branchCity;
  int? institutionID;
  int? localInstitutionID;

  Branch(
      {this.iD,
      this.trioID,
      this.branchesName = '',
      this.branchAddress = '',
      this.branchCountry = '',
      this.branchCity,
      this.institutionID,
      this.localInstitutionID});

  Branch.fromJson(Map<String, dynamic> json) {
    iD = json['LocalID'];
    trioID = json['ID'];
    branchesName = json['BranchesName'] ?? '';
    branchAddress = json['BranchAddress'] ?? '';
    branchCountry = json['BranchCountry'] ?? '';
    branchCity = json['BranchCity'];
    institutionID = json['InstitutionID'];
    localInstitutionID = json['LocalInstitutionID'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['LocalID'] = iD;
    data['ID'] = trioID;
    data['BranchesName'] = branchesName;
    data['BranchAddress'] = branchAddress;
    data['BranchCountry'] = branchCountry;
    data['BranchCity'] = branchCity;
    data['InstitutionID'] = institutionID;
    data['LocalInstitutionID'] = localInstitutionID;
    return data;
  }

  Map<String, dynamic> toTrio() {
    final Map<String, dynamic> data = {
      'ID': "${trioID ?? 0}",
      'CompanyID': "$companyID",
      'BranchesName': branchesName ?? '',
      'CityID': "${branchCity ?? 0}",
      'CountryID': branchCountry ?? '',
      'BranchAddress': branchAddress ?? '',
      'InstitutionID': "${institutionID ?? 0}",
    };
    return data;
  }
}
