import 'package:bdc/core/modal/constraints/global_values.dart';

List<Contact> contactList = [];

class Contact {
  int? iD;
  int? trioID;
  String? name;
  int? roleID;
  String? mobile;
  String? email;
  int? institutionID;
  int? localInstitutionID;

  Contact(
      {this.iD,
      this.trioID,
      this.name = '',
      this.roleID,
      this.mobile = '',
      this.email = '',
      this.institutionID,
      this.localInstitutionID});

  Contact.fromJson(Map<String, dynamic> json) {
    iD = json['LocalID'];
    trioID = json['ID'];
    name = json['Name'];
    roleID = json['RoleID'];
    mobile = json['Mobile'];
    email = json['Email'];
    institutionID = json['InstitutionID'];
    localInstitutionID = json['LocalInstitutionID'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['LocalID'] = iD;
    data['ID'] = trioID;
    data['Name'] = name;
    data['RoleID'] = roleID;
    data['Mobile'] = mobile;
    data['Email'] = email;
    data['InstitutionID'] = institutionID;
    data['LocalInstitutionID'] = localInstitutionID;
    return data;
  }

  Map<String, dynamic> toTrio() {
    final Map<String, dynamic> data = {
      'ID': "${trioID ?? 0}",
      'CompanyID': "$companyID",
      'Name': name ?? '',
      'Mobile': mobile ?? '',
      'RoleID': "${roleID ?? 0}",
      'Email': email ?? '',
      'InstitutionID': "${institutionID ?? 0}",
    };
    return data;
  }
}
