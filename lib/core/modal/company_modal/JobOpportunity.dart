import 'package:bdc/core/modal/constraints/global_values.dart';

List<JobOpportunities> jobOpportunitiesList = [];

class JobOpportunities {
  int? iD;
  int? trioID;
  int? institutionID;
  int? localInstitutionID;
  int? jobRoleID;
  String? exactProfessionDescription;
  int? totalTrainerRecruitment;
  int? genderID;
  String? nationalityID;
  double? workingHours;
  bool? socialSecurity;
  double? trainingDuration;
  int? number;
  bool? insurance;
  double? salaryRangeStart;
  double? salaryRangeEnd;
  int? remainingParticipants;

  JobOpportunities(
      {this.iD,
      this.trioID,
      this.institutionID,
      this.localInstitutionID,
      this.jobRoleID,
      this.exactProfessionDescription = '',
      this.totalTrainerRecruitment,
      this.genderID,
      this.nationalityID = '',
      this.workingHours,
      this.socialSecurity = false,
      this.trainingDuration,
      this.number,
      this.insurance = false,
      this.salaryRangeStart,
      this.salaryRangeEnd,
      this.remainingParticipants});

  JobOpportunities.fromJson(Map<String, dynamic> json) {
    iD = json['LocalID'];
    trioID = json['ID'];
    institutionID = json['InstitutionID'];
    localInstitutionID = json['LocalInstitutionID'];
    jobRoleID = json['JobRoleID'];
    exactProfessionDescription = json['ExactProfessionDescription'];
    totalTrainerRecruitment = json['TotalTraineerRecruitment'];
    genderID = json['GenderID'];
    nationalityID = json['NationalityID'];
    workingHours = json['WorkingHours'];
    socialSecurity = json['SocialSecurity'] is bool
        ? json['SocialSecurity']
        : json['SocialSecurity'] == 0
            ? false
            : true;
    trainingDuration = json['TrainingDuration'];
    number = json['Number'];
    insurance = json['Insurance'] is bool
        ? json['Insurance']
        : json['Insurance'] == 0
            ? false
            : true;
    salaryRangeStart = json['SalaryRangeStart'];
    salaryRangeEnd = json['SalaryRangeEnd'];
    remainingParticipants = json['RemainingParticipants'];
  }

  Map<String, dynamic> toTrio() {
    final Map<String, dynamic> data = {
      'ID': "${trioID ?? 0}",
      'CompanyID': "$companyID",
      'InstitutionID': "$institutionID",
      'JobRoleID': "${jobRoleID ?? 0}",
      'ExactProfessionDescription': "$exactProfessionDescription",
      'TotalTraineerRecruitment': "${totalTrainerRecruitment ?? 0}",
      'GenderID': "${genderID ?? 0}",
      'NationalityID': "$nationalityID",
      'WorkingHours': "${workingHours ?? 0}",
      'SocialSecurity': "${socialSecurity ?? 0}",
      'TrainingDuration': "${trainingDuration ?? 0}",
      'Number': "${number ?? 0}",
      'Insurance': "${insurance ?? false}",
      'SalaryRangeStart': "${salaryRangeStart ?? 0}",
      'SalaryRangeEnd': "${salaryRangeEnd ?? 0}",
      'RemainingParticipants': "${remainingParticipants ?? 0}",
    };
    return data;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['LocalID'] = iD;
    data['ID'] = trioID;
    data['InstitutionID'] = institutionID;
    data['LocalInstitutionID'] = localInstitutionID;
    data['JobRoleID'] = jobRoleID;
    data['ExactProfessionDescription'] = exactProfessionDescription;
    data['TotalTraineerRecruitment'] = totalTrainerRecruitment;
    data['GenderID'] = genderID;
    data['NationalityID'] = nationalityID;
    data['WorkingHours'] = workingHours;
    data['SocialSecurity'] = socialSecurity;
    data['TrainingDuration'] = trainingDuration;
    data['Number'] = number;
    data['Insurance'] = insurance;
    data['SalaryRangeStart'] = salaryRangeStart;
    data['SalaryRangeEnd'] = salaryRangeEnd;
    data['RemainingParticipants'] = remainingParticipants;
    return data;
  }
}
