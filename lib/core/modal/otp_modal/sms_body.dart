class SmsBody {
  late String code;
  late String massage;
  late String mobile;

  SmsBody({required this.code, required this.massage, required this.mobile});

  SmsBody.fromJson(Map<String, dynamic> json) {
    code = json['SMSCode'];
    massage = json['SMSBody'];
    mobile = json['Mobile'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['SMSCode'] = code;
    data['SMSBody'] = massage;
    data['Mobile'] = mobile;

    return data;
  }
}
