import 'package:bdc/core/modal/AplicationForm.dart';
import 'package:bdc/core/modal/company_modal/Contact.dart';
import 'package:bdc/core/modal/constraints/global_values.dart';
import 'package:bdc/core/modal/enums/UsersType.dart';
import 'package:intl/intl.dart';

import '../applicationFormTrans.dart';
import '../company_modal/Branch.dart';
import '../company_modal/JobOpportunity.dart';

List<Company> mainCompanyList = [];
Company? mainCompany;

class Company {
  int? iD;
  int? trioID;
  String? code;
  String? transactionDate;
  String? institutionNameAr;
  String? institutionNameEn;
  String? institutionID;
  int? sectorTypeID;
  String? institutionAddress;
  String? countryID;
  int? cityID;
  String? institutionEmail;
  String? institutionFax;
  String? institutionPhone;
  double? institutionEmployeeNumber;
  double? institutionEmployeeNumberPartTime;
  double? womanWorkingNumber;
  double? employeeSpecialNeeds;
  int? recruitTypesInCompany;
  int? contractTypesInCompany;
  String? location;
  int? projectID;
  int? createdUser;
  String? createdDate;
  String? note;
  int percentageOfCompletion = 0;

  List<Branch> branchList = [];
  List<JobOpportunities> jobOpportunityList = [];
  List<Contact> contactList = [];
  List<ApplicationForm> applicationFormList = [];
  List<ApplicationFormTrans> applicationFormTransList = [];

  Company({
    this.iD,
    this.trioID,
    this.code = '',
    this.transactionDate = '',
    this.institutionNameAr = '',
    this.institutionNameEn = '',
    this.institutionID = '',
    this.sectorTypeID,
    this.institutionAddress = '',
    this.countryID = '',
    this.cityID,
    this.institutionEmail = '',
    this.institutionFax = '',
    this.institutionPhone = '',
    this.institutionEmployeeNumber,
    this.institutionEmployeeNumberPartTime,
    this.womanWorkingNumber,
    this.employeeSpecialNeeds,
    this.recruitTypesInCompany,
    this.contractTypesInCompany,
    this.location = '',
    this.projectID,
    this.createdUser,
    this.createdDate = '',
    this.note = '',
  });

  Company.fromJson(Map<String, dynamic> json) {
    iD = json['LocalID'];
    trioID = json['ID'];
    code = json['Code'] ?? '';
    transactionDate = json['TransactionDate'] ?? '';
    institutionNameAr = json['InstitutionNameAr'] ?? '';
    institutionNameEn = json['InstitutionNameEn'] ?? '';
    institutionID = json['InstitutionID'] ?? '';
    sectorTypeID = json['SectorTypeID'];
    institutionAddress = json['InstitutionAddress'] ?? '';
    countryID = json['CountryID'] ?? '';
    cityID = json['CityID'];
    institutionEmail = json['InstitutionEmail'] ?? '';
    institutionFax = json['InstitutionFax'] ?? '';
    institutionPhone = json['InstitutionPhone'] ?? '';
    institutionEmployeeNumber = json['InstitutionEmployeeNumber'];
    institutionEmployeeNumberPartTime =
        json['InstitutionEmployeeNumberPartTime'];
    womanWorkingNumber = json['WomanWorkingNumber'];
    employeeSpecialNeeds = json['EmployeeSpecialNeeds'];
    recruitTypesInCompany = json['RecruitTypesInCompany'];
    contractTypesInCompany = json['ContractTypesInComapny'];
    location = json['Location'] ?? '';
    projectID = json['ProjectID'];
    createdUser = json['CreatedUser'];
    createdDate = json['CreatedDate'] ?? '';
    note = json['Note'] ?? '';
    percentageOfCompletion = json['PercentageOfCompletion'] ?? 0;

    branchList = <Branch>[];
    if (json['Branch'] != null) {
      json['Branch'].forEach((v) {
        branchList.add(Branch.fromJson(v));
      });
    }
    jobOpportunityList = <JobOpportunities>[];
    if (json['JobOpportunities'] != null) {
      json['JobOpportunities'].forEach((v) {
        jobOpportunityList.add(JobOpportunities.fromJson(v));
      });
    }
    contactList = <Contact>[];
    if (json['Contact'] != null) {
      json['Contact'].forEach((v) {
        contactList.add(Contact.fromJson(v));
      });
    }

    checkPercentageOfCompletion();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['LocalID'] = iD;
    data['ID'] = trioID;
    data['Code'] = code ?? '';
    data['TransactionDate'] = transactionDate ?? '';
    data['InstitutionNameAr'] = institutionNameAr ?? '';
    data['InstitutionNameEn'] = institutionNameEn ?? '';
    data['InstitutionID'] = institutionID ?? '';
    data['SectorTypeID'] = sectorTypeID;
    data['InstitutionAddress'] = institutionAddress ?? '';
    data['CountryID'] = countryID;
    data['CityID'] = cityID;
    data['InstitutionEmail'] = institutionEmail ?? "";
    data['InstitutionFax'] = institutionFax ?? '';
    data['InstitutionPhone'] = institutionPhone ?? '';
    data['InstitutionEmployeeNumber'] = institutionEmployeeNumber;
    data['InstitutionEmployeeNumberPartTime'] =
        institutionEmployeeNumberPartTime;
    data['WomanWorkingNumber'] = womanWorkingNumber;
    data['EmployeeSpecialNeeds'] = employeeSpecialNeeds;
    data['RecruitTypesInCompany'] = recruitTypesInCompany;
    data['ContractTypesInComapny'] = contractTypesInCompany;
    data['Location'] = location;
    data['ProjectID'] = projectID;
    data['CreatedUser'] = createdUser;
    data['CreatedDate'] = createdDate;
    data['Note'] = note;
    data['PercentageOfCompletion'] = percentageOfCompletion;
    // toTrio
    return data;
  }

  Map<String, dynamic> toTrio() {
    final Map<String, dynamic> data = {
      'ID': trioID ?? 0,
      'Code': "$code",
      'TransactionDate': transactionDate!.isNotEmpty
          ? DateFormat('yyyy-MM-dd').format(DateTime.parse(transactionDate!))
          : DateFormat('yyyy-MM-dd').format(DateTime.now()),
      'InstitutionNameAr': "$institutionNameAr",
      'InstitutionNameEn': "$institutionNameEn",
      'InstitutionID': "$institutionID",
      'SectorTypeID': sectorTypeID ?? 0,
      'InstitutionAddress': "$institutionAddress",
      'CountryID': "$countryID",
      'CityID': cityID ?? 0,
      'InstitutionEmail': "$institutionEmail",
      'InstitutionFax': "$institutionFax",
      'InstitutionPhone': "$institutionPhone",
      'InstitutionEmployeeNumber': institutionEmployeeNumber ?? 0,
      'InstitutionEmployeeNumberPartTime':
          institutionEmployeeNumberPartTime ?? 0,
      'WomanWorkingNumber': womanWorkingNumber ?? 0,
      'EmployeeSpecialNeeds': employeeSpecialNeeds ?? 0,
      'RecruitTypesInCompany': recruitTypesInCompany ?? 0,
      'ContractTypesInComapny': contractTypesInCompany ?? 0,
      'Location': "$location",
      'CreatedUser': userID,
      'Note': "$note",
    };

    return data;
  }

  checkPercentageOfCompletion() {
    percentageOfCompletion = 0;

    if (institutionNameAr!.isNotEmpty) {
      percentageOfCompletion += 10;
    }
    if (institutionNameEn!.isNotEmpty) {
      percentageOfCompletion += 10;
    }
    if (institutionID!.isNotEmpty) {
      percentageOfCompletion += 10;
    }
    if (UserType.CBO.name != userType) {
      if (sectorTypeID != null) {
        percentageOfCompletion += 10;
      }
    }
    if (countryID!.isNotEmpty) {
      percentageOfCompletion += 10;
    }
    if (cityID != null) {
      percentageOfCompletion += 10;
    }
    if (institutionEmail!.isNotEmpty) {
      percentageOfCompletion += 10;
    }
    if (institutionPhone!.isNotEmpty) {
      percentageOfCompletion += 10;
    }

    if (institutionEmployeeNumber != null) {
      if (UserType.CBO.name != userType) {
        percentageOfCompletion += 5;
      } else {
        percentageOfCompletion += 10;
      }
    }

    if (institutionEmployeeNumberPartTime != null) {
      percentageOfCompletion += 5;
    }

    if (womanWorkingNumber != null) {
      percentageOfCompletion += 5;
    }
    if (employeeSpecialNeeds != null) {
      percentageOfCompletion += 5;
    }
  }
}
