late GenUser mainUser;

class GenUser {
  int? userID;
  int? languageID;
  String? userName;
  String? email;
  String? mobile;
  String? nationality;
  String? image;
  int? companyID;

  GenUser(this.userID, this.languageID, this.userName, this.email, this.mobile,
      this.nationality, this.image, this.companyID);

  GenUser.fromJson(Map<String, dynamic> json) {
    userID = json['UserID'];
    languageID = json['LanguageID'];
    userName = json['UserName'] ?? '';
    companyID = json['CompanyID'];
    email = json['UserEmail'] ?? '';
    mobile = json['UserMobile'] ?? '';
    nationality = json['UserNationalityID'] ?? '';
    image = json['Image'] ?? '';
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['UserID'] = userID;
    data['LanguageID'] = languageID;
    data['UserName'] = userName;
    data['CompanyID'] = companyID;
    data['UserEmail'] = email;
    data['UserMobile'] = mobile;
    data['UserNationalityID'] = nationality;
    data['Image'] = image;
    return data;
  }
}
