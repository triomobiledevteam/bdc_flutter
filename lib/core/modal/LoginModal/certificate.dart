// ignore_for_file: constant_identifier_names

const CACHED_EMAIL = 'CACHED_EMAIL';
const CACHED_PASSWORD = 'CACHED_PASSWORD';
const CACHED_ACCOUNT_TYPE = 'CACHED_ACCOUNT_TYPE';

class Certificate {
  final String email;
  final String password;
  final String accountType;

  const Certificate(
      {required this.email, required this.password, required this.accountType});

  factory Certificate.fromMap(Map<String, dynamic> map) {
    return Certificate(
      password: map[CACHED_PASSWORD],
      email: map[CACHED_EMAIL],
      accountType: map[CACHED_ACCOUNT_TYPE],
    );
  }

  Map<String, dynamic> toMap() {
    return {
      CACHED_EMAIL: email,
      CACHED_PASSWORD: password,
      CACHED_ACCOUNT_TYPE: accountType,
    };
  }
}
