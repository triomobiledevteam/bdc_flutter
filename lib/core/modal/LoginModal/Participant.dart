import 'package:bdc/core/modal/AplicationForm.dart';
import 'package:bdc/core/modal/constraints/global_values.dart';
import 'package:bdc/core/modal/participant_modal/ParticipantExperience.dart';
import 'package:bdc/core/modal/participant_modal/ParticipantFamilyInfo.dart';
import 'package:bdc/core/modal/program/project.dart';
import 'package:intl/intl.dart';

import '../applicationFormTrans.dart';

List<Participant> mainParticipantList = [];

Participant? mainParticipant;

class Participant {
  int? iD;
  int? trioID;
  List<ParticipantExperience> experienceList = [];
  List<ParticipantFamilyInfo> familyInfoList = [];
  List<ApplicationForm> applicationFormList = [];
  List<ApplicationFormTrans> applicationFormTransList = [];
  List<Project> projectList = [];
  String? code;
  String? fullNameEn;
  String? fullNameAr;
  String? nationalityID;
  String? nationalID;
  String? securityNumberID;
  int? maritalStatusID;
  int? religionID;
  int? genderID;
  String? countryID;
  int? cityID;
  String? birthDate;
  String? address;
  String? alternateMobile;
  String? mobile;
  int? familyWorkMember;
  int? maritalParentStatuesID;
  String? email;
  int? professionalSector;
  int? educationalLevel;
  String? universityName;
  int? majorID;
  String? participantDate;
  int? familyNumber;
  String? participantStatusID;
  String? firstNameEn;
  String? secondNameEn;
  String? thirdNameEn;
  String? familyNameEn;
  String? firstNameAr;
  String? thirdNameAr;
  String? secondNameAr;
  String? familyNameAr;
  String? graduationDate;
  String? location;
  String? facebookName;
  bool? disability;
  String? createdDate;
  int? createdUser;
  String? note;
  int percentageOfCompletion = 0;

  Participant(
      {this.iD,
      this.trioID,
      this.code = '',
      this.fullNameEn = '',
      this.fullNameAr = '',
      this.nationalityID = '',
      this.nationalID = '',
      this.securityNumberID = '',
      this.maritalStatusID,
      this.religionID,
      this.genderID,
      this.countryID = '',
      this.cityID,
      this.birthDate = '',
      this.address = '',
      this.alternateMobile = '',
      this.mobile = '',
      this.familyWorkMember,
      this.maritalParentStatuesID,
      this.email = '',
      this.professionalSector,
      this.educationalLevel,
      this.universityName = '',
      this.majorID,
      this.participantDate = '',
      this.familyNumber,
      this.participantStatusID = '',
      this.firstNameEn = '',
      this.secondNameEn = '',
      this.thirdNameEn = '',
      this.familyNameEn = '',
      this.firstNameAr = '',
      this.thirdNameAr = '',
      this.secondNameAr = '',
      this.familyNameAr = '',
      this.graduationDate = '',
      this.location = '',
      this.facebookName = '',
      this.disability = false,
      this.createdDate,
      this.createdUser,
      this.note = '',
      this.percentageOfCompletion = 0});

  Participant.fromJson(Map<String, dynamic> json) {
    iD = json['LocalID'];
    trioID = json['ID'];
    code = json['Code'];
    fullNameEn = json['FullNameEn'] ?? '';
    fullNameAr = json['FullNameAr'] ?? '';
    nationalityID = json['NationalityID'] ?? '';
    nationalID = json['NationalID'] ?? '';
    securityNumberID = json['SecurityNumberID'] ?? '';
    maritalStatusID = json['MaritalStatusID'];
    religionID = json['ReligionID'];
    genderID = json['GenderID'];
    countryID = json['CountryID'] ?? '';
    cityID = json['CityID'];
    birthDate = json['BirthDate'] ?? '';
    address = json['Address'] ?? '';
    alternateMobile = json['AlternateMobile'] ?? '';
    mobile = json['Mobile'] ?? '';
    familyWorkMember = json['FamilyWorkMember'];
    maritalParentStatuesID = json['MaritalParentStatuesID'];
    email = json['Email'] ?? '';
    professionalSector = json['ProfessionalSector'];
    educationalLevel = json['EducationalLevel'];
    universityName = json['UniversityName'];
    majorID = json['MajorID'];
    participantDate = json['ParticipantDate'];
    familyNumber = json['FamilyNumber'];
    participantStatusID = json['ParticipantStatusID'] ?? '';
    firstNameEn = json['FirstNameEn'] ?? '';
    secondNameEn = json['SecondNameEn'] ?? '';
    thirdNameEn = json['ThirdNameEn'] ?? '';
    familyNameEn = json['FamilyNameEn'] ?? '';
    firstNameAr = json['FirstNameAr'] ?? '';
    thirdNameAr = json['ThirdNameAr'] ?? '';
    secondNameAr = json['SecondNameAr'] ?? '';
    familyNameAr = json['FamilyNameAr'] ?? '';
    graduationDate = json['GraduationDate'] ?? '';
    location = json['Location'] ?? '';
    facebookName = json['FacebookName'] ?? '';
    disability = json['Disability'] is bool
        ? json['Disability']
        : json['Disability'] == 0
            ? false
            : true;
    createdDate = json['CreatedDate'];
    createdUser = json['CreatedUser'];
    percentageOfCompletion = json['PercentageOfCompletion'] ?? 0;
    checkPercentageOfCompletion();
  }

  Map<String, dynamic> postRequestToJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['CompanyID'] = '$companyID';
    data['Code'] = "$code";
    data['NationalityID'] = "$nationalityID";
    data['NationalID'] = "$nationalID";
    data['SecurityNumberID'] = "$securityNumberID";
    data['MaritalStatusID'] = "${maritalStatusID ?? 0}";
    data['ReligionID'] = "${religionID ?? 0}";
    data['GenderID'] = "${genderID ?? 0}";
    data['CountryID'] = "$countryID";
    data['CityID'] = "$cityID";
    data['BirthDate'] = birthDate!.isNotEmpty
        ? DateFormat('yyyy-MM-dd').format(DateTime.parse(birthDate!))
        : '';
    data['Address'] = "$address";
    data['AlternateMobile'] = "$alternateMobile";
    data['Mobile'] = "$mobile";
    data['FamilyWorkMember'] = "${familyWorkMember ?? 0}";
    data['MaritalParentStatuesID'] = "${maritalParentStatuesID ?? 0}";
    data['Email'] = "$email";
    data['ProfessionalSector'] = "${professionalSector ?? 0}";
    data['EducationalLevel'] = "${educationalLevel ?? 0}";
    data['UniversityName'] = "#{this.universityName}";
    data['MajorID'] = "${majorID ?? 0}";
    data['ParticipantDate'] = participantDate!.isNotEmpty
        ? DateFormat('yyyy-MM-dd').format(DateTime.parse(participantDate!))
        : DateFormat('yyyy-MM-dd').format(DateTime.now());
    data['FamilyNumber'] = "${familyNumber ?? 0}";
    data['ParticipantStatusID'] = participantStatusID ?? '';
    data['FirstNameEn'] = "$firstNameEn";
    data['SecondNameEn'] = "$secondNameEn";
    data['ThirdNameEn'] = "$thirdNameEn";
    data['FamilyNameEn'] = "$familyNameEn";
    data['FirstNameAr'] = "$firstNameAr";
    data['ThirdNameAr'] = "$thirdNameAr";
    data['SecondNameAr'] = "$secondNameAr";
    data['FamilyNameAr'] = "$familyNameAr";
    data['GraduationDate'] = graduationDate!.isNotEmpty
        ? DateFormat('yyyy-MM-dd').format(DateTime.parse(graduationDate!))
        : '';
    data['Location'] = "$location";
    data['FacebookName'] = "$facebookName";
    data['Disability'] = "$disability";
    data['CreatedUser'] = "${createdUser!}";

    // data['PercentageOfCompletion'] = this.percentageOfCompletion;
    print(data);
    return data;
  }

  Map<String, dynamic> assignToJson() {
    final Map<String, dynamic> data = {
      'CompanyID': '$companyID',
      'Code': "$code",
      'NationalityID': '$nationalityID',
      'NationalID': "$nationalID",
      'SecurityNumberID': "$securityNumberID",
      'MaritalStatusID': maritalStatusID,
      'ReligionID': religionID,
      'GenderID': genderID,
      'CountryID': "$countryID",
      'CityID': cityID,
      'BirthDate': birthDate!.isNotEmpty
          ? DateFormat('yyyy-MM-dd').format(DateTime.parse(birthDate!))
          : '',
      'Address': "$address",
      'AlternateMobile': alternateMobile,
      'Mobile': "$mobile",
      'FamilyWorkMember': familyWorkMember,
      'MaritalParentStatuesID': maritalParentStatuesID,
      'Email': "$email",
      'ProfessionalSector': professionalSector,
      'EducationalLevel': educationalLevel,
      'UniversityName': universityName,
      'MajorID': majorID,
      'ParticipantDate': participantDate!.isNotEmpty
          ? DateFormat('yyyy-MM-dd').format(DateTime.parse(participantDate!))
          : DateFormat('yyyy-MM-dd').format(DateTime.now()),
      'FamilyNumber': familyNumber,
      'ParticipantStatusID': participantStatusID,
      'FirstNameEn': "$firstNameEn",
      'SecondNameEn': "$secondNameEn",
      'ThirdNameEn': "$thirdNameEn",
      'FamilyNameEn': "$familyNameEn",
      'FirstNameAr': "$firstNameAr",
      'ThirdNameAr': "$thirdNameAr",
      'SecondNameAr': "$secondNameAr",
      'FamilyNameAr': "$familyNameAr",
      'GraduationDate': graduationDate!.isNotEmpty
          ? DateFormat('yyyy-MM-dd').format(DateTime.parse(graduationDate!))
          : '',
      'Location': "$location",
      'FacebookName': "$facebookName",
      'Disability': disability,
      'CreatedUser': userID,
      'Note': "$note",
    };
    return data;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['LocalID'] = iD;
    data['ID'] = trioID;
    data['Code'] = code;
    data['FullNameEn'] = fullNameEn;
    data['FullNameAr'] = fullNameAr;
    data['NationalityID'] = nationalityID;
    data['NationalID'] = nationalID;
    data['SecurityNumberID'] = securityNumberID;
    data['MaritalStatusID'] = maritalStatusID;
    data['ReligionID'] = religionID;
    data['GenderID'] = genderID;
    data['CountryID'] = countryID;
    data['CityID'] = cityID;
    data['BirthDate'] = birthDate;
    data['Address'] = address;
    data['AlternateMobile'] = alternateMobile;
    data['Mobile'] = mobile;
    data['FamilyWorkMember'] = familyWorkMember;
    data['MaritalParentStatuesID'] = maritalParentStatuesID;
    data['Email'] = email;
    data['ProfessionalSector'] = professionalSector;
    data['EducationalLevel'] = educationalLevel;
    data['UniversityName'] = universityName;
    data['MajorID'] = majorID;
    data['ParticipantDate'] = participantDate;
    data['FamilyNumber'] = familyNumber;
    data['ParticipantStatusID'] = participantStatusID;
    data['FirstNameEn'] = firstNameEn;
    data['SecondNameEn'] = secondNameEn;
    data['ThirdNameEn'] = thirdNameEn;
    data['FamilyNameEn'] = familyNameEn;
    data['FirstNameAr'] = firstNameAr;
    data['ThirdNameAr'] = thirdNameAr;
    data['SecondNameAr'] = secondNameAr;
    data['FamilyNameAr'] = familyNameAr;
    data['GraduationDate'] = graduationDate;
    data['Location'] = location;
    data['FacebookName'] = facebookName;
    data['Disability'] = disability;
    data['CreatedDate'] = createdDate;
    data['CreatedUser'] = createdUser;
    data['PercentageOfCompletion'] = percentageOfCompletion;
    return data;
  }

  checkPercentageOfCompletion() {
    percentageOfCompletion = 0;

    if (firstNameEn!.isNotEmpty) {
      percentageOfCompletion += 5;
    }
    if (firstNameAr!.isNotEmpty) {
      percentageOfCompletion += 5;
    }
    if (secondNameEn!.isNotEmpty) {
      percentageOfCompletion += 5;
    }
    if (secondNameAr!.isNotEmpty) {
      percentageOfCompletion += 5;
    }
    if (thirdNameEn!.isNotEmpty) {
      percentageOfCompletion += 5;
    }
    if (thirdNameAr!.isNotEmpty) {
      percentageOfCompletion += 5;
    }
    if (familyNameEn!.isNotEmpty) {
      percentageOfCompletion += 5;
    }
    if (familyNameAr!.isNotEmpty) {
      percentageOfCompletion += 5;
    }
    if (nationalityID!.isNotEmpty) {
      percentageOfCompletion += 5;
    }

    if (mobile!.isNotEmpty) {
      percentageOfCompletion += 5;
    }
    if (email!.isNotEmpty) {
      percentageOfCompletion += 5;
    }
    if (genderID != null) {
      percentageOfCompletion += 5;
    }
    if (maritalStatusID != null) {
      percentageOfCompletion += 5;
    }

    //General
    if (familyWorkMember != null) {
      percentageOfCompletion += 5;
    }
    if (countryID!.isNotEmpty) {
      percentageOfCompletion += 5;
    }
    if (maritalParentStatuesID != null) {
      percentageOfCompletion += 5;
    }
    if (cityID != null) {
      percentageOfCompletion += 5;
    }
    if (familyNumber != null) {
      percentageOfCompletion += 5;
    }

    //Educational
    if (professionalSector != null) {
      percentageOfCompletion += 5;
    }
    if (educationalLevel != null) {
      percentageOfCompletion += 5;
    }
  }
}
