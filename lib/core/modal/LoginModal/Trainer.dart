import 'package:bdc/core/modal/trainer_modal/trainer_experience.dart';

import '../trainer_modal/trainer_certificate.dart';

Trainer? mainTrainer;

class Trainer {
  int? iD;
  String? code;
  String? transactionDate;
  String? trainerNameAr;
  String? trainerNameEn;
  String? countryID;
  int? cityID;
  int? educationalLevelID;
  int? majorID;
  int? trainingHours;
  int? genderID;
  int? freeHours;
  String? trainerStatusID;
  String? mobilePhone;
  String? email;
  List<TrainerCertificate> certificate = [];
  List<TrainerExperience> experience = [];
  late int percentageOfCompletion;

  Trainer(
      {this.iD,
      this.code,
      this.transactionDate,
      this.trainerNameAr,
      this.trainerNameEn,
      this.countryID,
      this.cityID,
      this.educationalLevelID,
      this.majorID,
      this.trainingHours,
      this.genderID,
      this.freeHours,
      this.trainerStatusID,
      this.mobilePhone,
      this.email,
      this.certificate = const [],
      this.experience = const []});

  Trainer.fromJson(Map<String, dynamic> json) {
    iD = json['ID'];
    code = json['Code'] ?? "";
    transactionDate = json['TransactionDate'] ?? "";
    trainerNameAr = json['TrainerNameAr'] ?? '';
    trainerNameEn = json['TrainerNameEn'] ?? '';
    countryID = json['CountryID'] ?? '';
    cityID = json['CityID'];
    educationalLevelID = json['EducationalLevelID'] ?? 0;
    majorID = json['MajorID'];
    trainingHours = json['TrainingHours'];
    genderID = json['GenderID'];
    freeHours = json['FreeHours'];
    trainerStatusID = json['TrainerStatusID'];
    mobilePhone = json['Mobile'] ?? '';
    email = json['Email'] ?? '';
    certificate = <TrainerCertificate>[];

    if (json['Certificate'] != null) {
      json['Certificate'].forEach((v) {
        certificate.add(TrainerCertificate.fromJson(v));
      });
    }
    experience = <TrainerExperience>[];

    if (json['Experience'] != null) {
      json['Experience'].forEach((v) {
        experience.add(TrainerExperience.fromJson(v));
      });
    }
    checkPercentageOfCompletion();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['ID'] = iD;
    data['Code'] = code;
    data['TransactionDate'] = transactionDate;
    data['TrainerNameAr'] = trainerNameAr;
    data['TrainerNameEn'] = trainerNameEn;
    data['CountryID'] = countryID;
    data['CityID'] = cityID;
    data['EducationalLevelID'] = educationalLevelID;
    data['MajorID'] = majorID;
    data['TrainingHours'] = trainingHours;
    data['GenderID'] = genderID;
    data['FreeHours'] = freeHours;
    data['TrainerStatusID'] = trainerStatusID;
    data['MobilePhone'] = mobilePhone;
    data['Email'] = email;
    if (certificate.isNotEmpty) {
      data['Certificate'] = certificate.map((v) => v.toJson()).toList();
    }
    if (experience.isNotEmpty) {
      data['Experience'] = experience.map((v) => v.toJson()).toList();
    }
    return data;
  }

  checkPercentageOfCompletion() {
    percentageOfCompletion = 0;

    if (trainerNameAr!.isNotEmpty) {
      percentageOfCompletion += 15;
    }
    if (trainerNameEn!.isNotEmpty) {
      percentageOfCompletion += 15;
    }
    if (mobilePhone!.isNotEmpty) {
      percentageOfCompletion += 10;
    }
    if (email!.isNotEmpty) {
      percentageOfCompletion += 10;
    }
    if (countryID!.isNotEmpty) {
      percentageOfCompletion += 10;
    }

    if (cityID != null) {
      percentageOfCompletion += 10;
    }
    if (educationalLevelID != null) {
      percentageOfCompletion += 10;
    }
    if (genderID != null) {
      percentageOfCompletion += 10;
    }

    if (majorID != null) {
      percentageOfCompletion += 10;
    }
  }
}
