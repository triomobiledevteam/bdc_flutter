late Employee mainEmployee;

class Employee {
  late int userID;
  late int companyID;
  String? fullName;
  String? image;
  String? mobile;
  String? email;

  Employee(
      {required this.userID,
      required this.companyID,
      this.fullName,
      this.image,
      this.mobile,
      this.email});

  Employee.fromJson(Map<String, dynamic> json) {
    userID = json['UserID'];
    companyID = json['CompanyID'];
    fullName = json['FullName'] ?? "";
    image = json['Image'] ?? "";
    mobile = json['Mobile'] ?? "";
    email = json['Email'] ?? "";
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['UserID'] = userID;
    data['CompanyID'] = companyID;
    data['FullName'] = fullName;
    data['Image'] = image;
    data['Mobile'] = mobile;
    data['Email'] = email;
    return data;
  }
}
