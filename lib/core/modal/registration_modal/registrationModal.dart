abstract class RegistrationModal {
  late String mobile;
  late String password;
  RegistrationModal(this.mobile, this.password);
  Map<String, dynamic> toJson();
}

class Registration {
  late String userType;
  late RegistrationModal registrationModal;

  Registration({required this.userType, required this.registrationModal});

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['UserType'] = userType;
    data['ERMSUsers'] = registrationModal.toJson();
    return data;
  }
}
