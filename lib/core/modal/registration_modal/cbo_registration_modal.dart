import 'package:bdc/core/modal/registration_modal/registrationModal.dart';

class CBORegistrationModal extends RegistrationModal {
  String? companyName;
  String? email;
  int? city;

  CBORegistrationModal(
      {this.companyName,
      this.email,
      required String mobile,
      required String password,
      this.city})
      : super(mobile, password);

  @override
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['CompanyName'] = companyName;
    data['Email'] = email;
    data['Mobile'] = mobile;
    data['Password'] = password;
    data['City'] = city;
    return data;
  }
}
