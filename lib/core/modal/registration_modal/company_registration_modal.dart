import 'package:bdc/core/modal/registration_modal/registrationModal.dart';

class CompanyRegistrationModal extends RegistrationModal {
  String? companyName;
  String? email;
  int? city;
  int? sectorType;

  CompanyRegistrationModal(
      {this.companyName,
      this.email,
      required String mobile,
      required String password,
      this.sectorType,
      this.city})
      : super(mobile, password);

  @override
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['CompanyName'] = companyName;
    data['Email'] = email;
    data['Mobile'] = mobile;
    data['Password'] = password;
    data['City'] = city;
    data['SectorType'] = sectorType;
    return data;
  }
}
