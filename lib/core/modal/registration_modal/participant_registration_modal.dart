import 'package:bdc/core/modal/registration_modal/registrationModal.dart';

class ParticipantRegistrationModal extends RegistrationModal {
  String userName;
  String nationality;
  String email;
  String birthDate;
  int project;
  int city;

  ParticipantRegistrationModal(
      {required this.userName,
      required this.nationality,
      required this.email,
      required String mobile,
      required String password,
      required this.birthDate,
      required this.project,
      required this.city})
      : super(mobile, password);

  @override
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['UserName'] = userName;
    data['Nationality'] = nationality;
    data['Email'] = email;
    data['Mobile'] = mobile;
    data['Password'] = password;
    data['BirthDate'] = birthDate;
    data['Project'] = project;
    data['City'] = city;
    return data;
  }
}
