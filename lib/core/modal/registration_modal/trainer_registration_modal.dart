import 'package:bdc/core/modal/registration_modal/registrationModal.dart';

class TrainerRegistrationModal extends RegistrationModal {
  late String userName;
  late String email;
  late int major;
  late int eduLevel;
  late int city;

  TrainerRegistrationModal(
      {required this.userName,
      required this.email,
      required String mobile,
      required String password,
      required this.major,
      required this.eduLevel,
      required this.city})
      : super(mobile, password);

  @override
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['UserName'] = userName;
    data['Email'] = email;
    data['Mobile'] = mobile;
    data['Password'] = password;
    data['Major'] = major;
    data['EducationalLevel'] = major;
    data['City'] = city;
    return data;
  }
}
