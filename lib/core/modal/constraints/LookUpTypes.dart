class LookUpTypes {
  static const String gender = 'Gender';
  static const String marital = 'Marital';
  static const String sector = 'Sector';
  static const String industry = 'Industry';
  static const String eduLevel = 'EducationalLevels';
  static const String major = 'Majors';
  static const String jobRole = 'JobRole';
  static const String country = 'Country';
  static const String city = 'City';
  static const String nationality = 'Nationality';
  static const String religion = 'Religion';
  static const String recruitment = 'Recruitment';
  static const String contractTypes = 'Contract';
  static const String skillsLevel = 'SkillsLevel';
  static const String experienceYears = 'ExperienceYears';
  static const String lastUsed = 'LastUsed';
}
