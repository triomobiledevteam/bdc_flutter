class Programs {
  int? projectID;
  int? projectPortfolioID;
  String? description;
  String? code;
  String? actualStartDate;
  String? projectStatusID;
  String? actualEndDate;
  String? projectLogo;
  String? projectBriefDescription;
  String? estimatedEndDate;
  String? estimatedStartDate;
  String? periodTypeID;
  // int? customerApplicationID;
  String? createdDate;
  int? createdUser;

  Programs(
      {this.projectID,
      this.projectPortfolioID,
      this.description,
      this.code,
      this.actualStartDate,
      this.projectStatusID,
      this.actualEndDate,
      this.projectLogo,
      this.projectBriefDescription,
      this.estimatedEndDate,
      this.estimatedStartDate,
      this.periodTypeID,
      // this.customerApplicationID,
      this.createdDate,
      this.createdUser});

  Programs.fromJson(Map<String, dynamic> json) {
    projectID = json['ProjectID'];
    projectPortfolioID = json['ProjectPortfolioID'] ?? -1;
    description = json['Description'] ?? '';
    code = json['Code'];
    actualStartDate = json['ActualStartDate'] ?? '';
    projectStatusID = json['ProjectStatusID'] ?? '';
    actualEndDate = json['ActualEndDate'] ?? "";
    projectLogo = json['ProjectLogo'] ?? '';
    projectBriefDescription = json['ProjectBriefDescription'] ?? '';
    estimatedEndDate = json['EstimatedEndDate'];
    estimatedStartDate = json['EstimatedStartDate'];
    periodTypeID = json['PeriodTypeID'];
    // isEnrolled = json['isEnrolled'];
    // customerApplicationID = json['CustomerApplicationID'];
    createdDate = json['CreatedDate'] ?? '';
    createdUser = json['CreatedUser'] ?? '';
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['ProjectID'] = projectID;
    data['ProjectPortfolioID'] = projectPortfolioID;
    data['Description'] = description;
    data['Code'] = code;
    data['ActualStartDate'] = actualStartDate;
    data['ProjectStatusID'] = projectStatusID;
    data['ActualEndDate'] = actualEndDate;
    data['ProjectLogo'] = projectLogo;
    data['ProjectBriefDescription'] = projectBriefDescription;
    data['EstimatedEndDate'] = estimatedEndDate;
    data['EstimatedStartDate'] = estimatedStartDate;
    data['PeriodTypeID'] = periodTypeID;
    // data['isEnrolled'] = isEnrolled;
    // data['CustomerApplicationID'] = customerApplicationID;
    data['CreatedDate'] = createdDate;
    data['CreatedUser'] = createdUser;
    return data;
  }
}
