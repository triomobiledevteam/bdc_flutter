List<Portfolios> portfolioList = [];

class Portfolios {
  int? portfolioID;
  int? companyID;
  String? description;
  String? postingTypeID;

  Portfolios(
      {this.portfolioID, this.companyID, this.description, this.postingTypeID});

  Portfolios.fromJson(Map<String, dynamic> json) {
    portfolioID = json['PortfolioID'];
    companyID = json['CompanyID'];
    description = json['Description'];
    postingTypeID = json['PostingTypeID'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['PortfolioID'] = portfolioID;
    data['CompanyID'] = companyID;
    data['Description'] = description;
    data['PostingTypeID'] = postingTypeID;
    return data;
  }
}
