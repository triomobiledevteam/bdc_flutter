class Project {
  int? projectID;

  Project({this.projectID});

  Project.fromJson(Map<String, dynamic> json) {
    projectID = json['projectID'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['projectID'] = projectID;
    return data;
  }
}

class ProjectList {
  List<Project>? project;

  ProjectList({this.project});

  ProjectList.fromJson(Map<String, dynamic> json) {
    project = <Project>[];
    if (json['Project'] != null) {
      json['Project'].forEach((v) {
        project!.add(Project.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (project != null) {
      data['Project'] = project!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
