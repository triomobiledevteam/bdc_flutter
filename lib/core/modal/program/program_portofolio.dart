import 'package:bdc/core/modal/program/portfolios.dart';

import 'Program.dart';

class ProjectPortfolios {
  List<Portfolios>? portfolios;
  List<Programs>? programs;

  ProjectPortfolios({this.portfolios, this.programs});

  ProjectPortfolios.fromJson(Map<String, dynamic> json) {
    if (json['Portfolios'] != null) {
      portfolios = <Portfolios>[];
      json['Portfolios'].forEach((v) {
        portfolios!.add(Portfolios.fromJson(v));
      });
    }
    if (json['Programs'] != null) {
      programs = <Programs>[];
      json['Programs'].forEach((v) {
        programs!.add(Programs.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (portfolios != null) {
      data['Portfolios'] = portfolios!.map((v) => v.toJson()).toList();
    }
    if (programs != null) {
      data['Programs'] = programs!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
