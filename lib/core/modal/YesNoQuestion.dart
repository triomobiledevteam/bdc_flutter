class YesNoQuestion {
  int? iD;
  String? description;
  bool yes;
  bool no;
  bool? notApplicable;

  YesNoQuestion(
      {this.iD,
      this.description,
      this.yes = false,
      this.no = false,
      this.notApplicable});
}
