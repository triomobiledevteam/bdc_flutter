import 'package:bdc/core/storage/db_client.dart';

class ApplicationForm {
  int? iD;
  String? descriptionEn;
  String? descriptionAr;
  bool? yes;
  bool? no;
  bool? notApplicable;
  String? formType;

  ApplicationForm(
      {this.iD,
      this.descriptionEn = '',
      this.descriptionAr = '',
      this.yes = false,
      this.no = false,
      this.notApplicable = false,
      this.formType});

  ApplicationForm.fromJson(Map<String, dynamic> json) {
    iD = json['ID'];
    descriptionEn = json['DescriptionEn'] ?? '';
    descriptionAr = json['DescriptionAr'] ?? '';
    yes = false;
    no = false;
    notApplicable = false;
    formType = json['FormType'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['ID'] = iD;
    data['DescriptionEn'] = descriptionEn;
    data['DescriptionAr'] = descriptionAr;
    data['FormType'] = formType;
    return data;
  }

  fetchToLocalDB(List<ApplicationForm> applicationFormList) async {
    final db = DatabaseClient.instance;
    List<ApplicationForm> formList = [];
    //City
    formList.addAll(applicationFormList);
    for (var element in formList) {
      await db.applicationFormDB(element);
    }
  }
}

class ApplicationFormType {
  static const String participant = 'Participant';
  static const String institution = 'Institution';
}
