class TrainerCertificate {
  int? iD;
  int? companyID;
  String? descriptionAr;
  String? descriptionEn;
  int? skillLevelID;
  int? experienceYearsID;
  int? lastUsedID;

  TrainerCertificate(
      {this.iD,
      this.companyID,
      this.descriptionAr,
      this.descriptionEn,
      this.skillLevelID,
      this.experienceYearsID,
      this.lastUsedID});

  TrainerCertificate.fromJson(Map<String, dynamic> json) {
    iD = json['ID'];
    companyID = json['CompanyID'];
    descriptionAr = json['DescriptionAr'] ?? '';
    descriptionEn = json['DescriptionEn'] ?? '';
    skillLevelID = json['SkillLevelID'];
    experienceYearsID = json['ExperienceYearsID'];
    lastUsedID = json['LastUsedID'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['ID'] = iD;
    data['CompanyID'] = companyID;
    data['DescriptionAr'] = descriptionAr;
    data['DescriptionEn'] = descriptionEn;
    data['SkillLevelID'] = skillLevelID;
    data['ExperienceYearsID'] = experienceYearsID;
    data['LastUsedID'] = lastUsedID;
    return data;
  }
}
