class TrainerExperience {
  int? iD;
  int? trainerID;
  String? descriptionAr;
  String? descriptionEn;
  String? startDate;
  String? endDate;
  String? location;
  String? address;
  int? companyIndustry;
  String? companyName;

  TrainerExperience(
      {this.iD,
      this.trainerID,
      this.descriptionAr = '',
      this.descriptionEn = '',
      this.startDate = '',
      this.endDate = '',
      this.location = '',
      this.address = '',
      this.companyIndustry,
      this.companyName = ''});

  TrainerExperience.fromJson(Map<String, dynamic> json) {
    iD = json['ID'];
    trainerID = json['TrainerID'];
    descriptionAr = json['DescriptionAr'] ?? '';
    descriptionEn = json['DescriptionEn'] ?? '';
    startDate = json['StartDate'] ?? '';
    endDate = json['EndDate'] ?? '';
    location = json['Location'] ?? '';
    address = json['Address'] ?? '';
    companyIndustry = json['CompanyIndustry'];
    companyName = json['CompanyName'] ?? '';
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['ID'] = iD;
    data['TrainerID'] = trainerID;
    data['DescriptionAr'] = descriptionAr;
    data['DescriptionEn'] = descriptionEn;
    data['StartDate'] = startDate;
    data['EndDate'] = endDate;
    data['Location'] = location;
    data['Address'] = address;
    data['CompanyIndustry'] = companyIndustry;
    data['CompanyName'] = companyName;
    return data;
  }
}
