import 'package:bdc/core/modal/constraints/global_values.dart';

import '../../utils/utils.dart';

List<ParticipantFamilyInfo> participantFamilyInfoList = [];

class ParticipantFamilyInfo {
  int? iD;
  int? trioID;
  String? fullNameEn;
  String? fullNameAr;
  int? genderID;
  bool? disability;
  String? disabilityDescription;
  String? dateOfBirth;
  int? participantID;
  int? localParticipantID;

  ParticipantFamilyInfo(
      {this.iD,
      this.trioID,
      this.fullNameEn = '',
      this.fullNameAr = '',
      this.genderID,
      this.disability = false,
      this.disabilityDescription = '',
      this.dateOfBirth = '',
      this.participantID,
      this.localParticipantID});

  ParticipantFamilyInfo.fromJson(Map<String, dynamic> json) {
    iD = json['LocalID'];
    trioID = json['ID'];
    fullNameEn = json['FullNameEn'] ?? '';
    fullNameAr = json['FullNameAr'] ?? "";
    genderID = json['GenderID'];
    disability = json['Disability'] is bool
        ? json['Disability']
        : json['Disability'] == 0
            ? false
            : true;
    disabilityDescription = json['DisabilityDescription'] ?? '';
    dateOfBirth = json['DateOfBirth'] ?? '';
    participantID = json['ParticipantID'];
    localParticipantID = json['LocalParticipantID'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['LocalID'] = iD;
    data['ID'] = trioID;
    data['FullNameEn'] = fullNameEn;
    data['FullNameAr'] = fullNameAr;
    data['GenderID'] = genderID;
    data['Disability'] = disability;
    data['DisabilityDescription'] = disabilityDescription;
    data['DateOfBirth'] = dateOfBirth;
    data['ParticipantID'] = participantID;
    data['LocalParticipantID'] = localParticipantID;
    return data;
  }

  Map<String, dynamic> toTrio() {
    final Map<String, dynamic> data = {
      'ID': "${trioID ?? 0}",
      'CompanyID': "$companyID",
      'FullNameEn': "$fullNameEn",
      'FullNameAr': "$fullNameAr",
      'GenderID': "${genderID ?? 0}",
      'Disability': "${disability ?? 0}",
      'DisabilityDescription': "$disabilityDescription",
      'DateOfBirth': formatDate('yyyy-MM-dd', dateOfBirth!),
      'ParticipantID': "$participantID",
    };
    return data;
  }
}
