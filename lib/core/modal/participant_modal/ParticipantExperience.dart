import 'package:bdc/core/modal/constraints/global_values.dart';
import 'package:intl/intl.dart';

List<ParticipantExperience> participantExperienceList = [];

class ParticipantExperience {
  int? iD;
  int? trioID;
  String? descriptionEn;
  String? descriptionAr;
  String? startDate;
  String? endDate;
  String? location;
  int? companyIndustry;
  String? companyName;
  String? note;
  int? participantID;
  int? localParticipantID;

  ParticipantExperience(
      {this.iD,
      this.trioID,
      this.descriptionEn = '',
      this.descriptionAr = '',
      this.startDate = '',
      this.endDate = '',
      this.location = '',
      this.companyIndustry,
      this.companyName = '',
      this.note = '',
      this.participantID,
      this.localParticipantID});

  ParticipantExperience.fromJson(Map<String, dynamic> json) {
    iD = json['LocalID'];
    trioID = json['ID'];
    descriptionEn = json['DescriptionEn'] ?? '';
    descriptionAr = json['DescriptionAr'] ?? '';
    startDate = json['StartDate'] ?? '';
    endDate = json['EndDate'] ?? '';
    location = json['Location'] ?? '';
    companyIndustry = json['CompanyIndustry'];
    companyName = json['CompanyName'] ?? '';
    note = json['Note'] ?? '';
    participantID = json['ParticipantID'];
    localParticipantID = json['LocalParticipantID'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['LocalID'] = iD;
    data['ID'] = trioID;
    data['DescriptionEn'] = descriptionEn;
    data['DescriptionAr'] = descriptionAr;
    data['StartDate'] = startDate;
    data['EndDate'] = endDate;
    data['Location'] = location;
    data['CompanyIndustry'] = companyIndustry;
    data['CompanyName'] = companyName;
    data['Note'] = note;
    data['ParticipantID'] = participantID;
    data['LocalParticipantID'] = localParticipantID;
    return data;
  }

  Map<String, dynamic> toTrio() {
    // this.trioID = this.trioID ?? 0;
    final Map<String, dynamic> data = {
      'ID': "${trioID ?? 0}",
      'CompanyID': "$companyID",
      'ParticipantID': "$participantID",
      'DescriptionEn': descriptionEn ?? '',
      'DescriptionAr': descriptionAr ?? '',
      'StartDate': startDate!.isNotEmpty
          ? DateFormat('yyyy-MM-dd').format(DateTime.parse(startDate!))
          : '',
      'EndDate': endDate!.isNotEmpty
          ? DateFormat('yyyy-MM-dd').format(DateTime.parse(endDate!))
          : '',
      'CompanyIndustry': "${companyIndustry ?? 0}",
      'CompanyName': companyName ?? '',
      'Location': location ?? '',
      'Note': note ?? '',
    };
    return data;
  }
}
