import 'dart:typed_data';

class AttachmentModal {
  int? id;
  int? localID;
  int? trioID;
  late Uint8List image;
  AttachmentModal({this.id, this.localID, this.trioID, required this.image});

  AttachmentModal.fromJson(Map<String, dynamic> json) {
    id = json['ID'];
    localID = json['LocalID'];
    trioID = json['TrioID'];
    image = json['File'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['ID'] = id;
    data['LocalID'] = localID;
    data['TrioID'] = trioID;
    data['File'] = image;
    return data;
  }
}
