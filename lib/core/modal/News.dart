List<News> newsList = [];

class News {
  int? iD;
  int? companyID;
  int? newsCategoryID;
  String? newsName;
  String? newsTitle;
  String? shortDescription;
  String? newsBody;
  bool? published;
  bool? allowComments;
  String? startDate;
  String? endDate;
  String? passwordToAccess;
  String? tags;
  String? featuredImage;
  String? seoFriendlyName;
  String? seoMetaTitle;
  String? seoMetaKeywords;
  String? seoMetaDescription;
  String? note;
  int? createdUser;
  String? createdDate;

  News(
      {this.iD,
      this.companyID,
      this.newsCategoryID,
      this.newsName,
      this.newsTitle,
      this.shortDescription,
      this.newsBody,
      this.published,
      this.allowComments,
      this.startDate,
      this.endDate,
      this.passwordToAccess,
      this.tags,
      this.featuredImage,
      this.seoFriendlyName,
      this.seoMetaTitle,
      this.seoMetaKeywords,
      this.seoMetaDescription,
      this.note,
      this.createdUser,
      this.createdDate});

  News.fromJson(Map<String, dynamic> json) {
    iD = json['ID'];
    companyID = json['CompanyID'];
    newsCategoryID = json['NewsCategoryID'] ?? '' as int?;
    newsName = json['NewsName'] ?? '';
    newsTitle = json['NewsTitle'] ?? '';
    shortDescription = json['ShortDescription'] ?? '';
    newsBody = json['NewsBody'] ?? '';
    published = json['Published'] ?? false;
    allowComments = json['AllowComments'] ?? false;
    startDate = json['StartDate'] ?? '';
    endDate = json['EndDate'] ?? '';
    passwordToAccess = json['PasswordToAccess'] ?? '';
    tags = json['Tags'] ?? '';
    featuredImage = json['FeaturedImage'] ?? '';
    seoFriendlyName = json['SeoFriendlyName'] ?? '';
    seoMetaTitle = json['SeoMetaTitle'] ?? '';
    seoMetaKeywords = json['SeoMetaKeywords'] ?? '';
    seoMetaDescription = json['SeoMetaDescription'] ?? '';
    note = json['Note'] ?? '';
    createdUser = json['CreatedUser'];
    createdDate = json['CreatedDate'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['ID'] = iD;
    data['CompanyID'] = companyID;
    data['NewsCategoryID'] = newsCategoryID;
    data['NewsName'] = newsName;
    data['NewsTitle'] = newsTitle;
    data['ShortDescription'] = shortDescription;
    data['NewsBody'] = newsBody;
    data['Published'] = published;
    data['AllowComments'] = allowComments;
    data['StartDate'] = startDate;
    data['EndDate'] = endDate;
    data['PasswordToAccess'] = passwordToAccess;
    data['Tags'] = tags;
    data['FeaturedImage'] = featuredImage;
    data['SeoFriendlyName'] = seoFriendlyName;
    data['SeoMetaTitle'] = seoMetaTitle;
    data['SeoMetaKeywords'] = seoMetaKeywords;
    data['SeoMetaDescription'] = seoMetaDescription;
    data['Note'] = note;
    data['CreatedUser'] = createdUser;
    data['CreatedDate'] = createdDate;
    return data;
  }
}
