import 'package:bdc/core/modal/map_service/Geometry.dart';

class Place {
  Geometry? geometry;
  String? name;
  String? vicinity;

  Place({this.geometry, this.name, this.vicinity});

  factory Place.fromJson(Map<String, dynamic> json) {
    return Place(
      geometry: Geometry.fromJson(json['geometry']),
      name: json['formatted_address'] ?? '',
      vicinity: json['vicinity'] ?? "",
    );
  }
}
