class MapLog {
  late double latitude;
  late double longitude;
  String address = '';
  late int zoom;

  MapLog({
    this.latitude = 0,
    this.longitude = 0,
    this.zoom = 17,
  });

  MapLog.fromJson(Map<String, dynamic> json) {
    latitude = json['latitude'] ?? 0;
    longitude = json['longitude'] ?? 0;
    zoom = json['zoom'] ?? 0;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['latitude'] = latitude;
    data['longitude'] = longitude;
    data['zoom'] = zoom;

    return data;
  }

  @override
  String toString() {
    // TODO: implement toString
    return '{"latitude":$latitude,"longitude":$longitude,"zoom":$zoom}';
  }
}
