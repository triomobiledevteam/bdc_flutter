import 'package:bdc/core/modal/AplicationForm.dart';

import 'constraints/global_values.dart';

class ApplicationFormTrans {
  int? iD;
  int? userID;
  int? trioUserID;
  int? applicationFromID;
  bool? yes;
  bool? no;
  bool? notApplicable;
  String? formType;

  ApplicationFormTrans(
      {this.iD,
      this.userID,
      this.trioUserID,
      this.applicationFromID,
      this.yes,
      this.no,
      this.notApplicable,
      this.formType});

  ApplicationFormTrans.fromApplicationFormToTrans(
    ApplicationForm applicationForm,
    this.userID,
  ) {
    trioUserID = userID;
    applicationFromID = applicationForm.iD;
    yes = applicationForm.yes;
    no = applicationForm.no;
    notApplicable = applicationForm.notApplicable;
  }

  ApplicationFormTrans.fromJson(Map<String, dynamic> json) {
    iD = json['ID'];
    trioUserID = json['UserID'];
    userID = json['LocalUserID'];
    applicationFromID = json['CheckListID'];
    yes = json['Yes'];
    no = json['No'];
    notApplicable = json['NotApplicable'];
    formType = json['FormType'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['ID'] = iD;
    data['UserID'] = trioUserID;
    data['LocalUserID'] = userID;
    data['CheckListID'] = applicationFromID;
    data['Yes'] = yes;
    data['No'] = no;
    data['NotApplicable'] = notApplicable;
    data['FormType'] = formType;
    return data;
  }

  Map<String, dynamic> applicationFormToTrio() {
    final Map<String, dynamic> data = {
      'CompanyID': "$companyID",
      'ParticipantID': "$trioUserID",
      'ParticipantCheckListID': "$applicationFromID",
      'Yes': "$yes",
      'No': "$no",
      'NotApplicable': "$notApplicable",
    };
    // CompanyID, ParticipantID, ParticipantCheckListID, Yes, No, NotApplicable
    return data;
  }

  Map<String, dynamic> employeeFormToTrio() {
    final Map<String, dynamic> data = {
      'CompanyID': "$companyID",
      'InstitutionID': "$trioUserID",
      'InstitutionCheckListID': "$applicationFromID",
      'Yes': "${yes ?? false}",
      'No': "${no ?? false}",
    };
    // CompanyID, InstitutionID, InstitutionCheckListID, Yes, No
    return data;
  }
}
