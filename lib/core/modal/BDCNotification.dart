List<BDCNotification> notificationList = [];

class BDCNotification {
  int? iD;
  int? companyID;
  String? announcementStatusID;
  String? createdDate;
  String? seenDate;
  String? confirmationDate;
  String? webLink;
  String? note;
  String? description;

  BDCNotification(
      {this.iD,
      this.companyID,
      this.announcementStatusID,
      this.createdDate,
      this.seenDate,
      this.confirmationDate,
      this.webLink,
      this.note,
      this.description});

  BDCNotification.fromJson(Map<String, dynamic> json) {
    iD = json['ID'];
    companyID = json['CompanyID'];
    announcementStatusID = json['AnnouncementStatusID'];
    createdDate = json['CreatedDate'] ?? "";
    seenDate = json['SeenDate'] ?? '';
    confirmationDate = json['ConfirmationDate'] ?? '';
    webLink = json['WebLink'] ?? "";
    note = json['Note'] ?? '';
    description = json['Description'] ?? '';
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['ID'] = iD;
    data['CompanyID'] = companyID;
    data['AnnouncementStatusID'] = announcementStatusID;
    data['CreatedDate'] = createdDate;
    data['SeenDate'] = seenDate;
    data['ConfirmationDate'] = confirmationDate;
    data['WebLink'] = webLink;
    data['Note'] = note;
    data['Description'] = description;
    return data;
  }
}
